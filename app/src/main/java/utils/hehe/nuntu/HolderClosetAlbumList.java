package utils.hehe.nuntu;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by snickwizard on 2/10/15.
 */
public class HolderClosetAlbumList extends BaseAdapter {

    private final Context mActivity;
    private final ArrayList<ClosetLocalAlbum> mLocalalbums;
    private LayoutInflater l_Inflater;
    private static HolderClosetTrackList track_list;
    private static float ScaleFactor = 1.00f;

    public HolderClosetAlbumList(Context ctx, ArrayList<ClosetLocalAlbum> albums) {
        this.mActivity = ctx;
        this.mLocalalbums = albums;
        l_Inflater = LayoutInflater.from(ctx);
        DisplayMetrics metrics = mActivity.getResources().getDisplayMetrics();
        ScaleFactor = (metrics.heightPixels / metrics.density) / 640.00f;
        Log.e("SCALE FACTOR", "by sceen height " + ScaleFactor);
    }

    public int getCount() {
        return mLocalalbums.size();
    }


    public Object getItem(int position) {
        return mLocalalbums.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {

        final ViewHolder holder;
        if (convertView == null) {
            convertView = l_Inflater.inflate(R.layout.closet_album_list_layout, null);
            holder = new ViewHolder();
            convertView.setTag(holder);

        } else {
            convertView.refreshDrawableState();
            holder = (ViewHolder) convertView.getTag();

        }
        if (ScaleFactor < 1.00f) {
            holder.parent = (CardView) convertView.findViewById(R.id.card_view);
            holder.parent.setScaleX(ScaleFactor);
            holder.parent.setScaleY(ScaleFactor);
        }
        holder.albumName = (TextView) convertView.findViewById(R.id.list_album_title);
        holder.albumAuthor = (TextView) convertView.findViewById(R.id.list_album_author);
        holder.cover = (ImageView) convertView.findViewById(R.id.list_album_cover);
        if (mLocalalbums != null && mLocalalbums.size() > position) {
            holder.albumName.setText(mLocalalbums.get(position).getAlbumTitle());
            holder.albumAuthor.setText(mLocalalbums.get(position).getAlbumAuthor());
            if (null != mLocalalbums.get(position).getCoverImage()) {
                holder.cover.setImageDrawable(mLocalalbums.get(position).getCoverImage());
            } else {
                holder.cover.setImageResource(R.drawable.music_logo);
            }
        }
        holder.cover.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                displayAlbumTracks(mLocalalbums.get(position).getAlbumTracks(), holder.cover.getDrawable());
            }
        });

        holder.albumName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                displayAlbumTracks(mLocalalbums.get(position).getAlbumTracks(), holder.cover.getDrawable());
            }
        });

        holder.cover.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                displayAlbumTracks(mLocalalbums.get(position).getAlbumTracks(), holder.cover.getDrawable());
            }
        });
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                displayAlbumTracks(mLocalalbums.get(position).getAlbumTracks(), holder.cover.getDrawable());
            }
        });
        return convertView;
    }

    public void updateLoadedAlbums(List<ClosetLocalAlbum> newlist) {
        mLocalalbums.clear();
        mLocalalbums.addAll(newlist);
        this.notifyDataSetChanged();
    }

    private void displayAlbumTracks(ArrayList<String> tracks, Drawable cover) {
        final ArrayList<ClosetLocalTrack> Tracks = new ArrayList<>();
        for (String trackLocation : tracks) {
            ClosetLocalTrack current = new ClosetLocalTrack();
            current.setTrackStatus(0);
            current.setTrackLocation(trackLocation);
            current.setTrackLength(NuntuUtils.getTrackLength(mActivity, new File(trackLocation)));
            current.setATrackTitle(NuntuUtils.getTrackTitle(mActivity, new File(trackLocation)));
            //Log.e("album tracks", trackLocation);
            Tracks.add(current);
        }
        if (Tracks.size() >= 1) {
            track_list = new HolderClosetTrackList(mActivity, Tracks);
            //Log.e("TRACK COUNT BEFORE", "" + track_list.getCount());
            FragmentNuntuCloset.updateTrackListView(track_list, Tracks);
            //Log.e("ALBUM SELECTION", "finished rendering");
            Intent intent = new Intent(mActivity, NuntuMPC.class);
            ArrayList<String> paths = new ArrayList<>();
            ArrayList<String> titles = new ArrayList<>();
            int i = 0;
            while (i < Tracks.size()) {
                paths.add(Tracks.get(i).getTrackLocation());
                titles.add(Tracks.get(i).getTrackTitle());
                i++;
            }
            Bundle extras = new Bundle();
            Bitmap bmp = ((BitmapDrawable) cover).getBitmap();
            if (bmp != null) {
                extras.putParcelable("cover", NuntuUtils.scaleBitmap(bmp, 128, 128));
            } else {
                extras.putParcelable("cover", null);
            }

            intent.putExtras(extras);
            intent.putStringArrayListExtra("songpath", paths);
            intent.putStringArrayListExtra("songtitle", titles);
            intent.putExtra("action", "play");
            intent.putExtra("from", "album");
            mActivity.startService(intent);
            track_list.setAsPlaying(0);
        }

    }

    static class ViewHolder {
        TextView albumName, albumAuthor;
        ImageView cover;
        CardView parent;
    }
}