package utils.hehe.nuntu;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.joanzapata.pdfview.PDFView;
import com.joanzapata.pdfview.listener.OnDrawListener;
import com.joanzapata.pdfview.listener.OnLoadCompleteListener;
import com.joanzapata.pdfview.listener.OnPageChangeListener;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;

/**
 * Created by snick on 2/26/15.
 */
public class ClosetPDFViewer extends ActionBarActivity {
    private static int changedPage, PageOnDisplay;
    private static int InitPageLoadTimer = 3000;
    final String TAG = "PDFVIEWER";
    private PDFView viewer;
    private TextView header, Showpage;
    private SeekBar PageChanger;
    public static ProgressBar UserHeaderProgress, loader;
    public static ImageView UserImage, UploadPicLogo;
    public static TextView HeaderEmail, UserName, UserRank, UserBal;
    private String ImageFilePath = "", UserID = "0";
    private boolean isImageFileReady = false;
    ProgressDialog _outDialog;
    private static Context PdfViewContext;
    private static int TotalPageNum = 0;
    String FileLocation, Header;
    private NavigationDrawerFragment drawerFragment;
    final static String SCREENSHOTS_LOCATIONS = Environment.getExternalStorageDirectory().toString() + "/Pictures/Screenshots/";
    private boolean isFullScreenMode = false;
    Toolbar toolbar;
    DrawerLayout parent;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        PdfViewContext = this;
        setContentView(R.layout.closet_book_viewer);
        toolbar = (Toolbar) findViewById(R.id.app_bar);
        parent = (DrawerLayout) findViewById(R.id.drawer_layout);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        drawerFragment = (NavigationDrawerFragment) getFragmentManager().findFragmentById(R.id.fragment_navigation_drawer_reader);
        if (Build.VERSION.SDK_INT >= 14) {
            getWindow().setDimAmount(0.5f);
        }

        FileLocation = getIntent().getStringExtra("path");
        Header = getIntent().getStringExtra("header");
        final SwipeRefreshLayout DrawerRefresher = (SwipeRefreshLayout) findViewById(R.id.refresh_drawer);
        UserImage = (ImageView) findViewById(R.id.user_image);
        UploadPicLogo = (ImageView) findViewById(R.id.upload_new);
        UploadPicLogo.setVisibility(View.GONE);
        HeaderEmail = (TextView) findViewById(R.id.user_email);
        UserName = (TextView) findViewById(R.id.user_name);
        UserRank = (TextView) findViewById(R.id.user_rank);
        UserBal = (TextView) findViewById(R.id.user_balance);
        UserHeaderProgress = (ProgressBar) findViewById(R.id.user_header_progress);
        viewer = (PDFView) findViewById(R.id.bookviewer);
        header = (TextView) findViewById(R.id.book_header);
        Showpage = (TextView) findViewById(R.id.showpage);
        PageChanger = (SeekBar) findViewById(R.id.page_changer);
        loader = (ProgressBar) findViewById(R.id.loader);
        header.setText(Header);
        _outDialog = new ProgressDialog(PdfViewContext);
        drawerFragment.setUp(R.id.fragment_navigation_drawer_reader, parent, toolbar, UserHeaderProgress, DrawerRefresher, "viewer");
        if (DrawerRefresher != null) {
            if (Build.VERSION.SDK_INT >= 14) {
                DrawerRefresher.setColorScheme(android.R.color.holo_blue_bright,
                        android.R.color.holo_green_light,
                        android.R.color.holo_orange_light,
                        android.R.color.holo_red_light);
            }

            DrawerRefresher.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    Log.e("REFRESH", "swipe refreshing in reader");
                    drawerFragment.getDrawerItemsAsync("/api/mobile/drawer/Android");
                }
            });
        }

        UserImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (UploadPicLogo.getVisibility() != View.VISIBLE) {
                    UploadPicLogo.setVisibility(View.VISIBLE);
                } else {
                    UploadPicLogo.setVisibility(View.GONE);
                }
            }
        });
        UploadPicLogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isImageFileReady) {
                    Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                    photoPickerIntent.setType("image/*");
                    startActivityForResult(photoPickerIntent, 1);
                } else {
                    UploadPicLogo.setBackgroundColor(Color.argb(0, 0, 0, 0));
                    UploadPicLogo.setImageResource(android.R.drawable.ic_menu_edit);
                    UploadPicLogo.setVisibility(View.GONE);
                    if (Integer.valueOf(UserID) >= 1 && ImageFilePath != null && ImageFilePath.length() >= 6) {
                        new NuntuPictureUploaderAsync(ClosetPDFViewer.this).execute(ImageFilePath, NuntuConstants.HOST.toString() + "/user/" + UserID + "/picture");
                        isImageFileReady = false;
                    } else {
                        Toast.makeText(PdfViewContext, "Image upload failed", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });
        PageChanger.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                viewer.jumpTo(progress);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        try {
            viewer.fromFile(new File(FileLocation))
                    .defaultPage(0)
                    .onLoad(new OnLoadCompleteListener() {
                        @Override
                        public void loadComplete(final int nbPages) {

                            TotalPageNum = viewer.getPageCount();
                            PageChanger.setMax(TotalPageNum);

                            Showpage.setText(1 + "/" + TotalPageNum);
                            Showpage.setVisibility(View.VISIBLE);
                            Showpage.postDelayed(new Runnable() {
                                public void run() {
                                    Showpage.setVisibility(View.GONE);
                                }
                            }, 2000);
                            Log.e(TAG, "finished loading this page " + nbPages + " of " + TotalPageNum);
                            InitPageLoadTimer = 3000 + (nbPages * 100);
                            Log.e(TAG, "current init timer " + InitPageLoadTimer);
                            loader.postDelayed(new Runnable() {
                                public void run() {
                                    Showpage.setVisibility(View.GONE);
                                    loader.setVisibility(View.GONE);
                                }
                            }, InitPageLoadTimer);
                        }
                    })
                    .onPageChange(new OnPageChangeListener() {
                        @Override
                        public void onPageChanged(int page, int pageCount) {
                            if (1 == page) {
                                changedPage = 0;
                            } else {
                                changedPage = page;
                            }
                            Showpage.setText(page + "/" + pageCount);
                            PageChanger.setProgress(changedPage);
                            Log.e(TAG, "finished changing page " + page + " of these pages " + pageCount);

                            Showpage.setVisibility(View.VISIBLE);
                            Showpage.postDelayed(new Runnable() {
                                public void run() {
                                    Showpage.setVisibility(View.GONE);
                                }
                            }, 2000);

                            if (PageOnDisplay != changedPage && changedPage >= 1 && changedPage < pageCount) {
                                loader.setVisibility(View.VISIBLE);
                                loader.postDelayed(new Runnable() {
                                    public void run() {
                                        loader.setVisibility(View.GONE);
                                    }
                                }, 1500);

                            } else {
                                loader.setVisibility(View.GONE);
                            }

                        }
                    }).onDraw(new OnDrawListener() {
                @Override
                public void onLayerDrawn(Canvas canvas, float pageWidth, float pageHeight, int displayedPage) {
                    PageOnDisplay = displayedPage;
                    Log.e(TAG, "finished drawing canvas...w " + canvas.getWidth() + "x h " + canvas.getHeight() + " on map of size width " + pageWidth + " x height " + pageHeight + " of page " + displayedPage);
                    if (PageOnDisplay != 0 && PageOnDisplay == changedPage) {
                        loader.setVisibility(View.GONE);
                        Showpage.setVisibility(View.VISIBLE);
                    }

                }
            }).load();


        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(this, "An error occurred ... " + e.getMessage(), Toast.LENGTH_LONG).show();
        }
        viewer.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (isFullScreenMode) {
                    header.setVisibility(View.VISIBLE);
                    toolbar.setVisibility(View.VISIBLE);
                    isFullScreenMode = false;
                } else {
                    header.setVisibility(View.GONE);
                    toolbar.setVisibility(View.GONE);
                    isFullScreenMode = true;
                }
                parent.invalidate();
                return false;
            }
        });
        initUserHeader("init");

    }


    public void initUserHeader(String mode) {
        try {

            String userLocalCredentials = User.getNuntuLocalLogin(PdfViewContext);
            if (userLocalCredentials != null) {
                try {
                    JSONObject UserData = new JSONObject(userLocalCredentials);
                    if (NuntuUtils.isValidEmail(UserData.getString("email"))) {
                        HeaderEmail.setText(UserData.getString("email"));
                        UserName.setText(UserData.getString("firstname") + " " + UserData.getString("lastname"));
                        UserRank.setText(UserData.getString("rank"));
                        UserBal.setText("---");
                        UserID = UserData.getString("userid");

                        try {
                            String ImageUrl = UserData.getString("image");
                            if (ImageUrl != null) {
                                if (ImageUrl.contains("../../../../")) {
                                    ImageUrl = NuntuConstants.HOST.toString() + ImageUrl.replace("../../../../", "/");
                                }
                                Picasso.with(PdfViewContext)
                                        .load(ImageUrl)
                                        .resize(128, 128)
                                        .centerCrop()
                                        .transform(new CircleTransform(84, 0))
                                        .placeholder(R.drawable.profile)
                                        .error(R.drawable.profile)
                                        .into(UserImage);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    } else {
                        HeaderEmail.setText(" ");
                        UserName.setText("Please log in");
                        UserRank.setText("---");
                        UserBal.setText("---");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (TelephonyInfo.hasConnection(PdfViewContext)) {
                    //load user balance and user image
                }
            } else {
                HeaderEmail.setText(" ");
                UserName.setText("Please log in");
                UserRank.setText("---");
                UserBal.setText("---");

            }

            if (!"refresh".equalsIgnoreCase(mode)) {
                if (UserHeaderProgress != null) {
                    UserHeaderProgress.setVisibility(View.VISIBLE);
                }
            } else {
                drawerFragment.getDrawerItemsAsync("/api/mobile/drawer/Android");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void logUserOut() {
        try {
            Log.e("Logout", "logging out ");
            if (!User.logout(PdfViewContext)) {
                User.logout(PdfViewContext);//another instance may retain Prefs..retry
            }
            //we weren't getting enough time to clear user prefs data
            if (_outDialog == null) {
                _outDialog = new ProgressDialog(PdfViewContext);
            }
            _outDialog.setMessage("    ... logging out ...  ");
            _outDialog.show();

            new Thread() {
                public void run() {

                    try {

                        Thread.sleep(2000);
                        Intent intent = new Intent(PdfViewContext, NuntuMPC.class);
                        intent.putExtra("action", "auto_destruct");
                        PdfViewContext.startService(intent);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                _outDialog.setMessage("    ... shutting down ...  ");
                            }
                        });


                        Thread.sleep(2000);

                    } catch (InterruptedException e) {
                        e.printStackTrace();

                    }
                    exitApp("forced");
                }

            }.start();


        } catch (Exception exc) {
            exc.printStackTrace();
            Log.e("Logout", "An exception occurred " + exc.getMessage());
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuItem CloseMenuItem = menu.add(0, 0, Menu.NONE, R.string.back_closet);
        if (Build.VERSION.SDK_INT < 14) {
            CloseMenuItem.setShowAsAction(MenuItem.SHOW_AS_ACTION_WITH_TEXT);
        } else {
            CloseMenuItem.setShowAsActionFlags(MenuItem.SHOW_AS_ACTION_WITH_TEXT);
        }

        CloseMenuItem.setIcon(android.R.drawable.ic_menu_close_clear_cancel);

        MenuItem OpenWithMenuItem = menu.add(0, 1, Menu.NONE, R.string.open_with);
        if (Build.VERSION.SDK_INT < 14) {
            OpenWithMenuItem.setShowAsAction(MenuItem.SHOW_AS_ACTION_WITH_TEXT);
        } else {
            OpenWithMenuItem.setShowAsActionFlags(MenuItem.SHOW_AS_ACTION_WITH_TEXT);
        }
        OpenWithMenuItem.setIcon(android.R.drawable.ic_menu_more);

        MenuItem ShareMenuItem = menu.add(0, 2, Menu.NONE, R.string.share_text);
        if (Build.VERSION.SDK_INT < 14) {
            ShareMenuItem.setShowAsAction(MenuItem.SHOW_AS_ACTION_WITH_TEXT);
        } else {
            ShareMenuItem.setShowAsActionFlags(MenuItem.SHOW_AS_ACTION_WITH_TEXT);
        }
        ShareMenuItem.setIcon(android.R.drawable.ic_menu_share);

        MenuItem FullScreenMenuItem = menu.add(0, 3, Menu.NONE, R.string.full_screen);
        if (Build.VERSION.SDK_INT < 14) {
            FullScreenMenuItem.setShowAsAction(MenuItem.SHOW_AS_ACTION_WITH_TEXT);
        } else {
            FullScreenMenuItem.setShowAsActionFlags(MenuItem.SHOW_AS_ACTION_WITH_TEXT);
        }
        FullScreenMenuItem.setIcon(android.R.drawable.ic_menu_crop);
        return true;
    }


    public boolean respondToSelectedDrawerItem(String mode, String item) {
        try {
            if ("online".equalsIgnoreCase(mode)) {
                if ("app".equalsIgnoreCase(item)) {
                    return true;
                } else if ("book".equalsIgnoreCase(item)) {
                    return true;
                } else if ("music".equalsIgnoreCase(item)) {
                    return true;
                }
            } else {
                if ("about".equalsIgnoreCase(item)) {
                    final Dialog dialog;
                    if (Build.VERSION.SDK_INT >= 14) {
                        dialog = new Dialog(PdfViewContext, android.R.style.Theme_DeviceDefault_Light_DialogWhenLarge);
                    } else {
                        dialog = new Dialog(PdfViewContext);
                    }
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setCanceledOnTouchOutside(true);
                    dialog.setContentView(R.layout.about_layout);
                    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    ScrollView SuperParent = (ScrollView) dialog.findViewById(R.id.parent);
                    RelativeLayout parent = (RelativeLayout) dialog.findViewById(R.id.aboutpopup);
                    TextView aboutTitle = (TextView) dialog.findViewById(R.id.AboutTitle);
                    TextView appVersion = (TextView) dialog.findViewById(R.id.aboutAppVersion);
                    TextView aboutText = (TextView) dialog.findViewById(R.id.aboutText);
                    TextView creditTitle = (TextView) dialog.findViewById(R.id.CreditTitle);
                    TextView credittext = (TextView) dialog.findViewById(R.id.CreditText);

                    TextView privacyTitle = (TextView) dialog.findViewById(R.id.Privacy);
                    TextView privacyText = (TextView) dialog.findViewById(R.id.PrivacyText);
                    ImageView closeBox = (ImageView) dialog.findViewById(R.id.closeDialog);
                    closeBox.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });
                    final TextView visitTerms = (TextView) dialog.findViewById(R.id.TermsLink);
                    aboutTitle.setText("Nuntu Android ");
                    appVersion.setText("v2.1.1");
                    aboutText.setText(NuntuConstants.ABOUT_APP.toString());
                    privacyTitle.setText("Privacy Policy");
                    privacyText.setText(NuntuConstants.NUNTU_PRIVACY.toString());
                    creditTitle.setText("Credits");
                    credittext.setText(NuntuConstants.NUNTU_CREDITS.toString());

                    if (visitTerms != null) {
                        visitTerms.setOnClickListener(new View.OnClickListener() {

                            @Override
                            public void onClick(View arg0) {
                                visitTerms.setMovementMethod(LinkMovementMethod.getInstance());
                            }

                        });

                    }

                    parent.setOnLongClickListener(new View.OnLongClickListener() {
                        @Override
                        public boolean onLongClick(View v) {
                            dialog.dismiss();
                            return true;
                        }
                    });
                    dialog.show();
                    return true;
                } else if ("logout".equalsIgnoreCase(item)) {
                    logUserOut();
                    return true;
                } else if ("login".equalsIgnoreCase(item)) {
                    new Nuntu().respondToSelectedDrawerItem("local", "login");
                    exitApp("soft");
                    return true;
                } else if ("help".equalsIgnoreCase(item)) {
                    return true;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 0:
                onBackPressed();
                break;
            case 1:
                Intent intent = new Intent();
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.setAction(Intent.ACTION_VIEW);
                String type = "application/pdf";
                intent.setDataAndType(Uri.fromFile(new File(FileLocation)), type);
                startActivity(intent);
                break;
            case 2:
                try {
                    final String screenURL = SCREENSHOTS_LOCATIONS + System.currentTimeMillis() + ".jpg";
                    Point size = new Point();
                    if (Build.VERSION.SDK_INT >= 13) {
                        Display display = getWindowManager().getDefaultDisplay();
                        display.getSize(size);
                    } else {
                        size.x = Device.getDeviceScreenSize("width", this, "normal");
                        size.y = Device.getDeviceScreenSize("height", this, "normal");
                    }
                    // Get root view
                    View view = findViewById(R.id.drawer_layout);

                    // Create the bitmap to use to draw the screenshot
                    final Bitmap bitmap = Bitmap.createBitmap(size.x, size.y, Bitmap.Config.ARGB_4444);
                    final Canvas canvas = new Canvas(bitmap);

                    // Get current theme to know which background to use

                    final Resources.Theme theme = ClosetPDFViewer.this.getTheme();
                    final TypedArray ta = theme.obtainStyledAttributes(new int[]{android.R.attr.windowBackground});
                    final int res = ta.getResourceId(0, 0);
                    final Drawable background = ClosetPDFViewer.this.getResources().getDrawable(res);

                    // Draw background
                    if (background != null) {
                        background.draw(canvas);
                    }

                    // Draw views
                    view.draw(canvas);

                    // Save the screenshot to the file system
                    FileOutputStream fos = null;
                    try {
                        final File sddir = new File(SCREENSHOTS_LOCATIONS);
                        if (!sddir.exists()) {
                            sddir.mkdirs();
                        }
                        fos = new FileOutputStream(screenURL);

                        if (!bitmap.compress(Bitmap.CompressFormat.JPEG, 90, fos)) {
                            Log.e("SCREENSHOT", "Compress/Write failed");
                        }
                        fos.flush();
                        fos.close();
                        File sc_shot = new File(screenURL);
                        if (sc_shot.exists()) {
                            Intent share = new Intent(Intent.ACTION_SEND);
                            share.setType("image/jpeg");
                            share.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(sc_shot));
                            share.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            share.putExtra(Intent.EXTRA_SUBJECT, "You will like this book");
                            share.putExtra(Intent.EXTRA_TEXT, "\nOn Nuntu, reading " + Header);
                            startActivity(Intent.createChooser(share, "share using..."));
                        } else {
                            Intent shareIntent = new Intent();
                            shareIntent.setAction(Intent.ACTION_SEND);
                            shareIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            shareIntent.putExtra(Intent.EXTRA_SUBJECT, "You will like this book");
                            shareIntent.putExtra(Intent.EXTRA_TEXT, "\nOn Nuntu, reading " + Header);
                            shareIntent.setType("text/plain");
                            startActivity(Intent.createChooser(shareIntent, "share using..."));

                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case 3:
                if (isFullScreenMode) {
                    header.setVisibility(View.VISIBLE);
                    toolbar.setVisibility(View.VISIBLE);
                    isFullScreenMode = false;
                } else {
                    header.setVisibility(View.GONE);
                    toolbar.setVisibility(View.GONE);
                    isFullScreenMode = true;
                }
                parent.invalidate();
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    private void exitApp(String mode) {
        try {
            if (_outDialog != null) {
                _outDialog.dismiss();
            }
            super.onDestroy();
            //destroy evidence
            if ("forced".equalsIgnoreCase(mode)) {
                getParent().onBackPressed();
            }
        } catch (Exception e) {
            e.printStackTrace();
            if ("forced".equalsIgnoreCase(mode)) {
                android.os.Process.killProcess(android.os.Process.myPid());
                System.exit(0);
            }
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            Log.d("NUNTU RESULT", "req " + requestCode + " resp " + resultCode + " data " + data.toString());
            switch (requestCode) {
                case 1:
                    try {
                        Uri selectedImage = data.getData();
                        if (selectedImage != null && selectedImage.getScheme().compareTo("content") == 0) {
                            Cursor cursor = getContentResolver().query(selectedImage, null, null, null, null);
                            if (cursor.moveToFirst()) {
                                int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                                selectedImage = Uri.parse(cursor.getString(column_index));
                                ImageFilePath = selectedImage.getPath();
                            }
                            cursor.close();
                            if (ImageFilePath == null || !new File(ImageFilePath).exists()) {
                                Toast.makeText(PdfViewContext, "Couldn't retrieve your image", Toast.LENGTH_LONG).show();
                            } else {
                                isImageFileReady = true;
                                UploadPicLogo.setImageResource(android.R.drawable.ic_menu_upload);
                                UploadPicLogo.setBackgroundColor(Color.argb(255, 0, 0, 0));
                                Picasso.with(PdfViewContext)
                                        .load(selectedImage)
                                        .transform(new CircleTransform(64, 0))
                                        .resize(128, 128)
                                        .centerCrop()
                                        .error(R.drawable.profile)
                                        .into(UserImage);
                                UserImage.invalidate();
                            }
                        } else {
                            Toast.makeText(PdfViewContext, "Couldn't retrieve your image", Toast.LENGTH_LONG).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onBackPressed() {
        try {
            if (isFullScreenMode) {
                header.setVisibility(View.VISIBLE);
                toolbar.setVisibility(View.VISIBLE);
                isFullScreenMode = false;
            } else {
                super.onBackPressed();
            }
        } catch (Exception e) {
            e.printStackTrace();

        }
    }

    @Override
    public void onResume() {
        try {
            super.onResume();
            initUserHeader("refresh");

        } catch (Exception exc) {
            exc.printStackTrace();
            Log.e("BACK", "exc " + exc.getMessage());
        }
    }
}
