package utils.hehe.nuntu;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.annotation.TargetApi;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.webkit.MimeTypeMap;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import org.apache.commons.io.FilenameUtils;
import org.apache.http.HttpVersion;
import org.apache.http.client.HttpClient;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.KeyStore;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * Created by snickwizard on 2/6/15.
 */
public class NuntuUtils {

    public static <T> ArrayList<T> removeDuplicates(ArrayList<T> list) {
        int size = list.size();
        int out = 0;
        {
            final Set<T> encountered = new HashSet<T>();
            for (int in = 0; in < size; in++) {
                final T t = list.get(in);
                final boolean first = encountered.add(t);
                if (first) {
                    list.set(out++, t);
                }
            }
        }
        while (out < size) {
            list.remove(--size);
        }

        return list;
    }

    public static boolean isAppInstalled(Context context, String packagename) {
        PackageManager pm = context.getPackageManager();
        try {
            PackageInfo info = pm.getPackageInfo(packagename, PackageManager.GET_ACTIVITIES);
            return info != null;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static void setListViewHeightBasedOnChildren(ListView listView) {

        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            return;
        }

        int totalHeight = 0;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            listItem.measure(0, MeasureSpec.UNSPECIFIED);
            totalHeight += listItem.getMeasuredHeight();
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
        listView.requestLayout();
    }

    public static String getAlbumTitle(Context ctx, File src) {
        String Title = null;
        try {
            if (Build.VERSION.SDK_INT >= 10) {
                MediaMetadataRetriever mediaMetadataRetriever = new MediaMetadataRetriever();
                Uri uri = Uri.fromFile(src);
                mediaMetadataRetriever.setDataSource(ctx, uri);
                Title = mediaMetadataRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ALBUM);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Title;
    }

    public static String getTrackTitle(Context ctx, File src) {
        String Title = null;
        try {
            if (Build.VERSION.SDK_INT >= 10) {
                MediaMetadataRetriever mediaMetadataRetriever = new MediaMetadataRetriever();
                Uri uri = Uri.fromFile(src);
                mediaMetadataRetriever.setDataSource(ctx, uri);
                Title = mediaMetadataRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_TITLE);
            }
            if (Title == null || Title.length() <= 1) {
                Title = src.getName().substring(0, (src.getName().length() - 4));

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return Title;
    }

    public static String getTrackLength(Context ctx, File src) {
        String Length = null;
        try {
            if (Build.VERSION.SDK_INT >= 10) {
                MediaMetadataRetriever mediaMetadataRetriever = new MediaMetadataRetriever();
                Uri uri = Uri.fromFile(src);
                mediaMetadataRetriever.setDataSource(ctx, uri);
                Length = mediaMetadataRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
            }

            if (Length == null) {
                Length = "0";
            }
            long millis = Long.valueOf(Length);
            return String.format("%02d:%02d", TimeUnit.MILLISECONDS.toMinutes(millis) -
                            TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)), // The change is in this line
                    TimeUnit.MILLISECONDS.toSeconds(millis) -
                            TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "00:00";
    }

    public static Bitmap getAlbumCover(Context ctx, File src) {
        Bitmap Init = null;
        try {
            if (Build.VERSION.SDK_INT >= 10) {
                MediaMetadataRetriever mediaMetadataRetriever = new MediaMetadataRetriever();
                Uri uri = Uri.fromFile(src);
                mediaMetadataRetriever.setDataSource(ctx, uri);
                byte[] data = mediaMetadataRetriever.getEmbeddedPicture();
                if (data != null) {
                    try {
                        BitmapFactory.Options o = new BitmapFactory.Options();
                        o.inPurgeable = true;
                        Bitmap Cover = BitmapFactory.decodeByteArray(data, 0, data.length, o);
                        if (Cover != null && Cover.getWidth() > 128) {
                            //our bitmap will be 128x128x4 in mem therefore +1KB to gc
                            if (getMallocHeap()[1] <= 66560) {
                                Init = Bitmap.createScaledBitmap(Cover, 64, 64, true);
                                System.gc();
                            } else {
                                Init = Bitmap.createScaledBitmap(Cover, 128, 128, true);
                            }
                            disposeBitmap(Cover);
                        }
                    } catch (OutOfMemoryError e) {
                        e.printStackTrace();
                    }
                }
            }
        } catch (Exception e) {
            System.gc();
            e.printStackTrace();
        }

        return Init;
    }


    public static void disposeBitmap(Bitmap bitmap) {
        bitmap.recycle();
        bitmap = null;
    }

    public static void dumpMap(HashMap<String, String> hm) {

        Set<String> keys = hm.keySet();  //get all keys
        for (String i : keys) {
            Log.e("MAP: ", "key = " + i + " value= " + hm.get(i));
        }

    }

    public static String removeExtension(String s) {

        String separator = System.getProperty("file.separator");
        String filename;

        // Remove the path upto the filename.
        int lastSeparatorIndex = s.lastIndexOf(separator);
        if (lastSeparatorIndex == -1) {
            filename = s;
        } else {
            filename = s.substring(lastSeparatorIndex + 1);
        }

        // Remove the extension.
        int extensionIndex = filename.lastIndexOf(".");
        if (extensionIndex == -1)
            return filename;

        return filename.substring(0, extensionIndex);
    }

    public static String capitalizeFirstLetter(String original) {
        try {
            if (original.length() == 0)
                return original;
            String Split[] = original.toLowerCase(Locale.ENGLISH).split(" ");
            String Capitalized = "";
            for (int i = 0; i < Split.length; i++) {
                if (Split[i] != null) {
                    if (Split[i].length() <= 1) {
                        Capitalized += " " + Split[i].substring(0, 1).toUpperCase();
                    } else {
                        Capitalized += " " + Split[i].substring(0, 1).toUpperCase() + Split[i].substring(1);
                    }
                }
            }
            return Capitalized;
        } catch (Exception e) {
            e.printStackTrace();
            return original;
        }
    }

    public static String formatToUrbanShort(String _uformat) {
        try {
            long value = Long.valueOf(_uformat);
            if (value < 1000L) {
                return "" + value;
            } else if (value >= 1000L && value < 1000000L) {
                DecimalFormatSymbols decimalFormatSymbols = new DecimalFormatSymbols();
                decimalFormatSymbols.setDecimalSeparator('.');
                decimalFormatSymbols.setGroupingSeparator(',');
                DecimalFormat decimalFormat = new DecimalFormat("#,##0.0", decimalFormatSymbols);
                return "" + decimalFormat.format(value / 1000L) + "k";
            } else if (value >= 1000000L && value < 1000000000L) {
                DecimalFormatSymbols decimalFormatSymbols = new DecimalFormatSymbols();
                decimalFormatSymbols.setDecimalSeparator('.');
                decimalFormatSymbols.setGroupingSeparator(',');
                DecimalFormat decimalFormat = new DecimalFormat("#,##0.0", decimalFormatSymbols);
                return "" + decimalFormat.format(value / 1000000L) + "m";
            } else if (value >= 1000000000L) {
                DecimalFormatSymbols decimalFormatSymbols = new DecimalFormatSymbols();
                decimalFormatSymbols.setDecimalSeparator('.');
                decimalFormatSymbols.setGroupingSeparator(',');
                DecimalFormat decimalFormat = new DecimalFormat("#,##0.0", decimalFormatSymbols);
                return "" + decimalFormat.format(value / 1000000000L) + "b";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "0";
    }

    public static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    public static String getMIMEType(String filepath) {
        try {
            String extension = FilenameUtils.getExtension(filepath).trim();
            if ("epub".equalsIgnoreCase(extension.trim())) {
                return "application/epub+zip";
            }
            String mimeTypeMap = MimeTypeMap.getFileExtensionFromUrl(extension);
            if (mimeTypeMap == null) {
                extension = filepath.substring(filepath.lastIndexOf("."));
                mimeTypeMap = MimeTypeMap.getFileExtensionFromUrl(extension);
            }
            if (mimeTypeMap != null && mimeTypeMap.length() >= 3) {
                return MimeTypeMap.getSingleton().getMimeTypeFromExtension(mimeTypeMap);
            } else {
                return MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
            }

        } catch (Exception e) {
            e.printStackTrace();
            return "unknown/unknown";
        }
    }

    public static String getUserData(Context ctx, String type) {
        String data = " ";
        final AccountManager am = AccountManager.get(ctx);
        final Account[] accounts = am.getAccounts();
        if (accounts != null) {
            for (final Account ac : accounts) {
                final String acname = ac.name;
                final String actype = ac.type;
                if (acname != null && actype != null) {
                    if ("email".equals(type)) {
                        if ("com.google".equals(actype)) {
                            data = acname;
                        }
                        if (data.length() == 0) {
                            if ("com.linkedin.android".equals(actype)) {
                                data = acname;
                            }
                        }
                        if (data.length() == 0) {
                            if ("com.facebook.auth.login".equals(actype)) {
                                data = acname;
                            }
                        }
                    } else if ("username".equals(type)) {
                        if ("com.skype.contacts.sync".equals(actype)) {
                            data = acname;
                        }
                    }
                }
            }
        }
        return data;
    }


    public static String getAlbumAuthor(Context ctx, File src) {
        String Author = null;
        try {
            if (Build.VERSION.SDK_INT >= 10) {
                MediaMetadataRetriever mediaMetadataRetriever = new MediaMetadataRetriever();
                Uri uri = Uri.fromFile(src);
                mediaMetadataRetriever.setDataSource(ctx, uri);
                Author = mediaMetadataRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ARTIST);
                if (null == Author) {
                    Author = mediaMetadataRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ALBUMARTIST);
                }
                if (null == Author) {
                    Author = mediaMetadataRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_AUTHOR);
                }

                if (null == Author) {
                    Author = mediaMetadataRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_COMPOSER);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Author;
    }

    public static void toggleDefaultKeyboard(Context ctx, String mode) {
        InputMethodManager imm;
        imm = (InputMethodManager) ctx.getSystemService(Context.INPUT_METHOD_SERVICE);
        if ("hide".equals(mode)) {
            imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
        } else {
            imm.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT, 0);
        }
    }

    public static HttpClient getSecureHttpClient() {
        try {
            KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
            trustStore.load(null, null);

            SSLSocketFactory sf = new NuntuSSLSocketFactory(trustStore);
            sf.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);

            HttpParams params = new BasicHttpParams();
            HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
            HttpProtocolParams.setContentCharset(params, HTTP.UTF_8);

            SchemeRegistry registry = new SchemeRegistry();
            registry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
            registry.register(new Scheme("https", sf, 443));

            ClientConnectionManager ccm = new ThreadSafeClientConnManager(params, registry);

            return new DefaultHttpClient(ccm, params);
        } catch (Exception e) {
            return new DefaultHttpClient();
        }
    }

    public static Bitmap scaleBitmap(Bitmap bitmap, int wantedWidth, int wantedHeight) {
        Bitmap output = Bitmap.createBitmap(wantedWidth, wantedHeight, Config.ARGB_8888);
        Canvas canvas = new Canvas(output);
        Matrix m = new Matrix();
        m.setScale((float) wantedWidth / bitmap.getWidth(), (float) wantedHeight / bitmap.getHeight());
        canvas.drawBitmap(bitmap, m, new Paint());

        return output;
    }

    public static long[] getMallocHeap() {

        final Runtime runtime = Runtime.getRuntime();
        final long freeHeapSize = runtime.totalMemory() - runtime.freeMemory();
        final long maxHeapSizeInMB = runtime.maxMemory();
        long[] mem = {freeHeapSize, maxHeapSizeInMB};
        return mem;
    }

    public ArrayList<String> getInstalledApps(Context appContext) {
        try {
            System.gc();
            ArrayList<String> packageNames = new ArrayList<String>();
            List<PackageInfo> packages = appContext.getPackageManager().getInstalledPackages(0);
            for (PackageInfo pack : packages) {
                packageNames.add(pack.packageName.trim().toLowerCase(Locale.ENGLISH));
            }

            return packageNames;
        } catch (Exception exc) {
            exc.printStackTrace();
            return null;
        }
    }

    public static void sendTick(final Context TickContext, final String ItemType, final String ItemID) {
        AsyncTask<String, String, String> getTask = new AsyncTask<String, String, String>() {
            String ServerResStatus = " ",
                    ServerRes = " ";

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                Toast.makeText(TickContext, "sending...", Toast.LENGTH_SHORT).show();
            }

            @Override
            protected String doInBackground(String... params) {
                String response = "";
                try {
                    URL url = new URL(params[0]);
                    HttpURLConnection urlConnection = (HttpURLConnection)
                            url.openConnection();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                    String line = "";
                    while ((line = reader.readLine()) != null) {
                        response += line + "\n";
                    }
                    if (!urlConnection.getContentType().toLowerCase(Locale.ENGLISH).equals("application/json")) {
                        Log.e("Bad Content", "Received bad content type while expecting sjon");
                    }

                    try {
                        JSONObject obj = new JSONObject(response.toString());
                        ServerResStatus = obj.getString("status");
                        ServerRes = obj.getString("details");

                    } catch (Exception exc) {

                    }

                } catch (Exception e) {
                    Log.e("EXC", "An exception occurred while getting data" + e.getMessage());
                }
                return response;


            }


            @Override
            protected void onPostExecute(String result) {
                super.onPostExecute(result);
                if (ServerResStatus.equalsIgnoreCase("success") || ServerRes.equalsIgnoreCase("failed")) {
                    Toast.makeText(TickContext, ServerRes, Toast.LENGTH_SHORT).show();
                }
            }

        };
        getTask.execute(NuntuConstants.HOST.toString() + "/tick/" + ItemType + "/" + ItemID);
    }

    public static void shareNuntuItem(final Context shareCtx, final String itemName, final String itemType, final String itemPublisher, final String itemID) {
        Intent shareIntent = new Intent();
        shareIntent.setAction(Intent.ACTION_SEND);
        shareIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        shareIntent.putExtra(Intent.EXTRA_SUBJECT, "You will like this " + itemType);
        shareIntent.putExtra(Intent.EXTRA_TEXT, "\nCheck out " + itemName + "\n\t by " + itemPublisher + " ..." + "\n\nGet it on Nuntu http://nuntu.hehelabs.com/" + itemType + "/" + itemType);
        shareIntent.setType("text/plain");
        shareCtx.startActivity(Intent.createChooser(shareIntent, "share " + itemName + " using..."));

    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    public static String getPath(final Context context, final Uri uri) {

        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }

                // TODO handle non-primary volumes
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                return getDataColumn(context, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[]{
                        split[1]
                };

                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {
            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    /**
     * Get the value of the data column for this Uri. This is useful for
     * MediaStore Uris, and other file-based ContentProviders.
     *
     * @param context       The context.
     * @param uri           The Uri to query.
     * @param selection     (Optional) Filter used in the query.
     * @param selectionArgs (Optional) Selection arguments used in the query.
     * @return The value of the _data column, which is typically a file path.
     */
    public static String getDataColumn(Context context, Uri uri, String selection,
                                       String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs, null);
            if (cursor != null && cursor.moveToFirst()) {
                final int column_index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(column_index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }


    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }
}
