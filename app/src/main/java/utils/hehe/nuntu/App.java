package utils.hehe.nuntu;

/**
 * Created by snickwizard on 2/3/15.
 */

public class App {


    private String name = " ";
    private String itemPublisher = " ";
    private String price = "0";
    private String AppID = "0";
    private String iconURL = " ";
    private String Status = "0";
    private String packageName = "com";


    public App() {

    }

    public String getAppName() {
        return name;
    }

    public void setAppName(String name) {
        this.name = name;
    }

    public String getAppID() {
        return AppID;
    }

    public void setAppID(String id) {
        this.AppID = id;
    }

    public String getAppPublisher() {
        return itemPublisher;
    }

    public void setAppPublisher(String itemPublisher) {
        this.itemPublisher = itemPublisher;
    }

    public String getAppPrice() {
        return price;
    }

    public void setAppPrice(String price) {
        this.price = price;
    }

    public String getAppPackageName() {
        return packageName;
    }

    public void setAppPackageName(String packname) {
        this.packageName = packname;
    }

    public String getStatus() {
        return this.Status;
    }

    public void setStatus(String status) {
        this.Status = status;
    }

    public String getAppImageURL() {
        return this.iconURL;
    }

    public void setAppImageURL(String url) {
        this.iconURL = url;
    }

    @Override
    public String toString() {

        return "\nid " + AppID + "\nname " + name + "\ndesc " + itemPublisher + "\nprice " + price + "\npackage " + packageName;

    }
}