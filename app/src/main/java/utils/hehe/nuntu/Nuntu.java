package utils.hehe.nuntu;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.BaseColumns;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.SearchView.OnQueryTextListener;
import android.support.v7.widget.SearchView.OnSuggestionListener;
import android.support.v7.widget.Toolbar;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.webkit.MimeTypeMap;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Locale;

public class Nuntu extends ActionBarActivity {

    private ProgressBar UserHeaderProgress;
    private static String[] Categories = {"CLOSET", "ALL", "APPS", "BOOKS", "MUSIC"};
    private static Context NuntuContext;
    private final String[] SEARCH_SUGGESTION_CURSOR_COLUMNS = {BaseColumns._ID, SearchManager.SUGGEST_COLUMN_TEXT_2};
    MatrixCursor SuggestCursor;
    public static ViewPager mPager;
    public static ImageView UserImage, UploadPicLogo;
    public static TextView HeaderEmail, UserName, UserRank, UserBal;

    private SearchView searchView;
    private NuntuSearchSuggestionAdapter mSearchSuggestionAdapter;
    public static String ImageFilePath = "", UserID = "0";
    private boolean isImageFileReady = false;
    ProgressDialog _outDialog;
    public static NavigationDrawerFragment drawerFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        NuntuContext = this;
        try {
            final Intent intent = getIntent();
            final String action = intent.getAction();

            if (Intent.ACTION_VIEW.equals(action)) {
                final Uri uri2 = intent.getData();
                final String uri = uri2.getEncodedPath() + "  complete: " + uri2.toString();
                Log.e("URI INTERPRETE", "file url " + uri);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        setContentView(R.layout.activity_nuntu);
        final Toolbar toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        drawerFragment = (NavigationDrawerFragment) getFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
        UserImage = (ImageView) findViewById(R.id.user_image);
        UploadPicLogo = (ImageView) findViewById(R.id.upload_new);
        UploadPicLogo.setVisibility(View.GONE);
        HeaderEmail = (TextView) findViewById(R.id.user_email);
        UserName = (TextView) findViewById(R.id.user_name);
        UserRank = (TextView) findViewById(R.id.user_rank);
        UserBal = (TextView) findViewById(R.id.user_balance);
        UserHeaderProgress = (ProgressBar) findViewById(R.id.user_header_progress);
        final SwipeRefreshLayout DrawerRefresher = (SwipeRefreshLayout) findViewById(R.id.refresh_drawer);
        _outDialog = new ProgressDialog(NuntuContext);
        drawerFragment.setUp(R.id.fragment_navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout), toolbar, UserHeaderProgress, DrawerRefresher, "nuntu");
        if (DrawerRefresher != null) {

            if (Build.VERSION.SDK_INT >= 14) {
                DrawerRefresher.setColorScheme(android.R.color.holo_blue_bright,
                        android.R.color.holo_green_light,
                        android.R.color.holo_orange_light,
                        android.R.color.holo_red_light);
            }

            DrawerRefresher.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    Log.e("REFRESH", "swipe refreshing");
                    drawerFragment.getDrawerItemsAsync("/api/mobile/drawer/Android");
                }
            });
        }

        UserImage.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (UploadPicLogo.getVisibility() != View.VISIBLE) {
                    UploadPicLogo.setVisibility(View.VISIBLE);
                } else {
                    UploadPicLogo.setVisibility(View.GONE);
                }
            }
        });
        UploadPicLogo.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isImageFileReady) {
                    Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                    photoPickerIntent.setType("image/*");
                    startActivityForResult(photoPickerIntent, 2);
                } else {
                    UploadPicLogo.setBackgroundColor(Color.argb(0, 0, 0, 0));
                    UploadPicLogo.setImageResource(android.R.drawable.ic_menu_edit);
                    UploadPicLogo.setVisibility(View.GONE);
                    if (Integer.valueOf(UserID) >= 1 && ImageFilePath != null && ImageFilePath.length() >= 6) {
                        new NuntuPictureUploaderAsync(Nuntu.this).execute(ImageFilePath, NuntuConstants.HOST.toString() + "/user/" + UserID + "/picture");
                        isImageFileReady = false;
                    } else {
                        Toast.makeText(NuntuContext, "Image upload failed", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });
        final SlidingTabLayout mTabs = (SlidingTabLayout) findViewById(R.id.tabs);
        mTabs.setCustomTabView(R.layout.custom_tab, R.id.customText);
        mTabs.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
            @Override
            public int getIndicatorColor(int position) {
                return getResources().getColor(R.color.accentColor);
            }
        });
        mTabs.setOnPageChangeListener(new OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {


            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        mPager = (ViewPager) findViewById(R.id.pager);

        if (mPager != null) {
            mPager.setAdapter(new NuntuPagerAdapter(getSupportFragmentManager()));
            mTabs.setViewPager(mPager);
            if (Build.VERSION.SDK_INT >= 11) {
                mPager.setPageTransformer(true, new NuntuDepthPageTransformer());
            }

        } else {
            Log.e("FRACASSO", "mpager is null");
        }


        initUserHeader("init");
    }

    public void respondToSelectedDrawerItem(String mode, String item) {
        try {
            if ("online".equalsIgnoreCase(mode)) {
                if ("app".equalsIgnoreCase(item)) {

                } else if ("book".equalsIgnoreCase(item)) {

                } else if ("music".equalsIgnoreCase(item)) {

                }
            } else {
                if ("closet".equalsIgnoreCase(item)) {

                } else if ("about".equalsIgnoreCase(item)) {
                    final Dialog dialog;
                    if (Build.VERSION.SDK_INT >= 14) {
                        dialog = new Dialog(NuntuContext, android.R.style.Theme_DeviceDefault_Light_DialogWhenLarge);
                    } else {
                        dialog = new Dialog(NuntuContext);
                    }
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setCanceledOnTouchOutside(true);
                    dialog.setContentView(R.layout.about_layout);
                    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                    ScrollView SuperParent = (ScrollView) dialog.findViewById(R.id.parent);
                    RelativeLayout parent = (RelativeLayout) dialog.findViewById(R.id.aboutpopup);
                    TextView aboutTitle = (TextView) dialog.findViewById(R.id.AboutTitle);
                    TextView appVersion = (TextView) dialog.findViewById(R.id.aboutAppVersion);
                    TextView aboutText = (TextView) dialog.findViewById(R.id.aboutText);
                    TextView creditTitle = (TextView) dialog.findViewById(R.id.CreditTitle);
                    TextView credittext = (TextView) dialog.findViewById(R.id.CreditText);

                    TextView privacyTitle = (TextView) dialog.findViewById(R.id.Privacy);
                    TextView privacyText = (TextView) dialog.findViewById(R.id.PrivacyText);
                    ImageView closeBox = (ImageView) dialog.findViewById(R.id.closeDialog);
                    closeBox.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });
                    final TextView visitTerms = (TextView) dialog.findViewById(R.id.TermsLink);
                    aboutTitle.setText("Nuntu Android ");
                    appVersion.setText("v2.1.1");
                    aboutText.setText(NuntuConstants.ABOUT_APP.toString());
                    privacyTitle.setText("Privacy Policy");
                    privacyText.setText(NuntuConstants.NUNTU_PRIVACY.toString());
                    creditTitle.setText("Credits");
                    credittext.setText(NuntuConstants.NUNTU_CREDITS.toString());

                    if (visitTerms != null) {
                        visitTerms.setOnClickListener(new OnClickListener() {

                            @Override
                            public void onClick(View arg0) {
                                visitTerms.setMovementMethod(LinkMovementMethod.getInstance());
                            }

                        });

                    }


                    parent.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });
                    dialog.show();
                } else if ("logout".equalsIgnoreCase(item)) {
                    logUserOut();
                } else if ("login".equalsIgnoreCase(item)) {
                    if (mPager != null) {
                        mPager.setCurrentItem(1);
                    } else {
                        Toast.makeText(NuntuContext, "swipe to 'ALL' to login ", Toast.LENGTH_LONG).show();
                    }
                } else if ("help".equalsIgnoreCase(item)) {

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void initUserHeader(String mode) {
        try {

            String userLocalCredentials = User.getNuntuLocalLogin(NuntuContext);
            if (userLocalCredentials != null) {
                try {
                    JSONObject UserData = new JSONObject(userLocalCredentials);
                    if (NuntuUtils.isValidEmail(UserData.getString("email"))) {
                        HeaderEmail.setText(UserData.getString("email"));
                        UserName.setText(UserData.getString("firstname") + " " + UserData.getString("lastname"));
                        UserRank.setText(UserData.getString("rank"));
                        UserBal.setText("---");
                        UserID = UserData.getString("userid");

                        try {
                            String ImageUrl = UserData.getString("image");
                            if (ImageUrl != null) {
                                if (ImageUrl.contains("../../../../")) {
                                    ImageUrl = NuntuConstants.HOST.toString() + ImageUrl.replace("../../../../", "/");
                                }
                                Picasso.with(NuntuContext)
                                        .load(ImageUrl)
                                        .resize(128, 128)
                                        .centerCrop()
                                        .transform(new CircleTransform(84, 0))
                                        .placeholder(R.drawable.profile)
                                        .error(R.drawable.profile)
                                        .into(UserImage);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    } else {
                        HeaderEmail.setText(" ");
                        UserName.setText("Please log in");
                        UserRank.setText("---");
                        UserBal.setText("---");
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (TelephonyInfo.hasConnection(NuntuContext)) {
                    //load user balance and user image
                }
            } else {
                HeaderEmail.setText(" ");
                UserName.setText("Please log in");
                UserRank.setText("---");
                UserBal.setText("---");

            }

            if (!"refresh".equalsIgnoreCase(mode)) {
                if (UserHeaderProgress != null) {
                    UserHeaderProgress.setVisibility(View.VISIBLE);
                }
            } else {
                drawerFragment.getDrawerItemsAsync("/api/mobile/drawer/Android");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void logUserOut() {
        try {
            Log.e("Logout", "logging out ");
            if (!User.logout(NuntuContext)) {
                User.logout(NuntuContext);//another instance may retain Prefs..retry
            }
            //we weren't getting enough time to clear user prefs data
            if (_outDialog == null) {
                _outDialog = new ProgressDialog(NuntuContext);
            }
            _outDialog.setMessage("    ... logging out ...  ");
            _outDialog.show();

            new Thread() {
                public void run() {

                    try {

                        Thread.sleep(2000);
                        Intent intent = new Intent(NuntuContext, NuntuMPC.class);
                        intent.putExtra("action", "auto_destruct");
                        NuntuContext.startService(intent);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                _outDialog.setMessage("    ... shutting down ...  ");
                            }
                        });


                        Thread.sleep(2000);

                    } catch (InterruptedException e) {
                        e.printStackTrace();

                    }
                    exitApp();
                }

            }.start();


        } catch (Exception exc) {
            exc.printStackTrace();
            Log.e("Logout", "An exception occurred " + exc.getMessage());
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuItem searchMenuItem = menu.add(Menu.NONE, Menu.NONE, 1, R.string.action_search);
        searchView = new SearchView(getSupportActionBar().getThemedContext());
        setupSearchView(searchView);
        // search menu item
        MenuItemCompat.setActionView(searchMenuItem, searchView);
        MenuItemCompat.setShowAsAction(searchMenuItem, MenuItem.SHOW_AS_ACTION_ALWAYS);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    private void exitApp() {
        try {
            if (_outDialog != null) {
                _outDialog.dismiss();
            }

            //destroy evidence
            int pid = android.os.Process.myPid();
            Log.e("KILLING PROC", "nuntu process id " + pid);
            android.os.Process.killProcess(pid);
            //clean up after ourselves ... any resources dealloc will go here
            this.finish();
            super.onDestroy();
            System.exit(0);
        } catch (Exception e) {
            System.exit(0);
            e.printStackTrace();

        }

    }

    @Override
    public void onBackPressed() {
        try {
            Intent intent = new Intent(NuntuContext, NuntuMPC.class);
            intent.putExtra("action", "miniplay");
            NuntuContext.startService(intent);
            super.onBackPressed();

        } catch (Exception exc) {
            exc.printStackTrace();
            Log.e("BACK", "exc " + exc.getMessage());
            exitApp();

        }
    }

    @Override
    public void onPause() {
        try {
            Intent intent = new Intent(NuntuContext, NuntuMPC.class);
            intent.putExtra("action", "miniplay");
            NuntuContext.startService(intent);
            super.onPause();

        } catch (Exception exc) {
            exc.printStackTrace();
            Log.e("BACK", "exc " + exc.getMessage());
        }
    }

    @Override
    public void onResume() {
        try {
            super.onResume();
            Intent intent = new Intent(NuntuContext, NuntuMPC.class);
            intent.putExtra("action", "maxplay");
            NuntuContext.startService(intent);
            initUserHeader("refresh");

        } catch (Exception exc) {
            exc.printStackTrace();
            Log.e("BACK", "exc " + exc.getMessage());
        }
    }

    private void searchLocalContent(String... params) {

    }

    private void setupSearchView(SearchView searchView) {
        // search hint
        searchView.setQueryHint("search nuntu");
        // text color
        AutoCompleteTextView searchText = (AutoCompleteTextView) searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text);
        searchText.setTextColor(getResources().getColor(R.color.primaryTextColor));
        searchText.setHintTextColor(Color.LTGRAY);
        // suggestion listeners
        searchView.setOnQueryTextListener(new OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                Toast.makeText(NuntuContext, "search: " + query, Toast.LENGTH_SHORT).show();
                return true;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                if (query.length() > 2) {
                    getSearchData(query);
                }
                return true;
            }
        });

        searchView.setOnSuggestionListener(new OnSuggestionListener() {
            @Override
            public boolean onSuggestionSelect(int position) {
                return false;
            }

            @Override
            public boolean onSuggestionClick(int position) {
                Cursor cursor = (Cursor) mSearchSuggestionAdapter.getItem(position);
                String title = cursor.getString(cursor.getColumnIndex(SearchManager.SUGGEST_COLUMN_TEXT_2));

                String[] TypeArrayed = title.replace("(", ",").split(",");
                String type = TypeArrayed[1].replace(")", "");
                String item = "";
                try {
                    item = URLEncoder.encode(TypeArrayed[0], "UTF-8");

                } catch (Exception exc) {
                    exc.printStackTrace();
                    item = TypeArrayed[0];
                }
                if ("album".equalsIgnoreCase(type)) {
                    type = "music";
                }
                respondToSearchUserSelection("/search/show/android/" + type + "/" + item, TypeArrayed[0], type);
                return true;
            }
        });
    }

    public void getSearchData(String URL) {
        AsyncTask<String, String, String> getTask = new AsyncTask<String, String, String>() {

            BufferedReader reader;
            ArrayList<String> searchRes;

            @Override
            protected void onPreExecute() {

                if (searchRes != null) {
                    searchRes.clear();
                } else {
                    searchRes = new ArrayList<>();
                }

            }

            @Override
            protected String doInBackground(String... params) {
                String response = "";
                try {

                    java.net.URL url = new URL(NuntuConstants.HOST.toString() + "/data/search/all/" + params[0]);
                    HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                    if (reader != null) {
                        reader.reset();
                        reader.close();
                    }
                    reader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                    String line = "";
                    while ((line = reader.readLine()) != null) {
                        response += line + "\n";
                    }
                    if (!urlConnection.getContentType().toLowerCase(Locale.ENGLISH).equals("application/json")) {
                        Log.e("Bad Content", "Received bad content type while expecting json");
                    }
                    Log.e("QUERY RES", response);
                    JSONObject obj = new JSONObject(response);
                    String query = obj.getString("status");
                    if ("success".equalsIgnoreCase(query)) {

                        if (searchRes.size() >= 1) {
                            searchRes.clear();
                            searchRes = null;
                            searchRes = new ArrayList<>();
                        }
                        JSONObject jsonData = new JSONObject(obj.getString("data"));
                        JSONArray apps = new JSONArray(jsonData.getString("app"));
                        JSONArray books = new JSONArray(jsonData.getString("book"));
                        JSONArray albums = new JSONArray(jsonData.getString("music"));


                        for (int i = 0; i < apps.length(); i++) {
                            JSONObject app = apps.getJSONObject(i);
                            searchRes.add(app.getString("app_name") + " (app)");
                        }


                        for (int i = 0; i < books.length(); i++) {
                            JSONObject book = books.getJSONObject(i);
                            searchRes.add(book.getString("book_name") + " (book)");
                        }


                        for (int i = 0; i < albums.length(); i++) {
                            JSONObject album = albums.getJSONObject(i);
                            searchRes.add(album.getString("music_name") + " (album)");
                        }

                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e("EXC", "An exception occurred while getting data" + e.getMessage() + "\n\n" + response);
                }

                return response;
            }

            protected void onPostExecute(String result) {
                super.onPostExecute(result);
                updateSearchSuggestion(searchRes);
            }
        };
        getTask.execute(URL);
    }

    private void updateSearchSuggestion(ArrayList<String> queryData) {

        SuggestCursor = new MatrixCursor(SEARCH_SUGGESTION_CURSOR_COLUMNS);

        int i = 0;
        while (queryData != null && i < queryData.size()) {
            String id = String.valueOf(i + i);
            String Entry = queryData.get(i);
            SuggestCursor.addRow(new String[]{id, Entry});
            i++;
            Log.e("QUERY ENTRY", Entry);
        }

        // searchview content
        if (mSearchSuggestionAdapter == null) {
            Log.e("REFILL", "just new");
            // create adapter
            mSearchSuggestionAdapter = new NuntuSearchSuggestionAdapter(this, SuggestCursor);
            // set adapter
            searchView.setSuggestionsAdapter(mSearchSuggestionAdapter);
        } else {
            Log.e("REFILL", "not new");
            // refill adapter
            mSearchSuggestionAdapter.refill(this, SuggestCursor);
            // set adapter
            searchView.setSuggestionsAdapter(mSearchSuggestionAdapter);
            mSearchSuggestionAdapter.notifyDataSetChanged();
        }

    }

    public void respondToSearchUserSelection(String URL, final String name, final String type) {
        AsyncTask<String, String, String> getTask = new AsyncTask<String, String, String>() {

            boolean OK = false;
            ProgressDialog LaunchSearch;
            String appid,
                    appname,
                    appPackage,
                    appprice,
                    appIcon;
            String bookid,
                    bookname,
                    booktype,
                    bookprice,
                    bookIcon;
            String musicid,
                    musicname,
                    musicPackage,
                    musicprice,
                    musicIcon;

            @Override
            protected void onPreExecute() {

                runOnUiThread(new Runnable() {
                    public void run() {
                        LaunchSearch = new ProgressDialog(NuntuContext);
                        LaunchSearch.setTitle("Loading " + type + " ...");
                        LaunchSearch.setMessage("getting detailed info about " + name);
                        LaunchSearch.show();
                    }

                });

            }

            @Override
            protected String doInBackground(String... params) {
                String response = "";
                try {

                    URL url = new URL(NuntuConstants.HOST.toString() + params[0]);
                    HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

                    BufferedReader reader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                    String line = "";
                    while ((line = reader.readLine()) != null) {
                        response += line + "\n";
                    }

                    System.out.print(response);
                    if (!urlConnection.getContentType().toLowerCase(Locale.ENGLISH).equals("application/json")) {
                        Log.e("Bad Content", "Received bad content type while expecting json");
                    }
                    JSONObject obj = new JSONObject(response);
                    JSONObject data = new JSONObject(obj.getString("data"));
                    String query = obj.getString("status");
                    if ("success".equalsIgnoreCase(query)) {
                        if ("app".equalsIgnoreCase(type)) {
                            appid = data.getString("app_id");
                            appname = data.getString("app_name");
                            appPackage = data.getString("packagename");
                            appprice = data.getString("app_price") + " " + data.getString("app_currency");
                            if (data.getString("app_icon").length() >= 5) {
                                String Icon = data.getString("app_icon");
                                Icon = Icon.replace("../", "");
                                appIcon = NuntuConstants.HOST.toString() + "/" + Icon;
                            } else {
                                appIcon = NuntuConstants.HOST.toString() + "/data/defaults/android_icon.png";
                            }
                            OK = true;
                        } else if ("book".equalsIgnoreCase(type)) {
                            bookid = data.getString("book_id");
                            bookname = data.getString("book_name");
                            booktype = data.getString("book_type");
                            bookprice = data.getString("book_price") + " " + data.getString("book_currency");
                            if (data.getString("book_icon").length() >= 5) {
                                String Icon = data.getString("book_icon");
                                Icon = Icon.replace("../", "");
                                bookIcon = NuntuConstants.HOST.toString() + "/" + Icon;
                            } else {
                                bookIcon = NuntuConstants.HOST.toString() + "/data/defaults/book_icon.png";
                            }
                            OK = true;
                        } else if ("music".equalsIgnoreCase(type)) {
                            musicid = data.getString("music_id");
                            musicname = data.getString("music_name");
                            musicprice = data.getString("music_price") + " " + data.getString("music_currency");
                            if (data.getString("music_icon").length() >= 5) {
                                String Icon = data.getString("music_icon");
                                Icon = Icon.replace("../", "");
                                musicIcon = NuntuConstants.HOST.toString() + "/" + Icon;
                            } else {
                                musicIcon = NuntuConstants.HOST.toString() + "/data/defaults/music_icon.png";
                            }
                            OK = true;
                        }
                    }
                    return response;
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e("EXC", "An exception occured while getting data" + e.getMessage());
                }
                return response;


            }

            protected void onPostExecute(String result) {
                super.onPostExecute(result);
                if (LaunchSearch != null) {
                    LaunchSearch.cancel();
                }
                if (OK) {
                    if ("app".equalsIgnoreCase(type)) {
                        String buttonText = "DOWNLOAD";
                        String[] Price = appprice.split(" ");
                        if (Price[0] == null) {
                            Price[0] = "0";
                        }
                        ArrayList<String> inApps = new NuntuUtils().getInstalledApps(NuntuContext);
                        if (inApps != null) {
                            if (Float.valueOf(Price[0]) > 0) {
                                buttonText = "BUY";
                            } else {
                                appprice = "FREE";
                            }

                            if (inApps.contains(appPackage.toLowerCase(Locale.ENGLISH).trim())) {
                                buttonText = "INSTALLED";
                            }
                        }

                       /* Intent intent=new Intent(NuntuContext,AppDetailActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.putExtra("appID",appid);
                        intent.putExtra("appPrice",appprice);
                        intent.putExtra("appName",appname);
                        intent.putExtra("packageName",appPackage);
                        intent.putExtra("appIcon",appIcon);
                        intent.putExtra("buttonText",buttonText);
                        NuntuContext.startActivity(intent);*/

                    } else if ("book".equalsIgnoreCase(type)) {

                        booktype = MimeTypeMap.getSingleton().getExtensionFromMimeType(booktype.trim());
                        Log.e("BOOK TYPE", "book type from mime " + booktype);

                        if (booktype == null) {
                            booktype = "pdf";
                        }
                        String BookFileLocation = "../";
                        String ButtonText = "DOWNLOAD";
                        if (bookprice != null) {
                            if (bookprice.trim().startsWith("0.00")) {
                                bookprice = "FREE";
                            } else {
                                ButtonText = "BUY";
                            }
                        } else {
                            bookprice = "FREE";
                        }
                        File BookStorage = null;
                        try {
                            String LocationSD = Environment.getExternalStorageDirectory().toString();
                            BookStorage = new File(LocationSD + "/Documents/NuntuBooks/" + bookname.replace(" ", "_") + "." + booktype);
                            if (BookStorage.exists()) {
                                BookFileLocation = BookStorage.getAbsolutePath();
                                ButtonText = "READ";
                            } else {
                                String LocationIN = Environment.getDataDirectory().toString();
                                BookStorage = new File(LocationIN + "/Documents/NuntuBooks/" + bookname.replace(" ", "_") + "." + booktype);
                                if (BookStorage.exists()) {
                                    BookFileLocation = BookStorage.getAbsolutePath();
                                    ButtonText = "READ";
                                }
                                Log.e("ALBUMLOCALTRACKS", "Folder at " + BookStorage.getAbsolutePath() + " does not exist");
                            }

                        } catch (Exception exc) {
                            exc.printStackTrace();
                            Log.d("ALBUMLOCALTRACKS", "File system access error " + exc.getMessage());
                        }

                        /*Intent intent=new Intent(NuntuContext,BookActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.putExtra("bookID",bookid);
                        intent.putExtra("bookName",bookname);
                        intent.putExtra("bookDesc",bookname);
                        intent.putExtra("bookPrice",bookprice);
                        intent.putExtra("bookIcon",bookIcon);
                        intent.putExtra("buttonText",ButtonText);
                        intent.putExtra("location",BookFileLocation);
                        NuntuContext.startActivity(intent);*/

                    } else if ("music".equalsIgnoreCase(type)) {
                        String buttonText = "DOWNLOAD", trackCount = "0";
                        ArrayList<String> tracks = null;
                        File albumStorage = null;
                        try {
                            String LocationSD = Environment.getExternalStorageDirectory().toString();
                            albumStorage = new File(LocationSD + "/Music/NuntuMusic/" + musicname.replace(" ", "_") + musicid);
                            if (albumStorage.exists()) {
                                tracks = new NuntuFile().getDirFiles(albumStorage);
                                Log.d("ALBUMLOCALTRACKS", "Album local tracks " + tracks.size());
                            } else {
                                String LocationIN = Environment.getDataDirectory().toString();
                                albumStorage = new File(LocationIN + "/Music/NuntuMusic/" + musicname.replace(" ", "_") + musicid);
                                if (albumStorage.exists()) {
                                    tracks = new NuntuFile().getDirFiles(albumStorage);
                                    Log.d("ALBUMLOCALTRACKS", "Album local tracks " + tracks.size());
                                }
                                Log.d("ALBUMLOCALTRACKS", "Folder at " + albumStorage.getAbsolutePath() + " does not exist");
                            }

                        } catch (Exception exc) {
                            exc.printStackTrace();
                            Log.d("ALBUMLOCALTRACKS", "File system access error " + exc.getMessage());
                        }

                        if (tracks != null) {
                            if (tracks.size() >= 1) {
                                trackCount = String.valueOf(tracks.size());
                                buttonText = "PLAY";
                            }
                        }

                        if (musicprice != null) {
                            if (musicprice.trim().startsWith("0.00")) {
                                musicprice = "FREE";
                            }
                        } else {
                            musicprice = "FREE";
                        }

                       /* Intent intent=new Intent(NuntuContext,AlbumDetailActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.putExtra("albumID",musicid);
                        intent.putExtra("albumName",musicname);
                        intent.putExtra("albumDesc",musicname);
                        intent.putExtra("albumPrice",musicprice);
                        intent.putExtra("albumIcon",musicIcon);
                        intent.putExtra("buttonText",buttonText);
                        intent.putExtra("trackcount",trackCount);
                        NuntuContext.startActivity(intent);*/
                    }
                } else {
                    Toast.makeText(NuntuContext, "An error occured while getting data for this item", Toast.LENGTH_LONG).show();
                }
            }

        };
        getTask.execute(URL);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            Log.d("NUNTU RESULT", "req " + requestCode + " resp " + resultCode + " data " + data.toString());
            switch (requestCode) {
                case 1:
                    if (resultCode == Activity.RESULT_OK) {
                        if (data != null) {
                            String status = data.getStringExtra("status");
                            String CurrentUserFname = data.getStringExtra("firstname");
                            String CurrentUserLname = data.getStringExtra("lastname");
                            String CurrentUserEmail = data.getStringExtra("email");
                            String faceID = data.getStringExtra("ID");
                            if ("proceed".equals(status)) {

                                try {
                                    JSONObject object = new JSONObject();
                                    object.put("source", "facebook");
                                    object.put("firstname", CurrentUserFname);
                                    object.put("lastname", CurrentUserLname);
                                    object.put("facebookID", faceID.trim());
                                    object.put("email", CurrentUserEmail.trim());
                                    String TxData = object.toString();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    Log.d("SENDING FACELOG", "exception " + e.getMessage());
                                }
                            } else {
                                FragmentNuntuHome.popLoginScreen();
                            }
                        } else {
                            FragmentNuntuHome.popLoginScreen();
                        }

                    } else {
                        FragmentNuntuHome.popLoginScreen();
                    }
                    break;
                case 2:
                    try {
                        Uri selectedImage = data.getData();
                        if (selectedImage != null && selectedImage.getScheme().compareTo("content") == 0) {
                            Cursor cursor = getContentResolver().query(selectedImage, null, null, null, null);
                            if (cursor.moveToFirst()) {
                                int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                                selectedImage = Uri.parse(cursor.getString(column_index));
                                ImageFilePath = selectedImage.getPath();
                            }
                            cursor.close();
                            if (ImageFilePath == null || !new File(ImageFilePath).exists()) {
                                Toast.makeText(NuntuContext, "Couldn't retrieve your image", Toast.LENGTH_LONG).show();
                            } else {
                                isImageFileReady = true;
                                UploadPicLogo.setImageResource(android.R.drawable.ic_menu_upload);
                                UploadPicLogo.setBackgroundColor(Color.argb(255, 0, 0, 0));
                                Picasso.with(NuntuContext)
                                        .load(selectedImage)
                                        .transform(new CircleTransform(84, 0))
                                        .resize(128, 128)
                                        .centerCrop()
                                        .error(R.drawable.profile)
                                        .into(UserImage);
                                UserImage.invalidate();
                            }
                        } else {
                            Toast.makeText(NuntuContext, "Couldn't retrieve your image", Toast.LENGTH_LONG).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    } catch (OutOfMemoryError ex) {
                        ex.printStackTrace();
                    }
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void changeTab(String category) {
        if ("home".equalsIgnoreCase(category)) {
            mPager.setCurrentItem(1);
        } else if ("app".equalsIgnoreCase(category)) {
            mPager.setCurrentItem(2);
        } else if ("book".equalsIgnoreCase(category)) {
            mPager.setCurrentItem(3);
        } else if ("music".equalsIgnoreCase(category)) {
            mPager.setCurrentItem(4);
        } else {
            mPager.setCurrentItem(0);
        }
    }

    class NuntuPagerAdapter extends FragmentPagerAdapter {

        String[] tabs;

        public NuntuPagerAdapter(FragmentManager fm) {
            super(fm);
            tabs = Categories;
        }

        @Override
        public Fragment getItem(int position) {
            Log.e("SELECT FRAG ", "currently selected " + position);
            switch (position) {
                case 0:
                    FragmentNuntuCloset closet = FragmentNuntuCloset.getInstance(position);
                    return closet;
                case 1:
                    FragmentNuntuHome all = FragmentNuntuHome.getInstance(position);
                    return all;
                case 2:
                    FragmentNuntuApp app = FragmentNuntuApp.getInstance(position);
                    return app;
                case 3:
                    FragmentNuntuBook book = FragmentNuntuBook.getInstance(position);
                    return book;
                case 4:
                    FragmentNuntuMusic music = FragmentNuntuMusic.getInstance(position);
                    return music;
                default:
                    return null;

            }
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return tabs[position];
        }

        @Override
        public int getCount() {
            return Categories.length;
        }
    }

}

