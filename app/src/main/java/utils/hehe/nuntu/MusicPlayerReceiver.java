package utils.hehe.nuntu;

import android.app.Activity;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.commons.io.FilenameUtils;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by gakuba on 4/17/2015.
 */
public class MusicPlayerReceiver extends Activity {
    private String TAG = "TagOpenMP3NUNTU";
    private Uri uri;
    ;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final Intent intent = getIntent();
        final String action = intent.getAction();
        if (Intent.ACTION_VIEW.equals(action)) {
            Log.e(TAG, "after action view");
            uri = intent.getData();
            String path = NuntuUtils.getPath(this, uri);
            if (path != null) {
                File file = new File(path);
                if (!file.exists()) {
                    Log.e(TAG, "after file not found at first ..." + path);
                    file = new File(path);
                }
                if (file.exists()) {
                    Log.e(TAG, "after file  found at second..." + path);
                    try {
                        Intent play = new Intent(this, NuntuMPC.class);
                        Bundle extras = new Bundle();
                        Bitmap bmp = NuntuUtils.getAlbumCover(this, file);
                        if (bmp != null) {
                            extras.putParcelable("cover", NuntuUtils.scaleBitmap(bmp, 128, 128));
                        } else {
                            extras.putParcelable("cover", null);
                        }
                        ArrayList<String> paths = new ArrayList<>();
                        paths.add(file.getAbsolutePath());

                        String title = NuntuUtils.getTrackTitle(this, file);
                        if (null == title) {
                            title = FilenameUtils.getBaseName(file.getAbsolutePath());
                        }
                        if (null == title) {
                            title = "Nuntu music track";
                        }
                        ArrayList<String> titles = new ArrayList<>();
                        titles.add(title);

                        play.putExtras(extras);
                        play.putStringArrayListExtra("songpath", paths);
                        play.putStringArrayListExtra("songtitle", titles);
                        play.putExtra("action", "play");
                        play.putExtra("from", "album");
                        this.startService(play);

                    } catch (Exception e) {
                        e.printStackTrace();
                        Log.e(TAG, "well exception happenned " + e.getMessage());
                        Toast.makeText(this, "Nuntu failed to play this track " + e.getMessage(), Toast.LENGTH_LONG).show();
                        Intent i = new Intent();
                        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        i.setAction(Intent.ACTION_VIEW);
                        String type = "audio/*";
                        i.setDataAndType(uri, type);
                        startActivity(i);
                    }

                } else {
                    Log.e(TAG, "after file not found at second " + path);
                    Toast.makeText(this, "Sorry, Nuntu cannot play this track...\nLocation hidden", Toast.LENGTH_LONG).show();
                    Intent i = new Intent();
                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    i.setAction(Intent.ACTION_VIEW);
                    String type = "audio/*";
                    i.setDataAndType(uri, type);
                    startActivity(i);
                }

            } else {
                Log.e(TAG, "intent uri wasn't set ");
                try {
                    Toast.makeText(this, "Sorry, Nuntu cannot play this track...\nNo data received", Toast.LENGTH_LONG).show();
                    Intent i = new Intent();
                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    i.setAction(Intent.ACTION_VIEW);
                    String type = "audio/*";
                    i.setDataAndType(uri, type);
                    startActivity(i);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } else {
            Log.e(TAG, "intent action wasn't set to view");
            try {
                Toast.makeText(this, "Sorry, Nuntu cannot play this track...\nNo data received", Toast.LENGTH_LONG).show();
                Intent i = new Intent();
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                i.setAction(Intent.ACTION_VIEW);
                String type = "audio/*";
                i.setDataAndType(uri, type);
                startActivity(i);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

       /* Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {*/
        try {
            MusicPlayerReceiver.this.finish();
            System.exit(0);
        } catch (Exception ex) {
            ex.printStackTrace();
            MusicPlayerReceiver.this.onBackPressed();
            MusicPlayerReceiver.this.onDestroy();
            System.exit(0);
        }
           /* }
        }, 5000);*/

    }
}
