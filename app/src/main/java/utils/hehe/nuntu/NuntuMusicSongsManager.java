package utils.hehe.nuntu;

import android.content.Context;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by snickwizard on 2/9/15.
 */
public class NuntuMusicSongsManager {
    // SDCard Path
    private String media_path = new String("Music/NuntuMusic/");
    private ArrayList<String> trackPaths = new ArrayList<>();
    private ArrayList<HashMap<String, String>> songsList = new ArrayList<HashMap<String, String>>();
    private Context ctx;

    // Constructor
    public NuntuMusicSongsManager(String path) {
        media_path = path;
    }

    public NuntuMusicSongsManager(Context ctx, ArrayList<String> paths) {
        media_path = null;
        trackPaths.clear();
        this.ctx = ctx;
        trackPaths.addAll(paths);
        // songsList
    }

    /**
     * Function to read all mp3 files from sdcard
     * and store the details in ArrayList
     */
    public ArrayList<HashMap<String, String>> getPlayList() {
        if (media_path != null) {
            File home = new File(media_path);

            if (home.exists() && home.listFiles(new FileExtensionFilter()).length > 0) {
                for (File file : home.listFiles(new FileExtensionFilter())) {
                    HashMap<String, String> song = new HashMap<String, String>();
                    song.put("songTitle", file.getName().substring(0, (file.getName().length() - 4)));
                    song.put("songPath", file.getPath());

                    // Adding each song to SongList
                    songsList.add(song);
                }
            }
        } else {
            for (String path : trackPaths) {
                HashMap<String, String> song = new HashMap<String, String>();
                song.put("songTitle", NuntuUtils.getTrackTitle(ctx, new File(path)));
                song.put("songPath", path);
                // Adding each song to SongList
                songsList.add(song);
            }
        }
        // return songs list array
        return songsList;
    }

    /**
     * Class to filter files which are having .mp3 extension
     */
    class FileExtensionFilter implements FilenameFilter {
        public boolean accept(File dir, String name) {
            return (name.endsWith(".mp3") || name.endsWith(".MP3"));
        }
    }
}