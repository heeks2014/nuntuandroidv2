package utils.hehe.nuntu;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.widget.CardView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Locale;

public class HolderAppBaseAdapter extends BaseAdapter {

    static Activity appcontext;
    private static ArrayList<App> itemDetailsrrayList;
    final String HOST = "http://nuntu.hehelabs.com";
    public String FocusedAppName, FocusedAppID, FocusedPackageName;
    public String InstallAppPackageName;
    AlertDialog.Builder alertboxDownload;
    NuntuBroadcastReceiver broadCastReceiver = null;
    int IconWidth = 128, columns = 2;
    private LayoutInflater l_Inflater;
    private String details = "", crespayUrl = "";
    private String OrderID;
    private String OrderKey;
    private ArrayList<String> appPermissions;
    private ArrayList<String> deviceList;
    private ArrayList<String> paymentMethods;
    private String user;

    public HolderAppBaseAdapter(Activity context, ArrayList<App> results, String userID, int columns) {
        itemDetailsrrayList = results;
        appcontext = context;
        this.columns = columns;
        if (appcontext != null) {
            l_Inflater = LayoutInflater.from(appcontext);
            user = userID;
            alertboxDownload = new AlertDialog.Builder(appcontext);
        }

        if (appcontext != null) {
            DisplayMetrics metrics = appcontext.getResources().getDisplayMetrics();
            IconWidth = (int) ((metrics.widthPixels / columns) / 1.75) - 4;
        }
    }

    public int getCount() {
        return itemDetailsrrayList.size();
    }

    public Object getItem(int position) {
        return itemDetailsrrayList.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {

        final ViewHolder holder;
        if (convertView == null) {
            convertView = l_Inflater.inflate(R.layout.item_row, null);
            holder = new ViewHolder();
            holder.txt_itemName = (TextView) convertView.findViewById(R.id.itemname);
            holder.txt_itemPublisher = (TextView) convertView.findViewById(R.id.item_publisher);
            holder.txt_itemPrice = (TextView) convertView.findViewById(R.id.itemoffer);
            holder.itemImage = (ImageView) convertView.findViewById(R.id.itemicon);

            holder.itemButton = (Button) convertView.findViewById(R.id.item_action_button);
            holder.itemTick = (Button) convertView.findViewById(R.id.item_like_button);
            holder.itemShare = (Button) convertView.findViewById(R.id.item_share_button);

            holder.buttons = (CardView) convertView.findViewById(R.id.item_controls);
            holder.ic_app_menu = (ImageView) convertView.findViewById(R.id.item_ic_menu);

            holder.ic_app_menu.setOnClickListener(new OnClickListener() {
                public void onClick(View v) {

                    if (holder.buttons.getVisibility() == View.VISIBLE) {
                        holder.buttons.setVisibility(View.GONE);
                    } else {
                        FragmentNuntuApp.hideOpenMenus();
                        holder.buttons.setVisibility(View.VISIBLE);
                    }
                }
            });
            convertView.setTag(holder);

        } else {
            convertView.refreshDrawableState();
            holder = (ViewHolder) convertView.getTag();

        }

        convertView.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                FragmentNuntuApp.hideOpenMenus();
            }
        });
        try {
            String AppPrice = itemDetailsrrayList.get(position).getAppPrice();
            if (AppPrice != null && AppPrice.contains(".00")) {
                AppPrice = AppPrice.replace(".00", "");
            }

            String AppPub = itemDetailsrrayList.get(position).getAppPublisher();
            if (AppPub != null) {
                AppPub = NuntuUtils.capitalizeFirstLetter(AppPub.toLowerCase());
            }

            String AppName = itemDetailsrrayList.get(position).getAppName();
            if (AppName != null) {
                AppName = NuntuUtils.capitalizeFirstLetter(AppName.toLowerCase());
            }

            holder.txt_itemName.setText(AppName);
            holder.txt_itemPublisher.setText(AppPub);
            holder.txt_itemPrice.setText(AppPrice);

            Picasso.with(appcontext)
                    .load(itemDetailsrrayList.get(position).getAppImageURL())
                    .resize(IconWidth, IconWidth)
                    .centerCrop()
                    .error(R.drawable.app_logo)
                    .placeholder(R.drawable.app_logo)
                    .into(holder.itemImage);

            if ("1".equals(itemDetailsrrayList.get(position).getStatus())) {
                holder.itemButton.setText("Installed");
            } else if ("2".equals(itemDetailsrrayList.get(position).getStatus())) {
                holder.itemButton.setText("Buy");

            } else {
                holder.itemButton.setText("Download");

            }

            holder.txt_itemPublisher.setOnClickListener(new OnClickListener() {
                public void onClick(View v) {
                    FragmentNuntuApp.hideOpenMenus();
                        /*Intent intent=new Intent(appcontext,AppDetailActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.putExtra("appID",itemDetailsrrayList.get(position).getAppID());
						intent.putExtra("appName",itemDetailsrrayList.get(position).getAppName());
						intent.putExtra("appIcon",itemDetailsrrayList.get(position).getAppImageURL());
						intent.putExtra("appPrice",itemDetailsrrayList.get(position).getAppPrice());
						intent.putExtra("buttonText",itemDetailsrrayList.get(position).getButtonText());
						intent.putExtra("packagename",itemDetailsrrayList.get(position).getAppPackageName());
						appcontext.startActivity(intent);*/
                }
            });

            holder.itemImage.setOnClickListener(new OnClickListener() {
                public void onClick(View v) {
                    FragmentNuntuApp.hideOpenMenus();
                        /*Intent intent=new Intent(appcontext,AppDetailActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.putExtra("appID",itemDetailsrrayList.get(position).getAppID());
						intent.putExtra("appName",itemDetailsrrayList.get(position).getAppName());
						intent.putExtra("appIcon",itemDetailsrrayList.get(position).getAppImageURL());
						intent.putExtra("appPrice",itemDetailsrrayList.get(position).getAppPrice());
						intent.putExtra("buttonText",itemDetailsrrayList.get(position).getButtonText());
						intent.putExtra("packagename",itemDetailsrrayList.get(position).getAppPackageName());
						appcontext.startActivity(intent);*/
                }
            });

            holder.itemTick.setOnClickListener(new OnClickListener() {
                public void onClick(View v) {
                    FragmentNuntuApp.hideOpenMenus();
                    sendTick(itemDetailsrrayList.get(position).getAppID());
                }
            });

            holder.itemShare.setOnClickListener(new OnClickListener() {
                public void onClick(View v) {
                    try {
                        FragmentNuntuApp.hideOpenMenus();
                        Intent shareIntent = new Intent();
                        shareIntent.setAction(Intent.ACTION_SEND);
                        shareIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        shareIntent.putExtra(Intent.EXTRA_SUBJECT, "You will like this app");
                        if (itemDetailsrrayList.get(position).getAppPublisher().length() > 80) {
                            shareIntent.putExtra(Intent.EXTRA_TEXT, "\nCheck out " + itemDetailsrrayList.get(position).getAppName() + "\n\n by " + itemDetailsrrayList.get(position).getAppPublisher().substring(0, 80) + " ..." + "\n\nGet it on Nuntu http://nuntu.hehelabs.com/app/" + itemDetailsrrayList.get(position).getAppID());
                        } else {
                            shareIntent.putExtra(Intent.EXTRA_TEXT, "\nCheck out " + itemDetailsrrayList.get(position).getAppName() + "\n\n by " + itemDetailsrrayList.get(position).getAppPublisher() + " ..." + "\n\nGet it on Nuntu http://nuntu.hehelabs.com/app/" + itemDetailsrrayList.get(position).getAppID());
                        }
                        shareIntent.setType("text/plain");
                        appcontext.startActivity(Intent.createChooser(shareIntent, "share " + itemDetailsrrayList.get(position).getAppName() + " using..."));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });


            holder.itemButton.setOnClickListener(new OnClickListener() {

                @SuppressWarnings("deprecation")
                public void onClick(View v) {
                    FragmentNuntuApp.hideOpenMenus();
                    if (broadCastReceiver == null) {
                        broadCastReceiver = new NuntuBroadcastReceiver();
                    }
                    registerBroadcastReceiver();
                    FocusedAppName = itemDetailsrrayList.get(position).getAppName();
                    FocusedAppID = itemDetailsrrayList.get(position).getAppID();
                    FocusedPackageName = itemDetailsrrayList.get(position).getAppPackageName();
                    String ClickAction = holder.itemButton.getText().toString().toLowerCase(Locale.ENGLISH).trim();
                    if ("installed".equalsIgnoreCase(ClickAction)) {
                        final Dialog dialog = new Dialog(appcontext);
                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        dialog.setContentView(R.layout.nuntu_custom_alertbox);
                        dialog.setCanceledOnTouchOutside(true);
                        TextView text = (TextView) dialog.findViewById(R.id.text);
                        text.setText(itemDetailsrrayList.get(position).getAppName().trim());
                        ImageView image = (ImageView) dialog.findViewById(R.id.image);
                        try {
                            image.setBackgroundDrawable(appcontext.getPackageManager().getApplicationIcon(itemDetailsrrayList.get(position).getAppPackageName()));
                        } catch (Exception e) {
                            image.setImageResource(R.drawable.app_logo);
                            e.printStackTrace();
                        }
                        Button Open = (Button) dialog.findViewById(R.id.dialogButtonNeg);
                        Open.setText("OPEN");
                        // if button is clicked, close the custom dialog
                        Open.setOnClickListener(new OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                                Intent LaunchIntent = appcontext.getPackageManager().getLaunchIntentForPackage(itemDetailsrrayList.get(position).getAppPackageName().trim());
                                appcontext.startActivity(LaunchIntent);

                            }
                        });

                        Button Delete = (Button) dialog.findViewById(R.id.dialogButtonNet);
                        Delete.setText("UNINSTALL");
                        // if button is clicked, close the custom dialog
                        Delete.setOnClickListener(new OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                                String packageName = "package:" + itemDetailsrrayList.get(position).getAppPackageName().trim();
                                Intent intent = new Intent(Intent.ACTION_DELETE);
                                intent.setData(Uri.parse(packageName));
                                appcontext.startActivity(intent);
                            }
                        });

                        Button Cancel = (Button) dialog.findViewById(R.id.dialogButtonPos);
                        Cancel.setText("CANCEL");
                        // if button is clicked, close the custom dialog
                        Cancel.setOnClickListener(new OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                            }
                        });

                        dialog.show();
                    } else if ("download".equalsIgnoreCase(ClickAction) || "buy".equalsIgnoreCase(ClickAction)) {
                        pullAppDownloadData(itemDetailsrrayList.get(position).getAppID());
                    } else {
                        AlertDialog.Builder builder = new AlertDialog.Builder(appcontext);
                        builder.setTitle("bad application");
                        builder.setMessage("There is something missing from this application prerequisites.\nDo you wish to report this app?");
                        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                Toast.makeText(appcontext, "Thank you! This feedback will help  publisher to improve " + itemDetailsrrayList.get(position).getAppName(), Toast.LENGTH_LONG).show();
                            }
                        });
                        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                // Do nothing
                                dialog.dismiss();
                            }
                        });
                        AlertDialog alert = builder.create();
                        alert.show();
                        //retrieving the button view in order to handle it.
                        Button negative_button = alert.getButton(DialogInterface.BUTTON_NEGATIVE);

                        Button neutral_button = alert.getButton(DialogInterface.BUTTON_NEUTRAL);

                        Button positive_button = alert.getButton(DialogInterface.BUTTON_POSITIVE);


                        if (neutral_button != null) {
                            neutral_button.setBackgroundDrawable(appcontext.getResources()
                                    .getDrawable(R.drawable.app_button_background));

                            neutral_button.setTextColor(appcontext.getResources()
                                    .getColor(android.R.color.white));
                        }
                        if (positive_button != null) {
                            positive_button.setBackgroundDrawable(appcontext.getResources()
                                    .getDrawable(R.drawable.app_button_background));

                            positive_button.setTextColor(appcontext.getResources()
                                    .getColor(android.R.color.white));
                        }

                        if (negative_button != null) {
                            negative_button.setBackgroundDrawable(appcontext.getResources()
                                    .getDrawable(R.drawable.app_button_background));

                            negative_button.setTextColor(appcontext.getResources()
                                    .getColor(android.R.color.white));
                        }

                    }
                }
            });
        } catch (Exception exc) {
            exc.printStackTrace();
            unregisterBroadcastReceiver();
        }

        holder.itemShare.setTransformationMethod(null);
        holder.itemTick.setTransformationMethod(null);
        holder.itemButton.setTransformationMethod(null);

        return convertView;
    }

    public void updateReceiptsList(ArrayList<App> newlist) {
        itemDetailsrrayList.clear();
        itemDetailsrrayList.addAll(newlist);
        this.notifyDataSetChanged();
    }

    @SuppressWarnings("deprecation")
    public void registerBroadcastReceiver() {
        try {

            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(Intent.ACTION_PACKAGE_ADDED);
            intentFilter.addAction(Intent.ACTION_PACKAGE_INSTALL);
            intentFilter.addAction(Intent.ACTION_PACKAGE_REMOVED);
            intentFilter.addAction(Intent.ACTION_PACKAGE_REPLACED);
            intentFilter.addDataScheme("package");
            intentFilter.setPriority(999);
            appcontext.registerReceiver(broadCastReceiver, intentFilter);
        } catch (Exception exc) {
            Toast.makeText(appcontext, "Exception caught: " + exc.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    public void unregisterBroadcastReceiver() {
        if (broadCastReceiver != null) {
            appcontext.unregisterReceiver(broadCastReceiver);
            Log.e("OK BROAD", " here is the broadcasting being unre");
        }
    }

    @SuppressWarnings("deprecation")
    private void pullAppDownloadData(String appID) {

        if (TelephonyInfo.hasConnection(appcontext)) {
            requestAppDonwload("/user/" + user + "/download/app/" + appID);
        } else {
            alertboxDownload.setIcon(R.drawable.nonet);
            alertboxDownload.setTitle("No internet");
            alertboxDownload.setMessage("Unable to download " + FocusedAppName + "" + "since there is no active connection with Nuntu");
            alertboxDownload.setNeutralButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface arg0, int arg1) {

                    arg0.dismiss();
                }
            });

            AlertDialog alert = alertboxDownload.create();
            alert.show();
            //retrieving the button view in order to handle it.
            Button negative_button = alert.getButton(DialogInterface.BUTTON_NEGATIVE);

            Button neutral_button = alert.getButton(DialogInterface.BUTTON_NEUTRAL);

            Button positive_button = alert.getButton(DialogInterface.BUTTON_POSITIVE);


            if (neutral_button != null) {
                neutral_button.setBackgroundDrawable(appcontext.getResources()
                        .getDrawable(R.drawable.app_button_background));

                neutral_button.setTextColor(appcontext.getResources()
                        .getColor(android.R.color.white));
            }
            if (positive_button != null) {
                positive_button.setBackgroundDrawable(appcontext.getResources()
                        .getDrawable(R.drawable.app_button_background));

                positive_button.setTextColor(appcontext.getResources()
                        .getColor(android.R.color.white));
            }

            if (negative_button != null) {
                negative_button.setBackgroundDrawable(appcontext.getResources()
                        .getDrawable(R.drawable.app_button_background));

                negative_button.setTextColor(appcontext.getResources()
                        .getColor(android.R.color.white));
            }
        }
    }


    public void requestAppDonwload(final String path) {
        AsyncTask<String, String, String> getTask = new AsyncTask<String, String, String>() {

            ProgressDialog progressDialog;
            String query = ""
                    ,
                    action
                    ,
                    DownloadLink;

            @Override
            protected void onPreExecute() {

                progressDialog = new ProgressDialog(appcontext);
                progressDialog.setMessage("Requesting details about " + FocusedAppName + "\nPlease wait...");
                progressDialog.setCancelable(false);
                progressDialog.setMax(60000);
                progressDialog.show();

            }

            @Override
            protected String doInBackground(String... params) {
                HttpClient httpclient = new DefaultHttpClient();
                // specify the URL you want to post to
                System.out.println(path);
                HttpPost httppost = new HttpPost(HOST + path);
                try {
                    httppost.setEntity(new StringEntity("request form nuntu android", "UTF8"));
                    httppost.addHeader("Content-type", "application/json");
                    httppost.addHeader("Accept", "application/json");
                    httppost.addHeader("User-Agent", "nuntumobile");
                    // send the variable and value, in other words post, to the URL
                    HttpResponse response = httpclient.execute(httppost);

                    HttpEntity entity = response.getEntity();
                    String Response = EntityUtils.toString(entity);
                    Log.d("Data Rec", Response);
                    try {
                        JSONObject ResponseJson = (JSONObject) new JSONTokener(Response).nextValue();
                        query = ResponseJson.getString("status");
                        details = ResponseJson.getString("details");
                        action = ResponseJson.getString("action").trim().toLowerCase(Locale.ENGLISH);
                        if ("success".equalsIgnoreCase(query)) {
                            appPermissions = new ArrayList<String>();
                            deviceList = new ArrayList<String>();
                            if ("pay".equalsIgnoreCase(action)) {
                                paymentMethods = new ArrayList<String>();
                                JSONObject Data = new JSONObject(ResponseJson.getString("data"));
                                OrderID = Data.getString("orderID");
                                OrderKey = Data.getString("orderKey");
                                JSONArray jsonPayments = new JSONArray(Data.getString("paymentMethod"));
                                System.out.print("payment array " + jsonPayments.toString());
                                for (int i = 0; i < jsonPayments.length(); i++) {
                                    JSONObject payEntry = jsonPayments.getJSONObject(i);
                                    System.out.print("payment entry obj " + payEntry.toString());
                                    System.out.println("payment type " + payEntry.getString("type"));
                                    String paymentType = payEntry.getString("type");
                                    paymentMethods.add(paymentType);
                                }

                                JSONObject Criteria = new JSONObject(ResponseJson.getString("criteria"));
                                JSONArray jsonArry = new JSONArray(Criteria.getString("assetFile"));
                                for (int i = 0; i < jsonArry.length(); i++) {
                                    appPermissions.add(jsonArry.getString(i));
                                }

                                JSONArray jsonDevices = new JSONArray(Criteria.getString("userDevice"));
                                for (int i = 0; i < jsonDevices.length(); i++) {
                                    JSONObject devEntry = jsonDevices.getJSONObject(i);
                                    deviceList.add(devEntry.getString("status"));
                                    deviceList.add(devEntry.getString("device"));
                                }
                            } else if ("prompt".equalsIgnoreCase(action)) {
                                DownloadLink = ResponseJson.getString("data");

                                JSONObject Criteria = new JSONObject(ResponseJson.getString("criteria"));
                                JSONArray jsonArry = new JSONArray(Criteria.getString("assetFile"));
                                for (int i = 0; i < jsonArry.length(); i++) {
                                    appPermissions.add(jsonArry.getString(i));
                                }

                                JSONArray jsonDevices = new JSONArray(Criteria.getString("userDevice"));
                                for (int i = 0; i < jsonDevices.length(); i++) {
                                    JSONObject devEntry = jsonDevices.getJSONObject(i);

                                    deviceList.add(devEntry.getString("status"));
                                    deviceList.add(devEntry.getString("device"));
                                }
                            }
                        } else {
                            Log.e("SHOOOOT", " couldn't get data");
                        }
                    } catch (Exception exc) {
                        Log.d("Exception in postData", "this detail " + exc.getMessage());

                    }

                } catch (ClientProtocolException e) {
                    Log.d("Exception in postData", "this detail " + e.getMessage());

                } catch (IOException e) {
                    Log.d("Exception in postData", "this detail " + e.getMessage());

                }

                return null;
            }


            @SuppressWarnings("deprecation")
            @Override
            protected void onPostExecute(String result) {
                super.onPostExecute(result);
                if (progressDialog != null) {
                    progressDialog.dismiss();
                }
                if ("success".equalsIgnoreCase(query)) {
                    promptAppPayBox(action, DownloadLink);
                } else if (details.length() >= 10) {
                    if (alertboxDownload != null) {
                        alertboxDownload.setTitle("There is a problem");
                        alertboxDownload.setMessage(details);
                        alertboxDownload.setNeutralButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface arg0, int arg1) {

                                arg0.dismiss();
                            }
                        });
                        AlertDialog alert = alertboxDownload.create();
                        alert.show();
                        //retrieving the button view in order to handle it.
                        Button negative_button = alert.getButton(DialogInterface.BUTTON_NEGATIVE);

                        Button neutral_button = alert.getButton(DialogInterface.BUTTON_NEUTRAL);

                        Button positive_button = alert.getButton(DialogInterface.BUTTON_POSITIVE);


                        if (neutral_button != null) {
                            neutral_button.setBackgroundDrawable(appcontext.getResources()
                                    .getDrawable(R.drawable.app_button_background));

                            neutral_button.setTextColor(appcontext.getResources()
                                    .getColor(android.R.color.white));
                        }
                        if (positive_button != null) {
                            positive_button.setBackgroundDrawable(appcontext.getResources()
                                    .getDrawable(R.drawable.app_button_background));

                            positive_button.setTextColor(appcontext.getResources()
                                    .getColor(android.R.color.white));
                        }

                        if (negative_button != null) {
                            negative_button.setBackgroundDrawable(appcontext.getResources()
                                    .getDrawable(R.drawable.app_button_background));

                            negative_button.setTextColor(appcontext.getResources()
                                    .getColor(android.R.color.white));
                        }
                    }
                }
            }

        };
        getTask.execute(path);
    }


    @SuppressWarnings("deprecation")
    private void promptAppPayBox(String action, final String downloadurl) {
        String appPerms = "";
        boolean isDeviceOK = false;
        final ArrayList<String> selectedPayment = new ArrayList<String>();

        final AlertDialog.Builder alert = new AlertDialog.Builder(appcontext);

        LinearLayout layout = new LinearLayout(appcontext);
        layout.setOrientation(LinearLayout.VERTICAL);


        final TextView serverBox = new TextView(appcontext);
        serverBox.setText(details + ":");
        serverBox.setTextColor(Color.WHITE);
        serverBox.setBackgroundColor(Color.argb(40, 0, 0, 0));
        layout.addView(serverBox);
        if ("pay".equalsIgnoreCase(action)) {
            final RadioGroup payButtons = new RadioGroup(appcontext);

            final RadioButton crespay = new RadioButton(appcontext);
            crespay.setText("Crespay");
            crespay.setTextColor(Color.WHITE);
            crespay.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    selectedPayment.clear();
                    selectedPayment.add("mmoney");
                }

            });

            final RadioButton nuntu = new RadioButton(appcontext);
            nuntu.setText("Nuntu");
            nuntu.setTextColor(Color.WHITE);
            nuntu.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    selectedPayment.clear();
                    selectedPayment.add("nuntumoney");
                }

            });


            final RadioButton paypal = new RadioButton(appcontext);
            paypal.setText("PayPal");
            paypal.setTextColor(Color.WHITE);
            paypal.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    selectedPayment.clear();
                    selectedPayment.add("paypal");
                }

            });

            final RadioButton BitCoin = new RadioButton(appcontext);
            BitCoin.setText("BitCoin");
            BitCoin.setTextColor(Color.WHITE);
            BitCoin.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    selectedPayment.clear();
                    selectedPayment.add("bitcoin");
                }

            });

            for (int p = 0; p < paymentMethods.size(); p++) {
                System.out.println("Pay method " + paymentMethods.get(p));
                if ("mmoney".equalsIgnoreCase(paymentMethods.get(p))) {
                    payButtons.addView(crespay);
                } else if ("nuntumoney".equalsIgnoreCase(paymentMethods.get(p))) {
                    payButtons.addView(nuntu);
                } else if ("paypal".equalsIgnoreCase(paymentMethods.get(p))) {
                    payButtons.addView(paypal);
                } else if ("bitcoin".equalsIgnoreCase(paymentMethods.get(p))) {
                    payButtons.addView(BitCoin);
                }
            }
            payButtons.setPadding(12, 12, 12, 12);
            payButtons.setBackgroundColor(Color.argb(40, 0, 0, 0));
            layout.addView(payButtons);
        } else {
            System.out.println("Pay MISSED");
        }

        final TextView permissionsHeader = new TextView(appcontext);
        permissionsHeader.setTextColor(Color.WHITE);
        permissionsHeader.setBackgroundColor(Color.argb(80, 0, 0, 0));
        layout.addView(permissionsHeader);


        final TextView permissions = new TextView(appcontext);

        for (int i = 0; i < appPermissions.size(); i++) {
            if (!appPermissions.get(i).contains("png") && !appPermissions.get(i).contains("--"))
                appPerms += "\t. " + appPermissions.get(i).replaceAll("[\n\r]", " ") + "\n";
        }
        permissions.setText(appPerms);
        permissions.setTextColor(Color.WHITE);
        permissions.setBackgroundColor(Color.argb(80, 0, 0, 0));
        layout.addView(permissions);

        final TextView deviceHeader = new TextView(appcontext);
        deviceHeader.setText("\t" + FocusedAppName.toUpperCase(Locale.ENGLISH) + " CAN RUN ON YOUR :");
        deviceHeader.setTextColor(Color.WHITE);
        deviceHeader.setBackgroundColor(Color.argb(80, 0, 0, 0));
        layout.addView(deviceHeader);

        for (int i = 0; i < deviceList.size() - 1; i++) {
            final TextView deviceText = new TextView(appcontext);
            String deviceName = "";
            if (deviceList.get(i).equalsIgnoreCase("success")) {
                isDeviceOK = true;
                deviceName = deviceList.get(i + 1).toString();
                deviceText.setText("\t" + deviceName + ":\tYES");
                deviceText.setTextColor(Color.argb(255, 20, 255, 40));
            } else if (deviceList.get(i).equalsIgnoreCase("failed")) {
                deviceName = deviceList.get(i + 1).toString();
                deviceText.setText("\t" + deviceName + ":\tNO");
                deviceText.setTextColor(Color.RED);
            }
            deviceText.setBackgroundColor(Color.argb(80, 0, 0, 0));
            layout.addView(deviceText);
        }

        alert.setIcon(R.drawable.logo_text_small);
        alert.setTitle("feels like home");
        alert.setView(layout);
        if ("pay".equalsIgnoreCase(action)) {
            alert.setPositiveButton("PAY", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int whichButton) {
                    if (selectedPayment.size() == 1) {
                        postPayment(selectedPayment.get(0));
                    } else {
                        Toast.makeText(appcontext, "Please choose at least one payment method.", Toast.LENGTH_LONG).show();
                    }


                    dialog.dismiss();
                }
            });

            alert.setNeutralButton("CLOSE", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int whichButton) {
                    dialog.dismiss();
                }
            });
            AlertDialog alertCustom = alert.create();
            alertCustom.show();
            //retrieving the button view in order to handle it.
            Button negative_button = alertCustom.getButton(DialogInterface.BUTTON_NEGATIVE);

            Button neutral_button = alertCustom.getButton(DialogInterface.BUTTON_NEUTRAL);

            Button positive_button = alertCustom.getButton(DialogInterface.BUTTON_POSITIVE);

            if (neutral_button != null) {
                neutral_button.setBackgroundDrawable(appcontext.getResources()
                        .getDrawable(R.drawable.app_button_background));

                neutral_button.setTextColor(appcontext.getResources()
                        .getColor(android.R.color.white));
            }
            if (positive_button != null) {
                positive_button.setBackgroundDrawable(appcontext.getResources()
                        .getDrawable(R.drawable.app_button_background));

                positive_button.setTextColor(appcontext.getResources()
                        .getColor(android.R.color.white));
            }

            if (negative_button != null) {
                negative_button.setBackgroundDrawable(appcontext.getResources()
                        .getDrawable(R.drawable.app_button_background));

                negative_button.setTextColor(appcontext.getResources()
                        .getColor(android.R.color.white));
            }

        } else if ("prompt".equalsIgnoreCase(action)) {
            if (isDeviceOK && downloadurl != null) {
                alert.setPositiveButton("DOWNLOAD", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int whichButton) {

                        Intent downloadApp = new Intent(appcontext, NuntuTransactDaemon.class);
                        if (downloadApp != null) {
                            downloadApp.putExtra("action", "download");
                            downloadApp.putExtra("type", "app");
                            downloadApp.putExtra("appID", FocusedAppID);
                            downloadApp.putExtra("appName", FocusedAppName);
                            downloadApp.putExtra("packagename", FocusedPackageName);
                            downloadApp.putExtra("link", downloadurl);
                            appcontext.startService(downloadApp);
                            Toast.makeText(appcontext, FocusedAppName + " is download is requested. check on it in notification bar.", Toast.LENGTH_LONG).show();
                        }

                        dialog.dismiss();

                    }
                });
            }
            alert.setNeutralButton("CLOSE", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int whichButton) {
                    dialog.dismiss();
                }
            });
            AlertDialog alertCustom = alert.create();
            alertCustom.show();
            //retrieving the button view in order to handle it.
            Button negative_button = alertCustom.getButton(DialogInterface.BUTTON_NEGATIVE);

            Button neutral_button = alertCustom.getButton(DialogInterface.BUTTON_NEUTRAL);

            Button positive_button = alertCustom.getButton(DialogInterface.BUTTON_POSITIVE);


            if (neutral_button != null) {
                neutral_button.setBackgroundDrawable(appcontext.getResources()
                        .getDrawable(R.drawable.app_button_background));

                neutral_button.setTextColor(appcontext.getResources()
                        .getColor(android.R.color.white));
            }
            if (positive_button != null) {
                positive_button.setBackgroundDrawable(appcontext.getResources()
                        .getDrawable(R.drawable.app_button_background));

                positive_button.setTextColor(appcontext.getResources()
                        .getColor(android.R.color.white));
            }

            if (negative_button != null) {
                negative_button.setBackgroundDrawable(appcontext.getResources()
                        .getDrawable(R.drawable.app_button_background));

                negative_button.setTextColor(appcontext.getResources()
                        .getColor(android.R.color.white));
            }
        }
    }


    private void postPayment(String payment) {

        if (payment.length() > 3) {
            String url = "/user/" + user + "/payment/" + payment + "/" + OrderID + "/" + OrderKey;
            requestPayment(payment, url);

        }

    }


    public void requestPayment(final String method, final String path) {
        AsyncTask<String, String, String> getTask = new AsyncTask<String, String, String>() {

            ProgressDialog progressPayment;
            String query
                    ,
                    DownloadUrl;

            @Override
            protected void onPreExecute() {

                progressPayment = new ProgressDialog(appcontext);
                progressPayment.setMessage("Opening payment. Please wait.");
                progressPayment.setCancelable(false);
                progressPayment.setMax(120000);
                progressPayment.show();

            }

            @Override
            protected String doInBackground(String... params) {
                HttpClient httpclient = new DefaultHttpClient();
                // specify the URL you want to post to
                System.out.println(path);
                HttpPost httppost = new HttpPost(HOST + path);
                try {
                    httppost.setEntity(new StringEntity("request form nuntu android", "UTF8"));
                    httppost.addHeader("Content-type", "application/json");
                    httppost.addHeader("Accept", "application/json");
                    httppost.addHeader("User-Agent", "nuntumobile");
                    // send the variable and value, in other words post, to the URL
                    HttpResponse response = httpclient.execute(httppost);

                    HttpEntity entity = response.getEntity();
                    String Response = EntityUtils.toString(entity);
                    Log.d("Data Rec", Response);
                    try {
                        JSONObject ResponseJson = (JSONObject) new JSONTokener(Response).nextValue();
                        query = ResponseJson.getString("status");
                        if ("success".equalsIgnoreCase(query)) {
                            if ("mmoney".equalsIgnoreCase(method)) {
                                crespayUrl = ResponseJson.getString("url");
                            } else if ("nuntumoney".equalsIgnoreCase(method)) {
                                details = ResponseJson.getString("message");
                                DownloadUrl = ResponseJson.getString("url");
                            } else if ("paypal".equalsIgnoreCase(method)) {
                                details = ResponseJson.getString("message");
                            } else if ("bitcoin".equalsIgnoreCase(method)) {
                                details = ResponseJson.getString("message");
                            }
                        } else if ("failed".equalsIgnoreCase(query)) {
                            details = ResponseJson.getString("message");
                        } else {
                            details = "server could not send valid response";
                        }

                    } catch (Exception exc) {
                        details = "An error occurred while requesting payment:\n " + exc.getMessage();
                        Log.d("Exception in postData", "this detail " + exc.getMessage());

                    }

                } catch (ClientProtocolException e) {
                    details = "An error occurred while requesting payment:\n " + e.getMessage();
                    Log.d("Exception in postData", "this detail " + e.getMessage());

                } catch (IOException e) {
                    details = "An error occurred while requesting payment:\n " + e.getMessage();
                    Log.d("Exception in postData", "this detail " + e.getMessage());

                }

                return null;
            }


            @SuppressWarnings("deprecation")
            @Override
            protected void onPostExecute(String result) {
                super.onPostExecute(result);
                if (progressPayment != null) {
                    progressPayment.dismiss();
                }

                if ("failed".equalsIgnoreCase(query)) {

                    final AlertDialog.Builder alertbox = new AlertDialog.Builder(appcontext);
                    alertbox.setTitle("payment failed");
                    alertbox.setMessage("Nuntu could not process this payment: \n" + details);
                    alertbox.setNeutralButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface arg0, int arg1) {

                            arg0.dismiss();
                        }
                    });
                    AlertDialog alert = alertbox.create();
                    alert.show();
                    Button neutral_button = alert.getButton(DialogInterface.BUTTON_NEUTRAL);

                    if (neutral_button != null) {
                        neutral_button.setBackgroundDrawable(appcontext.getResources()
                                .getDrawable(R.drawable.app_button_background));

                        neutral_button.setTextColor(appcontext.getResources()
                                .getColor(android.R.color.white));
                    }

                } else if ("success".equalsIgnoreCase(query)) {
                    if ("mmoney".equalsIgnoreCase(method)) {
                        popCresPayForm(crespayUrl);
                    } else if ("nuntumoney".equalsIgnoreCase(method)) {
                        final AlertDialog.Builder alertbox = new AlertDialog.Builder(appcontext);
                        alertbox.setTitle("payment was successful");
                        alertbox.setMessage(details);
                        alertbox.setNeutralButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface arg0, int arg1) {
                                arg0.dismiss();
                            }
                        });

                        alertbox.setPositiveButton("START DOWNLOAD", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface arg0, int arg1) {
                                arg0.dismiss();
                                Intent downloadApp = new Intent(appcontext, NuntuTransactDaemon.class);
                                if (downloadApp != null) {
                                    downloadApp.putExtra("action", "download");
                                    downloadApp.putExtra("type", "app");
                                    downloadApp.putExtra("appID", FocusedAppID);
                                    downloadApp.putExtra("appName", FocusedAppName);
                                    downloadApp.putExtra("packagename", FocusedPackageName);
                                    downloadApp.putExtra("link", DownloadUrl);
                                    appcontext.startService(downloadApp);
                                    Toast.makeText(appcontext, FocusedAppName + " is download is requested. check on it in notification bar.", Toast.LENGTH_LONG).show();
                                }
                            }
                        });
                        AlertDialog alert = alertbox.create();
                        alert.show();
                        Button neutral_button = alert.getButton(DialogInterface.BUTTON_NEUTRAL);

                        Button positive_button = alert.getButton(DialogInterface.BUTTON_POSITIVE);


                        if (neutral_button != null) {
                            neutral_button.setBackgroundDrawable(appcontext.getResources()
                                    .getDrawable(R.drawable.app_button_background));

                            neutral_button.setTextColor(appcontext.getResources()
                                    .getColor(android.R.color.white));
                        }
                        if (positive_button != null) {
                            positive_button.setBackgroundDrawable(appcontext.getResources()
                                    .getDrawable(R.drawable.app_button_background));

                            positive_button.setTextColor(appcontext.getResources()
                                    .getColor(android.R.color.white));
                        }
                    } else {
                        final AlertDialog.Builder alertbox = new AlertDialog.Builder(appcontext);
                        alertbox.setTitle("payment failed");
                        alertbox.setMessage("Paymeny method not supported by Nuntu");
                        alertbox.setNeutralButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface arg0, int arg1) {

                                arg0.dismiss();
                            }
                        });
                        AlertDialog alert = alertbox.create();
                        alert.show();
                        Button neutral_button = alert.getButton(DialogInterface.BUTTON_NEUTRAL);

                        if (neutral_button != null) {
                            neutral_button.setBackgroundDrawable(appcontext.getResources()
                                    .getDrawable(R.drawable.app_button_background));

                            neutral_button.setTextColor(appcontext.getResources()
                                    .getColor(android.R.color.white));
                        }
                    }
                } else {
                    final AlertDialog.Builder alertbox = new AlertDialog.Builder(appcontext);
                    alertbox.setTitle("payment failed");
                    alertbox.setMessage("Connection Error!");
                    alertbox.setNeutralButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface arg0, int arg1) {

                            arg0.dismiss();
                        }
                    });
                    AlertDialog alert = alertbox.create();
                    alert.show();
                    Button neutral_button = alert.getButton(DialogInterface.BUTTON_NEUTRAL);

                    if (neutral_button != null) {
                        neutral_button.setBackgroundDrawable(appcontext.getResources()
                                .getDrawable(R.drawable.app_button_background));

                        neutral_button.setTextColor(appcontext.getResources()
                                .getColor(android.R.color.white));
                    }
                }


            }

        };
        getTask.execute(path);
    }


    @SuppressWarnings("deprecation")
    @SuppressLint("SetJavaScriptEnabled")
    private void popCresPayForm(final String url) {

        final AlertDialog.Builder CresPopUp = new AlertDialog.Builder(appcontext);
        final ProgressDialog prDialog = new ProgressDialog(appcontext);
        prDialog.setMessage("Loading Crespay payment data. Please wait.");
        prDialog.setCancelable(false);
        prDialog.setMax(120000);

        LinearLayout CresPopUplayout = new LinearLayout(appcontext);
        CresPopUplayout.setOrientation(LinearLayout.VERTICAL);

        final TextView adviceHeader = new TextView(appcontext);
        adviceHeader.setText("In case you encounter soft keyboard troubles. Click 'OPEN WITH BROWSER' to enter payment data from your browser.");
        adviceHeader.setTextColor(Color.WHITE);
        adviceHeader.setBackgroundColor(Color.argb(80, 0, 0, 0));
        CresPopUplayout.addView(adviceHeader);

        final WebView crespayform = new WebView(appcontext);

        crespayform.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                    case MotionEvent.ACTION_UP:
                        if (!v.hasFocus()) {
                            v.requestFocus();
                        }
                        break;
                }
                return false;
            }
        });
        crespayform.setFocusableInTouchMode(true);
        crespayform.setFocusable(true);
        crespayform.setHapticFeedbackEnabled(true);
        crespayform.setClickable(true);
        crespayform.getSettings().setJavaScriptEnabled(true);
        crespayform.getSettings().setLoadWithOverviewMode(true);
        crespayform.getSettings().setUseWideViewPort(true);
        crespayform.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
                if (progress == 100) {
                    prDialog.dismiss();
                } else {
                    prDialog.show();
                }
            }
        });

        crespayform.loadUrl(url);

        CresPopUplayout.addView(crespayform);
        CresPopUp.setIcon(R.drawable.logo_text_small);
        CresPopUp.setTitle("Pay with Crespay");
        CresPopUp.setView(CresPopUplayout);
        CresPopUp.setNeutralButton("DONE", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int whichButton) {

                dialog.dismiss();
            }
        });

        CresPopUp.setPositiveButton("OPEN WITH BROWSER", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int whichButton) {
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                appcontext.startActivity(i);
                dialog.dismiss();
            }
        });
        AlertDialog alert = CresPopUp.create();
        alert.show();
        //retrieving the button view in order to handle it.
        Button negative_button = alert.getButton(DialogInterface.BUTTON_NEGATIVE);

        Button neutral_button = alert.getButton(DialogInterface.BUTTON_NEUTRAL);

        Button positive_button = alert.getButton(DialogInterface.BUTTON_POSITIVE);


        if (neutral_button != null) {
            neutral_button.setBackgroundDrawable(appcontext.getResources()
                    .getDrawable(R.drawable.app_button_background));

            neutral_button.setTextColor(appcontext.getResources()
                    .getColor(android.R.color.white));
        }
        if (positive_button != null) {
            positive_button.setBackgroundDrawable(appcontext.getResources()
                    .getDrawable(R.drawable.app_button_background));

            positive_button.setTextColor(appcontext.getResources()
                    .getColor(android.R.color.white));
        }

        if (negative_button != null) {
            negative_button.setBackgroundDrawable(appcontext.getResources()
                    .getDrawable(R.drawable.app_button_background));

            negative_button.setTextColor(appcontext.getResources()
                    .getColor(android.R.color.white));
        }
    }

    public void sendTick(String appID) {
        AsyncTask<String, String, String> getTask = new AsyncTask<String, String, String>() {
            String ServerResStatus = " "
                    ,
                    ServerRes = " ";

            @Override
            protected String doInBackground(String... params) {
                String response = "";
                try {
                    URL url = new URL(params[0]);
                    HttpURLConnection urlConnection = (HttpURLConnection)
                            url.openConnection();
                    BufferedReader
                            reader = new
                            BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                    String line = "";
                    while ((line = reader.readLine()) != null) {
                        response += line + "\n";
                    }
                    if (!urlConnection.getContentType().toLowerCase(Locale.ENGLISH).equals("application/json")) {
                        Log.e("Bad Content", "Received bad content type while expecting sjon");
                    }

                    try {
                        JSONObject obj = new JSONObject(response.toString());
                        ServerResStatus = obj.getString("status");
                        ServerRes = obj.getString("details");

                    } catch (Exception exc) {

                    }

                } catch (Exception e) {
                    Log.e("EXC", "An eception occured while getting data" + e.getMessage());
                }
                return response;


            }


            @Override
            protected void onPostExecute(String result) {
                if (ServerResStatus.equalsIgnoreCase("success") || ServerRes.equalsIgnoreCase("failed")) {
                    Toast.makeText(appcontext, ServerRes, Toast.LENGTH_SHORT).show();
                }
            }

        };
        getTask.execute(HOST + "/tick/app/" + appID);
    }

    static class ViewHolder {
        TextView txt_itemName;
        TextView txt_itemPublisher;
        TextView txt_itemPrice;
        ImageView itemImage;
        Button itemButton;
        Button itemShare;
        Button itemTick;
        CardView buttons;
        ImageView ic_app_menu;
    }

}