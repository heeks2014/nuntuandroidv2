package utils.hehe.nuntu;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.CardView;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import com.squareup.picasso.Picasso;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by snickwizard on 2/10/15.
 */
public class FragmentNuntuHome extends Fragment {


    private static final String DEBUG = "HOME FRAG";
    public static String HOME_LOAD_MODE = "init";
    private static SwipeRefreshLayout HomeSwipeLayout;
    private static GridView HomeGridView;
    private static HolderHomeBaseAdapter HomeItemsAdapter;
    private static RelativeLayout LoginParent;
    private static ImageView Facebook, AppBanner, BookBanner, MusicBanner;
    private static ViewFlipper Switcher;
    private static Button signinButton;
    private static Button btnSignUp;
    private static EditText LogInPass, LogInUserName;
    private static TextView LogInpassReset;
    private static ArrayList<HomeItem> HomeItems;
    private static boolean isSignInReady = false;
    private static Context HomeContext;
    protected static String CurrentUserEmail, CurrentUserPass;
    private static Activity parentActivity;
    private static ProgressDialog progressDialog;
    private static AsyncTask<String, Integer, Double> LoginTask;
    private static String[] BannerImageURLs = {"http://nuntu.hehelabs.com/data/defaults/user/apps.jpg",
            "http://nuntu.hehelabs.com/data/defaults/user/books.jpg",
            "http://nuntu.hehelabs.com/data/defaults/user/music.jpg",};

    public static FragmentNuntuHome getInstance(int position) {
        FragmentNuntuHome frag = new FragmentNuntuHome();
        Bundle args = new Bundle();
        args.putInt("position", position);
        args.putString("type", "app");
        frag.setArguments(args);
        Log.e(DEBUG, "in constructor with id " + position);
        return frag;

    }


    public static void popLoginScreen() {
        try {

            HomeSwipeLayout.setEnabled(false);
            LoginParent.setVisibility(View.VISIBLE);
            HomeSwipeLayout.setVisibility(View.GONE);
            Switcher.setVisibility(View.GONE);


            LogInpassReset.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View arg0) {
                    promptPassReset(parentActivity);
                }
            });

            signinButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    toggleSignInFields("textfiled");
                    if (!isSignInReady) {
                        if (!TelephonyInfo.hasConnection(HomeContext)) {
                            Toast.makeText(HomeContext, "No internet", Toast.LENGTH_LONG).show();
                        } else {
                            CurrentUserEmail = LogInUserName.getText().toString().trim();
                            CurrentUserPass = LogInPass.getText().toString().trim();
                            if (CurrentUserEmail.length() >= 3 && CurrentUserEmail.length() <= 32 && CurrentUserPass.length() >= 8 && CurrentUserPass.length() <= 32) {
                                JSONObject object = new JSONObject();
                                try {
                                    NuntuUtils.toggleDefaultKeyboard(HomeContext, "hide");
                                    object.put("uname", CurrentUserEmail);
                                    object.put("password", CurrentUserPass);
                                    String TxData = object.toString();
                                    LoginTask = new NuntuLoginAsync(parentActivity, "load_profile", true);
                                    LoginTask.execute(NuntuConstants.HOST.toString() + "/user/login", TxData, "focused", CurrentUserPass);
                                    isSignInReady = false;
                                    LogInpassReset.clearAnimation();
                                    LogInPass.clearAnimation();
                                    LogInUserName.clearAnimation();

                                    LogInpassReset.setVisibility(View.GONE);
                                    LogInPass.setVisibility(View.GONE);
                                    LogInUserName.setVisibility(View.GONE);

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                Toast.makeText(HomeContext, "Provide valid username and password.", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                }
            });
            btnSignUp.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    if (!TelephonyInfo.hasConnection(HomeContext)) {

                        Toast.makeText(HomeContext, "No internet", Toast.LENGTH_LONG).show();

                    } else {
                        isSignInReady = false;
                        LogInpassReset.clearAnimation();
                        LogInPass.clearAnimation();
                        LogInUserName.clearAnimation();

                        LoginParent.setVisibility(View.GONE);

                        Intent signUpPageRe = new Intent(HomeContext, SignUpActivity.class);
                        signUpPageRe.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        HomeContext.startActivity(signUpPageRe);

                    }
                }
            });

            LogInUserName.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                    if (NuntuUtils.isValidEmail(charSequence)) {
                        LogInUserName.setTextColor(Color.argb(255, 255, 165, 0));
                        CurrentUserEmail = LogInUserName.getText().toString();


                    } else {
                        LogInUserName.setTextColor(Color.argb(200, 125, 125, 125));
                    }
                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {

                    if (NuntuUtils.isValidEmail(charSequence)) {
                        LogInUserName.setTextColor(Color.argb(255, 255, 165, 0));
                        CurrentUserEmail = LogInUserName.getText().toString();


                    } else {
                        LogInUserName.setTextColor(Color.argb(200, 255, 0, 0));
                    }

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    if (NuntuUtils.isValidEmail(CurrentUserEmail)) {
                        LogInUserName.setTextColor(Color.argb(255, 255, 165, 0));
                        CurrentUserEmail = LogInUserName.getText().toString();
                    } else {
                        LogInUserName.setTextColor(Color.argb(200, 255, 0, 0));
                    }
                }
            });

            LogInUserName.setOnKeyListener(new View.OnKeyListener() {
                public boolean onKey(View v, int keyCode, KeyEvent event) {
                    // If the event is a key-down event on the "enter" button
                    if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                        CurrentUserEmail = LogInUserName.getText().toString();
                        if (!NuntuUtils.isValidEmail(CurrentUserEmail)) {
                            Toast.makeText(HomeContext, "Not sure if " + CurrentUserEmail + " is valid email", Toast.LENGTH_LONG).show();
                        }
                        return true;
                    }
                    return false;
                }
            });

            Facebook.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (TelephonyInfo.hasConnection(HomeContext)) {
                        LoginParent.setVisibility(View.GONE);
                        Intent FaceBookLoginIntent = new Intent(HomeContext, UserFacebookActivity.class);
                        FaceBookLoginIntent.putExtra("mode", "new");
                        FaceBookLoginIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        parentActivity.startActivityForResult(FaceBookLoginIntent, 1);
                    } else {
                        Toast.makeText(HomeContext, "No internet", Toast.LENGTH_LONG).show();
                    }
                }
            });

            final CharSequence suggestEmail = NuntuUtils.getUserData(parentActivity, "email");
            if (suggestEmail != null && suggestEmail.length() >= 5) {
                LogInUserName.setText(suggestEmail);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void toggleSignInFields(String mode) {


        if (!isSignInReady) {
            isSignInReady = true;
            Animation anim = AnimationUtils.loadAnimation(HomeContext, R.anim.show_signin_text);
            LogInpassReset.setVisibility(View.VISIBLE);
            LogInpassReset.startAnimation(anim);
            LogInPass.setVisibility(View.VISIBLE);
            LogInPass.startAnimation(anim);
            LogInUserName.setVisibility(View.VISIBLE);
            LogInUserName.startAnimation(anim);
        } else {
            isSignInReady = false;
            Animation anim = AnimationUtils.loadAnimation(HomeContext, R.anim.hide_signin_text);
            LogInpassReset.startAnimation(anim);
            LogInpassReset.setVisibility(View.GONE);
            LogInPass.startAnimation(anim);
            LogInPass.setVisibility(View.GONE);
            LogInUserName.startAnimation(anim);
            LogInUserName.setVisibility(View.GONE);
        }

    }

    private static void promptPassReset(Activity a) {

        final AlertDialog.Builder alert = new AlertDialog.Builder(a);
        LinearLayout layout = new LinearLayout(HomeContext);
        layout.setOrientation(LinearLayout.VERTICAL);
        final EditText emailBox = new EditText(HomeContext);
        emailBox.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
        emailBox.setHint(" your email ");
        emailBox.setGravity(Gravity.CENTER);
        emailBox.setBackgroundColor(Color.argb(200, 200, 200, 200));
        emailBox.setTextColor(Color.argb(255, 255, 165, 0));
        layout.addView(emailBox);
        alert.setView(layout);
        alert.setPositiveButton("RESET", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int whichButton) {
                String email = emailBox.getText().toString().trim();
                if (NuntuUtils.isValidEmail(email)) {
                    if (TelephonyInfo.hasConnection(HomeContext)) {
                        NuntuUtils.toggleDefaultKeyboard(HomeContext, "hide");
                        resetUserPassword(NuntuConstants.HOST.toString() + "/user/reset/password/" + email);
                    } else {
                        Toast.makeText(HomeContext, "Oops... no internet", Toast.LENGTH_LONG).show();
                    }

                } else {
                    Toast.makeText(HomeContext, "Was that your email ?", Toast.LENGTH_LONG).show();
                }

            }
        });

        alert.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int whichButton) {
                NuntuUtils.toggleDefaultKeyboard(HomeContext, "hide");
                dialog.dismiss();
            }
        });
        AlertDialog alert_box = alert.create();
        alert_box.show();
        //retrieving the button view in order to handle it.
        Button negative_button = alert_box.getButton(DialogInterface.BUTTON_NEGATIVE);

        Button positive_button = alert_box.getButton(DialogInterface.BUTTON_POSITIVE);

        if (positive_button != null) {
            positive_button.setBackgroundDrawable(HomeContext.getResources()
                    .getDrawable(R.drawable.app_button_background));

            positive_button.setTextColor(HomeContext.getResources()
                    .getColor(android.R.color.white));
        }

        if (negative_button != null) {
            negative_button.setBackgroundDrawable(HomeContext.getResources()
                    .getDrawable(R.drawable.app_button_background));

            negative_button.setTextColor(HomeContext.getResources()
                    .getColor(android.R.color.white));
        }

    }

    public static void resetUserPassword(String path) {
        AsyncTask<String, String, String> getTask = new AsyncTask<String, String, String>() {
            String resetQuery = "failed",
                    ResetDetails = "Error while resetting password",
                    Response;
            int httpCode = 0;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();

                if (progressDialog == null) {
                    progressDialog = new ProgressDialog(parentActivity);
                }
                progressDialog.setMessage("\t\t\t\t wait... ");
                progressDialog.setCancelable(false);
                progressDialog.show();
                NuntuUtils.toggleDefaultKeyboard(HomeContext, "hide");
            }

            @Override
            protected String doInBackground(String... params) {

                try {
                    HttpPost httppost = new HttpPost(params[0]);
                    HttpClient httpclient = NuntuUtils.getSecureHttpClient();
                    httppost.setEntity(new StringEntity("pass reset", "UTF8"));
                    httppost.addHeader("Content-type", "application/json");
                    httppost.addHeader("Accept", "application/json");
                    httppost.addHeader("User-Agent", "nuntumobile");
                    // send the variable and value, in other words post, to the URL
                    HttpResponse response = httpclient.execute(httppost);
                    httpCode = response.getStatusLine().getStatusCode();
                    HttpEntity entity = response.getEntity();
                    Response = EntityUtils.toString(entity);

                    try {
                        if (500 != httpCode) {
                            JSONObject obj = new JSONObject(Response);
                            resetQuery = obj.getString("status");
                            ResetDetails = obj.getString("details");
                        }
                    } catch (JSONException exc) {
                        Log.d("EXCEPTION ", "something bad to JSOn happen " + exc.getMessage() + "..." + response);
                    }

                } catch (Exception e) {
                    Log.e("EXC", "An exception occurred while getting data " + e.getMessage());
                }
                return Response;


            }

            @SuppressWarnings("deprecation")
            @Override
            protected void onPostExecute(String result) {
                if (progressDialog != null) {
                    progressDialog.dismiss();
                }
                Toast toast = Toast.makeText(HomeContext, ResetDetails, Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }

        };
        getTask.execute(path);
    }

    // Store instance variables based on arguments passed
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        HomeContext = getActivity().getApplicationContext();
        parentActivity = getActivity();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        try {
            final View layout = inflater.inflate(R.layout.fragment_home_list_layout, container, false);
            Switcher = (ViewFlipper) layout.findViewById(R.id.view_flipper);
            HomeSwipeLayout = (SwipeRefreshLayout) layout.findViewById(R.id.home_content_container);
            HomeGridView = (GridView) layout.findViewById(R.id.home_grid_view);

            if (Build.VERSION.SDK_INT >= 14) {
                HomeSwipeLayout.setColorScheme(android.R.color.holo_blue_bright,
                        android.R.color.holo_green_light,
                        android.R.color.holo_orange_light,
                        android.R.color.holo_red_light);
            }

            int width = Device.getDeviceScreenSize("width", HomeContext, "dpi");
            if (width < 240) {
                HomeGridView.setNumColumns(1);
            } else if (width >= 240 && width < 600) {
                HomeGridView.setNumColumns(2);
            } else if (width >= 600 && width <= 720) {
                HomeGridView.setNumColumns(3);
            } else {
                HomeGridView.setNumColumns(4);
            }
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            layoutParams.gravity = Gravity.CENTER_HORIZONTAL;

            AppBanner = new ImageView(parentActivity);
            AppBanner.setLayoutParams(layoutParams);
            AppBanner.setScaleType(ImageView.ScaleType.CENTER_CROP);
            AppBanner.setAdjustViewBounds(true);
            AppBanner.setImageResource(R.drawable.placeholder_apps);
            Switcher.addView(AppBanner);

            BookBanner = new ImageView(parentActivity);
            BookBanner.setLayoutParams(layoutParams);
            BookBanner.setScaleType(ImageView.ScaleType.CENTER_CROP);
            BookBanner.setAdjustViewBounds(true);
            BookBanner.setImageResource(R.drawable.placeholder_book);
            Switcher.addView(BookBanner);

            MusicBanner = new ImageView(parentActivity);
            MusicBanner.setLayoutParams(layoutParams);
            MusicBanner.setScaleType(ImageView.ScaleType.CENTER_CROP);
            MusicBanner.setAdjustViewBounds(true);
            MusicBanner.setImageResource(R.drawable.placeholder_music);
            Switcher.addView(MusicBanner);

            Switcher.setAutoStart(true);
            Switcher.setFlipInterval(16000);

            Switcher.setInAnimation(parentActivity, R.anim.fade_in);
            Switcher.setOutAnimation(parentActivity, R.anim.fade_out);
            HomeSwipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {

                    promptLogin("refresh");
                }
            });

            LoginParent = (RelativeLayout) layout.findViewById(R.id.LoginParent);
            Facebook = (ImageView) layout.findViewById(R.id.FaceBook);
            signinButton = (Button) layout.findViewById(R.id.btnSingIn);
            btnSignUp = (Button) layout.findViewById(R.id.btnSignUp);

            LogInUserName = (EditText) layout.findViewById(R.id.etUserName);
            LogInPass = (EditText) layout.findViewById(R.id.etPass);
            LogInpassReset = (TextView) layout.findViewById(R.id.ResetPass);

            return layout;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } catch (OutOfMemoryError e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        AppBanner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideOpenMenus();
            }
        });

        BookBanner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideOpenMenus();
            }
        });

        MusicBanner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideOpenMenus();
            }
        });

        HomeSwipeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideOpenMenus();
            }
        });

        promptLogin(HOME_LOAD_MODE);
    }


    private void promptLogin(final String loadMode) {
        try {

            String UserVaultData = User.getNuntuLocalLogin(HomeContext);
            if (UserVaultData == null || UserVaultData.length() < 10) {
                popLoginScreen();
                Log.e("HOME onViewCreated ", "Please log in");
            } else {
                Log.e("HOME onViewCreated ", "pulling custom profile data");
                HomeSwipeLayout.setEnabled(true);
                JSONObject Local = new JSONObject(UserVaultData);
                if (NuntuUtils.isValidEmail(Local.getString("email").trim()) && Local.getString("password") != null && Local.getString("password").length() >= 8) {
                    try {
                        if ("init".equalsIgnoreCase(HOME_LOAD_MODE)) {
                            JSONObject object = new JSONObject();
                            object.put("uname", Local.getString("email"));
                            object.put("password", Local.getString("password"));
                            String TxData = object.toString();
                            LoginTask = new NuntuLoginAsync(parentActivity, "load_profile", false);
                            LoginTask.execute(NuntuConstants.HOST.toString() + "/user/login", TxData, "unfocused", null);
                        } else {
                            loadUserCustomItemAsync(Local.getString("userid"), loadMode);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    User.logout(HomeContext);
                    popLoginScreen();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("HOME onViewCreated ", "exception " + e.getMessage());
            popLoginScreen();
        }
    }


    public static void loadUserCustomItemAsync(final String UserID, final String LoadMode) {

        AsyncTask<String, String, String> getTask = new AsyncTask<String, String, String>() {
            String response = "",
                    Query = "failed",
                    msg = "Nuntu couldn't retrieve your profile";


            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                parentActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        LoginParent.setVisibility(View.GONE);
                        HomeSwipeLayout.setVisibility(View.VISIBLE);
                        Switcher.setVisibility(View.VISIBLE);

                        Picasso.with(parentActivity)
                                .load(BannerImageURLs[0])
                                .error(R.drawable.placeholder_apps)
                                .placeholder(R.drawable.placeholder_apps)
                                .into(AppBanner);

                        Picasso.with(parentActivity)
                                .load(BannerImageURLs[1])
                                .error(R.drawable.placeholder_apps)
                                .placeholder(R.drawable.placeholder_apps)
                                .into(BookBanner);

                        Picasso.with(parentActivity)
                                .load(BannerImageURLs[2])
                                .error(R.drawable.placeholder_apps)
                                .placeholder(R.drawable.placeholder_apps)
                                .into(MusicBanner);


                        if (!HomeSwipeLayout.isRefreshing()) {
                            HomeSwipeLayout.setProgressViewOffset(false, 0, (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 24, parentActivity.getResources().getDisplayMetrics()));
                            HomeSwipeLayout.setRefreshing(true);


                        }
                    }
                });
            }

            @Override
            protected String doInBackground(String... params) {

                try {
                    if ("init_finished".equalsIgnoreCase(LoadMode)) {
                        return "ok";
                    }

                    URL url = new URL(params[0]);
                    HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                    urlConnection.addRequestProperty("User-Agent", "mobileuser");
                    BufferedReader reader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                    String line;
                    while ((line = reader.readLine()) != null) {
                        response += line + "\n";
                    }
                    if (!urlConnection.getContentType().toLowerCase(Locale.ENGLISH).equals("application/json")) {
                        Log.e("Bad Content", "Received bad content type while expecting json");
                    }
                    Log.e("RESP", response + "\n FROM \n" + url.getPath());
                    JSONObject obj = new JSONObject(response);
                    Query = obj.getString("status");

                    if ("success".equalsIgnoreCase(Query)) {
                        if (HomeItems != null) {
                            HomeItems.clear();
                        } else {
                            HomeItems = new ArrayList<>();
                        }

                        JSONObject Data = new JSONObject(obj.getString("data"));
                        JSONArray Apps = new JSONArray(Data.getString("apps"));
                        JSONArray Books = new JSONArray(Data.getString("books"));
                        JSONArray Music = new JSONArray(Data.getString("music"));
                        for (int a = 0; a < Apps.length(); a++) {
                            JSONObject app = Apps.getJSONObject(a);
                            HomeItem item = new HomeItem();
                            String appPack = app.getString("packagename");
                            if (appPack != null && appPack.length() >= 3) {

                                appPack = appPack.trim().trim().toLowerCase().replace("'", "");
                                item.setType("app");
                                item.setHomeItemResource(appPack);

                                if (app.getString("asset_id").length() != 0)
                                    item.setHomeItemID(app.getString("asset_id"));
                                else
                                    item.setHomeItemID("0");

                                if (app.getString("asset_name").length() != 0)
                                    item.setHomeItemName(app.getString("asset_name"));
                                else
                                    item.setHomeItemName("none");

                                if (app.getString("asset_publisher").length() != 0)
                                    item.setHomeItemPublisher(app.getString("asset_publisher"));
                                else
                                    item.setHomeItemPublisher("none");


                                String pricing = app.getString("asset_price");
                                float priceInt = 0;
                                if (pricing != null) {
                                    priceInt = Float.parseFloat(pricing);

                                }
                                if (0 != priceInt) {
                                    item.setHomeItemPrice(app.getString("asset_price") + " " + app.getString("asset_currency"));
                                } else {
                                    item.setHomeItemPrice("FREE");

                                }


                                if (app.getString("asset_banner").length() >= 5) {
                                    String Icon = app.getString("asset_banner");
                                    Icon = Icon.replace("../", "");
                                    item.setHomeItemImageURL(NuntuConstants.HOST.toString() + "/" + Icon);
                                } else {
                                    item.setHomeItemImageURL(NuntuConstants.HOST.toString() + "/data/defaults/android_icon.png");
                                }


                                if (NuntuUtils.isAppInstalled(HomeContext, appPack)) {
                                    item.setStatus("1");
                                }
                                HomeItems.add(item);
                            }
                        }

                        for (int b = 0; b < Books.length(); b++) {
                            JSONObject book = Books.getJSONObject(b);
                            HomeItem item = new HomeItem();
                            item.setType("book");

                            if (book.getString("asset_id").length() != 0)
                                item.setHomeItemID(book.getString("asset_id"));
                            else
                                item.setHomeItemID("0");

                            if (book.getString("asset_name").length() != 0)
                                item.setHomeItemName(book.getString("asset_name"));
                            else
                                item.setHomeItemName("none");

                            if (book.getString("asset_publisher").length() != 0)
                                item.setHomeItemPublisher(book.getString("asset_publisher"));
                            else
                                item.setHomeItemPublisher("none");

                            if (book.getString("packagename").length() != 0)
                                item.setHomeItemResource(book.getString("packagename").toLowerCase(Locale.ENGLISH).replace("'", ""));
                            else
                                item.setHomeItemResource("none");

                            if (book.getString("asset_price").length() != 0) {
                                String pricing = book.getString("asset_price");
                                float priceInt = 0;
                                if (!pricing.equalsIgnoreCase("0.00")) {
                                    priceInt = Float.parseFloat(pricing);

                                }
                                if (0 != priceInt) {
                                    item.setHomeItemPrice(book.getString("asset_price") + " " + book.getString("asset_currency"));
                                } else {
                                    item.setHomeItemPrice("FREE");

                                }
                            } else {
                                item.setHomeItemPrice("FREE");
                            }

                            if (book.getString("asset_banner").length() >= 5) {
                                String Icon = book.getString("asset_banner");
                                Icon = Icon.replace("../", "");
                                item.setHomeItemImageURL(NuntuConstants.HOST.toString() + "/" + Icon);
                            } else {
                                item.setHomeItemImageURL(NuntuConstants.HOST.toString() + "/data/defaults/android_icon.png");
                            }

                            HomeItems.add(item);
                        }

                        for (int m = 0; m < Music.length(); m++) {
                            JSONObject music = Music.getJSONObject(m);
                            HomeItem item = new HomeItem();
                            item.setType("music");

                            if (music.getString("asset_id").length() != 0)
                                item.setHomeItemID(music.getString("asset_id"));
                            else
                                item.setHomeItemID("0");

                            if (music.getString("asset_name").length() != 0)
                                item.setHomeItemName(music.getString("asset_name"));
                            else
                                item.setHomeItemName("none");

                            if (music.getString("asset_publisher").length() != 0)
                                item.setHomeItemPublisher(music.getString("asset_publisher"));
                            else
                                item.setHomeItemPublisher("none");

                            if (music.getString("packagename").length() != 0)
                                item.setHomeItemResource(music.getString("packagename").toLowerCase(Locale.ENGLISH).replace("'", ""));
                            else
                                item.setHomeItemResource("none");

                            if (music.getString("asset_price").length() != 0) {
                                String pricing = music.getString("asset_price");
                                float priceInt = 0;
                                if (!pricing.equalsIgnoreCase("0.00")) {
                                    priceInt = Float.parseFloat(pricing);

                                }
                                if (0 != priceInt) {
                                    item.setHomeItemPrice(music.getString("asset_price") + " " + music.getString("asset_currency"));
                                } else {
                                    item.setHomeItemPrice("FREE");

                                }
                            } else {
                                item.setHomeItemPrice("FREE");
                            }

                            if (music.getString("asset_banner").length() >= 5) {
                                String Icon = music.getString("asset_banner");
                                Icon = Icon.replace("../", "");
                                item.setHomeItemImageURL(NuntuConstants.HOST.toString() + "/" + Icon);
                            } else {
                                item.setHomeItemImageURL(NuntuConstants.HOST.toString() + "/data/defaults/android_icon.png");
                            }
                            HomeItems.add(item);
                        }
                    } else {
                        msg = obj.getString("data");
                    }

                } catch (Exception e) {
                    Log.e("EXC", "An exception occurred while getting data " + e.getMessage());
                }
                return null;


            }

            @SuppressWarnings("deprecation")
            @Override
            protected void onPostExecute(String result) {
                try {
                    parentActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (HomeItems != null && HomeItems.size() >= 1) {
                                HomeItemsAdapter = new HolderHomeBaseAdapter(parentActivity, UserID, HomeItems, HomeGridView.getNumColumns());
                                HomeGridView.setAdapter(HomeItemsAdapter);
                                HomeItemsAdapter.notifyDataSetChanged();
                                Log.e("OKAY", "  item count " + HomeItemsAdapter.getCount());
                                HOME_LOAD_MODE = "init_finished";

                            } else {
                                Toast toast = Toast.makeText(HomeContext, msg, Toast.LENGTH_LONG);
                                toast.setGravity(Gravity.CENTER, 0, 0);
                                toast.show();
                            }
                            HomeSwipeLayout.setRefreshing(false);
                        }
                    });
                    new Device().registerDeviceAsync(HomeContext);
                    new Nuntu().initUserHeader("refresh");
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }


        };
        getTask.execute(NuntuConstants.HOST.toString() + "/api/android/data/user/" + UserID + "/preferences/10");
    }


    public static void hideOpenMenus() {
        if (HomeGridView != null) {
            int c = HomeGridView.getChildCount();
            for (int i = 0; i < c; i++) {
                View view = HomeGridView.getChildAt(i);
                if (view != null) {
                    CardView IcMenu = (CardView) view.findViewById(R.id.item_controls);
                    if (IcMenu != null) {
                        IcMenu.setVisibility(View.GONE);
                    }
                }
            }
        }
    }


    private static void onCloseFragment() {
        if (LoginTask != null) {
            LoginTask.cancel(true);

        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (LoginTask != null) {
            LoginTask.cancel(true);

        }
    }


    @Override
    public void onResume() {
        promptLogin(HOME_LOAD_MODE);
        Log.e("DEBUG", "onResume of LoginFragment");
        super.onResume();
    }

    @Override
    public void onPause() {
        LoginParent.setVisibility(View.GONE);
        Log.e("DEBUG", "OnPause of loginFragment");
        super.onPause();
    }
}