package utils.hehe.nuntu;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class NuntuBroadcastReceiver extends BroadcastReceiver {
    Intent BroadcastIntent;

    @Override
    public void onReceive(Context context, Intent intent) {

        if (intent.getAction().equals(Intent.ACTION_PACKAGE_REMOVED) || intent.getAction().equals(Intent.ACTION_PACKAGE_FULLY_REMOVED)) {
            try {
                String packageName = intent.getData().getSchemeSpecificPart();
                String UninstalledPackageName = packageName.replace(".", "_").replace(" ", "") + ".apk";
                Log.e("Nuntu Broadcast", "Nuntu uninstall pack ..." + UninstalledPackageName);
                //BroadcastIntent = new Intent(NuntuPollService.class.getName());
                BroadcastIntent.putExtra("action", "clean");
                BroadcastIntent.putExtra("type", "uninstall");
                BroadcastIntent.putExtra("package", UninstalledPackageName);
                context.startService(BroadcastIntent);
            } catch (Exception exc) {
                Log.e("Nuntu Broadcast", "Nuntu uninstall intent exception ..." + exc.getMessage());
            }
        } else if (intent.getAction().equals(Intent.ACTION_PACKAGE_ADDED) || intent.getAction().equals(Intent.ACTION_PACKAGE_REPLACED) || intent.getAction().equals(Intent.ACTION_PACKAGE_FIRST_LAUNCH)) {
            try {
                String packageName = intent.getData().getSchemeSpecificPart();
                String InstalledPackageName = packageName.replace(".", "_").replace(" ", "") + ".apk";
                Log.e("Nuntu Broadcast", "Nuntu install pack ..." + InstalledPackageName);
                //BroadcastIntent = new Intent(NuntuPollService.class.getName());
                BroadcastIntent.putExtra("action", "clean");
                BroadcastIntent.putExtra("type", "install");
                BroadcastIntent.putExtra("package", InstalledPackageName);
                context.startService(BroadcastIntent);

            } catch (Exception exc) {
                Log.e("Nuntu Broadcast", "Nuntu Install intent exception ..." + exc.getMessage());
            }
        } else {
            try {
                //BroadcastIntent = new Intent(NuntuPollService.class.getName());
                BroadcastIntent.putExtra("action", "talkservice");
                context.startService(BroadcastIntent);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
