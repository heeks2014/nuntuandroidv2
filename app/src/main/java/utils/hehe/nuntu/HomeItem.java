package utils.hehe.nuntu;

/**
 * Created by snick on 2/27/15.
 */
public class HomeItem {

    String name = "",
            itemPublisher = "",
            price = "",
            HomeItemID = "",
            iconURL = "",
            Status = "",
            Type = "",
            Resource = "";

    public String getHomeItemName() {
        return name;
    }

    public void setHomeItemName(String _name) {
        name = _name;
    }

    public String getHomeItemID() {
        return HomeItemID;
    }

    public void setHomeItemID(String id) {
        HomeItemID = id;
    }

    public String getHomeItemPublisher() {
        return itemPublisher;
    }

    public void setHomeItemPublisher(String Publisher) {
        itemPublisher = Publisher;
    }

    public String getHomeItemPrice() {
        return price;
    }


    public void setHomeItemPrice(String str) {
        price = str;
    }


    public String getHomeItemResource() {
        return Resource;
    }

    public void setHomeItemResource(String packname) {
        Resource = packname;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }


    public String getHomeItemImageURL() {
        return iconURL;
    }

    public void setHomeItemImageURL(String url) {
        iconURL = url;
    }


    @Override
    public String toString() {

        return "\nid " + HomeItemID + "\nname " + name + "\ndesc " + itemPublisher + "\nprice " + price + "\npackage " + Resource;

    }


}
