package utils.hehe.nuntu;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.CardView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Locale;

public class HolderHomeBaseAdapter extends BaseAdapter {

    static Activity HomeContext;
    private static ArrayList<HomeItem> HomeItems;
    private LayoutInflater l_Inflater;
    private static int GridCol = 3;
    private int AppIconWidth, IconWidth, IconHeight;
    public static String userID = "0";
    private NuntuTransaction Transaction;

    public HolderHomeBaseAdapter(Activity context, String User, ArrayList<HomeItem> results, int numCol) {
        HomeItems = results;
        HomeContext = context;
        GridCol = numCol;
        userID = User;
        this.l_Inflater = LayoutInflater.from(HomeContext);
        DisplayMetrics metrics = HomeContext.getResources().getDisplayMetrics();
        IconWidth = (metrics.widthPixels / GridCol) - 4;
        IconHeight = (int) (IconWidth / 1.2);
        AppIconWidth = (int) ((metrics.widthPixels / numCol) / 1.75);
        Transaction = new NuntuTransaction(HomeContext, userID);
    }


    @Override
    public int getCount() {
        return HomeItems.size();
    }

    @Override
    public Object getItem(int position) {
        return HomeItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final ViewHolder holder;
        if (convertView == null) {
            convertView = l_Inflater.inflate(R.layout.item_row, null);
            holder = new ViewHolder();
            holder.txt_itemName = (TextView) convertView.findViewById(R.id.itemname);
            holder.txt_itemPublisher = (TextView) convertView.findViewById(R.id.item_publisher);
            holder.txt_itemPrice = (TextView) convertView.findViewById(R.id.itemoffer);
            holder.ImageTypeIcon = (ImageView) convertView.findViewById(R.id.item_type_icon);

            holder.controls = (CardView) convertView.findViewById(R.id.item_controls);
            holder.ActionButton = (Button) convertView.findViewById(R.id.item_action_button);
            holder.ShareButton = (Button) convertView.findViewById(R.id.item_share_button);
            holder.TickButton = (Button) convertView.findViewById(R.id.item_like_button);

            holder.itemImage = (ImageView) convertView.findViewById(R.id.itemicon);
            holder.ic_menu = (ImageView) convertView.findViewById(R.id.item_ic_menu);


            convertView.setTag(holder);

        } else {
            convertView.refreshDrawableState();
            holder = (ViewHolder) convertView.getTag();

        }


        try {

            String ItemPrice = HomeItems.get(position).getHomeItemPrice();
            if (ItemPrice != null && ItemPrice.contains(".00")) {
                ItemPrice = ItemPrice.replace(".00", "");
            }

            String ItemPub = HomeItems.get(position).getHomeItemPublisher();
            if (ItemPub != null) {
                ItemPub = NuntuUtils.capitalizeFirstLetter(ItemPub.toLowerCase());
            }

            String ItemName = HomeItems.get(position).getHomeItemName();
            if (ItemName != null) {
                ItemName = NuntuUtils.capitalizeFirstLetter(ItemName.toLowerCase());
            }

            String url = HomeItems.get(position).getHomeItemImageURL();


            holder.txt_itemName.setText(ItemName);
            holder.txt_itemPublisher.setText(ItemPub);
            holder.txt_itemPrice.setText(ItemPrice);

            holder.ImageTypeIcon.setVisibility(View.VISIBLE);
            holder.ImageTypeIcon.setImageResource(R.drawable.app_logo);
            if ("app".equalsIgnoreCase(HomeItems.get(position).getType())) {
                if ("1".equalsIgnoreCase(HomeItems.get(position).getStatus())) {
                    holder.ActionButton.setText("Installed");
                } else if ("FREE".equalsIgnoreCase(HomeItems.get(position).getHomeItemPrice())) {
                    holder.ActionButton.setText("Download");
                } else {
                    holder.ActionButton.setText("Buy");
                }

                holder.ImageTypeIcon.setImageResource(R.drawable.app_logo);
                if (url != null && url.contains("http")) {
                    Picasso.with(HomeContext)
                            .load(url)
                            .resize(AppIconWidth, AppIconWidth)
                            .centerCrop()
                            .error(R.drawable.app_logo)
                            .placeholder(R.drawable.app_logo)
                            .into(holder.itemImage);
                }

            } else if ("book".equalsIgnoreCase(HomeItems.get(position).getType())) {

                if ("1".equalsIgnoreCase(HomeItems.get(position).getStatus())) {
                    holder.ActionButton.setText("Open");
                } else if ("FREE".equalsIgnoreCase(HomeItems.get(position).getHomeItemPrice())) {
                    holder.ActionButton.setText("Download");
                } else {
                    holder.ActionButton.setText("Buy");
                }

                holder.ImageTypeIcon.setImageResource(R.drawable.book_logo);
                if (url != null && url.contains("http")) {
                    Picasso.with(HomeContext)
                            .load(url)
                            .resize(IconWidth, IconHeight)
                            .centerCrop()
                            .error(R.drawable.book_logo)
                            .placeholder(R.drawable.book_logo)
                            .into(holder.itemImage);
                }

            } else if ("music".equalsIgnoreCase(HomeItems.get(position).getType())) {
                if ("1".equalsIgnoreCase(HomeItems.get(position).getStatus())) {
                    holder.ActionButton.setText("Open");
                } else if ("FREE".equalsIgnoreCase(HomeItems.get(position).getHomeItemPrice())) {
                    holder.ActionButton.setText("Download");
                } else {
                    holder.ActionButton.setText("Buy");
                }

                holder.ImageTypeIcon.setImageResource(R.drawable.music_logo);
                if (url != null && url.contains("http")) {
                    Picasso.with(HomeContext)
                            .load(url)
                            .resize(IconWidth, IconHeight)
                            .centerCrop()
                            .error(R.drawable.music_logo)
                            .placeholder(R.drawable.music_logo)
                            .into(holder.itemImage);
                }
            }

            holder.TickButton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    holder.controls.setVisibility(View.GONE);
                    NuntuUtils.sendTick(HomeContext, HomeItems.get(position).getType(), HomeItems.get(position).getHomeItemID());
                }
            });

            holder.ShareButton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        holder.controls.setVisibility(View.GONE);
                        NuntuUtils.shareNuntuItem(HomeContext, HomeItems.get(position).getHomeItemName(),
                                HomeItems.get(position).getType(),
                                HomeItems.get(position).getHomeItemPublisher(),
                                HomeItems.get(position).getHomeItemID());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });


            holder.txt_itemName.setOnClickListener(new OnClickListener() {
                public void onClick(View v) {
                    respondtoItemClick(holder.controls, HomeItems.get(position));

                }
            });

            holder.txt_itemPublisher.setOnClickListener(new OnClickListener() {
                public void onClick(View v) {
                    respondtoItemClick(holder.controls, HomeItems.get(position));


                }
            });

            holder.txt_itemPrice.setOnClickListener(new OnClickListener() {
                public void onClick(View v) {
                    respondtoItemClick(holder.controls, HomeItems.get(position));
                }
            });


            holder.itemImage.setOnClickListener(new OnClickListener() {
                public void onClick(View v) {

                    respondtoItemClick(holder.controls, HomeItems.get(position));
                }
            });

            convertView.setOnClickListener(new OnClickListener() {
                public void onClick(View v) {

                    respondtoItemClick(holder.controls, HomeItems.get(position));
                }
            });

            holder.ic_menu.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (holder.controls.getVisibility() == View.VISIBLE) {
                        holder.controls.setVisibility(View.GONE);
                    } else {
                        FragmentNuntuHome.hideOpenMenus();
                        holder.controls.setVisibility(View.VISIBLE);
                    }
                }
            });
        } catch (Exception exc) {
            exc.printStackTrace();
            Log.e("OKAY", "exception rendering " + exc.getMessage());
        }

        holder.ActionButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.controls.setVisibility(View.GONE);
                if ("app".equalsIgnoreCase(HomeItems.get(position).getType())) {
                    String ClickAction = holder.ActionButton.getText().toString().toLowerCase(Locale.ENGLISH).trim();
                    if ("installed".equalsIgnoreCase(ClickAction)) {
                        final Dialog dialog = new Dialog(HomeContext);
                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        dialog.setContentView(R.layout.nuntu_custom_alertbox);
                        dialog.setCanceledOnTouchOutside(true);
                        TextView text = (TextView) dialog.findViewById(R.id.text);
                        text.setText(HomeItems.get(position).getHomeItemName().trim());
                        ImageView image = (ImageView) dialog.findViewById(R.id.image);
                        try {
                            image.setBackgroundDrawable(HomeContext.getPackageManager().getApplicationIcon(HomeItems.get(position).getHomeItemResource()));
                        } catch (Exception e) {
                            image.setImageResource(R.drawable.app_logo);
                            e.printStackTrace();
                        }
                        Button Open = (Button) dialog.findViewById(R.id.dialogButtonNeg);
                        Open.setText("OPEN");
                        // if button is clicked, close the custom dialog
                        Open.setOnClickListener(new OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                                Intent LaunchIntent = HomeContext.getPackageManager().getLaunchIntentForPackage(HomeItems.get(position).getHomeItemResource().trim());
                                HomeContext.startActivity(LaunchIntent);

                            }
                        });

                        Button Delete = (Button) dialog.findViewById(R.id.dialogButtonNet);
                        Delete.setText("UNINSTALL");
                        // if button is clicked, close the custom dialog
                        Delete.setOnClickListener(new OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                                String packageName = "package:" + HomeItems.get(position).getHomeItemResource().trim();
                                Intent intent = new Intent(Intent.ACTION_DELETE);
                                intent.setData(Uri.parse(packageName));
                                HomeContext.startActivity(intent);
                            }
                        });

                        Button Cancel = (Button) dialog.findViewById(R.id.dialogButtonPos);
                        Cancel.setText("CANCEL");
                        // if button is clicked, close the custom dialog
                        Cancel.setOnClickListener(new OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                            }
                        });

                        dialog.show();
                    } else if ("download".equalsIgnoreCase(ClickAction) || "buy".equalsIgnoreCase(ClickAction)) {
                        if (TelephonyInfo.hasConnection(HomeContext)) {
                            if (Transaction.initTransaction()) {
                                Transaction.requestAppDonwload(new String[]{HomeItems.get(position).getType(),
                                        HomeItems.get(position).getHomeItemID(),
                                        HomeItems.get(position).getHomeItemName(),
                                        HomeItems.get(position).getHomeItemResource()
                                });
                            } else {
                                Toast.makeText(HomeContext, "Nuntu Couldn't start transaction", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(HomeContext, "Please connect to internet", Toast.LENGTH_SHORT).show();
                        }
                    }

                } else if ("book".equalsIgnoreCase(HomeItems.get(position).getType())) {
                    String ClickAction = holder.ActionButton.getText().toString().toLowerCase(Locale.ENGLISH).trim();
                    if ("Read".equalsIgnoreCase(ClickAction)) {

                    } else if ("download".equalsIgnoreCase(ClickAction) || "buy".equalsIgnoreCase(ClickAction)) {
                        if (TelephonyInfo.hasConnection(HomeContext)) {
                            if (Transaction.initTransaction()) {
                                Transaction.requestAppDonwload(new String[]{HomeItems.get(position).getType(),
                                        HomeItems.get(position).getHomeItemID(),
                                        HomeItems.get(position).getHomeItemName(),
                                        HomeItems.get(position).getHomeItemResource()
                                });
                            } else {
                                Toast.makeText(HomeContext, "Nuntu Couldn't start transaction", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(HomeContext, "Please connect to internet", Toast.LENGTH_SHORT).show();
                        }


                    }
                } else if ("music".equalsIgnoreCase(HomeItems.get(position).getType())) {
                    final String ClickAction = holder.ActionButton.getText().toString().toLowerCase(Locale.ENGLISH).trim();
                    if ("play".equalsIgnoreCase(ClickAction)) {

                    } else if ("download".equalsIgnoreCase(ClickAction) || "buy".equalsIgnoreCase(ClickAction)) {
                        if (TelephonyInfo.hasConnection(HomeContext)) {
                            if (Transaction.initTransaction()) {
                                Transaction.requestAppDonwload(new String[]{HomeItems.get(position).getType(),
                                        HomeItems.get(position).getHomeItemID(),
                                        HomeItems.get(position).getHomeItemName(),
                                        HomeItems.get(position).getHomeItemResource()
                                });
                            } else {
                                Toast.makeText(HomeContext, "Nuntu Couldn't start transaction", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(HomeContext, "Please connect to internet", Toast.LENGTH_SHORT).show();
                        }


                    }
                }
            }
        });

        holder.ActionButton.setTransformationMethod(null);
        holder.ShareButton.setTransformationMethod(null);
        holder.TickButton.setTransformationMethod(null);

        return convertView;
    }

    private void respondtoItemClick(final CardView menu, final HomeItem item) {
        try {
            if (menu.getVisibility() != View.VISIBLE) {
                if ("book".equalsIgnoreCase(item.getType())) {
                    Intent intent = new Intent(HomeContext, BookActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("bookID", item.getHomeItemID());
                    intent.putExtra("location", item.getHomeItemResource());
                    HomeContext.startActivity(intent);
                } else if ("music".equalsIgnoreCase(item.getType())) {
                    Intent intent = new Intent(HomeContext, AlbumActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("albumID", item.getHomeItemID());
                    intent.putExtra("location", item.getHomeItemResource());
                    HomeContext.startActivity(intent);
                }
            }
            FragmentNuntuHome.hideOpenMenus();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    static class ViewHolder {
        TextView txt_itemName;
        TextView txt_itemPublisher;
        TextView txt_itemPrice;
        Button ActionButton, ShareButton, TickButton;
        CardView controls;
        ImageView itemImage, ImageTypeIcon, ic_menu;
    }
}