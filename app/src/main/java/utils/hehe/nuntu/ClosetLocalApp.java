package utils.hehe.nuntu;

import android.graphics.drawable.Drawable;

/**
 * Created by snickwizard on 2/11/15.
 */
public class ClosetLocalApp {
    private Drawable res;
    private String name;
    private String author;
    private String packageName;

    public Drawable getIcon() {
        return res;
    }

    public void setIcon(Drawable id) {
        res = id;
    }

    public String getAppTitle() {
        return name;
    }

    public void setAppTitle(String name) {
        this.name = name;
    }

    public String getAppAuthor() {
        return author;
    }

    public void setAppAuthor(String author) {
        this.author = author;
    }

    public String getAppPackageName() {
        return packageName;
    }

    public void setAppPackageName(String pack) {
        this.packageName = pack;
    }
}
