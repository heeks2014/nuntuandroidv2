package utils.hehe.nuntu;

/**
 * Created by snickwizard on 2/11/15.
 */
public class ClosetLocalTrack {
    private int status = 0;
    private String name = "unknown";
    private String length = "0:00";
    private String TrackLocation = "/../";

    public int getTrackStatus() {
        return this.status;
    }

    public void setTrackStatus(int status) {
        this.status = status;
    }

    public String getTrackTitle() {
        return name;
    }

    public String getTrackLength() {
        return length;
    }

    public void setTrackLength(String len) {
        this.length = len;
    }

    public String getTrackLocation() {
        return TrackLocation;
    }

    public void setTrackLocation(String locus) {
        this.TrackLocation = locus;
    }

    public void setATrackTitle(String name) {
        this.name = name;
    }

}