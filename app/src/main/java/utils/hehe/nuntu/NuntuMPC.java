package utils.hehe.nuntu;

/**
 * Created by snickwizard on 2/7/15.
 */

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.PixelFormat;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.ResultReceiver;
import android.os.Vibrator;
import android.support.v4.app.NotificationCompat;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Display;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListPopupWindow;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

public class NuntuMPC extends Service implements OnCompletionListener, SeekBar.OnSeekBarChangeListener, AudioManager.OnAudioFocusChangeListener {

    public static int ID_NOTIFICATION = 2018;
    private final String DEBUG_TAG = "NUNTU_MPS_DEBUG";
    private static ImageView AlbumCover, MiniAlbumCover;
    private static ImageButton btnPlay;
    private static SeekBar songProgressBar;
    private static TextView songTitleLabel, songLabelMinized;
    private static TextView songCurrentDurationLabel, songMaxDurationLabel;
    private WindowManager windowManager;
    private RelativeLayout NuntuFloatingViewMaximized, NuntuFloatingViewMinized, NuntuPlayerButtons;
    private WindowManager.LayoutParams params;
    private ListPopupWindow popup;
    private ImageButton btnNext;
    private ImageButton btnPrevious;
    private ImageButton btnRepeat;
    private ImageButton btnShuffle, btnClose;
    private ProgressBar MiniPlayerSkin;
    private Context NuntuMusicPlayerContext;
    private Boolean _enable = true;
    private int currentSongIndex = 0;
    private boolean isShuffle = false;
    private boolean isRepeat = false;
    private int mpOnNotificationPosition = 0;
    private ListenToPhoneState onCall;
    private AudioManager am;
    boolean mHasDoubleClicked = false;
    ArrayList<String> paths;
    ArrayList<String> titles;
    HeadeSetIntentReceiver HeadSetState;
    SwipeGestureListener gestureListener;
    private static boolean isVolumeFading = false;
    private static boolean hasPlayerStarted = false;
    public static String songTitle;
    NotificationManager manager;
    // Media Player
    private static MediaPlayer mp;
    // Handler to update UI timer, progress bar etc,.
    private static NuntuMusicUtilities utils;
    public static ArrayList<HashMap<String, String>> songsList;
    private static Handler mHandler;
    private static boolean mpOnPaused = false, isMinimized = false;
    private static boolean isHardPaused = false;
    public static Bitmap cover;
    //static ResultReceiver resultReceiver;

    private static Runnable mUpdatePlayerProgressTask = new Runnable() {
        public void run() {
            try {
                long totalDuration = mp.getDuration();
                long currentDuration = mp.getCurrentPosition();

                // Displaying Total Duration time
                songCurrentDurationLabel.setText("" + utils.milliSecondsToTimer(currentDuration));
                songMaxDurationLabel.setText("" + utils.milliSecondsToTimer(totalDuration));
                // Updating progress bar
                int progress = utils.getProgressPercentage(currentDuration, totalDuration);
                //Log.d("Progress", ""+progress);
                songProgressBar.setProgress(progress);

                // Running this thread after 100 milliseconds
                mHandler.postDelayed(this, 100);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    /**
     * Function to play a song
     *
     * @param songIndex - index of song
     */
    public static void playSong(int songIndex) {
        // Play song
        try {

            if (songsList != null && songsList.size() >= 1) {
                if (mp == null) {
                    mp = new MediaPlayer();
                } else {
                    mp.reset();
                }
                mp.setDataSource(songsList.get(songIndex).get("songPath"));
                mp.prepare();
                mp.setVolume(1, 1);
                mp.start();
                hasPlayerStarted = true;
                // Changing Button Image to pause image
                btnPlay.setImageResource(R.drawable.mp_action_pause);

                // set Progress bar values
                songProgressBar.setProgress(0);
                songProgressBar.setMax(100);

                // Updating progress bar
                updateProgressBar();
            } else {
                mp.stop();
                mp.release();
                hasPlayerStarted = false;
            }
            // Displaying Song title
            songTitle = songsList.get(songIndex).get("songTitle");
            if (songTitle != null) {
                songTitleLabel.setText(songTitle);
                songLabelMinized.setText(songTitle);
            } else {
                songTitleLabel.setText("Track " + songIndex + 1);
                songLabelMinized.setText("Track " + songIndex + 1);
            }
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalStateException e) {
            //  e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * Update timer on seekbar
     */
    public static void updateProgressBar() {
        mHandler.postDelayed(mUpdatePlayerProgressTask, 100);
    }

    @Override
    public IBinder onBind(Intent intent) {
        Log.e(DEBUG_TAG, "In onBind with intent=" + intent.getAction());
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStart(intent, startId);
        if (windowManager == null || NuntuFloatingViewMaximized == null || (NuntuFloatingViewMaximized.getVisibility() != View.VISIBLE && NuntuFloatingViewMinized.getVisibility() != View.VISIBLE)) {
            onCreate();
        }
        handleCommand(intent);
        return Service.START_STICKY_COMPATIBILITY;
    }

    @Override
    public void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);
        if (windowManager == null || NuntuFloatingViewMaximized == null || (NuntuFloatingViewMaximized.getVisibility() != View.VISIBLE && NuntuFloatingViewMinized.getVisibility() != View.VISIBLE)) {
            onCreate();
        }
        handleCommand(intent);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        NuntuMusicPlayerContext = this;

        if (mHandler == null) {
            mHandler = new Handler();
        }


        if (HeadSetState == null) {
            HeadSetState = new HeadeSetIntentReceiver();
        }
        IntentFilter filter = new IntentFilter(Intent.ACTION_HEADSET_PLUG);
        registerReceiver(HeadSetState, filter);
        if (onCall == null) {
            onCall = new ListenToPhoneState(mp, NuntuFloatingViewMaximized);
        }
        TelephonyManager mgr = (TelephonyManager) NuntuMusicPlayerContext.getSystemService(TELEPHONY_SERVICE);
        if (mgr != null) {
            Log.e("TEL MANAGER", " it was ok");
            mgr.listen(onCall, PhoneStateListener.LISTEN_NONE);
            Log.e("TEL MANAGER", " call state " + mgr.getCallState());
        } else {
            Log.e("TEL MANAGER", " it was null");
        }


        // Mediaplayer
        if (mp == null) {
            mp = new MediaPlayer();
        }
        if (utils == null) {
            utils = new NuntuMusicUtilities();
        }
        hasPlayerStarted = false;
        View view;
        //  ScreenWidth=Device.getDeviceScreenSize("width",this);
        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        if (Device.getDeviceScreenSize("width", NuntuMusicPlayerContext, "normal") > 720) {
            view = inflater.inflate(R.layout.nuntu_music_player_layout, null);
        } else {
            view = inflater.inflate(R.layout.nuntu_music_player_layout_small_screen, null);
        }
        if (windowManager == null) {
            windowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
        }

        NuntuFloatingViewMaximized = (RelativeLayout) view.findViewById(R.id.nuntu_player_maximized);
        NuntuPlayerButtons = (RelativeLayout) view.findViewById(R.id.player_controls);
        NuntuFloatingViewMinized = (RelativeLayout) view.findViewById(R.id.nuntu_player_minimized);
        AlbumCover = (ImageView) view.findViewById(R.id.music_player_album_cover);
        MiniAlbumCover = (ImageView) view.findViewById(R.id.mini_album_cover);
        btnPlay = (ImageButton) view.findViewById(R.id.bgnd_music_btn_play);
        btnNext = (ImageButton) view.findViewById(R.id.bgnd_music_btn_next);
        btnPrevious = (ImageButton) view.findViewById(R.id.bgnd_music_btn_prev);
        btnRepeat = (ImageButton) view.findViewById(R.id.nuntu_music__btn_repeat);
        btnShuffle = (ImageButton) view.findViewById(R.id.bgnd_music_btn_shuffle);
        btnClose = (ImageButton) view.findViewById(R.id.close_music_player);
        songProgressBar = (SeekBar) view.findViewById(R.id.playing_track_progressbar);
        songCurrentDurationLabel = (TextView) view.findViewById(R.id.playing_track_progresstimer);
        songMaxDurationLabel = (TextView) view.findViewById(R.id.playing_track_maxtimer);
        songTitleLabel = (TextView) view.findViewById(R.id.playing_track_title);
        songLabelMinized = (TextView) view.findViewById(R.id.TrackTitleMinimized);
        MiniPlayerSkin = (ProgressBar) view.findViewById(R.id.TrackPlayerProgress);
        params = new WindowManager.LayoutParams(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.TYPE_PHONE, WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE, PixelFormat.TRANSLUCENT);
        params.x = 0;
        params.y = 10;
        //params.horizontalMargin=40;
        params.gravity = Gravity.TOP | Gravity.CENTER_HORIZONTAL;
        windowManager.addView(NuntuFloatingViewMaximized, params);
        try {


            // Listeners
            if (gestureListener == null) {
                gestureListener = new SwipeGestureListener(NuntuMusicPlayerContext);
            }
            NuntuFloatingViewMaximized.setOnTouchListener(gestureListener);
            AlbumCover.setOnTouchListener(gestureListener);
            NuntuPlayerButtons.setOnTouchListener(gestureListener);
            NuntuFloatingViewMinized.setOnTouchListener(gestureListener);
            NuntuFloatingViewMinized.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    isMinimized = true;
                    toggleMusicPlayer();
                }
            });

            songProgressBar.setOnSeekBarChangeListener(this); // Important
            mp.setOnCompletionListener(this); // Important

            btnClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    isMinimized = false;
                    toggleMusicPlayer();

                }
            });


            AlbumCover.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View arg0) {
                    try {
                        if (_enable) {
                            initiatePopupWindow(NuntuFloatingViewMaximized, titles);
                            _enable = false;
                        } else {
                            _enable = true;
                            if (popup != null) {
                                popup.dismiss();
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

            btnPlay.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View arg0) {
                    try {
                        if (mp != null) {
                            // check for already playing
                            if (mp.isPlaying()) {
                                isHardPaused = true;
                                if (!isVolumeFading) {
                                    new FadePlayerVolume().execute("down");
                                }

                            } else {
                                isHardPaused = false;
                                if (!isVolumeFading) {
                                    new FadePlayerVolume().execute("up");
                                }
                            }
                        } else {

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        //mp.reset();
                    }

                }

            });

            /**
             * Next button click event
             * Plays next song by taking currentSongIndex + 1
             * */
            btnNext.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View arg0) {
                    // check if next song is there or not
                    if (currentSongIndex < (songsList.size() - 1)) {
                        playSong(currentSongIndex + 1);
                        currentSongIndex = currentSongIndex + 1;
                    } else {
                        // play first song
                        playSong(0);
                        currentSongIndex = 0;
                    }

                }
            });

            /**
             * Back button click event
             * Plays previous song by currentSongIndex - 1 or restart
             * */
            btnPrevious.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View arg0) {
                    //restart
                    if (mp != null && mp.getCurrentPosition() >= 30000) {
                        playSong(currentSongIndex);
                    } else {
                        if (currentSongIndex > 0) {
                            playSong(currentSongIndex - 1);
                            currentSongIndex = currentSongIndex - 1;
                        } else {
                            // play last song
                            playSong(songsList.size() - 1);
                            currentSongIndex = songsList.size() - 1;
                        }
                    }
                }
            });

            /**
             * Button Click event for Repeat button
             * Enables repeat flag to true
             * */
            btnRepeat.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View arg0) {
                    if (isRepeat) {
                        isRepeat = false;
                        Toast.makeText(getApplicationContext(), "Repeat is OFF", Toast.LENGTH_SHORT).show();
                        btnRepeat.setBackgroundColor(getResources().getColor(R.color.nullColor));
                        btnRepeat.invalidate();
                    } else {
                        // make repeat to true
                        isRepeat = true;
                        Toast.makeText(getApplicationContext(), "Repeat is ON", Toast.LENGTH_SHORT).show();
                        // make shuffle to false
                        if (isShuffle) {
                            btnShuffle.setBackgroundColor(getResources().getColor(R.color.nullColor));
                        }
                        isShuffle = false;
                        btnRepeat.setBackgroundColor(getResources().getColor(R.color.primaryColor));
                    }
                }
            });

            /**
             * Button Click event for Shuffle button
             * Enables shuffle flag to true
             * */
            btnShuffle.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View arg0) {
                    if (isShuffle) {
                        isShuffle = false;
                        Toast.makeText(getApplicationContext(), "Shuffle is OFF", Toast.LENGTH_SHORT).show();
                        btnShuffle.setBackgroundColor(getResources().getColor(R.color.nullColor));
                        btnShuffle.invalidate();
                    } else {
                        // make repeat to true
                        isShuffle = true;
                        Toast.makeText(getApplicationContext(), "Shuffle is ON", Toast.LENGTH_SHORT).show();
                        // make shuffle to false
                        if (isRepeat) {
                            btnRepeat.setBackgroundColor(getResources().getColor(R.color.nullColor));
                        }
                        isRepeat = false;
                        btnShuffle.setBackgroundColor(getResources().getColor(R.color.primaryColor));

                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
        NuntuFloatingViewMaximized.setVisibility(View.GONE);
    }

    private void initiatePopupWindow(View anchor, final ArrayList<String> values) {
        try {
            Display display = ((WindowManager) getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
            popup = new ListPopupWindow(NuntuMusicPlayerContext);
            popup.setAnchorView(anchor);
            popup.setWidth((int) (display.getWidth() / (1.5)));
            popup.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, values) {
                @Override
                public View getView(int position, View convertView, ViewGroup parent) {
                    TextView textView = (TextView) super.getView(position, convertView, parent);
                    if (position == currentSongIndex) {
                        textView.setTextColor(getResources().getColor(R.color.primaryColor));
                    } else {
                        textView.setTextColor(Color.WHITE);
                    }
                    return textView;
                }
            });

            popup.setOnItemClickListener(new OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> arg0, View view, int position, long id3) {

                    try {
                        popup.dismiss();
                        currentSongIndex = position;
                        playSong(currentSongIndex);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
            popup.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void hidePlayerWithNotification() {

        try {
            NuntuFloatingViewMaximized.setVisibility(View.GONE);
            NuntuFloatingViewMinized.setVisibility(View.GONE);
            if (popup != null) {
                popup.dismiss();
            }
            if (!isVolumeFading) {
                new FadePlayerVolume().execute("down");
            }
            NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
                    .setSmallIcon(R.drawable.ic_launcher)
                    .setContentTitle("Nuntu Music")
                    .setOngoing(true)
                    .setAutoCancel(true)
                    .setTicker("paused")
                    .setContentText("click to resume playback");
            if (cover != null) {
                builder.setLargeIcon(NuntuUtils.scaleBitmap(cover, 64, 64));
            } else {
                builder.setLargeIcon(NuntuUtils.scaleBitmap(BitmapFactory.decodeResource(getApplicationContext().getResources(), R.drawable.headset), 64, 64));
            }
            Intent notificationIntent = new Intent(getApplicationContext(), NuntuMPC.class);
            notificationIntent.putExtra("from", "notification");
            PendingIntent contentIntent = PendingIntent.getService(getApplicationContext(), 0, notificationIntent, 0);
            builder.setContentIntent(contentIntent);
            // Add as notification
            manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            manager.notify(ID_NOTIFICATION, builder.build());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy() {

        Log.e(DEBUG_TAG, "in onDestroy()");
        try {
            NuntuFloatingViewMaximized.setVisibility(View.INVISIBLE);
            windowManager.removeViewImmediate(NuntuFloatingViewMaximized);

        } catch (Exception e) {
            try {
                NuntuFloatingViewMaximized.setVisibility(View.INVISIBLE);
                windowManager.removeView(NuntuFloatingViewMaximized);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            e.printStackTrace();
        }
        try {
            if (songsList != null) {
                songsList.clear();
                songsList = null;
            }
            if (mp != null) {
                mp.stop();
                mp.release();
                hasPlayerStarted = false;
                mp = null;
            }
            NuntuFloatingViewMinized = null;
            NuntuFloatingViewMaximized = null;
            this.stopSelf();
            super.onDestroy();
            //destroy evidence
            android.os.Process.killProcess(android.os.Process.myPid());
            System.exit(0);
        } catch (Exception e) {
            e.printStackTrace();
            if (mp != null) {
                mp.release();
                hasPlayerStarted = false;
            }
            this.stopSelf();
            super.onDestroy();
            System.exit(0);
        }


        try {
            if (HeadSetState != null) {

                unregisterReceiver(HeadSetState);
            }

            if (popup != null) {
                popup.dismiss();
                popup = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        if (mHandler != null) {
            mHandler.removeCallbacks(null);
        }
        if (paths != null) {
            paths.clear();
            paths = null;
        }
        if (titles != null) {
            titles.clear();
            titles = null;
        }

        if (am != null) {
            am.unloadSoundEffects();

        }

        mpOnNotificationPosition = 0;
        mpOnPaused = false;
        isHardPaused = false;
        hasPlayerStarted = false;
        onCall = null;
        am = null;
        mp = null;
        windowManager = null;
        NuntuFloatingViewMaximized = null;
        NuntuFloatingViewMinized = null;
        gestureListener = null;

        AlbumCover = null;
        btnPlay = null;
        btnNext = null;
        btnPrevious = null;
        btnRepeat = null;
        btnShuffle = null;
        btnClose = null;
        songProgressBar = null;
        songTitleLabel = null;
        songCurrentDurationLabel = null;
        songMaxDurationLabel = null;
        utils = null;

        currentSongIndex = 0;
        isShuffle = false;
        isRepeat = false;
        NuntuMusicPlayerContext = null;

        mHasDoubleClicked = false;
        _enable = true;
        System.gc();
    }

    private void toggleMusicPlayer() {
        try {
            if (!isMinimized) {
                isMinimized = true;
                // NuntuFloatingViewMaximized.invalidate();
                AlbumCover.setVisibility(View.GONE);
                btnRepeat.setVisibility(View.GONE);
                btnShuffle.setVisibility(View.GONE);
                btnClose.setVisibility(View.GONE);
                NuntuPlayerButtons.setVisibility(View.GONE);
                songProgressBar.setVisibility(View.GONE);
                songCurrentDurationLabel.setVisibility(View.GONE);
                songMaxDurationLabel.setVisibility(View.GONE);
                songTitleLabel.setVisibility(View.GONE);
                NuntuFloatingViewMaximized.setVisibility(View.VISIBLE);
                songLabelMinized.setVisibility(View.VISIBLE);
                MiniPlayerSkin.setVisibility(View.VISIBLE);
                NuntuFloatingViewMinized.setVisibility(View.VISIBLE);
                // NuntuFloatingViewMaximized.requestLayout();
                Log.e(DEBUG_TAG, "minimized finished");

            } else {
                isMinimized = false;
                AlbumCover.setVisibility(View.VISIBLE);
                NuntuPlayerButtons.setVisibility(View.VISIBLE);
                btnRepeat.setVisibility(View.VISIBLE);
                btnShuffle.setVisibility(View.VISIBLE);
                btnClose.setVisibility(View.VISIBLE);
                songProgressBar.setVisibility(View.VISIBLE);
                songCurrentDurationLabel.setVisibility(View.VISIBLE);
                songMaxDurationLabel.setVisibility(View.VISIBLE);
                songTitleLabel.setVisibility(View.VISIBLE);
                NuntuFloatingViewMaximized.setBackgroundResource(R.drawable.nuntu_bgnd_player_style);
                NuntuFloatingViewMaximized.setVisibility(View.VISIBLE);

                songLabelMinized.setVisibility(View.GONE);
                MiniPlayerSkin.setVisibility(View.GONE);
                NuntuFloatingViewMinized.setVisibility(View.GONE);
                Log.e(DEBUG_TAG, "maximed finished");
            }

            if (cover != null) {
                Log.e("COVER", "album cover was valid");
                AlbumCover.setImageBitmap(cover);
                MiniAlbumCover.setImageBitmap(cover);
            } else {
                Log.e("COVER", "album cover was gone");
                if (Build.VERSION.SDK_INT >= 16) {
                    MiniAlbumCover.setImageDrawable(AlbumCover.getDrawable());
                } else {
                    MiniAlbumCover.setImageDrawable(AlbumCover.getDrawable());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void handleCommand(Intent intent) {
        try {
            if (intent != null) {
                String action = intent.getStringExtra("action");
                if (manager != null) {
                    manager.cancel(ID_NOTIFICATION);
                    manager = null;
                }
                if ("miniplay".equalsIgnoreCase(action)) {

                    if (hasPlayerStarted) {
                        isMinimized = false;
                        toggleMusicPlayer();
                        Log.e("PLAYER VIEW", "it has been min'ed");
                    }

                } else if ("maxplay".equalsIgnoreCase(action)) {
                    if (hasPlayerStarted) {
                        isMinimized = true;
                        toggleMusicPlayer();
                        Log.e("PLAYER VIEW", "it has been max'ed");
                    }
                } else if ("auto_destruct".equalsIgnoreCase(action)) {
                    onDestroy();
                } else {
                    isMinimized = !isMinimized; //keep same player skin
                    if (am == null) {
                        am = (AudioManager) NuntuMusicPlayerContext.getSystemService(AUDIO_SERVICE);
                    }
                    am.requestAudioFocus(this, AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN);

                    toggleMusicPlayer();
                    if ("notification".equalsIgnoreCase(intent.getStringExtra("from"))) {
                        if (!isVolumeFading) {
                            new FadePlayerVolume().execute("up");
                        }

                        if (songTitle != null) {
                            songTitleLabel.setText(songTitle);
                            songLabelMinized.setText(songTitle);
                        } else {
                            songTitleLabel.setText("Track " + currentSongIndex + 1);
                            songLabelMinized.setText("Track " + currentSongIndex + 1);
                        }
                    } else if ("album".equalsIgnoreCase(intent.getStringExtra("from"))) {
                        //  resultReceiver = intent.getParcelableExtra("receiver");
                        if (songsList != null)
                            songsList.clear();
                        else {
                            songsList = new ArrayList<>();
                        }

                        // Getting all songs list
                        paths = intent.getStringArrayListExtra("songpath");
                        titles = intent.getStringArrayListExtra("songtitle");
                        cover = intent.getParcelableExtra("cover");
                        if (cover != null) {
                            AlbumCover.setImageBitmap(cover);
                            MiniAlbumCover.setImageBitmap(cover);
                        }
                        int i = 0;
                        while (i < paths.size()) {
                            HashMap<String, String> current = new HashMap<>();
                            current.put("songPath", paths.get(i));
                            current.put("songTitle", titles.get(i));
                            songsList.add(current);
                            i++;
                        }
                        // By default play first song
                        playSong(0);
                    } else if ("track".equalsIgnoreCase(intent.getStringExtra("from"))) {
                        // resultReceiver = intent.getParcelableExtra("receiver");
                        String path = intent.getStringExtra("path");
                        String title = intent.getStringExtra("title");
                        if (songsList == null) {
                            songsList = new ArrayList<>();
                        }
                        if (path != null) {
                            boolean alreadyExist = false;
                            HashMap<String, String> current = new HashMap<>();
                            current.put("songPath", path);
                            current.put("songTitle", title);
                            for (int i = 0; i < songsList.size(); i++) {
                                if (songsList.get(i).get("songTitle").equalsIgnoreCase(title)) {
                                    alreadyExist = true;
                                    currentSongIndex = i;
                                    playSong(currentSongIndex);
                                    break;
                                }
                            }
                            if (!alreadyExist) {
                                songsList.add(current);
                                currentSongIndex = songsList.size() - 1;
                                playSong(currentSongIndex);
                            }
                        }
                    }
                }
            } else {
                Log.e("NUNTU MP", "intent was null");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromTouch) {

    }

    /**
     * When user starts moving the progress handler
     */
    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
        // remove message Handler from updating progress bar
        mHandler.removeCallbacks(mUpdatePlayerProgressTask);
    }

    /**
     * When user stops moving the progress handler
     */
    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        try {
            int totalDuration = mp.getDuration();
            int currentPosition = utils.progressToTimer(seekBar.getProgress(), totalDuration);

            // forward or backward to certain seconds
            mp.seekTo(currentPosition);

            // update timer progress again
            updateProgressBar();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     *
     * */

    /**
     * On Song Playing completed
     * if repeat is ON play same song again
     * if shuffle is ON play random song
     */
    @Override
    public void onCompletion(MediaPlayer arg0) {

        if (songsList != null && songsList.size() >= 1) {
            // check for repeat is ON or OFF
            if (isRepeat) {
                // repeat is on play same song again
                playSong(currentSongIndex);
            } else if (isShuffle) {
                // shuffle is on - play a random song
                Random rand = new Random();
                currentSongIndex = rand.nextInt((songsList.size() - 1) - 0 + 1) + 0;
                playSong(currentSongIndex);
            } else {
                // no repeat or shuffle ON - play next song
                if (currentSongIndex < (songsList.size() - 1)) {
                    playSong(currentSongIndex + 1);
                    currentSongIndex = currentSongIndex + 1;
                } else {
                    // play first song
                    playSong(0);
                    currentSongIndex = 0;
                }
            }
        }
    }

    @Override
    public void onAudioFocusChange(int focus) {
        try {
            Log.e("AUDIO", "focus " + focus);
            switch (focus) {
                case AudioManager.AUDIOFOCUS_GAIN:
                    if (!isHardPaused) {
                        if (!isVolumeFading) {
                            new FadePlayerVolume().execute("up");
                        }
                    }
                    break;
                default:
                case AudioManager.AUDIOFOCUS_LOSS:
                case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT:
                case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK:
                case AudioManager.AUDIOFOCUS_REQUEST_FAILED:
                    if (mp != null && mp.isPlaying()) {
                        mpOnNotificationPosition = mp.getCurrentPosition();
                        mp.pause();
                        mpOnPaused = true;
                        btnPlay.setImageResource(R.drawable.mp_action_play);
                    }
                    break;
            }
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }
    }

    private class HeadeSetIntentReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(Intent.ACTION_HEADSET_PLUG)) {
                int state = intent.getIntExtra("state", -1);
                switch (state) {
                    case 0:
                        Log.e("headesetStatus", "Headset is unplugged");
                        if (mp != null && mp.getCurrentPosition() >= 1000) {
                            mpOnNotificationPosition = mp.getCurrentPosition();
                            mp.pause();
                            mpOnPaused = true;
                            btnPlay.setImageResource(R.drawable.mp_action_play);
                        }
                        break;
                    case 1:
                        Log.e("headesetStatus", "Headset is plugged");
                        if (!isVolumeFading) {
                            new FadePlayerVolume().execute("up");
                        }

                        break;
                    default:
                        Log.e("headesetStatus", "I have no idea what the headset state is");
                }
            }
        }
    }

    private class FadePlayerVolume extends AsyncTask<String, Void, String> {
        private String MODE = "up";
        private float Volume = 1.0f;

        @Override
        protected void onPreExecute() {
            isVolumeFading = true;
            if (mpOnPaused || mp != null && mp.isPlaying()) {
                btnPlay.setImageResource(R.drawable.mp_action_pause);
            } else {
                btnPlay.setImageResource(R.drawable.mp_action_play);
            }
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                MODE = params[0];
                if ("up".equals(MODE)) {
                    Volume = 0f;
                    if (mp != null && mpOnPaused) {
                        mp.seekTo(mpOnNotificationPosition);
                        mp.start();
                    }
                }
                for (int i = 0; i < 10; i++) {
                    if ("up".equals(MODE)) {
                        Volume = Volume + 0.1f;
                    } else {
                        Volume = Volume - 0.1f;
                    }
                    try {
                        Log.e("VOL", "Volume at" + Volume);
                        if (Volume > 1f) {
                            Volume = 1f;
                        } else if (Volume < 0) {
                            Volume = 0;
                        }

                        mp.setVolume(Volume, Volume);
                        Thread.sleep(200);
                    } catch (InterruptedException e) {

                        e.printStackTrace();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            try {
                isVolumeFading = false;
                if (popup != null) {
                    popup.dismiss();
                }

                if ("up".equals(MODE)) {
                    mpOnPaused = false;
                    btnPlay.setImageResource(R.drawable.mp_action_pause);
                } else {
                    if (mp != null && mp.isPlaying()) {
                        mp.pause();
                        mpOnNotificationPosition = mp.getCurrentPosition();
                        btnPlay.setImageResource(R.drawable.mp_action_play);
                        mpOnPaused = true;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    class SwipeGestureListener extends SimpleOnGestureListener implements View.OnTouchListener {
        Context context;
        GestureDetector gDetector;
        private WindowManager.LayoutParams paramsF = params;
        private int initialY, initialX, dx, dy, tapCount = 0;
        private float initialTouchY, initialTouchX;
        private long pressTime = 0, lastPressTime = 0;
        ;

        public SwipeGestureListener(Context context) {
            this(context, null);
        }

        public SwipeGestureListener(Context context, GestureDetector gDetector) {

            if (gDetector == null)
                gDetector = new GestureDetector(context, this);

            this.context = context;
            this.gDetector = gDetector;
        }

        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            try {
                Log.e(DEBUG_TAG, "on fling");
                if ((Math.abs(e1.getY() - e2.getY()) >= 100 && Math.abs(velocityY) >= 200) || (Math.abs(e1.getX() - e2.getX()) >= 80 && Math.abs(velocityX) >= 200)) {
                    onVibrate(200);
                    System.gc();
                    onDestroy();
                }
                return super.onFling(e1, e2, velocityX, velocityY);
            } catch (Exception e) {
                e.printStackTrace();
                onDestroy();
                return false;
            }

        }

        @Override
        public boolean onTouch(View v, MotionEvent event) {

            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    // Get current time in nano seconds.
                    pressTime = System.currentTimeMillis();
                    initialY = paramsF.y;
                    initialTouchY = event.getRawY();
                    tapCount++;
                    initialX = paramsF.x;
                    initialTouchX = event.getRawX();
                    // If double click...
                    if (tapCount >= 2 && (pressTime - lastPressTime) <= 400) {
                        tapCount = 0;
                        mHasDoubleClicked = true;
                        hidePlayerWithNotification();
                    } else {     // If not double click....
                        mHasDoubleClicked = false;
                    }
                    lastPressTime = pressTime;
                    Log.e(DEBUG_TAG, "on down");
                    break;
                case MotionEvent.ACTION_UP:
                    Log.e(DEBUG_TAG, "on up");

                case MotionEvent.ACTION_MOVE:
                    dx = (int) (event.getRawX() - initialTouchX);
                    dy = (int) (event.getRawY() - initialTouchY);
                    paramsF.x = initialX + dx;
                    paramsF.y = initialY + dy;
                    if (windowManager != null) {
                        windowManager.updateViewLayout(NuntuFloatingViewMaximized, paramsF);
                    }
            }
            return gDetector.onTouchEvent(event);
        }

    }

    private boolean onVibrate(long millis) {
        try {
            Vibrator vib = (Vibrator) NuntuMusicPlayerContext.getSystemService(Context.VIBRATOR_SERVICE);
            // Vibrate for 500 milliseconds
            vib.vibrate(millis);
            Log.e("NUNTU VIB", "on vibrate");
            return true;
        } catch (Exception e) {
            e.printStackTrace();

        }
        return false;
    }

}