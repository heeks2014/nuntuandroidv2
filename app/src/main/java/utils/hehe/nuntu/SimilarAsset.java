package utils.hehe.nuntu;

/**
 * Created by gakuba on 4/20/2015.
 */
public class SimilarAsset {


    String name = "",
            itemPublisher = "",
            price = "",
            AssetID = "",
            iconURL = "",
            Status = "",
            Type = "",
            Resource = "";

    public String getAssetName() {
        return name;
    }

    public void setAssetName(String _name) {
        name = _name;
    }

    public String getAssetID() {
        return AssetID;
    }

    public void setAssetID(String id) {
        AssetID = id;
    }

    public String getAssetPublisher() {
        return itemPublisher;
    }

    public void setAssetPublisher(String Publisher) {
        itemPublisher = Publisher;
    }

    public String getAssetResource() {
        return Resource;
    }

    public void setAssetResource(String packname) {
        Resource = packname;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }


    public String getAssetImageURL() {
        return iconURL;
    }

    public void setAssetImageURL(String url) {
        iconURL = url;
    }


    @Override
    public String toString() {

        return "\nid " + AssetID + "\nname " + name + "\npub " + itemPublisher + "\nprice " + price + "\nres " + Resource;

    }

}
