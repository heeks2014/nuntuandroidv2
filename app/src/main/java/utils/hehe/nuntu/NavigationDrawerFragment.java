package utils.hehe.nuntu;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class NavigationDrawerFragment extends Fragment {


    private ArrayList<DrawerItem> DrawerItems;
    private DrawerLayout mDrawerLayout;
    private RecyclerView recyclerView;
    private CustomDrawerAdapter adapter;
    private ActionBarDrawerToggle mDrawerToggle;
    private Context DrawerFragContext;
    private String callingActivityTag;
    private ProgressBar UserHeaderProgress;
    private  SwipeRefreshLayout DrawerRefresher;

    public NavigationDrawerFragment() {

    }

    public void closeDrawer() {
        if (mDrawerLayout != null) {
            mDrawerLayout.closeDrawers();
        }
    }

    public void getDrawerItemsAsync(String route) {

        AsyncTask<String, String, String> getTask = new AsyncTask<String, String, String>() {
            String query = "failed";

            @Override
            protected void onPreExecute() {
                Log.e("FRAG", "started getDrawerItemsAsync for " + callingActivityTag);
                DrawerRefresher.setRefreshing(true);
                UserHeaderProgress.setVisibility(View.VISIBLE);
            }

            @Override
            protected String doInBackground(String... params) {
                String response = "";
                try {

                    URL url = new URL(NuntuConstants.HOST.toString() + params[0]);
                    Log.e("LINK", url.getFile());
                    HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

                    BufferedReader reader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                    String line;

                    while ((line = reader.readLine()) != null) {
                        response += line + "\n";
                    }
                    if (!urlConnection.getContentType().equalsIgnoreCase("application/json")) {
                        Log.e("Bad Content", "Received bad content type while expecting json");
                        return null;
                    }

                    JSONObject obj = new JSONObject(response);
                    query = obj.getString("status");
                    if ("success".equalsIgnoreCase(query)) {

                        if (DrawerItems != null) {
                            DrawerItems.clear();
                        } else {
                            DrawerItems = new ArrayList<>();
                        }
                        try {
                            JSONObject ServerData = new JSONObject(obj.getString("data"));
                            JSONArray Bargain = new JSONArray(ServerData.getString("bargain"));
                            if (Bargain.length() >= 1) {
                                DrawerItems.add(new DrawerItem("BARGAIN", "header"));
                                for (int i = 0; i < Bargain.length(); i++) {
                                    JSONObject entry = Bargain.getJSONObject(i);
                                    String Icon = entry.getString("icon");
                                    Icon = Icon.replace("../", "");
                                    DrawerItems.add(new DrawerItem(entry.getString("name"), entry.getString("type"), entry.getString("id"), NuntuConstants.HOST.toString() + "/" + Icon));
                                }
                            }

                            JSONArray CrazeData = new JSONArray(ServerData.getString("craze"));
                            if (CrazeData.length() >= 1) {
                                DrawerItems.add(new DrawerItem("CRAZE", "header"));
                                for (int i = 0; i < CrazeData.length(); i++) {
                                    JSONObject entry = CrazeData.getJSONObject(i);
                                    String Icon = entry.getString("icon");
                                    Icon = Icon.replace("../", "");
                                    DrawerItems.add(new DrawerItem(entry.getString("name"), entry.getString("type"), entry.getString("id"), NuntuConstants.HOST.toString() + "/" + Icon));
                                }
                            }

                            JSONArray HintData = new JSONArray(ServerData.getString("hint"));
                            if (HintData.length() >= 1) {
                                DrawerItems.add(new DrawerItem("HINT", "header"));
                                for (int i = 0; i < HintData.length(); i++) {
                                    JSONObject entry = HintData.getJSONObject(i);
                                    String Icon = entry.getString("icon");
                                    Icon = Icon.replace("../", "");
                                    DrawerItems.add(new DrawerItem(entry.getString("name"), entry.getString("type"), entry.getString("id"), NuntuConstants.HOST.toString() + "/" + Icon));
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        DrawerItems.add(new DrawerItem("APPLICATION", "header"));
                        DrawerItems.add(new DrawerItem("Settings", "local", "0", android.R.drawable.ic_menu_info_details));
                        DrawerItems.add(new DrawerItem("About", "local", "0", android.R.drawable.ic_menu_info_details));
                        DrawerItems.add(new DrawerItem("Help", "local", "0", android.R.drawable.ic_menu_info_details));
                        if (Integer.valueOf(Nuntu.UserID) >= 1) {
                            DrawerItems.add(new DrawerItem("Logout", "local", "0", android.R.drawable.ic_menu_info_details));
                        } else {
                            DrawerItems.add(new DrawerItem("Login", "local", "0", android.R.drawable.ic_menu_info_details));
                        }
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e("EXC", "An exception occured while getting data" + e.getMessage());
                }


                return response;


            }

            protected void onPostExecute(String result) {
                super.onPostExecute(result);
                try {
                    DrawerRefresher.setRefreshing(false);
                    UserHeaderProgress.setVisibility(View.INVISIBLE);

                    if ("success".equalsIgnoreCase(query) && DrawerItems != null && DrawerItems.size() >= 1) {
                        adapter.refreshDrawer(DrawerItems, callingActivityTag);
                        if (recyclerView != null) {
                            recyclerView.setAdapter(adapter);
                        }
                    } else {
                        Toast.makeText(DrawerFragContext, "Couldn't get specials from Nuntu", Toast.LENGTH_SHORT).show();
                    }

                } catch (Exception exc) {
                    exc.printStackTrace();
                    Log.e("LAYOUTERROR", "An exception thrown at post ex " + exc.getMessage());
                }
            }
        };
        getTask.execute(route);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DrawerFragContext = getActivity().getApplicationContext();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        try {
            DrawerItems = new ArrayList<>();
            DrawerItems.add(new DrawerItem("APPLICATION", "header"));
            DrawerItems.add(new DrawerItem("Settings", "local", "0", android.R.drawable.ic_menu_info_details));
            DrawerItems.add(new DrawerItem("About", "local", "0", android.R.drawable.ic_menu_info_details));
            DrawerItems.add(new DrawerItem("Help", "local", "0", android.R.drawable.ic_menu_info_details));
            View layout = inflater.inflate(R.layout.fragment_navigation_drawer, container, false);
            recyclerView = (RecyclerView) layout.findViewById(R.id.drawerList);
            adapter = new CustomDrawerAdapter(getActivity(), NavigationDrawerFragment.this, DrawerItems, callingActivityTag);
            recyclerView.setAdapter(adapter);
            recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

            return layout;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } catch (OutOfMemoryError e) {
            e.printStackTrace();
            return null;
        }
    }

    public void setUp(int fragmentId, DrawerLayout drawerLayout, Toolbar toolbar, ProgressBar pb, SwipeRefreshLayout par, String callingActivity) {
        mDrawerLayout = drawerLayout;
        UserHeaderProgress = pb;
        DrawerRefresher = par;
        callingActivityTag = callingActivity;
        mDrawerToggle = new ActionBarDrawerToggle(getActivity(), drawerLayout, toolbar, R.string.drawer_open, R.string.drawer_close) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                getActivity().invalidateOptionsMenu();

            }

            @Override
            public void onConfigurationChanged(Configuration newConfig) {
                super.onConfigurationChanged(newConfig);
                mDrawerToggle.onConfigurationChanged(newConfig);
            }

            @Override
            public boolean onOptionsItemSelected(MenuItem item) {
                // Pass the event to ActionBarDrawerToggle, if it returns
                // true, then it has handled the app icon touch event
                return mDrawerToggle.onOptionsItemSelected(item);
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);

                getActivity().invalidateOptionsMenu();

            }
        };
        mDrawerToggle.setDrawerIndicatorEnabled(true);
        mDrawerToggle.setHomeAsUpIndicator(R.drawable.menu);

        mDrawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerLayout.post(new Runnable() {
            @Override
            public void run() {
                mDrawerToggle.syncState();
            }
        });

        try {
            getDrawerItemsAsync("/api/mobile/drawer/Android");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
