package utils.hehe.nuntu;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;
import android.app.Activity;

public class UserAppActivity extends Activity {

	static Context context;
	ListView userapplist;
	ProgressBar loadingUserApps;
	final String HOST="http://nuntu.hehelabs.com";
	private String userID="0";
	private String IMEI;
	private ArrayList<UserApp> userApps;
	UserAppBaseAdapter userappsadapter;
	Handler handler ;
	TimerTask updateButtonsTask;
	Timer timer;
	NuntuBroadcastReceiver broadCastReceiver=null;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.my_apps_list_layout);
		loadingUserApps =(ProgressBar)findViewById(R.id.userAppProgressBar);
		userapplist= (ListView)findViewById(R.id.user_applist_layout);
		context = this;
		IMEI=new Device().getPhoneIMEI(context);
		handler =new Handler();
		loadingUserApps.setVisibility(View.VISIBLE);
		loadingUserApps.setIndeterminate(true);					 	
		try {
			SharedPreferences prefs = getSharedPreferences("nuntuprefs", Context.MODE_PRIVATE);
			String VaultData= prefs.getString("nuntuvault"," "); 
			if(VaultData.length()>=15) {
				String UserCred=new NuntuCrypto().decrypt(VaultData);						
				JSONObject obj = new JSONObject(UserCred.toString());			
				userID=obj.getString("userid");					
			}

		} catch (Exception e) {		
			e.printStackTrace();
		}
		if(userID!=null&&0<Integer.valueOf(userID)) {
			if(hasConnection()) {			
				getuserApps(HOST+"/data/userapp/android/"+userID+"/"+IMEI);
			}else {
				new WaitForEnable().execute("10000");
			}
		}else {
			Toast.makeText(this,"your nuntu vault could not be opened",Toast.LENGTH_LONG).show();
		}

	}


	public static boolean hasConnection() {
		final ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

		final NetworkInfo wifiNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
		if (wifiNetwork != null && wifiNetwork.isConnected()) {
			return true;
		}

		final NetworkInfo mobileNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
		if (mobileNetwork != null && mobileNetwork.isConnected()) {
			return true;
		}

		final NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
		if (activeNetwork != null && activeNetwork.isConnected()) {
			return true;
		}

		return false;
	}

	public class WaitForEnable extends AsyncTask<String, Void, Void>{
		ProgressDialog wait;
		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			try {
				setMobileDataEnabled(context,true);
			}catch(Exception e) {
				e.printStackTrace();
			}

			runOnUiThread(new Runnable() {		
				@Override
				public void run() 
				{
					wait =new ProgressDialog(context);
					if(wait!=null) {
						wait.setCancelable(true);
						wait.setMessage("connecting to nuntu...");
						wait.show();
					}
				}});
		}


		@Override
		protected Void doInBackground(String... params) {

			try {

				Log.e("WAIIIT", "Going to sleep for "+params[0]+" milliseconds");
				Thread.sleep(Integer.valueOf(params[0]));
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			if(wait!=null) {
				wait.dismiss();
			}
			//Update your here
			super.onPostExecute(result);
			getuserApps(HOST+"/data/userapp/android/"+userID);
		}
	}

	@SuppressWarnings("rawtypes")
	private void setMobileDataEnabled(Context context, boolean enabled) throws ClassNotFoundException, NoSuchFieldException, NoSuchMethodException {
		try {
			final ConnectivityManager conman = (ConnectivityManager)  context.getSystemService(Context.CONNECTIVITY_SERVICE);		
			final Class conmanClass = Class.forName(conman.getClass().getName());
			final Field connectivityManagerField = conmanClass.getDeclaredField("mService");
			connectivityManagerField.setAccessible(true);
			final Object connectivityManager = connectivityManagerField.get(conman);
			final Class connectivityManagerClass =  Class.forName(connectivityManager.getClass().getName());
			@SuppressWarnings("unchecked")
			final Method setMobileDataEnabledMethod = connectivityManagerClass.getDeclaredMethod("setMobileDataEnabled", Boolean.TYPE);
			setMobileDataEnabledMethod.setAccessible(true);


			setMobileDataEnabledMethod.invoke(connectivityManager, enabled);
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void getuserApps(final String URL) {
		AsyncTask<String,String,String> getTask = new AsyncTask<String,String,String>(){
			String query,details="we could not reach nuntu";
			@Override
			protected void onPreExecute() {
				if(loadingUserApps!=null) {
					loadingUserApps.setVisibility(View.VISIBLE);
				}
			}

			@Override
			protected String doInBackground(String... params) {
				String response = "";
				try{

					URL url = new URL(params[0].trim());
					HttpURLConnection urlConnection = (HttpURLConnection)url.openConnection();
					urlConnection.addRequestProperty("User-Agent",IMEI);
					urlConnection.addRequestProperty("mode","all");					
					Log.e("HTTP",urlConnection.getResponseMessage());
					BufferedReader reader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
					String line = "";					
					while((line = reader.readLine()) != null){
						response += line + "\n";			
					}               
					if(!urlConnection.getContentType().toLowerCase(Locale.ENGLISH).equals("application/json")) {
						Log.e("Bad Content","Received bad content type while expecting json");
					}	   

					Log.e("APPS",response);
					if(response!=null&&response.length()>=5) {
						JSONObject obj = new JSONObject(response.toString());	                                
						query=obj.getString("status");
						details=obj.getString("data");						
						if("success".equalsIgnoreCase(query)) {
							userApps=new ArrayList<UserApp>();
							JSONArray App = new JSONArray(details);
							for(int i=0;i<App.length();i++) {
								JSONObject app=App.getJSONObject(i);
								UserApp thisApp=new UserApp();
								thisApp.setUserAppName(app.getString("name"));
								thisApp.setUserAppID(app.getString("appid"));
								thisApp.setUserAppIcon(HOST+"/"+app.getString("icon").replace("../",""));
								thisApp.setUserAppVersion("Version: "+app.getString("version"));
								thisApp.setUserAppVersionCode(app.getString("versioncode"));
								int versioncode=Integer.valueOf(app.getString("versioncode"));
								String packagename=app.getString("packagename").replace("'","").trim();
								thisApp.setUserAppPackage(packagename);
								thisApp.setUserAppDownloadID(app.getString("downloadID"));
								thisApp.setUserAppDownloadKey(app.getString("downloadKey"));
								int installedversioncode=getInstallPackageVersionInfo(packagename);
								if(installedversioncode!=-1){
									if(versioncode>installedversioncode) {
										thisApp.setUserAppStatus("4");
									}else {
										thisApp.setUserAppStatus("1");
									}
								}else  {
									if("2".equals(app.getString("status"))||"0".equals(app.getString("status"))) {
										thisApp.setUserAppStatus(app.getString("status"));
									}else {
										thisApp.setUserAppStatus("0");	
									}
								}

								userApps.add(thisApp);
							}
						}else {
							AlertDialog.Builder alertbox = new AlertDialog.Builder(context);
							alertbox.setTitle(query).setMessage(details);
							alertbox.setNeutralButton("OK", new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface arg0, int arg1) {
									arg0.dismiss();
								}
							});
						}
					}
				}
				catch (Exception e){
					e.printStackTrace();
					Log.e("EXC","An exception occured while getting app data"+e.getMessage());	
				}
				return response;


			}

			protected void onPostExecute(String result) {
				super.onPostExecute(result);
				loadingUserApps.setVisibility(View.GONE);
				if(userApps!=null&&userApps.size()>=1){					
					userappsadapter = new UserAppBaseAdapter(context,userApps,userID);		
					userapplist.setAdapter(userappsadapter);	
					registerAppsUpdator(userApps,userapplist);
					broadCastReceiver=new NuntuBroadcastReceiver();
					registerBroadcastReceiver();
				}
			}
		};
		getTask.execute(URL);
	} 

	private void registerAppsUpdator(final List<UserApp> appList,final ListView applist) {
		timer = new Timer();
		updateButtonsTask = new TimerTask() {
			public void run() {
				handler.post(new Runnable() {
					public void run() {
						System.gc();
						boolean isInstalled=false;
						Log.e("SHITTT UP","before update");
						List<PackageInfo> packages = context.getPackageManager().getInstalledPackages(0);
						ArrayList<UserApp>apps = new ArrayList<UserApp>(new LinkedHashSet<UserApp>(appList));
						if(apps!=null&&apps.size()>=1) {
							for(int i=0;i<apps.size();i++){
								for(PackageInfo pack : packages) {
									if(apps.get(i).getUserAppPackage().equalsIgnoreCase(pack.packageName)) {
										if(Integer.valueOf(apps.get(i).getUserAppVersionCode())>pack.versionCode) {
											apps.get(i).setUserAppStatus("4");	
										}else {
											apps.get(i).setUserAppStatus("1");
										}
										isInstalled=true;
										break;
									}else {
										isInstalled=false;
									}
								}
								if(!isInstalled&&("1".equals(apps.get(i).getUserAppStatus())||"4".equals(apps.get(i).getUserAppStatus()))) {
									apps.get(i).setUserAppStatus("3");
								}
								Log.e("SHITTT UP",apps.get(i).getUserAppStatus());
							}   
							userappsadapter.updateReceiptsList(apps);
							userappsadapter.notifyDataSetChanged();	
							
						}
					}
				}); 
			}
		};

		timer.schedule(updateButtonsTask,10000,5000);			
	}


	private int getInstallPackageVersionInfo(String appPackageName) {       

		List<PackageInfo> packs = context.getPackageManager().getInstalledPackages(0);
		int version=-1;
		for(int i=0;i<packs.size();i++) 
		{
			PackageInfo p = packs.get(i);
			if(p.packageName.equalsIgnoreCase(appPackageName.trim())){
				version= p.versionCode;	
				break;							
			}
		}
		return version;
	}



	@SuppressWarnings("deprecation")
	public void registerBroadcastReceiver() {	
		try {

			IntentFilter intentFilter = new IntentFilter();
			intentFilter.addAction(Intent.ACTION_PACKAGE_ADDED);
			intentFilter.addAction(Intent.ACTION_PACKAGE_INSTALL);
			intentFilter.addAction(Intent.ACTION_PACKAGE_REMOVED);
			intentFilter.addAction(Intent.ACTION_PACKAGE_REPLACED);
			intentFilter.addDataScheme("package");
			intentFilter.setPriority(999);
			context.registerReceiver(broadCastReceiver,intentFilter);  	 
		}catch(Exception exc) {
			Toast.makeText(context, "Exception caught: "+exc.getMessage(),Toast.LENGTH_SHORT).show();	
		}
	}
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		if(timer!=null) {
			timer.cancel();
		}
		if(updateButtonsTask!=null) {
			updateButtonsTask.cancel();
		}
		try {
			if(broadCastReceiver!=null) {
				context.unregisterReceiver(broadCastReceiver);
				Log.e("OK BROAD"," here is the broadcasting being unre");
			}
		}catch(Exception e) {
			e.printStackTrace();
		}

		this.finish(); 	  
	}

}
