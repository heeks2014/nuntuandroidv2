package utils.hehe.nuntu;

public class Comment {

    private String author, date, title, message, avatarUrl;

    public void setCommentTitle(String titre) {
        this.title = titre;
    }

    public String getCommentAuthor() {
        return this.author;
    }

    public void setCommentAuthor(String name) {
        this.author = name;
    }

    public String getCommentDate() {
        return this.date;
    }

    public void setCommentDate(String date) {
        this.date = date;
    }

    public String getCommentTile() {
        return this.title;
    }

    public String getCommentBody() {
        return this.message;
    }

    public void setCommentBody(String body) {
        this.message = body;
    }

    public String getCommentorAvatarUrl() {
        return this.avatarUrl;
    }

    public void setCommentorAvatarUrl(String url) {
        this.avatarUrl = url;
    }

}
