package utils.hehe.nuntu;


import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.PowerManager;
import android.provider.Settings;
import android.provider.Settings.SettingNotFoundException;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.webkit.MimeTypeMap;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Locale;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

// Need the following import to get access to the app resources, since this
// class is in a sub-package.


public class NuntuTransactDaemon extends Service {
    final String HOST = "http://nuntu.hehelabs.com";
    private final String TAG = "PACKAGE MANAGER: ";
    private final int AppNotID = 120, AlbumNotID = 121, BookNotID = 122, TrackNotID = 123, PendingNotID = 124;
    public String packURI, ToInstallPackageName;
    public int pendingDownloads = 0;
    Device SysCall = null;
    NuntuFile filesys = null;
    NotificationManager AppNotificationManager = null, AlbumNotificationManager = null,
            BookNotificationManager = null, TrackNotificationManager = null, PendingNotificationManager = null;
    NotificationCompat.Builder AppBuilder = null, AlbumBuilder = null, BookBuilder = null, TrackBuilder = null, PendingBuilder = null;
    Notification forForeground = null;
    int AlbumLocaltracks = 0;
    long DownloadedBookSize = 0;
    String AlbumLocation = "/nuntu/Music", NuntuMountedPlace = " ";
    Timer t = null;
    TimerTask updatePendingCount = null;
    LinkedList<Map<String, ArrayList<String>>> DownloadingApps = null;
    LinkedList<Map<String, ArrayList<String>>> DownloadingBooks = null;
    LinkedList<Map<String, ArrayList<String>>> DownloadingAlbums = null;
    LinkedList<Map<String, ArrayList<String>>> DownloadingTracks = null;
    LinkedList<Map<String, ArrayList<String>>> InstallingApps = null;
    ArrayList<String> pendingAppParams = null;
    ArrayList<String> pendingInstallParams = null;
    ArrayList<String> pendingBookParams = null;
    ArrayList<String> pendingAlbumParams = null;
    ArrayList<String> pendingTrackParams = null;
    String PhoneIMEI;
    String currentAppID, currentBookID, currentAlbumID, currentTrackID;
    private Context downloadContext;
    private String action = "none";
    private String PlayTrackLocation = "", ReadBookLocation = "", ReadBookType = "application/pdf";
    private boolean isFirstTrack = true, isFirstApp = true, isFirstAlbum = true, isFirstBook = true;

    public static long getFolderSize(File f) {
        long size = 0;
        if (f.isDirectory()) {
            for (File file : f.listFiles()) {
                size += getFolderSize(file);
            }
        } else {
            size = f.length();
        }
        return size;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        if (downloadContext == null) {
            downloadContext = getApplicationContext();
        }
        if (SysCall == null) {
            SysCall = new Device();
        }

        PhoneIMEI = SysCall.getPhoneIMEI(downloadContext);
        Log.e("TROUBLE", "this phone imei " + PhoneIMEI);

        if (filesys == null) {
            filesys = new NuntuFile();
        }
        if (forForeground == null) {
            forForeground = new Notification();
        }
        if (DownloadingApps == null) {
            DownloadingApps = new LinkedList<>();
        }
        if (downloadContext == null) {
            DownloadingApps = new LinkedList<>();
        }
        if (DownloadingBooks == null) {
            DownloadingBooks = new LinkedList<>();
        }
        if (DownloadingAlbums == null) {
            DownloadingAlbums = new LinkedList<>();
        }
        if (DownloadingTracks == null) {
            DownloadingTracks = new LinkedList<>();
        }


        if (InstallingApps == null) {
            InstallingApps = new LinkedList<>();
        }

        if (AppNotificationManager == null) {
            AppNotificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        }
        if (AlbumNotificationManager == null) {
            AlbumNotificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        }
        if (BookNotificationManager == null) {
            BookNotificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        }
        if (TrackNotificationManager == null) {
            TrackNotificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        }
        if (PendingNotificationManager == null) {
            PendingNotificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        }
        if (AppBuilder == null) {
            AppBuilder = new NotificationCompat.Builder(downloadContext);
        }
        if (AlbumBuilder == null) {
            AlbumBuilder = new NotificationCompat.Builder(downloadContext);
        }
        if (BookBuilder == null) {
            BookBuilder = new NotificationCompat.Builder(downloadContext);
        }
        if (TrackBuilder == null) {
            TrackBuilder = new NotificationCompat.Builder(downloadContext);
        }
        if (PendingBuilder == null) {
            PendingBuilder = new NotificationCompat.Builder(downloadContext);
        }

        registerPendingDownloadsUpdator();
        flushNuntuAppsFolder();

        System.setProperty("http.keepAlive", "false");
    }

    @Override
    public void onStart(Intent intent, int startid) {
        handleCommand(intent);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        handleCommand(intent);
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        // Make sure our notification is gone.
        super.onDestroy();
        stopForegroundCompat("....");

    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onLowMemory() {

        onDestroy();
    }

    void handleCommand(Intent intent) {
        try {
            if (intent != null) {
                action = intent.getStringExtra("action");
                if ("download".equalsIgnoreCase(action)) {
                    if ("album".equalsIgnoreCase(intent.getStringExtra("type"))) {
                        if (allowToQueue("music", intent.getStringExtra("ID"))) {
                            Log.e("BEFORE", "" + pendingDownloads);
                            pendingDownloads++;
                            Log.e("AFTER", "" + pendingDownloads);

                            String AlbumName = intent.getStringExtra("Name");
                            pendingAlbumParams = new ArrayList<>();
                            pendingAlbumParams.add(intent.getStringExtra("ID"));
                            pendingAlbumParams.add(AlbumName);
                            pendingAlbumParams.add(intent.getStringExtra("link"));
                            pendingAlbumParams.add(intent.getStringExtra("tracks"));
                            Map<String, ArrayList<String>> thisAlbum = new HashMap<>();
                            thisAlbum.put("music", pendingAlbumParams);
                            if (Build.VERSION.SDK_INT >= 9) {
                                DownloadingAlbums.push(thisAlbum);
                            } else {
                                DownloadingAlbums.add(thisAlbum);
                            }

                            if (isFirstAlbum) {
                                createAlbumNotification(AlbumName, "wait for " + AlbumName + " to be  downloaded");
                            }
                            DownloadAlbumFile();

                        }
                    } else if ("app".equalsIgnoreCase(intent.getStringExtra("type"))) {
                        if (allowToQueue("app", intent.getStringExtra("ID"))) {
                            Log.e("BEFORE", "" + pendingDownloads);
                            pendingDownloads++;
                            Log.e("AFTER", "" + pendingDownloads);
                            pendingAppParams = new ArrayList<>();
                            pendingAppParams.add(intent.getStringExtra("ID"));
                            pendingAppParams.add(intent.getStringExtra("Name"));
                            pendingAppParams.add(intent.getStringExtra("resource").toLowerCase(Locale.ENGLISH));
                            pendingAppParams.add(intent.getStringExtra("link"));
                            Map<String, ArrayList<String>> thisApp = new HashMap<>();
                            thisApp.put("app", pendingAppParams);
                            if (Build.VERSION.SDK_INT >= 9) {
                                DownloadingApps.push(thisApp);
                            } else {
                                DownloadingApps.add(thisApp);
                            }
                            if (isFirstApp) {
                                createAppNotification("Downloading app ", "downloading " + intent.getStringExtra("appName") + "...");
                            }
                            DownloadAppFile();
                        }
                    } else if ("track".equalsIgnoreCase(intent.getStringExtra("type"))) {
                        if (allowToQueue("track", intent.getStringExtra("ID"))) {
                            Log.e("BEFORE", "" + pendingDownloads);
                            pendingDownloads++;
                            Log.e("AFTER", "" + pendingDownloads);
                            pendingTrackParams = new ArrayList<>();
                            pendingTrackParams.add(intent.getStringExtra("ID"));
                            pendingTrackParams.add(intent.getStringExtra("Name"));
                            pendingTrackParams.add(intent.getStringExtra("link"));
                            Map<String, ArrayList<String>> thisTrack = new HashMap<>();
                            thisTrack.put("track", pendingTrackParams);
                            if (Build.VERSION.SDK_INT >= 9) {
                                DownloadingTracks.push(thisTrack);
                            } else {
                                DownloadingTracks.add(thisTrack);
                            }

                            if (isFirstTrack) {
                                createTrackNotification("Downloading song ", "downloading " + intent.getStringExtra("trackName") + "...");
                            }
                            DownloadTrackFile();
                        }

                    } else if ("book".equalsIgnoreCase(intent.getStringExtra("type"))) {
                        if (allowToQueue("book", intent.getStringExtra("ID"))) {
                            Log.e("BEFORE", "" + pendingDownloads);
                            pendingDownloads++;
                            Log.e("AFTER", "" + pendingDownloads);
                            pendingBookParams = new ArrayList<>();
                            pendingBookParams.add(intent.getStringExtra("ID"));
                            pendingBookParams.add(intent.getStringExtra("Name"));
                            pendingBookParams.add(intent.getStringExtra("link"));
                            Map<String, ArrayList<String>> thisBook = new HashMap<>();
                            thisBook.put("book", pendingBookParams);
                            if (Build.VERSION.SDK_INT >= 9) {
                                DownloadingBooks.push(thisBook);
                            } else {
                                DownloadingBooks.add(thisBook);
                            }

                            if (isFirstBook) {
                                createBookNotification("Downloading book ", "downloading " + intent.getStringExtra("bookName") + "...");
                            }
                            DownloadBookFile();
                        }

                    } else {
                        onDestroy();
                    }
                } else if ("autodestruct".equalsIgnoreCase(action)) {
                    onDestroy();
                }

            } else {
                onDestroy();
            }
        } catch (Exception exc) {
            onDestroy();
        }
    }

    private boolean allowToQueue(String type, String appPackage) {
        if ("app".equalsIgnoreCase(type)) {
            if (!isFirstApp && appPackage.trim().equalsIgnoreCase(currentAppID.trim())) {
                return false;
            }
            for (int i = 0; i < DownloadingApps.size(); i++) {
                String appID = DownloadingApps.get(i).get("app").get(0);
                if (appPackage.trim().equalsIgnoreCase(appID.trim())) {
                    Log.e("NOQUEUE", " app " + appID + " already into queue");
                    return false;

                }

            }
        } else if ("book".equalsIgnoreCase(type)) {

            if (!isFirstBook && appPackage.trim().equalsIgnoreCase(currentBookID.trim())) {
                return false;
            }
            for (int i = 0; i < DownloadingBooks.size(); i++) {
                String appPack = DownloadingBooks.get(i).get("book").get(0);
                if (appPackage.trim().equalsIgnoreCase(appPack.trim())) {
                    Log.e("NOQUEUE", " book " + appPack + " already into queue");
                    return false;
                }

            }
        } else if ("music".equalsIgnoreCase(type)) {

            if (!isFirstAlbum && appPackage.trim().equalsIgnoreCase(currentAlbumID.trim())) {
                return false;
            }
            for (int i = 0; i < DownloadingAlbums.size(); i++) {
                String appPack = DownloadingAlbums.get(i).get("music").get(0);
                if (appPackage.trim().equalsIgnoreCase(appPack.trim())) {
                    Log.e("NOQUEUE", " album " + appPack + " already into queue");
                    return false;
                }

            }
        } else if ("track".equalsIgnoreCase(type)) {

            if (!isFirstTrack && appPackage.trim().equalsIgnoreCase(currentTrackID.trim())) {
                return false;
            }

            for (int i = 0; i < DownloadingTracks.size(); i++) {
                String appPack = DownloadingTracks.get(i).get("track").get(0);
                if (appPackage.trim().equalsIgnoreCase(appPack.trim())) {
                    Log.e("NOQUEUE", " track " + appPack + " already into queue");
                    return false;
                }

            }
        }

        return true;
    }

    private void registerPendingDownloadsUpdator() {

        if (t != null) {
            t.cancel();
        }

        t = new Timer();

        if (updatePendingCount != null) {
            updatePendingCount.cancel();
        }

        updatePendingCount = new TimerTask() {
            public void run() {

                if (pendingDownloads > 1) {
                    try {
                        PendingBuilder.setContentTitle("NUNTU")
                                .setContentText("(" + pendingDownloads + ")" + "Pending downloads")
                                .setSmallIcon(R.drawable.download);
                        PendingNotificationManager.notify(PendingNotID, PendingBuilder.build());
                    } catch (Exception exc) {
                        Log.e("PENDUP", "Exception ccurred while updating downloads pending count" + exc.getMessage());
                    }
                } else {
                    if (PendingNotificationManager != null) {
                        PendingNotificationManager.cancel(PendingNotID);
                    }

                }

            }
        };
        t.schedule(updatePendingCount, 1000, 1000);
    }

    public void createAppNotification(String title, String message) {
        PendingIntent dummy = PendingIntent.getActivity(getApplicationContext(),
                0, new Intent(), PendingIntent.FLAG_UPDATE_CURRENT);
        AppBuilder.setOngoing(false)
                .setProgress(100, 0, false)
                .setContentTitle(title)
                .setContentText(message)
                .setSmallIcon(R.drawable.download)
                .setContentIntent(dummy);
        AppNotificationManager.notify(AppNotID, AppBuilder.build());

    }

    public void createBookNotification(String title, String message) {
        BookBuilder.setOngoing(false)
                .setProgress(100, 0, false)
                .setContentTitle(title)
                .setContentText(message)
                .setSmallIcon(R.drawable.download);
        BookNotificationManager.notify(BookNotID, BookBuilder.build());

    }

    public void editAppNotification(String status, String title, String message) {
        if (AppBuilder != null) {
            AppBuilder.setContentTitle(title)
                    .setContentText(message)
                    .setProgress(0, 0, false);
            if ("failed".equalsIgnoreCase(status)) {
                AppBuilder.setAutoCancel(true);
                AppBuilder.setSmallIcon(android.R.drawable.ic_dialog_alert);
            } else {
                AppBuilder.setAutoCancel(true);
                AppBuilder.setSmallIcon(R.drawable.download);
            }

            AppNotificationManager.notify(AppNotID, AppBuilder.build());
        }
    }

    public void editBookNotification(String status, String title, String message) {
        if (BookBuilder != null) {
            BookBuilder.setContentTitle(title)
                    .setContentText(message)
                    .setProgress(0, 0, false);
            if ("failed".equalsIgnoreCase(status)) {
                BookBuilder.setSmallIcon(android.R.drawable.ic_dialog_alert);
            } else {
                File thisBook = new File(ReadBookLocation);
                long localsize = thisBook.length();
                if (localsize < DownloadedBookSize) {
                    BookBuilder.setSmallIcon(android.R.drawable.ic_dialog_alert);
                    BookBuilder.setAutoCancel(true);
                    BookBuilder.setContentTitle("downloaded failed");
                    BookBuilder.setContentText("downloaded file is corrupted");
                    Log.e("BOOK FILE ERROR", ReadBookLocation + "...local " + localsize + " server size " + DownloadedBookSize);
                } else {
                    BookBuilder.setAutoCancel(true);
                    BookBuilder.setSmallIcon(R.drawable.download);
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setDataAndType(Uri.fromFile(thisBook), ReadBookType);
                    PendingIntent pi = PendingIntent.getActivity(getApplicationContext(), 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
                    BookBuilder.setContentIntent(pi);
                }
            }

            BookNotificationManager.notify(BookNotID, BookBuilder.build());
        }
    }

    public void AppprogressUpdate(String appname, int percentageComplete, final int id) {

        AppBuilder.setContentText("getting " + appname + " installation data...  " + percentageComplete + "%");
        AppBuilder.setProgress(100, percentageComplete, false);
        AppNotificationManager.notify(id, AppBuilder.build());
    }

    public void BookprogressUpdate(String bookname, int percentageComplete, final int id) {

        BookBuilder.setContentText(bookname + " ... (" + percentageComplete + " %)");
        BookBuilder.setProgress(100, percentageComplete, false);
        BookNotificationManager.notify(id, BookBuilder.build());
    }

    public void TrackprogressUpdate(String trackname, int percentageComplete, final int id) {

        TrackBuilder.setContentText("downloading " + trackname + " (" + percentageComplete + "%)");
        TrackBuilder.setProgress(100, percentageComplete, false);
        TrackNotificationManager.notify(TrackNotID, TrackBuilder.build());
    }

    public void finishAppProgress() {
        AudioManager audio = (AudioManager) downloadContext.getSystemService(Context.AUDIO_SERVICE);
        switch (audio.getRingerMode()) {
            case AudioManager.RINGER_MODE_NORMAL:
                playSoundNotification();
                AppBuilder.setProgress(0, 0, false);
                AppBuilder.setContentText("Please confirm application installation...");
                AppNotificationManager.notify(AppNotID, AppBuilder.build());
                break;
            case AudioManager.RINGER_MODE_SILENT:
                break;
            case AudioManager.RINGER_MODE_VIBRATE:
                AppBuilder.setVibrate(new long[]{1000L});
                AppBuilder.setProgress(0, 0, false);
                AppBuilder.setContentText("Please confirm application installation...");
                AppNotificationManager.notify(AppNotID, AppBuilder.build());
                break;
        }

    }

    public void createAlbumNotification(String title, String message) {
        AlbumBuilder.setOngoing(false)
                .setProgress(100, 0, false)
                .setContentTitle(title)
                .setContentText(message)
                .setSmallIcon(R.drawable.download);
        AlbumNotificationManager.notify(AlbumNotID, AlbumBuilder.build());

    }

    public void editAlbumNotification(String status, String title, String message) {
        if (AlbumBuilder != null) {
            AlbumBuilder.setContentTitle(title);
            AlbumBuilder.setContentText(message);
            if ("failed".equalsIgnoreCase(status)) {
                AlbumBuilder.setSmallIcon(android.R.drawable.ic_dialog_alert);
            } else if ("next".equalsIgnoreCase(status)) {
                AlbumBuilder.setSmallIcon(R.drawable.download);
            } else {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setDataAndType(Uri.fromFile(new File(AlbumLocation)), "audio/*");
                PendingIntent pi = PendingIntent.getActivity(getApplicationContext(), 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
                AlbumBuilder.setProgress(0, 0, false);
                AlbumBuilder.setContentIntent(pi);
                AlbumBuilder.setSmallIcon(R.drawable.download);
                AlbumBuilder.setAutoCancel(true);
            }

            AlbumNotificationManager.notify(AlbumNotID, AlbumBuilder.build());
        }
    }

    public void AlbumprogressUpdate(String albumname, int percentageComplete, final int id) {

        AlbumBuilder.setContentText("downloading " + albumname + "..." + percentageComplete + "%");
        AlbumBuilder.setProgress(100, percentageComplete, false);
        AlbumNotificationManager.notify(id, AlbumBuilder.build());
    }

    public void finishAlbumProgress() {
        AudioManager audio = (AudioManager) downloadContext.getSystemService(Context.AUDIO_SERVICE);
        switch (audio.getRingerMode()) {
            case AudioManager.RINGER_MODE_NORMAL:
                playSoundNotification();
                AlbumBuilder.setProgress(0, 0, false);
                AlbumBuilder.setContentText("Preparing your album songs...");
                AlbumNotificationManager.notify(AlbumNotID, AlbumBuilder.build());
                break;
            case AudioManager.RINGER_MODE_SILENT:
                AlbumBuilder.setProgress(0, 0, false);
                AlbumBuilder.setContentText("Preparing your album songs...");
                AlbumNotificationManager.notify(AlbumNotID, AlbumBuilder.build());
                break;
            case AudioManager.RINGER_MODE_VIBRATE:
                AlbumBuilder.setVibrate(new long[]{1000L});
                AlbumBuilder.setProgress(0, 0, false);
                AlbumBuilder.setContentText("Preparing your album songs...");
                AlbumNotificationManager.notify(AlbumNotID, AlbumBuilder.build());
                break;
        }

    }

    public void finishBookProgress() {
        AudioManager audio = (AudioManager) downloadContext.getSystemService(Context.AUDIO_SERVICE);
        switch (audio.getRingerMode()) {
            case AudioManager.RINGER_MODE_NORMAL:
                playSoundNotification();
                BookBuilder.setContentTitle("book download finished");
                BookBuilder.setProgress(0, 0, false);
                BookBuilder.setContentText("Browse to your documents folder or refresh nuntu books page");
                BookNotificationManager.notify(BookNotID, BookBuilder.build());
                break;
            case AudioManager.RINGER_MODE_SILENT:
                BookBuilder.setContentTitle("book download finished");
                BookBuilder.setProgress(0, 0, false);
                BookBuilder.setContentText("Browse to your documents folder or refresh nuntu books page");
                BookNotificationManager.notify(BookNotID, BookBuilder.build());
                break;
            case AudioManager.RINGER_MODE_VIBRATE:
                BookBuilder.setVibrate(new long[]{1000L});
                BookBuilder.setContentTitle("book download finished");
                BookBuilder.setProgress(0, 0, false);
                BookBuilder.setContentText("Browse to your documents folder or refresh nuntu books page");
                BookNotificationManager.notify(BookNotID, BookBuilder.build());
                break;
        }

    }

    public void finishTrackProgress() {
        try {
            AudioManager audio = (AudioManager) downloadContext.getSystemService(Context.AUDIO_SERVICE);
            switch (audio.getRingerMode()) {
                case AudioManager.RINGER_MODE_NORMAL:
                    TrackBuilder.setProgress(0, 0, false);
                    TrackBuilder.setContentText("Finished song download");
                    break;
                case AudioManager.RINGER_MODE_SILENT:
                    break;
                case AudioManager.RINGER_MODE_VIBRATE:
                    TrackBuilder.setVibrate(new long[]{1000L});
                    TrackBuilder.setProgress(0, 0, false);
                    TrackBuilder.setContentText("Finished song download");
                    break;
            }
            TrackNotificationManager.notify(TrackNotID, TrackBuilder.build());
        } catch (Exception exc) {
            Log.e("SONGDONE", "an exception was thrown " + exc.getMessage());
        }
    }

    public void createTrackNotification(String title, String message) {
        TrackBuilder.setOngoing(false)
                .setContentTitle(title)
                .setContentText(message)
                .setProgress(100, 0, false)
                .setSmallIcon(R.drawable.download);
        TrackNotificationManager.notify(TrackNotID, TrackBuilder.build());
    }

    public void editTrackNotification(String status, String title, String message) {
        if (TrackBuilder != null) {
            TrackBuilder.setContentTitle(title)
                    .setContentText(message);
            if ("next".equalsIgnoreCase(status)) {
                TrackNotificationManager.notify(TrackNotID, TrackBuilder.build());
            } else if ("failed".equalsIgnoreCase(status)) {
                TrackBuilder.setAutoCancel(true);
                TrackBuilder.setProgress(0, 0, false)
                        .setSmallIcon(android.R.drawable.ic_dialog_alert);
                TrackNotificationManager.notify(TrackNotID, TrackBuilder.build());
            } else if ("success".equalsIgnoreCase(status)) {
                TrackBuilder.setAutoCancel(true);
                TrackBuilder.setSmallIcon(R.drawable.download);
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setDataAndType(Uri.fromFile(new File(PlayTrackLocation)), "audio/*");
                PendingIntent pi = PendingIntent.getActivity(getApplicationContext(), 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
                TrackBuilder.setContentIntent(pi);
                TrackNotificationManager.notify(TrackNotID, TrackBuilder.build());
            }

        }
    }

    private void DownloadAppFile() {

        if (!DownloadingApps.isEmpty()) {

            Map<String, ArrayList<String>> currentApp;
            if (Build.VERSION.SDK_INT >= 9) {
                currentApp = DownloadingApps.pop();
            } else {
                currentApp = DownloadingApps.remove(DownloadingApps.lastIndexOf(this));
            }

            ArrayList<String> currentAppData = currentApp.get("app");
            if (!isFirstApp) {

                editAppNotification("next", "downloading app", "Downloading " + currentAppData.get(1));
            }

            String appPackFileName = currentAppData.get(2);
            appPackFileName = appPackFileName.replace(".", "_").replace(" ", "").trim();
            currentAppID = currentAppData.get(0);
            final DownloadAppFileTask downloadTask = new DownloadAppFileTask(NuntuTransactDaemon.this);
            downloadTask.execute(HOST + currentAppData.get(3), appPackFileName + ".apk", currentAppData.get(2), currentAppData.get(1));
        }
    }

    private void DownloadBookFile() {

        if (!DownloadingBooks.isEmpty()) {


            Map<String, ArrayList<String>> currentBook;

            if (Build.VERSION.SDK_INT >= 9) {
                currentBook = DownloadingBooks.pop();
            } else {
                currentBook = DownloadingBooks.remove(DownloadingBooks.lastIndexOf(this));
            }

            ArrayList<String> currentBookData = currentBook.get("book");
            if (!isFirstBook) {

                editBookNotification("next", "downloading book", "Downloading " + currentBookData.get(1));
            }

            currentBookID = currentBookData.get(0);
            final DownloadBookFileTask downloadTask = new DownloadBookFileTask(NuntuTransactDaemon.this);
            downloadTask.execute(HOST + currentBookData.get(2), currentBookData.get(1));
        }
    }

    private void DownloadTrackFile() {

        if (!DownloadingTracks.isEmpty()) {


            Map<String, ArrayList<String>> currentTrack;

            if (Build.VERSION.SDK_INT >= 9) {
                currentTrack = DownloadingTracks.pop();
            } else {
                currentTrack = DownloadingTracks.remove(DownloadingTracks.lastIndexOf(this));
            }
            ArrayList<String> currentTrackData = currentTrack.get("track");
            if (!isFirstTrack) {

                editTrackNotification("next", "downloading song", "Downloading " + currentTrackData.get(1));
            }

            currentTrackID = currentTrackData.get(0);
            final DownloadTrackFileTask downloadTask = new DownloadTrackFileTask(NuntuTransactDaemon.this);
            downloadTask.execute(HOST + currentTrackData.get(2), currentTrackData.get(1).replace(" ", "_"));
        } else {
            finishTrackProgress();
        }
    }

    private void DownloadAlbumFile() {
        Log.e("ALBUM DOWN", "new download requested");
        if (!DownloadingAlbums.isEmpty()) {

            Map<String, ArrayList<String>> currentAlbum;

            if (Build.VERSION.SDK_INT >= 9) {

                currentAlbum = DownloadingAlbums.pop();
                Log.e("album", "new way pop remaining " + DownloadingAlbums.size());
            } else {

                currentAlbum = DownloadingAlbums.remove(DownloadingAlbums.lastIndexOf(DownloadingAlbums));
                Log.e("album", "old way pop remaining " + DownloadingAlbums.size());
            }
            ArrayList<String> currentAlbumData = currentAlbum.get("music");
            editAlbumNotification("next", currentAlbumData.get(1), " downloading album ");
            final DownloadAlbumFileTask downloadTask = new DownloadAlbumFileTask(NuntuTransactDaemon.this);
            AlbumLocaltracks = Integer.valueOf(currentAlbumData.get(3));
            currentAlbumID = currentAlbumData.get(0);
            if (AlbumLocaltracks == 1) {
                downloadTask.execute(HOST + currentAlbumData.get(2), currentAlbumData.get(1).replace(" ", "_") + currentAlbumData.get(0));
            } else {
                downloadTask.execute(HOST + currentAlbumData.get(2), currentAlbumData.get(1).replace(" ", "_") + currentAlbumData.get(0) + ".zip");
            }
        } else {
            Log.e("ALBUM DOWN", "DownloadingAlbums is empty");
            DownloadingAlbums.clear();
        }
    }

    @SuppressLint("InlinedApi")
    public int InstallApplication(int Httpcode) {
        String location = "";
        try {
            if (!InstallingApps.isEmpty()) {
                Map<String, ArrayList<String>> currentInstall = InstallingApps.pop();
                final String PackageName = currentInstall.get("install").get(0);
                final Uri packageURI = Uri.parse(PackageName.toString().trim());
                location = currentInstall.get("install").get(1);

                final File file = new File(location);
                if (!file.exists()) {
                    Log.e(TAG, "installation file does not exist.." + location);
                    return -1;
                }

                new Handler().post(new Runnable() {
                    @SuppressWarnings("deprecation")
                    @Override
                    public void run() {
                        int isAllowed = 0;
                        try {
                            isAllowed = Settings.Secure.getInt(getContentResolver(), Settings.Secure.INSTALL_NON_MARKET_APPS);
                        } catch (SettingNotFoundException e) {
                            Log.e("SETTINGS", "damn " + e.getMessage());
                        }
                        if (isAllowed != 0) {
                            if (Build.VERSION.SDK_INT >= 14) {
                                Intent intent = new Intent(Intent.ACTION_INSTALL_PACKAGE, packageURI);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                intent.addFlags(Intent.FLAG_FROM_BACKGROUND);

                                String extension = MimeTypeMap.getFileExtensionFromUrl(Uri.fromFile(file).toString());
                                String mimetype = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
                                intent.setDataAndType(Uri.fromFile(file), mimetype);
                                downloadContext.startActivity(intent);

                            } else {
                                Intent intent = new Intent(Intent.ACTION_VIEW, packageURI);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                intent.addFlags(Intent.FLAG_FROM_BACKGROUND);

                                String extension = MimeTypeMap.getFileExtensionFromUrl(Uri.fromFile(file).toString());
                                String mimetype = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
                                intent.setDataAndType(Uri.fromFile(file), mimetype);

                                downloadContext.startActivity(intent);

                            }
                        } else {

                            Intent intent = new Intent(Intent.ACTION_VIEW, packageURI);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.addFlags(Intent.FLAG_FROM_BACKGROUND);

                            String extension = MimeTypeMap.getFileExtensionFromUrl(Uri.fromFile(file).toString());
                            String mimetype = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
                            intent.setDataAndType(Uri.fromFile(file), mimetype);

                            downloadContext.startActivity(intent);
                        }
                    }
                });
                return 0;
            } else {
                return Httpcode;
            }

        } catch (Exception e) {
            Log.e(TAG, "Exception occurred message " + e.getMessage() + "\n cause " + e.getCause().toString());
            return -3;
        }
    }

    /**
     * This is a wrapper around the new stopForeground method, using the older
     * APIs if it is not available.
     */
    void stopForegroundCompat(String id) {
        Log.e("NTD", "stopping NTD service");
        this.stopSelf();
        action = "none";
        packURI = null;
        if (updatePendingCount != null)
            updatePendingCount.cancel();
        if (t != null)
            t.cancel();
        updatePendingCount = null;
        t = null;
        downloadContext = null;

        SysCall = null;
        filesys = null;
        forForeground = null;

        DownloadingApps = null;
        DownloadingBooks = null;
        DownloadingAlbums = null;
        DownloadingTracks = null;
        InstallingApps = null;

        if (AppNotificationManager != null) {
            AppNotificationManager.cancelAll();
        }
        if (AlbumNotificationManager != null) {
            AlbumNotificationManager.cancelAll();
        }
        if (BookNotificationManager != null) {
            BookNotificationManager.cancelAll();
        }
        if (TrackNotificationManager != null) {
            TrackNotificationManager.cancelAll();
        }

        AppNotificationManager = null;
        AlbumNotificationManager = null;
        BookNotificationManager = null;
        TrackNotificationManager = null;
        PendingNotificationManager = null;

        AppBuilder = null;
        AlbumBuilder = null;
        BookBuilder = null;
        TrackBuilder = null;
        PendingBuilder = null;

    }

    private void playSoundNotification() {
        try {
            Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notification);
            r.play();
        } catch (Exception e) {
            Log.e("TAG AUDIO", "failed to play notification audio " + e.getLocalizedMessage());
            e.printStackTrace();
        }
    }

    @SuppressLint("NewApi")
    private void flushNuntuAppsFolder() {

        String SDCardRoot;
        if (filesys.hasSDStorage(true)) {
            SDCardRoot = Environment.getExternalStorageDirectory().toString();
        } else {
            SDCardRoot = Environment.getDataDirectory().toString();
        }
        File NuntuDownloads = new File(SDCardRoot + "/nuntu/downloads/apps");
        NuntuMountedPlace = NuntuDownloads.getAbsolutePath();
        if (NuntuDownloads.exists()) {
            // get all the files from a directory
            File[] fList = NuntuDownloads.listFiles();
            for (File file : fList) {
                if (file.isFile()) {
                    if (file.lastModified() + 7200000 <= System.currentTimeMillis()) {
                        file.delete();
                    }
                }
            }
        }
        //
        long appsize = getFolderSize(NuntuDownloads);
        if (NuntuDownloads.exists() && appsize >= 524288000) {
            NuntuDownloads.delete();
            Log.e("FOLDER FLUSH ", "Nuntu finished flushing apps folder with size " + appsize + " bytes");
        } else {
            Log.e("FOLDER FLUSH ", "Nuntu did not flushing apps folder with size " + appsize + " bytes");
        }

    }

    private class DownloadAppFileTask extends AsyncTask<String, Integer, String> {

        String AppFileLocation = "/nuntu/apps/";
        String packageName = " ";
        String appName = " ";
        private Context context;
        private int HTTPcode = 0;
        private PowerManager.WakeLock mWakeLock;

        public DownloadAppFileTask(Context context) {
            this.context = context;
            isFirstApp = false;
        }

        @Override
        protected String doInBackground(String... appParams) {
            InputStream input = null;
            OutputStream output = null;
            HttpURLConnection connection = null;

            try {

                String SDCardRoot;
                if (filesys.hasSDStorage(true)) {
                    SDCardRoot = Environment.getExternalStorageDirectory().toString();
                } else {
                    SDCardRoot = Environment.getDataDirectory().toString();
                }
                File NuntuDownloads = new File(SDCardRoot + "/nuntu/downloads/apps");
                if (!NuntuDownloads.exists()) {
                    if (!NuntuDownloads.mkdirs()) {
                        Log.e("failedmkdir", "Failed to mkdir for nuntu downloads");
                    }
                }

                AppFileLocation = NuntuDownloads.getAbsolutePath() + "/." + appParams[1];
                Log.e(TAG, "downloading from " + appParams[0] + " to file at " + AppFileLocation);
                URL url = new URL(appParams[0]);
                packageName = appParams[2];
                appName = appParams[3];
                connection = (HttpURLConnection) url.openConnection();
                connection.addRequestProperty("User-Agent", PhoneIMEI);
                connection.connect();

                // expect HTTP 200 OK, so we don't mistakenly save error report
                // instead of the file

                Log.e(TAG, "Server returned HTTP " + connection.getResponseCode()
                        + " " + connection.getResponseMessage());

                if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {

                    HTTPcode = connection.getResponseCode();
                    return null;
                }

                HTTPcode = 200;


                String contentType = connection.getContentType();

                int fileLength = connection.getContentLength();

                File prevDownload = new File(AppFileLocation);
                if (prevDownload.exists()) {
                    if (prevDownload.length() == fileLength) {
                        HTTPcode = 200;
                        return null;
                    } else {
                        prevDownload.delete();
                    }
                }

                Log.e(TAG, "Server said content type was " + contentType);
                Log.e(TAG, "file is byes of " + fileLength);
                // download the file
                input = connection.getInputStream();
                output = new FileOutputStream(AppFileLocation);
                byte data[] = new byte[4096];
                long total = 0;
                int count;
                while ((count = input.read(data)) != -1) {
                    total += count;
                    // publishing the progress....
                    if (fileLength > 0) {
                        publishProgress((int) (total * 100 / fileLength));
                        output.write(data, 0, count);
                    }
                }

            } catch (Exception e) {
                Log.e("BAD REQUEST ", "This is an exception " + e.getMessage());
                e.printStackTrace();
                return e.toString();
            } finally {
                try {
                    if (output != null)
                        output.close();
                    if (input != null)
                        input.close();
                } catch (IOException ignored) {
                    Log.e(TAG, "IOException " + ignored.getMessage());
                }

                if (connection != null)
                    connection.disconnect();

            }

            if (connection != null) {
                connection.disconnect();
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // take CPU lock to prevent CPU from going off if the user
            // presses the power button during download
            PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
            mWakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, getClass().getName());
            mWakeLock.acquire();
        }


        @Override
        protected void onProgressUpdate(Integer... progress) {
            super.onProgressUpdate(progress);
            AppprogressUpdate(appName, progress[0], AppNotID);
        }

        @Override
        protected void onPostExecute(String result) {

            Log.e("BEFORE DEC", "" + pendingDownloads);
            if (pendingDownloads > 0) {
                pendingDownloads--;
            }
            Log.e("AFTER DEC", "" + pendingDownloads);

            if (mWakeLock != null && mWakeLock.isHeld()) {
                mWakeLock.release();
            }

            finishAppProgress();
            if (HTTPcode == 200) {
                if (AppFileLocation != null && AppFileLocation.length() >= 2) {
                    pendingInstallParams = new ArrayList<String>();
                    pendingInstallParams.add(packageName);
                    pendingInstallParams.add(AppFileLocation);
                    Map<String, ArrayList<String>> appData = new HashMap<String, ArrayList<String>>();
                    appData.put("install", pendingInstallParams);
                    InstallingApps.add(appData);
                }
            }
            int Res = InstallApplication(HTTPcode);
            switch (Res) {
                case 0:
                    editAppNotification("success", "confirm install", "Please complete installation");
                    break;
                default:
                    editAppNotification("failed", "install failed", "Installation failed...Error Code " + Res);
                    break;
            }
            DownloadAppFile();
        }
    }


    private class DownloadAlbumFileTask extends AsyncTask<String, Integer, String> {

        String AlbumFileLocation;
        String AlbumMusicLocation;
        String albumName = "nuntu album";
        private Context context;
        private int HTTPcode = 0;
        private PowerManager.WakeLock musicWakeLock;

        public DownloadAlbumFileTask(Context context) {
            this.context = context;
            isFirstAlbum = false;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // take CPU lock to prevent CPU from going off if the user
            // presses the power button during download
            PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
            musicWakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, getClass().getName());
            musicWakeLock.acquire();
            HTTPcode = 400;
        }

        @Override
        protected String doInBackground(String... sUrl) {
            InputStream input = null;
            OutputStream output = null;
            HttpURLConnection connection = null;
            Log.e("HTTP MADE", "requesting download");

            try {
                albumName = sUrl[1];
                File NuntuDownloads, NuntuMusic;
                if (filesys.hasSDStorage(true)) {

                    NuntuDownloads = new File(Environment.getExternalStorageDirectory() + "/nuntu/downloads/albums");
                    NuntuMusic = new File(Environment.getExternalStorageDirectory() + "/Music/NuntuMusic");
                } else {

                    NuntuDownloads = new File(Environment.getDataDirectory() + "/nuntu/downloads/albums");
                    NuntuMusic = new File(Environment.getDataDirectory() + "/Music/NuntuMusic");
                }

                if (!NuntuDownloads.exists()) {
                    if (!NuntuDownloads.mkdirs()) {
                        Log.e("failedmkdir", "Failed to mkdir for nuntu downloads");
                    }
                }

                if (!NuntuMusic.exists()) {
                    if (!NuntuMusic.mkdirs()) {
                        Log.e("failedmkdir", "Failed to mkdir for nuntu music");
                    }
                }


                AlbumFileLocation = NuntuDownloads.getAbsolutePath() + "/" + sUrl[1].replace(" ", "_");
                AlbumMusicLocation = NuntuMusic.getAbsolutePath() + "/" + sUrl[1].replace(".zip", "").replace(" ", "_") + "/";

                if (filesys.getDirFiles(new File(AlbumMusicLocation)).size() >= AlbumLocaltracks) {
                    HTTPcode = 200;
                    Log.e("ALBUM OK", "all songs alreadythere");
                    return "alreadythere";

                } else {
                    Log.e("ALBUM NEW", "all songs will be downloaded");
                }

                URL url = new URL(sUrl[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.addRequestProperty("User-Agent", PhoneIMEI);
                connection.setConnectTimeout(0);
                connection.connect();
                Log.e(TAG, "downloading from " + sUrl[0] + " to file at " + AlbumFileLocation);
                if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {

                    HTTPcode = connection.getResponseCode();
                    Log.e("Album Download", "Server returned HTTP " + connection.getResponseCode()
                            + " " + connection.getResponseMessage());
                }

                HTTPcode = 300;
                Log.e(TAG, "Server returned HTTP " + connection.getResponseCode()
                        + " " + connection.getResponseMessage());

                String contentType;
                int fileLength = connection.getContentLength();
                String ServercontentType = connection.getContentType();
                Log.e(TAG, "Server said content type was " + ServercontentType);
                Log.e(TAG, "file is byes of " + fileLength);
                if (AlbumLocaltracks == 1) {
                    contentType = MimeTypeMap.getSingleton().getExtensionFromMimeType(ServercontentType);
                    if (contentType == null) {
                        String[] contentTypeArrayed = ServercontentType.split("/");
                        if (contentTypeArrayed != null) {
                            contentType = contentTypeArrayed[1];
                        } else {
                            contentType = "mp3";
                        }
                    }
                    AlbumFileLocation = NuntuMusic.getAbsolutePath() + "/" + sUrl[1] + "." + contentType;
                    if (new File(AlbumFileLocation).exists()) {
                        HTTPcode = 200;
                        return "ok";
                    }

                } else if (AlbumLocaltracks >= 2) {
                    contentType = MimeTypeMap.getSingleton().getExtensionFromMimeType(ServercontentType);
                    if (contentType == null) {
                        String[] contentTypeArrayed = ServercontentType.split("/");
                        if (contentTypeArrayed != null) {
                            contentType = contentTypeArrayed[1];
                        } else {
                            contentType = "zip";
                        }
                    }
                } else {
                    HTTPcode = 400;
                    return null;
                }
                // download the file
                input = connection.getInputStream();
                output = new FileOutputStream(AlbumFileLocation);
                byte data[] = new byte[4096];
                long total = 0;
                int count;
                while ((count = input.read(data)) != -1) {
                    total += count;
                    if (fileLength > 0) {
                        publishProgress((int) (total * 100 / fileLength));
                        output.write(data, 0, count);
                    }
                }
                HTTPcode = 200;

            } catch (Exception e) {

                e.printStackTrace();
            } finally {
                try {
                    if (output != null)
                        output.close();
                    if (input != null)
                        input.close();
                } catch (IOException ignored) {
                    ignored.printStackTrace();
                }

                if (connection != null)
                    connection.disconnect();

            }

            if (connection != null) {
                connection.disconnect();
            }

            return "ok";
        }


        @Override
        protected void onProgressUpdate(Integer... progress) {
            super.onProgressUpdate(progress);
            AlbumprogressUpdate(albumName, progress[0], AlbumNotID);
        }

        @Override
        protected void onPostExecute(String result) {

            Log.e("BEFORE DEC", "" + pendingDownloads);
            if (pendingDownloads > 0) {
                pendingDownloads--;
            }
            Log.e("AFTER DEC", "" + pendingDownloads);

            if (musicWakeLock != null && musicWakeLock.isHeld()) {
                musicWakeLock.release();
            }
            finishAlbumProgress();
            if (HTTPcode == 200) {
                if (AlbumLocaltracks == 1) {
                    AlbumLocation = AlbumFileLocation;
                    editAlbumNotification("success", "success", "please locate song at " + AlbumFileLocation);

                } else if (!extractAlbumSongs(AlbumFileLocation, AlbumMusicLocation)) {
                    editAlbumNotification("failed", "failed", "An error occurred while preparing downloaded songs");
                } else {
                    editAlbumNotification("success", "success", "Album was successfully downloaded.");
                }
            } else {
                editAlbumNotification("failed", "Download failed", "Album download error: " + HTTPcode);
            }
            DownloadAlbumFile();
        }


        private void dirChecker(String dir) {
            File f = new File(dir);

            if (!f.isDirectory()) {
                f.mkdirs();
            }
        }


        public boolean extractAlbumSongs(String _zipFile, String targetLocation) {
            InputStream is;
            ZipInputStream zis;
            AlbumLocation = targetLocation;

            try {
                //create target location folder if not exist
                dirChecker(targetLocation);

                String filename;
                is = new FileInputStream(_zipFile);
                zis = new ZipInputStream(new BufferedInputStream(is));
                ZipEntry ze;
                byte[] buffer = new byte[1024];
                int count;

                while ((ze = zis.getNextEntry()) != null) {
                    // zapis do souboru
                    filename = ze.getName();

                    // Need to create directories if not exists, or
                    // it will generate an Exception...
                    if (ze.isDirectory()) {
                        File fmd = new File(targetLocation + "/" + filename);
                        fmd.mkdirs();
                        continue;
                    }

                    FileOutputStream fout = new FileOutputStream(targetLocation + "/" + filename.replace(" ", "_"));

                    //
                    while ((count = zis.read(buffer)) != -1) {
                        fout.write(buffer, 0, count);
                    }

                    fout.close();
                    zis.closeEntry();
                }

                zis.close();
                new File(_zipFile).delete();
                return true;
            } catch (Exception e) {
                Log.e("EXTRACT ", "extraction failed");
                return false;
            }
        }
    }

    private class DownloadBookFileTask extends AsyncTask<String, Integer, String> {

        String BookFileLocation, BookTitle;
        private Context context;
        private int HTTPcode = 0;
        private PowerManager.WakeLock mWakeLock;

        public DownloadBookFileTask(Context context) {
            this.context = context;
            isFirstBook = false;
            Log.e("BEFORE DEC", "" + pendingDownloads);
        }

        @Override
        protected String doInBackground(String... sUrl) {
            InputStream input = null;
            OutputStream output = null;
            HttpURLConnection connection = null;

            try {

                String SDCardRoot;
                if (filesys.hasSDStorage(true)) {
                    SDCardRoot = Environment.getExternalStorageDirectory().toString();
                } else {
                    SDCardRoot = Environment.getDataDirectory().toString();
                }
                File NuntuTrackDownload = new File(SDCardRoot + "/Documents/NuntuBooks");
                if (!NuntuTrackDownload.exists()) {
                    if (!NuntuTrackDownload.mkdirs()) {
                        Log.e("failedmkdir", "Failed to mkdir for Music");
                    }
                }

                BookFileLocation = NuntuTrackDownload.getAbsolutePath() + "/" + sUrl[1];
                BookFileLocation = BookFileLocation.replace(" ", "_");
                BookTitle = sUrl[1];
                Log.e(TAG, "downloading from " + sUrl[0] + " to file at " + sUrl[1]);
                URL url = new URL(sUrl[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.addRequestProperty("User-Agent", PhoneIMEI);
                connection.connect();

                // expect HTTP 200 OK, so we don't mistakenly save error report
                // instead of the file
                if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {

                    HTTPcode = connection.getResponseCode();
                    return null;
                }

                HTTPcode = 200;

                Log.e(TAG, "Server returned HTTP " + connection.getResponseCode()
                        + " " + connection.getResponseMessage());

                String contentType = connection.getContentType();

                int fileLength = connection.getContentLength();

                File prevDownload = new File(BookFileLocation);
                if (prevDownload.exists()) {
                    if (prevDownload.length() == fileLength) {
                        HTTPcode = 200;
                        return null;
                    } else {
                        prevDownload.delete();
                    }
                }

                Log.e(TAG, "Server said content type was " + contentType);
                Log.e(TAG, "file is byes of " + fileLength);
                if (contentType != null) {
                    ReadBookType = contentType;
                    contentType = MimeTypeMap.getSingleton().getExtensionFromMimeType(contentType);
                    if (contentType == null) {
                        contentType = "pdf";
                        Log.e("BOOK FILE ", "no extension bro...just ... " + contentType);
                    } else {
                        Log.e("BOOK FILE ", "extension is " + contentType);
                    }
                }
                // download the file

                DownloadedBookSize = fileLength;
                BookFileLocation += "." + contentType;


                input = connection.getInputStream();
                output = new FileOutputStream(BookFileLocation.replace(" ", "_"));
                byte data[] = new byte[4096];
                long total = 0;
                int count;
                while ((count = input.read(data)) != -1) {
                    total += count;
                    // publishing the progress....
                    if (fileLength > 0) {
                        publishProgress((int) (total * 100 / fileLength));
                        output.write(data, 0, count);
                    }
                }

                //int permission=Runtime.getRuntime().exec("chmod 666 " + BookFileLocation).exitValue();
                //Log.e(TAG,"exit value of set apk permission is "+permission +" at file "+BookFileLocation);

            } catch (Exception e) {
                return e.toString();
            } finally {
                try {
                    if (output != null)
                        output.close();
                    if (input != null)
                        input.close();
                } catch (IOException ignored) {
                    Log.e(TAG, "IOException " + ignored.getMessage());
                }

                if (connection != null)
                    connection.disconnect();

            }

            if (connection != null) {
                connection.disconnect();
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // take CPU lock to prevent CPU from going off if the user
            // presses the power button during download
            PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
            mWakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, getClass().getName());
            mWakeLock.acquire();
        }


        @Override
        protected void onProgressUpdate(Integer... progress) {
            super.onProgressUpdate(progress);
            BookprogressUpdate(BookTitle, progress[0], BookNotID);
        }

        @Override
        protected void onPostExecute(String result) {

            Log.e("BEFORE DEC", "" + pendingDownloads);
            if (pendingDownloads > 0) {
                pendingDownloads--;
            }
            Log.e("AFTER DEC", "" + pendingDownloads);

            ReadBookLocation = BookFileLocation;
            if (mWakeLock != null && mWakeLock.isHeld()) {
                mWakeLock.release();
            }

            finishBookProgress();
            if (HTTPcode == 200) {
                if (DownloadingBooks.isEmpty()) {
                    editBookNotification("success", "book download complete", "browse at " + ReadBookLocation);
                } else {
                    editTrackNotification("cancel", "", "");
                }
            } else {
                editBookNotification("failed", "book download failed", "This book couldn't be downloaded..." + HTTPcode);
            }
            DownloadBookFile();
            System.gc();
        }
    }


    private class DownloadTrackFileTask extends AsyncTask<String, Integer, String> {

        String TrackFileLocation, trackTitle;
        private Context context;
        private int HTTPcode = 0;
        private PowerManager.WakeLock mWakeLock;

        public DownloadTrackFileTask(Context context) {
            this.context = context;
            isFirstTrack = false;

        }

        @Override
        protected String doInBackground(String... sUrl) {
            InputStream input = null;
            OutputStream output = null;
            HttpURLConnection connection = null;

            try {

                String SDCardRoot;
                if (filesys.hasSDStorage(true)) {
                    SDCardRoot = Environment.getExternalStorageDirectory().toString();
                } else {
                    SDCardRoot = Environment.getDataDirectory().toString();
                }
                File NuntuTrackDownload = new File(SDCardRoot + "/Music/NuntuMusic");
                if (!NuntuTrackDownload.exists()) {
                    if (!NuntuTrackDownload.mkdirs()) {
                        Log.e("failedmkdir", "Failed to mkdir for Music");
                    }
                }

                TrackFileLocation = NuntuTrackDownload.getAbsolutePath() + "/" + sUrl[1];
                trackTitle = sUrl[1];
                Log.e(TAG, "downloading from " + sUrl[0] + " to file at " + sUrl[1]);
                URL url = new URL(sUrl[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.addRequestProperty("User-Agent", PhoneIMEI);
                connection.connect();

                // expect HTTP 200 OK, so we don't mistakenly save error report
                // instead of the file
                if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {

                    HTTPcode = connection.getResponseCode();
                    return null;
                }

                HTTPcode = 200;

                Log.e(TAG, "Server returned HTTP " + connection.getResponseCode()
                        + " " + connection.getResponseMessage());

                String ServercontentType = connection.getContentType();

                int fileLength = connection.getContentLength();


                Log.e(TAG, "Server said content type was " + ServercontentType);
                Log.e(TAG, "file is byes of  " + fileLength);
                // download the file
                String contentType = MimeTypeMap.getSingleton().getExtensionFromMimeType(ServercontentType);
                if (contentType == null || contentType.length() < 3) {
                    String[] contentTypeArrayed = ServercontentType.split("/");
                    if (contentTypeArrayed != null) {
                        contentType = contentTypeArrayed[1];
                    } else {
                        contentType = ServercontentType;
                    }
                }
                TrackFileLocation += "." + contentType;

                File prevDownload = new File(TrackFileLocation);
                if (prevDownload.exists()) {
                    Log.e(TAG, "file already exist " + fileLength);
                    if (prevDownload.length() == fileLength) {
                        HTTPcode = 200;
                        Log.e(TAG, "file already exist and has same size" + fileLength);
                        return null;
                    } else {
                        Log.e(TAG, "file already exist but corrupted deleted" + fileLength);
                        prevDownload.delete();
                    }
                }

                input = connection.getInputStream();
                output = new FileOutputStream(TrackFileLocation);
                byte data[] = new byte[4096];
                long total = 0;
                int count;
                while ((count = input.read(data)) != -1) {
                    total += count;
                    // publishing the progress....
                    if (fileLength > 0) {
                        publishProgress((int) (total * 100 / fileLength));
                        output.write(data, 0, count);
                    }
                }

                //int permission=Runtime.getRuntime().exec("chmod 666 " + TrackFileLocation).exitValue();
                //Log.e(TAG,"exit value of set apk permission is "+permission +" at file "+TrackFileLocation);

            } catch (Exception e) {
                return e.toString();
            } finally {
                try {
                    if (output != null)
                        output.close();
                    if (input != null)
                        input.close();
                } catch (IOException ignored) {
                    Log.e(TAG, "IOException " + ignored.getMessage());
                }

                if (connection != null)
                    connection.disconnect();

            }

            if (connection != null) {
                connection.disconnect();
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // take CPU lock to prevent CPU from going off if the user
            // presses the power button during download
            PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
            mWakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, getClass().getName());
            mWakeLock.acquire();
        }


        @Override
        protected void onProgressUpdate(Integer... progress) {
            super.onProgressUpdate(progress);
            TrackprogressUpdate(trackTitle, progress[0], TrackNotID);
        }

        @Override
        protected void onPostExecute(String result) {

            Log.e("BEFORE DEC", "" + pendingDownloads);
            if (pendingDownloads > 0) {
                pendingDownloads--;
            }
            Log.e("AFTER DEC", "" + pendingDownloads);

            PlayTrackLocation = TrackFileLocation;
            if (mWakeLock != null && mWakeLock.isHeld()) {
                mWakeLock.release();
            }
            finishTrackProgress();
            if (HTTPcode == 200) {
                if (DownloadingTracks.isEmpty()) {
                    editTrackNotification("success", "song download complete", "Songs are located at " + PlayTrackLocation);
                } else {
                    editTrackNotification("cancel", "", "");
                }
            } else {
                editTrackNotification("failed", "song download failed", "This song couldn't be downloaded..." + HTTPcode);
            }
            DownloadTrackFile();
            System.gc();
        }
    }
}