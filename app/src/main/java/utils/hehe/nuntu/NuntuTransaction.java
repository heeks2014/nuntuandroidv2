package utils.hehe.nuntu;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by snick on 4/14/15.
 */
public class NuntuTransaction {
    private String UserID;
    private Activity HostingParent;
    private SharedPreferences TransactionSession;
    SharedPreferences.Editor editor;
    // Shared pref mode
    int PRIVATE_MODE = 0;
    private static final String PREF_NAME = "NuntuTransation";
    private static final String PREF_STARTED_TRANS = "hasTransactionStarted";
    private static final String PREF_FINISHED_TRANS = "hasTransactionFinished";
    private static final String PREF_USERID = "userID";
    private static final String PREF_TRANS_TYPE = "transactionType";
    private static final String PREF_TRANS_PARAMS = "transactionParameters";
    private static String selectedPayment = "crespay";

    public NuntuTransaction(Activity HostParent, String UserID) {
        try {
            this.UserID = UserID;
            this.HostingParent = HostParent;
            this.TransactionSession = HostingParent.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
            editor = TransactionSession.edit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public boolean initTransaction() {
        try {
            editor.putBoolean(PREF_STARTED_TRANS, true);
            editor.putBoolean(PREF_FINISHED_TRANS, false);
            editor.putString(PREF_USERID, UserID);
            editor.apply();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public void endTransaction() {
        try {
            editor.putBoolean(PREF_STARTED_TRANS, false);
            editor.putBoolean(PREF_FINISHED_TRANS, true);
            editor.apply();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void requestAppDonwload(final String[] asset) {
        AsyncTask<String, String, String> getTask = new AsyncTask<String, String, String>() {

            ProgressDialog progressDialog;
            String query
                    ,
                    action
                    ,
                    details
                    ,
                    DownloadLink
                    ,
                    OrderID
                    ,
                    OrderKey;

            ArrayList<String> deviceList
                    ,
                    AssetFileProperties
                    ,
                    paymentMethods;

            @Override
            protected void onPreExecute() {
                HostingParent.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progressDialog = new ProgressDialog(HostingParent);
                        progressDialog.setMessage("\t ... wait ... ");
                        progressDialog.setCancelable(false);
                        progressDialog.show();
                    }
                });

            }

            @Override
            protected String doInBackground(String... params) {
                final String DownloadURL = NuntuConstants.HOST.toString() + "/user/" + UserID + "/download/" + asset[0] + "/" + asset[1];
                HttpClient httpclient = new DefaultHttpClient();
                Log.e("DOWNLOAD PATH", DownloadURL);
                HttpPost httppost = new HttpPost(DownloadURL);
                try {
                    httppost.setEntity(new StringEntity("request form nuntu android", "UTF8"));
                    httppost.addHeader("Content-type", "application/json");
                    httppost.addHeader("Accept", "application/json");
                    httppost.addHeader("User-Agent", NuntuConstants.GHOST_AGENT.toString());
                    HttpResponse response = httpclient.execute(httppost);
                    HttpEntity entity = response.getEntity();
                    String Response = EntityUtils.toString(entity);
                    Log.e("Data Rec", Response);
                    try {
                        JSONObject ResponseJson = (JSONObject) new JSONTokener(Response).nextValue();
                        query = ResponseJson.getString("status");
                        details = ResponseJson.getString("details");
                        if ("success".equalsIgnoreCase(query)) {
                            action = ResponseJson.getString("action").trim().toLowerCase(Locale.ENGLISH);
                            AssetFileProperties = new ArrayList<>();
                            deviceList = new ArrayList<>();
                            if ("pay".equalsIgnoreCase(action)) {
                                paymentMethods = new ArrayList<>();
                                JSONObject Data = new JSONObject(ResponseJson.getString("data"));
                                OrderID = Data.getString("orderID");
                                OrderKey = Data.getString("orderKey");
                                JSONArray jsonPayments = new JSONArray(Data.getString("paymentMethod"));
                                for (int i = 0; i < jsonPayments.length(); i++) {
                                    JSONObject payEntry = jsonPayments.getJSONObject(i);
                                    String paymentType = payEntry.getString("type");
                                    paymentMethods.add(paymentType);
                                }

                                JSONObject Criteria = new JSONObject(ResponseJson.getString("criteria"));
                                JSONArray jsonArry = new JSONArray(Criteria.getString("assetFile"));
                                for (int i = 0; i < jsonArry.length(); i++) {
                                    AssetFileProperties.add(jsonArry.getString(i));
                                }

                                JSONArray jsonDevices = new JSONArray(Criteria.getString("userDevice"));
                                for (int i = 0; i < jsonDevices.length(); i++) {
                                    JSONObject devEntry = jsonDevices.getJSONObject(i);
                                    deviceList.add(devEntry.getString("status"));
                                    deviceList.add(devEntry.getString("device"));
                                }
                            } else if ("prompt".equalsIgnoreCase(action)) {
                                DownloadLink = ResponseJson.getString("data");
                                JSONObject Criteria = new JSONObject(ResponseJson.getString("criteria"));

                                if ("app".equalsIgnoreCase(asset[0])) {
                                    JSONArray jsonArry = new JSONArray(Criteria.getString("assetFile"));
                                    for (int i = 0; i < jsonArry.length(); i++) {
                                        AssetFileProperties.add(jsonArry.getString(i));
                                    }
                                } else if ("book".equalsIgnoreCase(asset[0])) {
                                    try {
                                        JSONObject filedetails = new JSONObject(Criteria.getString("assetFile"));
                                        AssetFileProperties.add("Book Type " + filedetails.getString("type"));
                                        float size = Integer.valueOf(filedetails.getString("size")) / 1048510.00f;

                                        AssetFileProperties.add("Book Size " + size);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                                JSONArray jsonDevices = new JSONArray(Criteria.getString("userDevice"));
                                for (int i = 0; i < jsonDevices.length(); i++) {
                                    JSONObject devEntry = jsonDevices.getJSONObject(i);
                                    deviceList.add(devEntry.getString("status"));
                                    deviceList.add(devEntry.getString("device"));
                                }
                            }
                        }
                    } catch (Exception exc) {
                        exc.printStackTrace();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

                return null;
            }


            @SuppressWarnings("deprecation")
            @Override
            protected void onPostExecute(String result) {
                super.onPostExecute(result);
                if (progressDialog != null) {
                    progressDialog.dismiss();
                }

                if ("success".equalsIgnoreCase(query)) {
                    try {
                        JSONObject TxParams = new JSONObject();
                        TxParams.put("download", DownloadLink);
                        TxParams.put("action", action);
                        TxParams.put("orderID", OrderID);
                        TxParams.put("orderKey", OrderKey);

                        editor.putString(PREF_TRANS_TYPE, "download");
                        editor.putString(PREF_TRANS_PARAMS, TxParams.toString());
                        editor.apply();
                        promptAppPayBox(new String[]{action, asset[0], asset[1], asset[2], asset[3], DownloadLink, OrderID, OrderKey}, deviceList, AssetFileProperties, paymentMethods);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    if (details == null) {
                        details = "Download couldn't go through.\nCheck your internet.";
                    }
                    AlertDialog.Builder builder = new AlertDialog.Builder(HostingParent);
                    builder.setMessage(details);
                    builder.setNeutralButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
                    AlertDialog alert = builder.create();
                    Button neutral_button = alert.getButton(DialogInterface.BUTTON_NEUTRAL);
                    if (neutral_button != null) {
                        neutral_button.setBackgroundDrawable(HostingParent.getApplicationContext().getResources().getDrawable(R.drawable.app_button_background));
                        neutral_button.setTextColor(HostingParent.getApplicationContext().getResources().getColor(android.R.color.white));
                    }
                    alert.show();
                }
            }

        };
        getTask.execute();

    }

    private void promptAppPayBox(final String params[], ArrayList<String> UserRegistedDevices, ArrayList<String> AssetFileDetails, ArrayList<String> AvailablePayments) {
        String appPerms = "";
        boolean isDeviceOK = false;

        final AlertDialog.Builder alert = new AlertDialog.Builder(HostingParent);

        LinearLayout layout = new LinearLayout(HostingParent);
        layout.setOrientation(LinearLayout.VERTICAL);

        final TextView serverBox = new TextView(HostingParent);
        serverBox.setText("Properties and details of " + params[3] + ":\n");
        serverBox.setTextSize(14);
        serverBox.setTextColor(Color.WHITE);
        serverBox.setGravity(Gravity.CENTER);
        serverBox.setBackgroundColor(HostingParent.getResources().getColor(R.color.primaryColor));
        layout.addView(serverBox);


        if ("pay".equalsIgnoreCase(params[0])) {
            final RadioGroup payButtons = new RadioGroup(HostingParent);

            final RadioButton crespay = new RadioButton(HostingParent);
            crespay.setText("Crespay");
            crespay.setTextColor(Color.WHITE);
            crespay.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    selectedPayment = "mmoney";
                }

            });

            final RadioButton nuntu = new RadioButton(HostingParent);
            nuntu.setText("Nuntu");
            nuntu.setTextColor(Color.WHITE);
            nuntu.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    selectedPayment = "nuntumoney";
                }

            });


            final RadioButton paypal = new RadioButton(HostingParent);
            paypal.setText("PayPal");
            paypal.setTextColor(Color.WHITE);
            paypal.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    selectedPayment = "paypal";
                }

            });

            final RadioButton BitCoin = new RadioButton(HostingParent);
            BitCoin.setText("BitCoin");
            BitCoin.setTextColor(Color.WHITE);
            BitCoin.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    selectedPayment = "bitcoin";
                }

            });

            for (int p = 0; p < AvailablePayments.size(); p++) {
                Log.e("Pay method ", AvailablePayments.get(p));
                if ("mmoney".equalsIgnoreCase(AvailablePayments.get(p))) {
                    payButtons.addView(crespay);
                } else if ("nuntumoney".equalsIgnoreCase(AvailablePayments.get(p))) {
                    payButtons.addView(nuntu);
                } else if ("paypal".equalsIgnoreCase(AvailablePayments.get(p))) {
                    payButtons.addView(paypal);
                } else if ("bitcoin".equalsIgnoreCase(AvailablePayments.get(p))) {
                    payButtons.addView(BitCoin);
                }
            }
            payButtons.setPadding(12, 12, 12, 12);
            payButtons.setBackgroundColor(Color.argb(40, 0, 0, 0));
            layout.addView(payButtons);
        }

        final TextView permissions = new TextView(HostingParent);

        for (int i = 0; i < AssetFileDetails.size(); i++) {
            String Entry = AssetFileDetails.get(i);
            if (Entry != null && Entry.length() >= 3 && !Entry.contains("png") && !Entry.contains("--")) {
                Entry = Entry.replaceAll("[^\\x0A\\x0D\\x20-\\x7E]", "");
                Entry = Entry.replace("usesfeature", "");
                appPerms += "\t. " + Entry.replaceAll("[\n\r]", " ") + "\n";

            }
        }
        permissions.setText(appPerms);
        permissions.setTextColor(Color.WHITE);
        permissions.setBackgroundColor(Color.argb(80, 0, 0, 0));
        serverBox.setTextSize(11);
        layout.addView(permissions);

        final TextView deviceHeader = new TextView(HostingParent);
        deviceHeader.setText("\t" + params[3].toUpperCase(Locale.ENGLISH) + " CAN RUN ON YOUR :");
        deviceHeader.setTextColor(Color.WHITE);
        deviceHeader.setBackgroundColor(Color.argb(80, 0, 0, 0));
        layout.addView(deviceHeader);
        boolean hasUserDevice = false;
        for (int i = 0; i < UserRegistedDevices.size() - 1; i++) {
            final TextView deviceText = new TextView(HostingParent);
            String deviceName = "";
            if (UserRegistedDevices.get(i).equalsIgnoreCase("success")) {
                isDeviceOK = true;
                deviceName = UserRegistedDevices.get(i + 1);
                if (i >= UserRegistedDevices.size() - 2) {
                    deviceText.setText("\t-- " + deviceName + "\n\n\n");
                } else {
                    deviceText.setText("\t-- " + deviceName + "\n");
                }
                deviceText.setTextColor(Color.rgb(154, 205, 50));
                deviceText.setBackgroundColor(Color.argb(80, 0, 0, 0));
                layout.addView(deviceText);
                hasUserDevice = true;
            }

            if (!hasUserDevice) {
                new Device().registerDeviceAsync(HostingParent);
            }

        }

        if ("app".equalsIgnoreCase(params[1]) && !hasUserDevice) {
            final TextView NodeviceText = new TextView(HostingParent);
            NodeviceText.setTextColor(Color.rgb(160, 82, 45));
            NodeviceText.setGravity(Gravity.CENTER);
            NodeviceText.setText("This app isn't compatible with any of your devices");
            NodeviceText.setBackgroundColor(Color.argb(80, 0, 0, 0));
            layout.addView(NodeviceText);
        }

        alert.setIcon(R.drawable.download);
        alert.setTitle("Download " + params[3]);
        alert.setView(layout);
        if ("pay".equalsIgnoreCase(params[0])) {
            alert.setPositiveButton("PAY", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int whichButton) {
                    postPayment(new String[]{UserID, selectedPayment, params[6], params[7]});
                    dialog.dismiss();
                }
            });

            alert.setNeutralButton("CLOSE", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int whichButton) {
                    dialog.dismiss();
                }
            });
            AlertDialog alertCustom = alert.create();
            alertCustom.show();
            //retrieving the button view in order to handle it.
            Button negative_button = alertCustom.getButton(DialogInterface.BUTTON_NEGATIVE);

            Button neutral_button = alertCustom.getButton(DialogInterface.BUTTON_NEUTRAL);

            Button positive_button = alertCustom.getButton(DialogInterface.BUTTON_POSITIVE);

            if (neutral_button != null) {
                neutral_button.setBackgroundDrawable(HostingParent.getResources()
                        .getDrawable(R.drawable.app_button_background));

                neutral_button.setTextColor(HostingParent.getResources()
                        .getColor(android.R.color.white));
            }
            if (positive_button != null) {
                positive_button.setBackgroundDrawable(HostingParent.getResources()
                        .getDrawable(R.drawable.app_button_background));

                positive_button.setTextColor(HostingParent.getResources()
                        .getColor(android.R.color.white));
            }

            if (negative_button != null) {

                negative_button.setBackgroundDrawable(HostingParent.getResources()
                        .getDrawable(R.drawable.app_button_background));

                negative_button.setTextColor(HostingParent.getResources()
                        .getColor(android.R.color.white));
            }

        } else if ("prompt".equalsIgnoreCase(params[0])||"download".equalsIgnoreCase(params[0])) {
            if (isDeviceOK && params[5] != null) {
                alert.setPositiveButton("DOWNLOAD", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int whichButton) {
                        Intent downloadApp = new Intent(HostingParent, NuntuTransactDaemon.class);
                        downloadApp.putExtra("action", "download");
                        downloadApp.putExtra("type", params[1]);
                        downloadApp.putExtra("ID", params[2]);
                        downloadApp.putExtra("Name", params[3]);
                        downloadApp.putExtra("resource", params[4]);
                        downloadApp.putExtra("link", params[5]);
                        HostingParent.startService(downloadApp);
                        Toast.makeText(HostingParent, params[3] + " is download is requested. check on it in notification bar.", Toast.LENGTH_LONG).show();
                        dialog.dismiss();

                    }
                });
            }
            alert.setNeutralButton("CLOSE", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int whichButton) {
                    dialog.dismiss();
                }
            });
            AlertDialog alertCustom = alert.create();
            alertCustom.show();
            //retrieving the button view in order to handle it.
            Button negative_button = alertCustom.getButton(DialogInterface.BUTTON_NEGATIVE);

            Button neutral_button = alertCustom.getButton(DialogInterface.BUTTON_NEUTRAL);

            Button positive_button = alertCustom.getButton(DialogInterface.BUTTON_POSITIVE);


            if (neutral_button != null) {
                neutral_button.setBackgroundDrawable(HostingParent.getResources()
                        .getDrawable(R.drawable.app_button_background));

                neutral_button.setTextColor(HostingParent.getResources()
                        .getColor(android.R.color.white));
            }
            if (positive_button != null) {
                positive_button.setBackgroundDrawable(HostingParent.getResources()
                        .getDrawable(R.drawable.app_button_background));

                positive_button.setTextColor(HostingParent.getResources()
                        .getColor(android.R.color.white));
            }

            if (negative_button != null) {
                negative_button.setBackgroundDrawable(HostingParent.getResources()
                        .getDrawable(R.drawable.app_button_background));

                negative_button.setTextColor(HostingParent.getResources()
                        .getColor(android.R.color.white));
            }
        }
    }


    private void postPayment(final String[] PayMentParams) {

        if (PayMentParams.length >= 4) {
            String url = NuntuConstants.SECURE_HOST.toString() + "/user/" + PayMentParams[0] + "/payment/" + PayMentParams[1] + "/" + PayMentParams[2] + "/" + PayMentParams[3];
            requestPayment(PayMentParams[1], url, new String[]{PayMentParams[4], PayMentParams[5], PayMentParams[6], PayMentParams[7]});
        } else {

        }
    }


    public void requestPayment(final String method, final String path, final String[] params) {
        AsyncTask<String, String, String> getTask = new AsyncTask<String, String, String>() {

            ProgressDialog progressPayment;
            String query
                    ,
                    crespayUrl
                    ,
                    details
                    ,
                    DownloadUrl;

            @Override
            protected void onPreExecute() {
                HostingParent.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progressPayment = new ProgressDialog(HostingParent);
                        progressPayment.setMessage("\t ...  wait.... \nOpening " + method + " payment");
                        progressPayment.setCancelable(false);
                        progressPayment.show();
                    }
                });


            }

            @Override
            protected String doInBackground(String... params) {
                HttpClient httpclient = new DefaultHttpClient();
                // specify the URL you want to post to
                HttpPost httppost = new HttpPost(path);
                try {
                    httppost.setEntity(new StringEntity("request form nuntu android", "UTF8"));
                    httppost.addHeader("Content-type", "application/json");
                    httppost.addHeader("Accept", "application/json");
                    httppost.addHeader("User-Agent", "nuntumobile");
                    // send the variable and value, in other words post, to the URL
                    HttpResponse response = httpclient.execute(httppost);

                    HttpEntity entity = response.getEntity();
                    String Response = EntityUtils.toString(entity);
                    Log.d("Data Rec", Response);
                    try {
                        JSONObject ResponseJson = (JSONObject) new JSONTokener(Response).nextValue();
                        query = ResponseJson.getString("status");
                        if ("success".equalsIgnoreCase(query)) {
                            if ("mmoney".equalsIgnoreCase(method)) {
                                crespayUrl = ResponseJson.getString("url");
                            } else if ("nuntumoney".equalsIgnoreCase(method)) {
                                details = ResponseJson.getString("message");
                                DownloadUrl = ResponseJson.getString("url");
                            } else if ("paypal".equalsIgnoreCase(method)) {
                                details = ResponseJson.getString("message");
                            } else if ("bitcoin".equalsIgnoreCase(method)) {
                                details = ResponseJson.getString("message");
                            }
                        } else if ("failed".equalsIgnoreCase(query)) {
                            details = ResponseJson.getString("message");
                        } else {
                            details = "server could not send valid response";
                        }

                    } catch (Exception exc) {
                        details = "An error occurred while requesting payment:\n " + exc.getMessage();
                        Log.d("Exception in postData", "this detail " + exc.getMessage());

                    }

                } catch (ClientProtocolException e) {
                    details = "An error occurred while requesting payment:\n " + e.getMessage();
                    Log.d("Exception in postData", "this detail " + e.getMessage());

                } catch (IOException e) {
                    details = "An error occurred while requesting payment:\n " + e.getMessage();
                    Log.d("Exception in postData", "this detail " + e.getMessage());

                }

                return null;
            }


            @SuppressWarnings("deprecation")
            @Override
            protected void onPostExecute(String result) {
                super.onPostExecute(result);
                if (progressPayment != null) {
                    progressPayment.dismiss();
                }

                if ("failed".equalsIgnoreCase(query)) {

                    final AlertDialog.Builder alertbox = new AlertDialog.Builder(HostingParent);
                    alertbox.setTitle("payment failed");
                    alertbox.setMessage("Nuntu could not process this payment: \n" + details);
                    alertbox.setNeutralButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface arg0, int arg1) {

                            arg0.dismiss();
                        }
                    });
                    AlertDialog alert = alertbox.create();
                    alert.show();
                    Button neutral_button = alert.getButton(DialogInterface.BUTTON_NEUTRAL);

                    if (neutral_button != null) {
                        neutral_button.setBackgroundDrawable(HostingParent.getResources()
                                .getDrawable(R.drawable.app_button_background));

                        neutral_button.setTextColor(HostingParent.getResources()
                                .getColor(android.R.color.white));
                    }

                } else if ("success".equalsIgnoreCase(query)) {
                    if ("mmoney".equalsIgnoreCase(method)) {
                        popCresPayForm(crespayUrl);
                    } else if ("nuntumoney".equalsIgnoreCase(method)) {
                        final AlertDialog.Builder alertbox = new AlertDialog.Builder(HostingParent);
                        alertbox.setTitle("payment was successful");
                        alertbox.setMessage(details);
                        alertbox.setNeutralButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface arg0, int arg1) {
                                arg0.dismiss();
                            }
                        });

                        alertbox.setPositiveButton("START DOWNLOAD", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface arg0, int arg1) {
                                arg0.dismiss();
                                Intent downloadApp = new Intent(HostingParent, NuntuTransactDaemon.class);
                                downloadApp.putExtra("action", "download");
                                downloadApp.putExtra("type", "app");
                                downloadApp.putExtra("appID", params[0]);
                                downloadApp.putExtra("appName", params[1]);
                                downloadApp.putExtra("packagename", params[2]);
                                downloadApp.putExtra("link", DownloadUrl);
                                HostingParent.startService(downloadApp);
                                Toast.makeText(HostingParent, params[1] + " download is requested. check on it in notification bar.", Toast.LENGTH_LONG).show();

                            }
                        });
                        AlertDialog alert = alertbox.create();
                        alert.show();
                        Button neutral_button = alert.getButton(DialogInterface.BUTTON_NEUTRAL);

                        Button positive_button = alert.getButton(DialogInterface.BUTTON_POSITIVE);


                        if (neutral_button != null) {
                            neutral_button.setBackgroundDrawable(HostingParent.getResources()
                                    .getDrawable(R.drawable.app_button_background));

                            neutral_button.setTextColor(HostingParent.getResources()
                                    .getColor(android.R.color.white));
                        }
                        if (positive_button != null) {
                            positive_button.setBackgroundDrawable(HostingParent.getResources()
                                    .getDrawable(R.drawable.app_button_background));

                            positive_button.setTextColor(HostingParent.getResources()
                                    .getColor(android.R.color.white));
                        }
                    } else {
                        final AlertDialog.Builder alertbox = new AlertDialog.Builder(HostingParent);
                        alertbox.setTitle("payment failed");
                        alertbox.setMessage("Paymeny method not supported by Nuntu");
                        alertbox.setNeutralButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface arg0, int arg1) {

                                arg0.dismiss();
                            }
                        });
                        AlertDialog alert = alertbox.create();
                        alert.show();
                        Button neutral_button = alert.getButton(DialogInterface.BUTTON_NEUTRAL);

                        if (neutral_button != null) {
                            neutral_button.setBackgroundDrawable(HostingParent.getResources()
                                    .getDrawable(R.drawable.app_button_background));

                            neutral_button.setTextColor(HostingParent.getResources()
                                    .getColor(android.R.color.white));
                        }
                    }
                } else {
                    final AlertDialog.Builder alertbox = new AlertDialog.Builder(HostingParent);
                    alertbox.setTitle("payment failed");
                    alertbox.setMessage("Connection Error!");
                    alertbox.setNeutralButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface arg0, int arg1) {

                            arg0.dismiss();
                        }
                    });
                    AlertDialog alert = alertbox.create();
                    alert.show();
                    Button neutral_button = alert.getButton(DialogInterface.BUTTON_NEUTRAL);

                    if (neutral_button != null) {
                        neutral_button.setBackgroundDrawable(HostingParent.getResources()
                                .getDrawable(R.drawable.app_button_background));

                        neutral_button.setTextColor(HostingParent.getResources()
                                .getColor(android.R.color.white));
                    }
                }


            }

        };
        getTask.execute(path);
    }


    private void popCresPayForm(final String url) {

        final AlertDialog.Builder CresPopUp = new AlertDialog.Builder(HostingParent);
        final ProgressDialog prDialog = new ProgressDialog(HostingParent);
        prDialog.setMessage("Loading Crespay payment data. Please wait.");
        prDialog.setCancelable(false);
        prDialog.setMax(120000);

        LinearLayout CresPopUplayout = new LinearLayout(HostingParent);
        CresPopUplayout.setOrientation(LinearLayout.VERTICAL);

        final TextView adviceHeader = new TextView(HostingParent);
        adviceHeader.setText("In case you encounter soft keyboard troubles. Click 'OPEN WITH BROWSER' to enter payment data from your browser.");
        adviceHeader.setTextColor(Color.WHITE);
        adviceHeader.setBackgroundColor(Color.argb(80, 0, 0, 0));
        CresPopUplayout.addView(adviceHeader);

        final WebView crespayform = new WebView(HostingParent);

        crespayform.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                    case MotionEvent.ACTION_UP:
                        if (!v.hasFocus()) {
                            v.requestFocus();
                        }
                        break;
                }
                return false;
            }
        });
        crespayform.setFocusableInTouchMode(true);
        crespayform.setFocusable(true);
        crespayform.setHapticFeedbackEnabled(true);
        crespayform.setClickable(true);
        crespayform.getSettings().setJavaScriptEnabled(true);
        crespayform.getSettings().setLoadWithOverviewMode(true);
        crespayform.getSettings().setUseWideViewPort(true);
        crespayform.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
                if (progress == 100) {
                    prDialog.dismiss();
                } else {
                    prDialog.show();
                }
            }
        });

        crespayform.loadUrl(url);

        CresPopUplayout.addView(crespayform);
        CresPopUp.setIcon(R.drawable.logo_text_small);
        CresPopUp.setTitle("Pay with Crespay");
        CresPopUp.setView(CresPopUplayout);
        CresPopUp.setNeutralButton("DONE", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int whichButton) {

                dialog.dismiss();
            }
        });

        CresPopUp.setPositiveButton("OPEN WITH BROWSER", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int whichButton) {
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                HostingParent.startActivity(i);
                dialog.dismiss();
            }
        });
        AlertDialog alert = CresPopUp.create();
        alert.show();
        //retrieving the button view in order to handle it.
        Button negative_button = alert.getButton(DialogInterface.BUTTON_NEGATIVE);

        Button neutral_button = alert.getButton(DialogInterface.BUTTON_NEUTRAL);

        Button positive_button = alert.getButton(DialogInterface.BUTTON_POSITIVE);


        if (neutral_button != null) {
            neutral_button.setBackgroundDrawable(HostingParent.getResources()
                    .getDrawable(R.drawable.app_button_background));

            neutral_button.setTextColor(HostingParent.getResources()
                    .getColor(android.R.color.white));
        }
        if (positive_button != null) {
            positive_button.setBackgroundDrawable(HostingParent.getResources()
                    .getDrawable(R.drawable.app_button_background));

            positive_button.setTextColor(HostingParent.getResources()
                    .getColor(android.R.color.white));
        }

        if (negative_button != null) {
            negative_button.setBackgroundDrawable(HostingParent.getResources()
                    .getDrawable(R.drawable.app_button_background));

            negative_button.setTextColor(HostingParent.getResources()
                    .getColor(android.R.color.white));
        }
    }

}
