package utils.hehe.nuntu;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.EditText;

/**
 * Created by snick on 2/23/15.
 */
public class NuntuEditText extends EditText {

    public NuntuEditText(Context context) {
        super(context);
    }

    public NuntuEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        UiUtil.setCustomFont(this, context, attrs,
                R.styleable.utils_hehe_nuntu_NuntuEditText,
                R.styleable.utils_hehe_nuntu_NuntuTextView_font);
    }

    public NuntuEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        UiUtil.setCustomFont(this, context, attrs,
                R.styleable.utils_hehe_nuntu_NuntuEditText,
                R.styleable.utils_hehe_nuntu_NuntuTextView_font);
    }
}