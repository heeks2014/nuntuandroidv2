package utils.hehe.nuntu;

import android.os.Environment;
import android.util.Log;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

public class NuntuFile {


    private static boolean checkFsWritable() {
        String directoryName = Environment.getExternalStorageDirectory().toString() + "/DCIM";
        File directory = new File(directoryName);
        if (!directory.isDirectory()) {
            if (!directory.mkdirs()) {
                return false;
            }
        }
        return directory.canWrite();
    }

    public static List<File> listf(String directoryName) {
        File directory = new File(directoryName);

        List<File> resultList = new ArrayList<File>();

        // get all the files from a directory
        File[] fList = directory.listFiles();
        resultList.addAll(Arrays.asList(fList));
        for (File file : fList) {
            if (file.isFile()) {
                System.out.println(file.getAbsolutePath());
            } else if (file.isDirectory()) {
                resultList.addAll(listf(file.getAbsolutePath()));
            }
        }
        //System.out.println(fList);
        return resultList;
    }

    public boolean deleteApplicationPackage(String location) {
        return new File(location).delete();
    }

    private String getToday() {
        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
        java.util.Date currentLocalTime = cal.getTime();
        DateFormat date = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
        date.setTimeZone(TimeZone.getTimeZone("GMT"));
        String localTime = date.format(currentLocalTime);
        return localTime;
    }

    private String getNow() {
        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
        java.util.Date currentLocalTime = cal.getTime();
        DateFormat date = new SimpleDateFormat("yyy-MM-dd HH:mm:ss z", Locale.US);
        date.setTimeZone(TimeZone.getTimeZone("GMT"));
        String localTime = date.format(currentLocalTime);
        return localTime;
    }

    public boolean hasSDStorage(boolean requireWriteAccess) {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            if (requireWriteAccess) {
                boolean writable = checkFsWritable();
                return writable;
            } else {
                return true;
            }
        } else if (!requireWriteAccess && Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            return true;
        }
        return false;
    }

    public void logToFile(String log) {
        File SDCardRoot;
        String FileLocation;
        String FileName = getToday() + "_nuntulogs.txt";
        String LogMsq = getNow() + ":\n" + log;
        if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            SDCardRoot = Environment.getExternalStorageDirectory();
        } else {
            SDCardRoot = Environment.getDataDirectory();
        }

        FileLocation = SDCardRoot.getAbsolutePath() + "/nuntu/logs";
        File file = new File(FileLocation);
        try {
            if (!file.exists()) {
                if (!file.mkdirs()) {
                    Log.d("Nuntu Log", "Failed to create parent directories : ");
                }
            }
            FileWriter fw = new FileWriter(file.getAbsoluteFile() + "/" + FileName);
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write(LogMsq);
            bw.close();
        } catch (Exception e) {
            Log.d("Nuntu Log", "File write failed: " + e.getMessage().toString());
        }
    }

    public ArrayList<String> getDirFiles(File f) {
        ArrayList<String> fileList = new ArrayList<String>();
        File[] files = f.listFiles();
        if (files != null) {
            fileList.clear();
            for (File file : files) {
                fileList.add(file.getPath());
            }
        }
        return fileList;
    }
}
