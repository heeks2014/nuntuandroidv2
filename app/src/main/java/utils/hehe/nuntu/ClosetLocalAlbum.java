package utils.hehe.nuntu;

import android.graphics.drawable.Drawable;

import java.util.ArrayList;

/**
 * Created by snickwizard on 2/10/15.
 */
public class ClosetLocalAlbum {
    private Drawable res;
    private String name = "Unkown";
    private String author = "Unkown";
    private ArrayList<String> tracks;

    public Drawable getCoverImage() {
        return res;
    }

    public void setCoverImage(Drawable id) {
        res = id;
    }

    public String getAlbumTitle() {
        return name;
    }

    public void setAlbumTitle(String name) {
        if (name != null) {
            this.name = name;
        }
    }

    public String getAlbumAuthor() {
        return author;
    }

    public void setAlbumAuthor(String author) {
        if (author != null) {
            this.author = author;
        }
    }

    public ArrayList<String> getAlbumTracks() {
        return this.tracks;
    }

    public void setAlbumTracks(ArrayList<String> tracks) {
        this.tracks = tracks;
    }
}
