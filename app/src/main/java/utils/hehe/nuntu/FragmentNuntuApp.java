package utils.hehe.nuntu;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by snickwizard on 2/10/15.
 */
public class FragmentNuntuApp extends Fragment {


    private static final String DEBUG = "APPS FRAG";
    private static String PROMO_IMAGE_URL = "http://nuntu.hehelabs.com/data/defaults/user/apps.jpg";
    private static SwipeRefreshLayout appSwipeLayout;
    private static Context appContext;
    ArrayList<App> AppList;
   static  GridView appGridView;
    ImageView promoImage;
    HolderAppBaseAdapter appAdapter;

    public static FragmentNuntuApp getInstance(int position) {
        FragmentNuntuApp frag = new FragmentNuntuApp();
        Bundle args = new Bundle();
        args.putInt("position", position);
        args.putString("type", "app");
        frag.setArguments(args);
        Log.e(DEBUG, "in constructor with id " + position);
        return frag;

    }

    // Store instance variables based on arguments passed
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        appContext = getActivity().getBaseContext();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        try {
            final View layout = inflater.inflate(R.layout.fragment_app_list_layout, container, false);
            appSwipeLayout = (SwipeRefreshLayout) layout.findViewById(R.id.app_content_container);
            appGridView = (GridView) layout.findViewById(R.id.grid_view);
            promoImage = (ImageView) layout.findViewById(R.id.promo_banner);

            int width = Device.getDeviceScreenSize("width", appContext,"dpi");
            if (width < 240) {
                appGridView.setNumColumns(1);
            } else if (width > 240 && width < 600) {
                appGridView.setNumColumns(2);
            } else {
                appGridView.setNumColumns(3);
            }
            if (Build.VERSION.SDK_INT >= 14) {
                appSwipeLayout.setColorScheme(android.R.color.holo_blue_bright,
                        android.R.color.holo_green_light,
                        android.R.color.holo_orange_light,
                        android.R.color.holo_red_light);
            } else {
                appSwipeLayout.setColorScheme(Color.argb(255, 255, 165, 0),
                        Color.argb(180, 255, 165, 0),
                        Color.argb(125, 255, 165, 0),
                        Color.argb(72, 255, 165, 0));
            }

            Picasso.with(appContext)
                    .load(PROMO_IMAGE_URL)
                    .error(R.drawable.placeholder_apps)
                    .placeholder(R.drawable.placeholder_apps)
                    .into(promoImage);

            appSwipeLayout.setProgressViewOffset(false, 0, (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 24, getResources().getDisplayMetrics()));
            appSwipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    getListAppsAsync("/api/list/app/android/100");
                }
            });
            getListAppsAsync("/api/list/app/android/100");

            return layout;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } catch (OutOfMemoryError e) {
            e.printStackTrace();
            return null;
        }
    }


    public void getListAppsAsync(String route) {

        AsyncTask<String, String, String> getTask = new AsyncTask<String, String, String>() {
            String query = "failed";

            @Override
            protected void onPreExecute() {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        appSwipeLayout.setProgressViewOffset(false, 0, (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 24, getResources().getDisplayMetrics()));
                        appSwipeLayout.setRefreshing(true);
                    }
                });
            }

            @Override
            protected String doInBackground(String... params) {
                String response = "";
                try {
                    URL url = new URL(NuntuConstants.HOST.toString() + params[0]);
                    HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                    urlConnection.setConnectTimeout(30000);
                    BufferedReader reader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                    String line = "";

                    while ((line = reader.readLine()) != null) {
                        response += line + "\n";
                    }
                    if (!urlConnection.getContentType().equalsIgnoreCase("application/json")) {
                        Log.e("Bad Content", "Received bad content type while expecting json");
                        return null;
                    }
                    Log.e("SERVER DATA", response.toString());
                    JSONObject obj = new JSONObject(response.toString());
                    query = obj.getString("status");
                    if ("success".equalsIgnoreCase(query)) {

                        if (AppList != null) {
                            AppList.clear();
                        } else {
                            AppList = new ArrayList<App>();
                        }
                        JSONArray apps = new JSONArray(obj.getString("data"));
                        for (int i = 0; i < apps.length(); i++) {
                            JSONObject app = apps.getJSONObject(i);
                            App current = new App();

                            if (app.getString("app_banner") != null) {
                                String AppImage = app.getString("app_banner").replace("../", "");
                                current.setAppImageURL(NuntuConstants.HOST.toString() + "/" + AppImage);
                            }
                            current.setAppID(app.getString("app_id"));
                            current.setAppName(app.getString("app_name"));
                            if (null == app.getString("app_price") || app.getString("app_price").startsWith("0.00")) {
                                current.setAppPrice("FREE");
                                current.setStatus("0");
                            } else {
                                current.setStatus("2");
                                current.setAppPrice(app.getString("app_price") + " " + app.getString("app_currency"));
                            }

                            if (NuntuUtils.isAppInstalled(appContext, app.getString("packagename"))) {
                                current.setStatus("1");
                            }

                            current.setAppPackageName(app.getString("packagename"));
                            current.setAppPublisher(app.getString("app_publisher"));
                            AppList.add(current);
                        }

                    }
                    return "ok";
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e("EXC", "An exception occurred while getting data" + e.getMessage());
                }
                return response;


            }

            protected void onPostExecute(String result) {
                super.onPostExecute(result);
                try {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            appSwipeLayout.setRefreshing(false);
                            if (AppList != null && AppList.size() >= 1) {
                                appAdapter = new HolderAppBaseAdapter(getActivity(), AppList, "0", appGridView.getNumColumns());
                                appGridView.setAdapter(appAdapter);
                                appAdapter.notifyDataSetChanged();
                            }
                        }
                    });


                } catch (Exception exc) {
                    exc.printStackTrace();
                    Log.e("LAYOUTERROR", "An exception thrown at post ex " + exc.getMessage());
                }
            }
        };
        getTask.execute(route);
    }


    public static void hideOpenMenus() {
        if (appGridView != null) {
            int c = appGridView.getChildCount();
            for (int i = 0; i < c; i++) {
                View view = appGridView.getChildAt(i);
                if (view != null) {
                    CardView IcMenu = (CardView) view.findViewById(R.id.item_controls);
                    if (IcMenu != null) {
                        IcMenu.setVisibility(View.GONE);
                    }
                }
            }
        }
    }

}