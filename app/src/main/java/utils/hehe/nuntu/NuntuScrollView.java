package utils.hehe.nuntu;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.ScrollView;

/**
 * Created by snickwizard on 2/11/15.
 */

public class NuntuScrollView extends ScrollView {

    public NuntuScrollView(Context context) {
        super(context);
    }

    public NuntuScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public NuntuScrollView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        final int action = ev.getAction();
        switch (action) {
            case MotionEvent.ACTION_DOWN:

                Log.e("VerticalScrollview", "onInterceptTouchEvent: DOWN super false");
                super.onTouchEvent(ev);
                break;

            case MotionEvent.ACTION_MOVE:
                return false; // redirect MotionEvents to ourself

            case MotionEvent.ACTION_CANCEL:
                Log.e("VerticalScrollview", "onInterceptTouchEvent: CANCEL super false");
                super.onTouchEvent(ev);
                break;

            case MotionEvent.ACTION_UP:
                Log.e("VerticalScrollview", "onInterceptTouchEvent: UP super false");
                return false;

            default:
                Log.e("VerticalScrollview", "onInterceptTouchEvent: " + action);
                break;
        }

        return false;
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        super.onTouchEvent(ev);
        Log.e("VerticalScrollview", "onTouchEvent. action: " + ev.getAction());
        return true;
    }
}