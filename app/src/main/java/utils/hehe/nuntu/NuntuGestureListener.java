package utils.hehe.nuntu;

import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;

/**
 * Created by snick on 2/27/15.
 */
public class NuntuGestureListener extends GestureDetector.SimpleOnGestureListener {


    public static int YScroll = 0;

    // Override s all the callback methods of GestureDetector.SimpleOnGestureListener
    @Override
    public boolean onSingleTapUp(MotionEvent ev) {
        Log.e("2WAY ", " single tap  at x " + ev.getX() + " y " + ev.getY());
        return super.onSingleTapUp(ev);
    }

    @Override
    public void onShowPress(MotionEvent ev) {
        Log.e("2WAY ", " show press  at x " + ev.getX() + " y " + ev.getY());
        super.onShowPress(ev);
    }

    @Override
    public void onLongPress(MotionEvent ev) {
        Log.e("2WAY ", " long press  at x " + ev.getX() + " y " + ev.getY());
        super.onLongPress(ev);
    }

    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
        Log.e("2WAY ", " scrolling x " + distanceX + " y " + distanceY);
        YScroll = (int) distanceY;
        return super.onScroll(e1, e2, distanceX, distanceY);
    }

    @Override
    public boolean onDown(MotionEvent ev) {
        Log.e("2WAY ", " down at x " + ev.getX() + " y " + ev.getY());
        return super.onDown(ev);
    }

    @Override
    public boolean onFling(MotionEvent ev1, MotionEvent ev2, float velocityX, float velocityY) {
        Log.e("2WAY ", " fling with x " + velocityX + " y " + velocityY);
        YScroll = (int) (velocityY * 0.005f);
        return super.onFling(ev1, ev2, velocityX, velocityY);
    }
}
