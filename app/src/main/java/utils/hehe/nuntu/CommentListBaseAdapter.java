package utils.hehe.nuntu;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import utils.hehe.nuntu.R;

import com.squareup.picasso.Picasso;

public class CommentListBaseAdapter extends BaseAdapter {

    private static ArrayList<Comment> itemDetailsrrayList;
    Context appcontext;
    String ClickAction;
    private LayoutInflater l_Inflater;

    public CommentListBaseAdapter(Context context, ArrayList<Comment> results) {
        itemDetailsrrayList = results;
        appcontext = context;
        l_Inflater = LayoutInflater.from(context);
    }

    public int getCount() {
        return itemDetailsrrayList.size();
    }

    public Object getItem(int position) {
        return itemDetailsrrayList.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = l_Inflater.inflate(R.layout.comment_list_view, null);
            holder = new ViewHolder();
            holder.commentorName = (TextView) convertView.findViewById(R.id.BoxcommentorName);
            holder.commentNameDate = (TextView) convertView.findViewById(R.id.BoxcommentDate);
            holder.commentBody = (TextView) convertView.findViewById(R.id.BoxcommentBody);
            holder.commentTitle = (TextView) convertView.findViewById(R.id.BoxcommentTitle);
            holder.commentorImage = (ImageView) convertView.findViewById(R.id.BoxcommentorImage);
            convertView.setTag(holder);

        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.commentorName.setText(itemDetailsrrayList.get(position).getCommentAuthor());
        holder.commentNameDate.setText(itemDetailsrrayList.get(position).getCommentDate());
        holder.commentBody.setText(itemDetailsrrayList.get(position).getCommentBody());
        holder.commentTitle.setText(itemDetailsrrayList.get(position).getCommentTile());
        Picasso.with(appcontext)
                .load(itemDetailsrrayList.get(position).getCommentorAvatarUrl())
                .error(R.drawable.profile)
                .placeholder(R.drawable.profile)
                .transform(new CircleTransform(32, 0))
                .resize(64, 64)
                .into(holder.commentorImage);
        return convertView;
    }

    static class ViewHolder {
        TextView commentorName;
        TextView commentNameDate;
        TextView commentTitle;
        TextView commentBody;
        ImageView commentorImage;
    }
}