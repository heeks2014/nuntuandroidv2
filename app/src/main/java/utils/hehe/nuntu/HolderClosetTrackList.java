package utils.hehe.nuntu;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by snickwizard on 2/11/15.
 */
public class HolderClosetTrackList extends BaseAdapter {

    public static Context mActivity;
    public static ArrayList<ClosetLocalTrack> mAlbumTracks;
    public static HolderClosetTrackList instance;
    public LayoutInflater l_Inflater;

    public HolderClosetTrackList(Context ctx, ArrayList<ClosetLocalTrack> albums) {
        mActivity = ctx;
        mAlbumTracks = albums;
        l_Inflater = LayoutInflater.from(mActivity);
        instance = this;

    }

    public static HolderClosetTrackList getInstance() {
        Log.e("TRACKLIST", "instance requested");
        if (instance != null) {
            return instance;
        } else {
            return new HolderClosetTrackList(mActivity, mAlbumTracks);
        }
    }

    public int getCount() {
        return mAlbumTracks.size();
    }


    public Object getItem(int position) {
        return mAlbumTracks.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {

        final ViewHolder holder;
        if (convertView == null) {
            convertView = l_Inflater.inflate(R.layout.closet_album_tracklist_layout, null);
            holder = new ViewHolder();
            convertView.setTag(holder);

        } else {
            convertView.refreshDrawableState();
            holder = (ViewHolder) convertView.getTag();

        }

        holder.TrackName = (TextView) convertView.findViewById(R.id.track_name);
        holder.TrackLen = (TextView) convertView.findViewById(R.id.track_length);
        holder.Playing = (ImageView) convertView.findViewById(R.id.play_pause_icon);
        holder.TrackName.setText(mAlbumTracks.get(position).getTrackTitle());
        holder.TrackLen.setText(mAlbumTracks.get(position).getTrackLength());

        if (0 == mAlbumTracks.get(position).getTrackStatus()) {
            holder.Playing.setImageResource(R.drawable.play);
            holder.TrackName.setTextColor(Color.rgb(90, 90, 90));
            holder.TrackLen.setTextColor(Color.rgb(90, 90, 90));
        } else {
            holder.TrackName.setTextColor(mActivity.getResources().getColor(R.color.primaryColor));
            holder.TrackLen.setTextColor(mActivity.getResources().getColor(R.color.primaryColor));
            holder.Playing.setImageResource(R.drawable.headset);
            Animation anim = new AlphaAnimation(0.0f, 1.0f);
            anim.setDuration(330);
            anim.setStartOffset(20);
            anim.setRepeatMode(Animation.REVERSE);
            anim.setRepeatCount(3);
            anim.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {
                    Log.e("ANIMATION", "animation started");
                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    mAlbumTracks.get(position).setTrackStatus(0);
                    holder.Playing.setImageResource(R.drawable.play);
                    holder.TrackName.setTextColor(Color.rgb(90, 90, 90));
                    holder.TrackLen.setTextColor(Color.rgb(90, 90, 90));
                    Log.e("ANIMATION", "animation finished");
                }

                @Override
                public void onAnimationRepeat(Animation animation) {
                    Log.e("ANIMATION", "animation repeated");
                }
            });
            holder.Playing.startAnimation(anim);
        }

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playTrackInBackground(position);
            }
        });

        holder.TrackName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playTrackInBackground(position);
            }
        });

        holder.TrackLen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playTrackInBackground(position);
            }
        });

        holder.Playing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playTrackInBackground(position);
            }
        });

        holder.TrackLen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playTrackInBackground(position);
            }
        });

        return convertView;
    }

    private void playTrackInBackground(int position) {
        setAsPlaying(position);
        Intent intent = new Intent(mActivity, NuntuMPC.class);
        intent.putExtra("action", "play");
        intent.putExtra("path", mAlbumTracks.get(position).getTrackLocation());
        intent.putExtra("title", mAlbumTracks.get(position).getTrackTitle());
        intent.putExtra("from", "track");
        mActivity.startService(intent);

    }


    public void setAsPlaying(int position) {
        if (mAlbumTracks != null) {
            for (ClosetLocalTrack track : mAlbumTracks) {
                track.setTrackStatus(0);
            }
            Log.e("PLAYING", "paying track number ... " + position);
            mAlbumTracks.get(position).setTrackStatus(1);
            this.notifyDataSetChanged();
        } else {
            Log.e("PLAYING", "there are currently no tracks to play");
        }
    }

    static class ViewHolder {
        TextView TrackName, TrackLen;
        ImageView Playing;
    }
}
