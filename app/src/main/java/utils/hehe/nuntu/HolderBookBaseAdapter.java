package utils.hehe.nuntu;

/**
 * Created by snick on 4/13/15.
 */


import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Locale;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.widget.CardView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

public class HolderBookBaseAdapter extends BaseAdapter {

    private static ArrayList<Book> itemDetailsrrayList;
    static Activity bookcontext;

    private LayoutInflater l_Inflater;
    public String FocusedBookName = "Book", FocusedBookType, FocusedBookSize, FocusedBookID;
    final String HOST = "http://nuntu.hehelabs.com";
    private String details, crespayUrl;
    private String OrderID;
    private String OrderKey;
    private ArrayList<String> paymentMethods;
    int ScreenColumns = 2;
    public static String userID = "0";
    AlertDialog.Builder alertboxDownload;
    private int DeviceWidth, IconWidth = 128, IconHeight = 0;

    public HolderBookBaseAdapter(Activity context, ArrayList<Book> results, String user, int ColNum) {
        itemDetailsrrayList = results;
        bookcontext = context;
        l_Inflater = LayoutInflater.from(bookcontext);
        userID = user;
        this.ScreenColumns = ColNum;
        alertboxDownload = new AlertDialog.Builder(bookcontext);

        DisplayMetrics metrics = new DisplayMetrics();
        bookcontext.getWindowManager().getDefaultDisplay().getMetrics(metrics);
        DeviceWidth = metrics.widthPixels;

        if (DeviceWidth > 1024) {
            DeviceWidth = 1024;
        }

        IconWidth = (metrics.widthPixels / ColNum) - 4;
        IconHeight = (int) (IconWidth / 1.2);
    }

    public int getCount() {
        return itemDetailsrrayList.size();
    }

    public Object getItem(int position) {
        return itemDetailsrrayList.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = l_Inflater.inflate(R.layout.item_row, null);
            holder = new ViewHolder();
            holder.txt_itemName = (TextView) convertView.findViewById(R.id.itemname);
            holder.txt_itemPublisher = (TextView) convertView.findViewById(R.id.item_publisher);
            holder.txt_itemPrice = (TextView) convertView.findViewById(R.id.itemoffer);
            holder.buttons = (CardView) convertView.findViewById(R.id.item_controls);
            holder.itemImage = (ImageView) convertView.findViewById(R.id.itemicon);
            holder.itemTick = (Button) convertView.findViewById(R.id.item_like_button);
            holder.itemShare = (Button) convertView.findViewById(R.id.item_share_button);
            holder.itemButton = (Button) convertView.findViewById(R.id.item_action_button);
            holder.ic_book_menu = (ImageView) convertView.findViewById(R.id.item_ic_menu);

            convertView.setTag(holder);

        } else {
            holder = (ViewHolder) convertView.getTag();
        }


        holder.txt_itemName.setText(itemDetailsrrayList.get(position).getbookName());
        holder.txt_itemPublisher.setText(itemDetailsrrayList.get(position).getbookPublisher());
        holder.txt_itemPrice.setText(itemDetailsrrayList.get(position).getbookPrice());

        Picasso.with(bookcontext)
                .load(itemDetailsrrayList.get(position).getbookBanner())
                .placeholder(R.drawable.book_logo)
                .error(R.drawable.book_logo)
                .resize(IconWidth, IconHeight)
                .centerCrop()
                .into(holder.itemImage);


        if ("1".equals(itemDetailsrrayList.get(position).getbookStatus())) {
            holder.itemButton.setText("Read");
        } else if ("2".equals(itemDetailsrrayList.get(position).getbookStatus())) {
            holder.itemButton.setText("Buy");

        } else {
            holder.itemButton.setText("Download");

        }

        holder.ic_book_menu.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {

                if (holder.buttons.getVisibility() == View.VISIBLE) {
                    holder.buttons.setVisibility(View.GONE);
                } else {
                    FragmentNuntuBook.hideOpenMenus();
                    holder.buttons.setVisibility(View.VISIBLE);
                }
            }
        });

        convertView.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                handleItemClick(holder.buttons, itemDetailsrrayList.get(position));
            }
        });

        holder.txt_itemPrice.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                handleItemClick(holder.buttons, itemDetailsrrayList.get(position));

            }
        });

        holder.txt_itemName.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                handleItemClick(holder.buttons, itemDetailsrrayList.get(position));

            }
        });

        holder.txt_itemPublisher.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                handleItemClick(holder.buttons, itemDetailsrrayList.get(position));

            }
        });
        holder.itemImage.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                handleItemClick(holder.buttons, itemDetailsrrayList.get(position));
            }
        });

        holder.itemButton.setOnClickListener(new OnClickListener() {
            @SuppressWarnings("deprecation")
            public void onClick(View v) {
                holder.buttons.setVisibility(View.GONE);
                final String ClickAction = holder.itemButton.getText().toString().toLowerCase(Locale.ENGLISH).trim();
                if ("download".equalsIgnoreCase(ClickAction) || "buy".equalsIgnoreCase(ClickAction)) {

                } else if ("read".equalsIgnoreCase(ClickAction)) {

                }

            }
        });

        holder.itemShare.setTransformationMethod(null);
        holder.itemTick.setTransformationMethod(null);
        holder.itemButton.setTransformationMethod(null);

        return convertView;
    }

    private void handleItemClick(CardView menu, Book item) {
        try {
            if (menu.getVisibility() != View.VISIBLE) {
                Intent intent = new Intent(bookcontext, BookActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("bookID", item.getbookID());
                intent.putExtra("location", item.getbookLocation());
                bookcontext.startActivity(intent);
            }
            FragmentNuntuBook.hideOpenMenus();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    static class ViewHolder {
        TextView txt_itemName;
        TextView txt_itemPublisher;
        TextView txt_itemPrice;
        ImageView itemImage, ic_book_menu;
        Button itemButton, itemTick, itemShare;
        CardView buttons;
    }
}
