package utils.hehe.nuntu;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by gakuba on 4/20/2015.
 */
public class HolderSimilarAsset extends BaseAdapter {

    private final Context mActivity;
    private ArrayList<SimilarAsset> mLocalAssets;
    private LayoutInflater l_Inflater;
    private static float ScaleFactor = 1.00f;

    public HolderSimilarAsset(Context ctx, ArrayList<SimilarAsset> Assets) {
        this.mActivity = ctx;
        mLocalAssets = Assets;
        l_Inflater = LayoutInflater.from(ctx);
        DisplayMetrics metrics = mActivity.getResources().getDisplayMetrics();
        ScaleFactor = (metrics.heightPixels / metrics.density) / 640.00f;
        Log.e("SCALE FACTOR", "by sceen height " + ScaleFactor + " pix " + metrics.heightPixels + " density " + metrics.density);
    }

    public int getCount() {
        return mLocalAssets.size();
    }


    public Object getItem(int position) {
        return mLocalAssets.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {

        final ViewHolder holder;

        if (convertView == null) {
            convertView = l_Inflater.inflate(R.layout.closet_book_list_layout, null);
            holder = new ViewHolder();

            convertView.setTag(holder);

        } else {
            convertView.refreshDrawableState();
            holder = (ViewHolder) convertView.getTag();

        }
        if (ScaleFactor < 1.00f) {
            holder.parent = (CardView) convertView.findViewById(R.id.card_view);
            holder.parent.setScaleX(ScaleFactor);
            holder.parent.setScaleY(ScaleFactor);
        }
        holder.assetName = (TextView) convertView.findViewById(R.id.book_name);
        holder.assetAuthor = (TextView) convertView.findViewById(R.id.book_publisher);
        holder.cover = (ImageView) convertView.findViewById(R.id.book_icon);

        holder.assetName.setText(mLocalAssets.get(position).getAssetName());
        holder.assetAuthor.setText(mLocalAssets.get(position).getAssetPublisher());

        holder.assetName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
        holder.assetAuthor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        });
        if ("app".equalsIgnoreCase(mLocalAssets.get(position).getType())) {
            Picasso.with(mActivity)
                    .load(mLocalAssets.get(position).getAssetImageURL())
                    .resize(256, 256)
                    .centerCrop()
                    .error(R.drawable.app_logo)
                    .placeholder(R.drawable.app_logo)
                    .into(holder.cover);
        } else if ("book".equalsIgnoreCase(mLocalAssets.get(position).getType())) {
            Picasso.with(mActivity)
                    .load(mLocalAssets.get(position).getAssetImageURL())
                    .resize(256, 256)
                    .centerCrop()
                    .error(R.drawable.book_logo)
                    .placeholder(R.drawable.book_logo)
                    .into(holder.cover);
        } else if ("music".equalsIgnoreCase(mLocalAssets.get(position).getType())) {
            Picasso.with(mActivity)
                    .load(mLocalAssets.get(position).getAssetImageURL())
                    .resize(256, 256)
                    .centerCrop()
                    .error(R.drawable.music_logo)
                    .placeholder(R.drawable.music_logo)
                    .into(holder.cover);
        }
        holder.cover.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });


        return convertView;
    }


    static class ViewHolder {
        TextView assetName, assetAuthor;
        ImageView cover;
        CardView parent;

    }
}