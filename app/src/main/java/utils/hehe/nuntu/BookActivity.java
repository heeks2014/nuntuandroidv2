package utils.hehe.nuntu;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;
import org.lucasr.twowayview.TwoWayView;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;


public class BookActivity extends ActionBarActivity {

    private static Context bookDetailContext;
    private final static String _WAIT = "...";
    TextView BookDesc,
            BookDescTag,
            BookPrice,
            BookName,
            BookHeader,
            BookPublisher,
            BookDwonloadCount,
            BookTicks,
            BookSize,
            HeaderEmail,
            UserName,
            UserRank,
            UserBal,
            BookPageCount,
            BookCategory,
            MoreCommentsTag;

    private ProgressBar UserHeaderProgress, SimilarLoaderProgress;
    private ImageView UserImage,
            UploadPicLogo,
            bookIconImage,
            ActionLogo,
            BookDescCollapseLogo;

    private ProgressDialog _outDialog;
    private NavigationDrawerFragment drawerFragment;
    private Toolbar toolbar;
    private DrawerLayout parent;
    private TwoWayView SimilarBooks;
    private ListView BookCommentList;
    private ScrollView ScrollableParent;

    private String ImageFilePath = "", UserID = "0", BookID = "0", FileLocation = "";
    private boolean isImageFileReady = false;
    private ArrayList<SimilarAsset> BookSimilarList;
    private HolderSimilarAsset SimilarBooksAdapter;
    private CommentRemoteAsync BooksCommentLoader;

    @SuppressWarnings("deprecation")
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        bookDetailContext = this;
        setContentView(R.layout.detail_book_layout);

        toolbar = (Toolbar) findViewById(R.id.app_bar);
        parent = (DrawerLayout) findViewById(R.id.book_detail_drawer_layout);
        ScrollableParent = (ScrollView) findViewById(R.id.scrollable);
        SimilarLoaderProgress = (ProgressBar) findViewById(R.id.similar_loader);

        UserImage = (ImageView) findViewById(R.id.user_image);
        UploadPicLogo = (ImageView) findViewById(R.id.upload_new);
        UploadPicLogo.setVisibility(View.GONE);
        HeaderEmail = (TextView) findViewById(R.id.user_email);
        UserName = (TextView) findViewById(R.id.user_name);
        UserRank = (TextView) findViewById(R.id.user_rank);
        UserBal = (TextView) findViewById(R.id.user_balance);
        UserHeaderProgress = (ProgressBar) findViewById(R.id.user_header_progress);

        BookHeader = (TextView) findViewById(R.id.book_header);
        bookIconImage = (ImageView) findViewById(R.id.bookIcon);
        BookPrice = (TextView) findViewById(R.id.bookPrice);
        BookName = (TextView) findViewById(R.id.bookdetailname);
        BookPublisher = (TextView) findViewById(R.id.bookPublisher);
        BookCategory = (TextView) findViewById(R.id.bookCategory);
        BookSize = (TextView) findViewById(R.id.booksize);
        BookPageCount = (TextView) findViewById(R.id.bookPageCount);
        BookTicks = (TextView) findViewById(R.id.ticksCount);
        BookDwonloadCount = (TextView) findViewById(R.id.downCount);
        ActionLogo = (ImageView) findViewById(R.id.action_logo);
        BookDescTag = (TextView) findViewById(R.id.bookDetailTag);
        BookDescCollapseLogo = (ImageView) findViewById(R.id.collapseIcon);
        BookDesc = (TextView) findViewById(R.id.bookDetail);
        SimilarBooks = (TwoWayView) findViewById(R.id.similar_book_list);
        BookCommentList = (ListView) findViewById(R.id.book_comments);
        MoreCommentsTag = (TextView) findViewById(R.id.moreComments);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        drawerFragment = (NavigationDrawerFragment) getFragmentManager().findFragmentById(R.id.fragment_navigation_drawer_book_detail);
        if (Build.VERSION.SDK_INT >= 14) {
            getWindow().setDimAmount(0.5f);
        }

        FileLocation = getIntent().getStringExtra("path");
        BookID = getIntent().getStringExtra("bookID");
        final SwipeRefreshLayout DrawerRefresher = (SwipeRefreshLayout) findViewById(R.id.refresh_drawer);


        drawerFragment.setUp(R.id.fragment_navigation_drawer_book_detail, parent, toolbar, UserHeaderProgress, DrawerRefresher, "viewer");
        if (DrawerRefresher != null) {
            if (Build.VERSION.SDK_INT >= 14) {
                DrawerRefresher.setColorScheme(android.R.color.holo_blue_bright,
                        android.R.color.holo_green_light,
                        android.R.color.holo_orange_light,
                        android.R.color.holo_red_light);
            }

            DrawerRefresher.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    Log.e("REFRESH", "swipe refreshing in reader");
                    drawerFragment.getDrawerItemsAsync("/api/mobile/drawer/Android");
                }
            });
        }
        final RotateAnimation Expand = new RotateAnimation(0, 180, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        Expand.setInterpolator(new LinearInterpolator());
        Expand.setDuration(300);
        Expand.setFillEnabled(true);
        Expand.setFillAfter(true);
        Expand.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                AlphaAnimation show = new AlphaAnimation(0, 1);
                show.setInterpolator(new LinearInterpolator());
                show.setDuration(250);
                show.setFillEnabled(true);
                show.setFillAfter(true);
                BookDesc.startAnimation(show);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                BookDesc.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        final RotateAnimation Collapse = new RotateAnimation(180, 0, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        Collapse.setInterpolator(new LinearInterpolator());
        Collapse.setDuration(300);
        Collapse.setFillEnabled(true);
        Collapse.setFillAfter(true);
        Collapse.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                AlphaAnimation hide = new AlphaAnimation(1, 0);
                hide.setInterpolator(new LinearInterpolator());
                hide.setDuration(250);
                hide.setFillEnabled(true);
                hide.setFillAfter(true);
                BookDesc.startAnimation(hide);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                BookDesc.clearAnimation();
                BookDesc.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        BookDescTag.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                BookDescCollapseLogo.clearAnimation();
                BookDesc.clearAnimation();
                if (BookDesc.getVisibility() != View.VISIBLE) {
                    BookDescCollapseLogo.startAnimation(Expand);
                } else {
                    BookDescCollapseLogo.startAnimation(Collapse);
                }
            }
        });

        BookDescCollapseLogo.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                BookDescCollapseLogo.clearAnimation();
                BookDesc.clearAnimation();
                if (BookDesc.getVisibility() != View.VISIBLE) {
                    BookDescCollapseLogo.startAnimation(Expand);
                } else {
                    BookDescCollapseLogo.startAnimation(Collapse);
                }
            }
        });

        UserImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (UploadPicLogo.getVisibility() != View.VISIBLE) {
                    UploadPicLogo.setVisibility(View.VISIBLE);
                } else {
                    UploadPicLogo.setVisibility(View.GONE);
                }
            }
        });
        UploadPicLogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isImageFileReady) {
                    Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                    photoPickerIntent.setType("image/*");
                    startActivityForResult(photoPickerIntent, 2);
                } else {
                    UploadPicLogo.setBackgroundColor(Color.argb(0, 0, 0, 0));
                    UploadPicLogo.setImageResource(android.R.drawable.ic_menu_edit);
                    UploadPicLogo.setVisibility(View.GONE);
                    if (Integer.valueOf(UserID) >= 1 && ImageFilePath != null && ImageFilePath.length() >= 6) {
                        new NuntuPictureUploaderAsync(BookActivity.this).execute(ImageFilePath, NuntuConstants.HOST.toString() + "/user/" + UserID + "/picture");
                    } else {
                        Toast.makeText(bookDetailContext, "Image upload failed", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });

        _outDialog = new ProgressDialog(this);

        getBookDataAsync();
        getSimilarBooksAsync("/api/list/similar/android/100");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuItem BackStack = menu.add(0, 0, Menu.NONE, R.string.back_menu_item);
        if (Build.VERSION.SDK_INT < 14) {
            BackStack.setShowAsAction(MenuItem.SHOW_AS_ACTION_WITH_TEXT);
        } else {
            BackStack.setShowAsActionFlags(MenuItem.SHOW_AS_ACTION_WITH_TEXT);
        }

        MenuItem ShareMenuItem = menu.add(0, 1, Menu.NONE, R.string.share_text);
        if (Build.VERSION.SDK_INT < 14) {
            ShareMenuItem.setShowAsAction(MenuItem.SHOW_AS_ACTION_WITH_TEXT);
        } else {
            ShareMenuItem.setShowAsActionFlags(MenuItem.SHOW_AS_ACTION_WITH_TEXT);
        }
        ShareMenuItem.setIcon(android.R.drawable.ic_menu_share);

        MenuItem TickMenuItem = menu.add(0, 2, Menu.NONE, R.string.tick_text);
        if (Build.VERSION.SDK_INT < 14) {
            TickMenuItem.setShowAsAction(MenuItem.SHOW_AS_ACTION_WITH_TEXT);
        } else {
            TickMenuItem.setShowAsActionFlags(MenuItem.SHOW_AS_ACTION_WITH_TEXT);
        }
        TickMenuItem.setIcon(android.R.drawable.ic_menu_set_as);

        BackStack.setIcon(android.R.drawable.ic_menu_close_clear_cancel);

        MenuItem HoldMenuItem = menu.add(0, 3, Menu.NONE, R.string.put_on_hold);
        if (Build.VERSION.SDK_INT < 14) {
            HoldMenuItem.setShowAsAction(MenuItem.SHOW_AS_ACTION_WITH_TEXT);
        } else {
            HoldMenuItem.setShowAsActionFlags(MenuItem.SHOW_AS_ACTION_WITH_TEXT);
        }
        HoldMenuItem.setIcon(android.R.drawable.ic_menu_add);

        return true;
    }

    private void getBookDataAsync() {
        AsyncTask<String, String, String> getTask = new AsyncTask<String, String, String>() {
            String query = "none",
                    _bookDesc = "-",
                    _bookCategory = "-",
                    _bookPageCount = "-",
                    _bookSize = "-",
                    _bookTicks = "-",
                    _bookDwonloadCount = "-",
                    _bookPublisher = "-",
                    _bookName = "-",
                    _bookPrice = "-",
                    icon_url = NuntuConstants.HOST.toString() + "/data/defaults/book.png";

            @Override
            protected void onPreExecute() {
                BookDesc.setText(_WAIT);
                BookCategory.setText(_WAIT);
                BookPageCount.setText(_WAIT);
                BookSize.setText(_WAIT);
                BookTicks.setText(_WAIT);
                BookDwonloadCount.setText(_WAIT);
                BookPublisher.setText(_WAIT);
                BookHeader.setText(_WAIT);
                BookName.setText(_WAIT);
                BookPrice.setText(_WAIT);
            }

            @Override
            protected String doInBackground(String... params) {
                String response = "";
                try {
                    URL url = new URL(NuntuConstants.HOST.toString() + params[0]);
                    HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                    urlConnection.setConnectTimeout(30000);
                    BufferedReader reader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                    String line = "";

                    while ((line = reader.readLine()) != null) {
                        response += line + "\n";
                    }
                    if (!urlConnection.getContentType().equalsIgnoreCase("application/json")) {
                        Log.e("Bad Content", "Received bad content type while expecting json");
                        return null;
                    }
                    Log.e("NUNTU SERVER DATA", response);
                    JSONObject obj = new JSONObject(response);
                    query = obj.getString("status");
                    if ("success".equalsIgnoreCase(query)) {
                        JSONObject BookData = new JSONObject(obj.getString("data"));
                        _bookDesc = BookData.getString("book_story");
                        _bookCategory = BookData.getString("book_category");
                        _bookPageCount = BookData.getString("book_likes");
                        _bookSize = BookData.getString("book_size");
                        _bookDwonloadCount = BookData.getString("book_download_count");
                        _bookTicks = BookData.getString("book_likes");
                        _bookPublisher = BookData.getString("book_publisher");
                        _bookName = BookData.getString("book_name");
                        if (BookData.getString("book_price").startsWith("0.00")) {
                            _bookPrice = "FREE";
                        } else {
                            _bookPrice = BookData.getString("book_price") + " " + BookData.getString("book_currency");
                        }
                        icon_url = BookData.getString("book_icon");
                        if (icon_url.contains("../../../../")) {
                            icon_url = NuntuConstants.HOST.toString() + icon_url.replace("../../../../", "/");
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e("NUNTU EXC", "An exception occurred while getting data" + e.getMessage());
                }

                return query;
            }

            protected void onPostExecute(String result) {
                super.onPostExecute(result);
                try {
                    BookDesc.setText(_bookDesc);
                    BookCategory.setText(_bookCategory);
                    BookPageCount.setText(_bookPageCount);
                    BookTicks.setText(NuntuUtils.formatToUrbanShort(_bookTicks) + " ticks");
                    BookSize.setText(_bookSize);
                    BookDwonloadCount.setText(NuntuUtils.formatToUrbanShort(_bookDwonloadCount) + " downloads");
                    BookPublisher.setText(_bookPublisher);
                    BookHeader.setText(_bookName);
                    BookName.setText(_bookName);
                    BookPrice.setText(_bookPrice);
                    Picasso.with(BookActivity.this)
                            .load(icon_url)
                            .resize(256, 256)
                            .transform(new CircleTransform(5, 2))
                            .centerCrop()
                            .error(R.drawable.book_logo)
                            .placeholder(R.drawable.book_logo)
                            .into(bookIconImage);
                } catch (Exception exc) {
                    exc.printStackTrace();

                }
            }
        };
        getTask.execute("/data/book/info/" + BookID);
    }

    private void getSimilarBooksAsync(String route) {

        AsyncTask<String, String, String> getTask = new AsyncTask<String, String, String>() {
            String query = "failed";

            @Override
            protected void onPreExecute() {
                SimilarLoaderProgress.setVisibility(View.VISIBLE);
            }

            @Override
            protected String doInBackground(String... params) {
                String response = "";
                try {
                    URL url = new URL(NuntuConstants.HOST.toString() + params[0]);
                    HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                    urlConnection.setConnectTimeout(30000);
                    BufferedReader reader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                    String line = "";

                    while ((line = reader.readLine()) != null) {
                        response += line + "\n";
                    }
                    if (!urlConnection.getContentType().equalsIgnoreCase("application/json")) {
                        Log.e("Bad Content", "Received bad content type while expecting json");
                        return null;
                    }
                    Log.e("SERVER DATA", response);
                    JSONObject obj = new JSONObject(response);
                    query = obj.getString("status");
                    if ("success".equalsIgnoreCase(query)) {

                        if (BookSimilarList != null) {
                            BookSimilarList.clear();
                        } else {
                            BookSimilarList = new ArrayList<>();
                        }

                        JSONArray musics = new JSONArray(obj.getString("data"));
                        for (int i = 0; i < 20; i++) {
                            JSONObject music = musics.getJSONObject(i);
                            SimilarAsset current = new SimilarAsset();
                            BookSimilarList.add(current);
                        }

                    }
                    //return "ok";
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e("EXC", "An exception occurred while getting data" + e.getMessage());
                }
                if (BookSimilarList != null) {
                    BookSimilarList.clear();
                } else {
                    BookSimilarList = new ArrayList<>();
                }

                for (int i = 0; i < 20; i++) {
                    SimilarAsset current = new SimilarAsset();
                    current.setAssetID("" + i + 1);
                    current.setType("book");
                    current.setAssetPublisher("Angela's Capsule " + i + 1);
                    current.setAssetName("Breaking " + i + 1 + " Bricks");
                    if (i % 2 == 0) {
                        current.setAssetImageURL("http://nuntu.hehelabs.com/data/promos/albums/419/Siriprize_rmx_covers/BITE-.png");
                    } else {
                        current.setAssetImageURL("http://nuntu.hehelabs.com/data/promos/albums/499/Empty_House_covers/Banner.jpg");
                    }
                    BookSimilarList.add(current);
                }

                return response;


            }

            protected void onPostExecute(String result) {
                super.onPostExecute(result);
                SimilarLoaderProgress.setVisibility(View.GONE);
                try {


                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (BookSimilarList != null && BookSimilarList.size() >= 1) {
                                SimilarBooksAdapter = new HolderSimilarAsset(BookActivity.this, BookSimilarList);
                                SimilarBooks.setAdapter(SimilarBooksAdapter);
                                SimilarBooksAdapter.notifyDataSetChanged();
                            }
                        }
                    });
                    BooksCommentLoader = new CommentRemoteAsync(bookDetailContext, BookCommentList);
                    BooksCommentLoader.getCommentsAsync(NuntuConstants.HOST.toString() + "/data/mobile_book/" + BookID + "/comments", false);
                } catch (Exception exc) {
                    exc.printStackTrace();
                }
            }
        };
        getTask.execute(route);
    }
}
