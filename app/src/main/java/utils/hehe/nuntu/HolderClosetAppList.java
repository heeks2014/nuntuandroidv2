package utils.hehe.nuntu;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by snickwizard on 2/11/15.
 */
public class HolderClosetAppList extends BaseAdapter {

    private final Context mActivity;
    private ArrayList<ClosetLocalApp> mApps;
    private LayoutInflater l_Inflater;
    private static float ScaleFactor = 1.00f;
    public HolderClosetAppList(Context ctx, ArrayList<ClosetLocalApp> apps) {
        this.mActivity = ctx;
        mApps = apps;
        l_Inflater = LayoutInflater.from(ctx);
        DisplayMetrics metrics = mActivity.getResources().getDisplayMetrics();
        ScaleFactor = (metrics.heightPixels / metrics.density) / 640.00f;
        Log.e("SCALE FACTOR", "by sceen height " + ScaleFactor);
    }

    public int getCount() {
        return mApps.size();
    }


    public Object getItem(int position) {
        return mApps.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {

        final ViewHolder holder;
        if (convertView == null) {
            convertView = l_Inflater.inflate(R.layout.closet_applist_layout, null);
            holder = new ViewHolder();

            convertView.setTag(holder);

        } else {
            convertView.refreshDrawableState();
            holder = (ViewHolder) convertView.getTag();

        }
        if (ScaleFactor <1.00f) {
            holder.parent = (CardView) convertView.findViewById(R.id.card_view);
            holder.parent.setScaleX(ScaleFactor);
            holder.parent.setScaleY(ScaleFactor);
        }
        holder.AppName = (TextView) convertView.findViewById(R.id.app_name);
        holder.AppPublisher = (TextView) convertView.findViewById(R.id.app_publisher);
        holder.Icon = (ImageView) convertView.findViewById(R.id.app_icon);
        holder.AppName.setText(mApps.get(position).getAppTitle());
        holder.AppPublisher.setText(mApps.get(position).getAppAuthor());
        holder.AppPublisher.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent LaunchIntent = mActivity.getPackageManager().getLaunchIntentForPackage(mApps.get(position).getAppPackageName().trim());
                mActivity.startActivity(LaunchIntent);
            }
        });
        holder.Icon.setImageDrawable(mApps.get(position).getIcon());
        holder.Icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent LaunchIntent = mActivity.getPackageManager().getLaunchIntentForPackage(mApps.get(position).getAppPackageName().trim());
                mActivity.startActivity(LaunchIntent);
            }
        });
        holder.AppName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent LaunchIntent = mActivity.getPackageManager().getLaunchIntentForPackage(mApps.get(position).getAppPackageName().trim());
                mActivity.startActivity(LaunchIntent);
            }
        });
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent LaunchIntent = mActivity.getPackageManager().getLaunchIntentForPackage(mApps.get(position).getAppPackageName().trim());
                mActivity.startActivity(LaunchIntent);
            }
        });
        return convertView;
    }

    public void updateLoadedAlbums(List<ClosetLocalApp> newlist) {
        mApps.clear();
        mApps.addAll(newlist);
        this.notifyDataSetChanged();
    }

    static class ViewHolder {
        TextView AppName, AppPublisher;
        ImageView Icon;
        CardView parent;
    }
}