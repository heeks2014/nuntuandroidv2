package utils.hehe.nuntu;

import android.content.Context;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Map;

/**
 * Created by snickwizard on 2/10/15.
 */
public class FragmentNuntuMusic extends Fragment {

    private static final String DEBUG = "APPS FRAG";
    private static SwipeRefreshLayout musicSwipeLayout;
    private static ImageView Banner;
    GridView musicGridView;
    private static String PROMO_IMAGE_URL = "http://nuntu.hehelabs.com/data/defaults/user/music.jpg";
    private static Context MusicFragContext;
    public static ArrayList<Album> AlbumList;
    HolderMusicBaseAdapter musicAdapter;

    public static FragmentNuntuMusic getInstance(int position) {
        FragmentNuntuMusic frag = new FragmentNuntuMusic();
        Bundle args = new Bundle();
        args.putInt("position", position);
        args.putString("type", "app");
        frag.setArguments(args);
        Log.e(DEBUG, "in constructor with id " + position);
        return frag;

    }


    // Store instance variables based on arguments passed
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MusicFragContext = getActivity().getApplicationContext();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        try {
            final View layout = inflater.inflate(R.layout.fragment_music_list_layout, container, false);
            musicSwipeLayout = (SwipeRefreshLayout) layout.findViewById(R.id.music_content_container);
            musicGridView = (GridView) layout.findViewById(R.id.grid_view);
            Banner = (ImageView) layout.findViewById(R.id.promo_banner);


            Picasso.with(MusicFragContext)
                    .load(PROMO_IMAGE_URL)
                    .error(R.drawable.placeholder_music)
                    .placeholder(R.drawable.placeholder_music)
                    .into(Banner);

            musicSwipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {


                }
            });


            int width = Device.getDeviceScreenSize("width", MusicFragContext,"dpi");
            if (width < 240) {
                musicGridView.setNumColumns(1);
            } else if (width > 240 && width < 600) {
                musicGridView.setNumColumns(2);
            } else {
                musicGridView.setNumColumns(3);
            }
            if (Build.VERSION.SDK_INT >= 14) {
                musicSwipeLayout.setColorScheme(android.R.color.holo_blue_bright,
                        android.R.color.holo_green_light,
                        android.R.color.holo_orange_light,
                        android.R.color.holo_red_light);
            } else {
                musicSwipeLayout.setColorScheme(Color.argb(255, 255, 165, 0),
                        Color.argb(180, 255, 165, 0),
                        Color.argb(125, 255, 165, 0),
                        Color.argb(72, 255, 165, 0));
            }


            musicSwipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    getListMusicAsync("/api/list/music/android/100");
                }
            });

            getListMusicAsync("/api/list/music/android/100");

            return layout;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } catch (OutOfMemoryError e) {
            e.printStackTrace();
            return null;
        }
    }

    public void getListMusicAsync(String route) {

        AsyncTask<String, String, String> getTask = new AsyncTask<String, String, String>() {
            String query = "failed";

            @Override
            protected void onPreExecute() {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        musicSwipeLayout.setProgressViewOffset(false, 0, (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 24, getResources().getDisplayMetrics()));
                        musicSwipeLayout.setRefreshing(true);
                    }
                });
            }

            @Override
            protected String doInBackground(String... params) {
                String response = "";
                try {
                    URL url = new URL(NuntuConstants.HOST.toString() + params[0]);
                    HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                    urlConnection.setConnectTimeout(30000);
                    BufferedReader reader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                    String line = "";

                    while ((line = reader.readLine()) != null) {
                        response += line + "\n";
                    }
                    if (!urlConnection.getContentType().equalsIgnoreCase("application/json")) {
                        Log.e("Bad Content", "Received bad content type while expecting json");
                        return null;
                    }
                    Log.e("SERVER DATA", response);
                    JSONObject obj = new JSONObject(response);
                    query = obj.getString("status");
                    if ("success".equalsIgnoreCase(query)) {

                        if (AlbumList != null) {
                            AlbumList.clear();
                        } else {
                            AlbumList = new ArrayList<>();
                        }

                        JSONArray musics = new JSONArray(obj.getString("data"));
                        for (int i = 0; i < musics.length(); i++) {
                            JSONObject music = musics.getJSONObject(i);
                            Album current = new Album();

                            if (music.getString("music_banner") != null) {
                                String AppImage = music.getString("music_banner").replace("../", "");
                                current.setAlbumBanner(NuntuConstants.HOST.toString() + "/" + AppImage);
                            }
                            current.setAlbumID(music.getString("music_id"));
                            current.setAlbumName(music.getString("music_name"));
                            if (null == music.getString("music_price") || music.getString("music_price").startsWith("0.00")) {
                                current.setAlbumPrice("FREE");
                                current.setAlbumStatus("0");
                            } else {
                                current.setAlbumStatus("2");
                                current.setAlbumPrice(music.getString("music_price") + " " + music.getString("music_currency"));
                            }
                            ArrayList<String> AlbumLocalTracks = searchAlbumTracks(music.getString("music_name"));
                            if (AlbumLocalTracks != null && AlbumLocalTracks.size() >= 1) {
                                current.setAlbumStatus("1");
                                current.setAlbumLocalTracks(AlbumLocalTracks);
                            }

                            current.setAlbumPublisher(music.getString("music_publisher"));
                            AlbumList.add(current);
                        }

                    }
                    return "ok";
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e("EXC", "An exception occurred while getting data" + e.getMessage());
                }
                return response;


            }

            protected void onPostExecute(String result) {
                super.onPostExecute(result);
                try {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            musicSwipeLayout.setRefreshing(false);
                            if (AlbumList != null && AlbumList.size() >= 1) {
                                musicAdapter = new HolderMusicBaseAdapter(getActivity(), AlbumList, "0", musicGridView.getNumColumns());
                                musicGridView.setAdapter(musicAdapter);
                                musicAdapter.notifyDataSetChanged();
                            }
                        }
                    });


                } catch (Exception exc) {
                    exc.printStackTrace();
                    Log.e("LAYOUTERROR", "An exception thrown at post ex " + exc.getMessage());
                }
            }
        };
        getTask.execute(route);
    }

    private ArrayList<String> searchAlbumTracks(String Name) {
        try {
            if (FragmentNuntuCloset.MusicList != null) {
                for (int i = 0; i < FragmentNuntuCloset.MusicList.size(); i++) {
                    Log.e("ALBUM NAME"," at "+i+" we have "+FragmentNuntuCloset.MusicList.get(i).getAlbumTitle());
                    if (FragmentNuntuCloset.MusicList.get(i).getAlbumTitle().contains(Name)) {
                        return FragmentNuntuCloset.MusicList.get(i).getAlbumTracks();
                    }
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}