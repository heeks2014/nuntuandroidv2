package utils.hehe.nuntu;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

public class UserAppBaseAdapter extends BaseAdapter {
    private final LayoutInflater l_Inflater;
    final String HOST = "http://nuntu.hehelabs.com";
    private static ArrayList<UserApp> itemDetailsrrayList;
    static Context userappcontext;
    private String userID;

    public UserAppBaseAdapter(Context context, ArrayList<UserApp> results, String currentUser) {
        itemDetailsrrayList = results;
        userappcontext = context;
        l_Inflater = LayoutInflater.from(context);
        userID = currentUser;

    }

    @Override
    public int getCount() {
        return itemDetailsrrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return itemDetailsrrayList.get(position);
    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = l_Inflater.inflate(R.layout.my_apps_layout, null);
            holder = new ViewHolder();
            convertView.setTag(holder);

        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.AppName = (TextView) convertView.findViewById(R.id.my_app_name);
        holder.AppVersion = (TextView) convertView.findViewById(R.id.my_app_version);
        holder.AppIcon = (ImageView) convertView.findViewById(R.id.my_app_icon);
        holder.AppIcon.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent LaunchIntent = userappcontext.getPackageManager().getLaunchIntentForPackage(itemDetailsrrayList.get(position).getUserAppPackage().trim());
                    userappcontext.startActivity(LaunchIntent);
                    Log.e("START APP", itemDetailsrrayList.get(position).getUserAppPackage().trim());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        holder.AppAction = (Button) convertView.findViewById(R.id.my_app_action_button);

        holder.AppAction.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if ("uninstall".equalsIgnoreCase(holder.AppAction.getText().toString())) {
                    String packageName = "package:" + itemDetailsrrayList.get(position).getUserAppPackage().trim();
                    Intent intent = new Intent(Intent.ACTION_DELETE);
                    intent.setData(Uri.parse(packageName));
                    userappcontext.startActivity(intent);
                } else if ("update".equalsIgnoreCase(holder.AppAction.getText().toString())) {
                    Intent downloadApp = new Intent(userappcontext, NuntuTransactDaemon.class);
                    if (downloadApp != null) {
                        String downloadurl = "/user/" + userID + "/claim/download/" + itemDetailsrrayList.get(position).getUserAppDownloadID() + "/" + itemDetailsrrayList.get(position).getUserAppDownloadKey();
                        downloadApp.putExtra("action", "download");
                        downloadApp.putExtra("type", "app");
                        downloadApp.putExtra("appID", itemDetailsrrayList.get(position).getUserAppID());
                        downloadApp.putExtra("appName", itemDetailsrrayList.get(position).getUserAppName());
                        downloadApp.putExtra("packagename", itemDetailsrrayList.get(position).getUserAppPackage());
                        downloadApp.putExtra("link", downloadurl);
                        userappcontext.startService(downloadApp);
                        Toast.makeText(userappcontext, itemDetailsrrayList.get(position).getUserAppName() + " is download is requested. check on it in notification bar.", Toast.LENGTH_LONG + Toast.LENGTH_LONG).show();
                    }
                } else if ("remove".equalsIgnoreCase(holder.AppAction.getText().toString())) {
                    Toast.makeText(userappcontext, itemDetailsrrayList.get(position).getUserAppName() + " will be removed from your Nuntu app list.", Toast.LENGTH_LONG + Toast.LENGTH_LONG).show();
                } else {
                    Intent downloadApp = new Intent(userappcontext, NuntuTransactDaemon.class);
                    if (downloadApp != null) {
                        String downloadurl = "/user/" + userID + "/claim/download/" + itemDetailsrrayList.get(position).getUserAppDownloadID() + "/" + itemDetailsrrayList.get(position).getUserAppDownloadKey();
                        downloadApp.putExtra("action", "download");
                        downloadApp.putExtra("type", "app");
                        downloadApp.putExtra("appID", itemDetailsrrayList.get(position).getUserAppID());
                        downloadApp.putExtra("appName", itemDetailsrrayList.get(position).getUserAppName());
                        downloadApp.putExtra("packagename", itemDetailsrrayList.get(position).getUserAppPackage());
                        downloadApp.putExtra("link", downloadurl);
                        userappcontext.startService(downloadApp);
                        Toast.makeText(userappcontext, itemDetailsrrayList.get(position).getUserAppName() + " is download is requested. check on it in notification bar.", Toast.LENGTH_LONG + Toast.LENGTH_LONG).show();
                    }
                }
            }
        });

        holder.AppName.setText(itemDetailsrrayList.get(position).getUserAppName());
        holder.AppVersion.setText(itemDetailsrrayList.get(position).getUserAppVersion());

        Picasso.with(userappcontext)
                .load(itemDetailsrrayList.get(position).getUserAppIcon())
                .error(R.drawable.app_logo).
                placeholder(R.drawable.app_logo)
                .resize(120, 120).into(holder.AppIcon);

        if ("1".equals(itemDetailsrrayList.get(position).getUserAppStatus())) {
            holder.AppAction.setText("UNINSTALL");
        } else if ("2".equals(itemDetailsrrayList.get(position).getUserAppStatus())) {
            holder.AppAction.setText("REMOVE");
        } else if ("3".equals(itemDetailsrrayList.get(position).getUserAppStatus())) {
            holder.AppAction.setText("REINSTALL");
        } else if ("4".equals(itemDetailsrrayList.get(position).getUserAppStatus())) {
            holder.AppAction.setText("UPDATE");
        } else {
            holder.AppAction.setText("DOWNLOAD");
        }
        return convertView;
    }

    static class ViewHolder {
        TextView AppName;
        TextView AppVersion;
        ImageView AppIcon;
        Button AppAction;
    }

    public void updateReceiptsList(List<UserApp> newlist) {
        itemDetailsrrayList.clear();
        itemDetailsrrayList.addAll(newlist);
        this.notifyDataSetChanged();
    }

    public static boolean hasConnection() {
        final ConnectivityManager cm = (ConnectivityManager) userappcontext.getSystemService(Context.CONNECTIVITY_SERVICE);

        final NetworkInfo wifiNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        if (wifiNetwork != null && wifiNetwork.isConnected()) {
            return true;
        }

        final NetworkInfo mobileNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if (mobileNetwork != null && mobileNetwork.isConnected()) {
            return true;
        }

        final NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (activeNetwork != null && activeNetwork.isConnected()) {
            return true;
        }

        return false;
    }
}
