package utils.hehe.nuntu;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.File;
import java.io.IOException;

public class Device extends Activity {

    private String phoneNumber = null;
    private String IMEI = null;
    private String isp = null;

    public static int getDeviceScreenSize(String dim, Context ctx, String mode) {
        try {
            DisplayMetrics metrics = ctx.getResources().getDisplayMetrics();
            if ("dpi".equalsIgnoreCase(mode)) {
                float ScaleFactor = metrics.density;
                if ("width".equals(dim)) {
                    return (int) (metrics.widthPixels / ScaleFactor);
                } else {
                    return (int) (metrics.heightPixels / ScaleFactor);
                }
            } else {
                if ("width".equals(dim)) {
                    return metrics.widthPixels;
                } else {
                    return metrics.heightPixels;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            if ("width".equals(dim)) {
                return 240;
            } else {
                return 420;
            }

        }
    }

    public String getPhoneNumber(Context appCont) {
        if (phoneNumber == null) {
            TelephonyManager systIDs = (TelephonyManager) appCont.getSystemService(Context.TELEPHONY_SERVICE);
            phoneNumber = systIDs.getLine1Number();
        }
        return this.phoneNumber;
    }

    public String getPhoneIMEI(Context appCont) {
        try {
            if (IMEI == null) {
                TelephonyInfo DoubleSim = TelephonyInfo.getInstance(appCont);
                if (DoubleSim != null) {
                    if (!DoubleSim.isDualSIM()) {
                        TelephonyManager systIDs = (TelephonyManager) appCont.getSystemService(Context.TELEPHONY_SERVICE);
                        IMEI = systIDs.getDeviceId();
                        if ((IMEI == null || IMEI.length() <= 5) && Build.VERSION.SDK_INT >= 9) {
                            IMEI = Build.SERIAL;
                        }
                        if (IMEI == null || IMEI.length() <= 3) {
                            IMEI = getMacAddress(appCont);
                        }
                        if (IMEI == null || IMEI.length() <= 3) {
                            IMEI = Build.ID;
                        }
                    } else {
                        IMEI = DoubleSim.getImeiSIM1();
                        if (IMEI == null || IMEI.length() <= 10) {
                            TelephonyManager systIDs = (TelephonyManager) appCont.getSystemService(Context.TELEPHONY_SERVICE);
                            IMEI = systIDs.getDeviceId();
                            if (IMEI == null || IMEI.length() <= 5 && Build.VERSION.SDK_INT >= 9) {
                                IMEI = Build.SERIAL;
                            }
                            if (IMEI == null || IMEI.length() <= 3) {
                                IMEI = getMacAddress(appCont);
                            }
                            if (IMEI == null || IMEI.length() <= 3) {
                                IMEI = Build.ID;
                            }
                        }
                    }
                } else {
                    TelephonyManager systIDs = (TelephonyManager) appCont.getSystemService(Context.TELEPHONY_SERVICE);
                    IMEI = systIDs.getDeviceId();
                    if ((IMEI == null || IMEI.length() <= 5) && Build.VERSION.SDK_INT >= 9) {
                        IMEI = Build.SERIAL;
                    }
                    if (IMEI == null || IMEI.length() <= 3) {
                        IMEI = getMacAddress(appCont);
                    }
                    if (IMEI == null || IMEI.length() <= 3) {
                        IMEI = Build.ID;
                    }
                }
                return this.IMEI;
            } else {
                return IMEI;
            }

        } catch (Exception exc) {
            Log.e("IMEI", "damn it " + exc.getMessage());
            return null;
        }
    }

    public String getMacAddress(Context context) {
        WifiManager wimanager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        return wimanager.getConnectionInfo().getMacAddress();
    }

    public String getPhoneSerial() {
        if (!Build.SERIAL.isEmpty()) {
            return Build.SERIAL;
        } else {
            if (!Build.DEVICE.isEmpty()) {
                return Build.DEVICE;
            } else {
                return "NOTFOUND";
            }
        }
    }

    public String getPhoneISP(Context appCont) {
        if (isp == null) {
            TelephonyManager systIDs = (TelephonyManager) appCont.getSystemService(Context.TELEPHONY_SERVICE);
            isp = systIDs.getNetworkOperatorName();
            if (isp != null && isp.length() <= 2)
                isp = systIDs.getSimOperatorName();
            if (isp != null && isp.length() <= 2)
                isp = systIDs.getNetworkOperator();
            if (isp != null && isp.length() <= 2)
                isp = systIDs.getSimOperator();
        }
        return this.isp;
    }

    public void registerDeviceAsync(final Context DeviceReg) {
        AsyncTask<String, String, String> RegTask = new AsyncTask<String, String, String>() {
            String query = "failed",
                    details = "haha",
                    userid;
            final Context RepeaterContext = DeviceReg;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                try {
                    String UserVaultData = User.getNuntuLocalLogin(DeviceReg);
                    if (UserVaultData != null && UserVaultData.length() > 10) {
                        JSONObject Local = new JSONObject(UserVaultData);
                        userid = Local.getString("userid");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            protected String doInBackground(String... params) {
                if (Integer.valueOf(userid) >= 1) {
                    JSONObject deviceData = null;
                    try {
                        deviceData = new JSONObject();
                        String internalStorge = "0";
                        String externalStorage = "0";
                        DisplayMetrics displayMetrics = DeviceReg.getResources().getDisplayMetrics();
                        String width = String.valueOf(displayMetrics.widthPixels);
                        String height = String.valueOf(displayMetrics.heightPixels);

                        deviceData.put("device_height", height);
                        deviceData.put("device_width", width);

                        if (Build.VERSION.SDK_INT >= 9) {
                            internalStorge = String.valueOf(new File(DeviceReg.getFilesDir().getAbsoluteFile().toString()).getFreeSpace());
                            externalStorage = String.valueOf(new File(DeviceReg.getExternalFilesDir(null).toString()).getFreeSpace());
                        }
                        deviceData.put("main_storage", internalStorge);
                        deviceData.put("ext_storage", externalStorage);

                        if (Build.MODEL != null && Build.HARDWARE != null && Build.ID != null && Build.MANUFACTURER != null) {
                            deviceData.put("device_type", Build.MODEL + "@" + Build.HARDWARE + "@" + Build.ID + "@" + Build.MANUFACTURER);
                        } else {
                            deviceData.put("device_type", "ADV" + userid);
                        }

                        deviceData.put("imei", getPhoneIMEI(DeviceReg));
                        deviceData.put("sn", getPhoneSerial());
                        deviceData.put("owner", userid);
                        deviceData.put("os", "android");
                        deviceData.put("owner", userid);
                        deviceData.put("osversion", String.valueOf(Build.VERSION.SDK_INT));

                    } catch (Exception e) {
                        e.getStackTrace();
                    }

                    HttpClient httpclient = new DefaultHttpClient();
                    // specify the URL you want to post to
                    HttpPost httppost = new HttpPost(NuntuConstants.HOST.toString() + "/user/" + userid + "/device");
                    try {
                        if (deviceData == null) {
                            return null;
                        }
                        httppost.setEntity(new StringEntity(deviceData.toString(), "UTF8"));
                        httppost.addHeader("Content-type", "application/json");
                        httppost.addHeader("Accept", "application/json");
                        httppost.addHeader("User-Agent", "nuntumobile");
                        // send the variable and value, in other words post, to the URL
                        HttpResponse response = httpclient.execute(httppost);

                        HttpEntity entity = response.getEntity();
                        String Response = EntityUtils.toString(entity);
                        Log.e("Reg Data Rec", Response);
                        try {
                            JSONObject ResponseJson = (JSONObject) new JSONTokener(Response).nextValue();
                            query = ResponseJson.getString("status");
                            details = ResponseJson.getString("details");
                        } catch (Exception exc) {
                            exc.printStackTrace();
                        }

                    } catch (ClientProtocolException e) {
                        e.getStackTrace();

                    } catch (IOException e) {
                        e.printStackTrace();

                    }
                    return query;
                } else {
                    return null;
                }
            }

            @Override
            protected void onPostExecute(String result) {
                super.onPostExecute(result);
                if (!"success".equalsIgnoreCase(result) && !details.contains("-3000")) {
                    registerDeviceAsync(RepeaterContext);
                }
            }
        };
        RegTask.execute();

    }
}
