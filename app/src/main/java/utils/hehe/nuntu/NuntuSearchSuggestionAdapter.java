package utils.hehe.nuntu;

/**
 * Created by snickwizard on 2/7/15.
 */

import android.app.SearchManager;
import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;


public class NuntuSearchSuggestionAdapter extends CursorAdapter {
    public NuntuSearchSuggestionAdapter(Context context, Cursor cursor) {
        super(context, cursor, 0);
    }


    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.search_suggestion_item, parent, false);
        return view;
    }


    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        // reference
        RelativeLayout root = (RelativeLayout) view;
        TextView titleTextView = (TextView) root.findViewById(R.id.search_suggestion_item_title);
        TextView subtitleTextView = (TextView) root.findViewById(R.id.search_suggestion_item_subtitle);
        final int index2 = cursor.getColumnIndex(SearchManager.SUGGEST_COLUMN_TEXT_2);
        titleTextView.setText(" ");
        subtitleTextView.setText(cursor.getString(index2));
    }


    public void refill(Context context, Cursor cursor) {
        changeCursor(cursor);
    }
}

