package utils.hehe.nuntu;

public class UserApp {

	private String appID=" ";
	private String appName=" ";
	private String appIcon=" ";
	private String appStatus=" ";
	private String appVersion=" ";
	private String appVersionCode="0";
	private String appPackage=" ";
	private String appDownloadID=" ";
	private String appDownloadKey=" ";
	//setters
	public void setUserAppName(String val) {
		this.appName=val;
	}

	public void setUserAppIcon(String val) {
		this.appIcon=val;
	}



	public void setUserAppStatus(String val) {
		this.appStatus=val;
	}

	public void setUserAppVersion(String val) {
		this.appVersion=val;
	}

	public void setUserAppVersionCode(String val) {
		this.appVersionCode=val;
	}

	public void setUserAppPackage(String val) {
		this.appPackage=val;
	}

	public void setUserAppID(String string) {
		this.appID=string;

	} 

	public void setUserAppDownloadID(String string) {
		this.appDownloadID=string;

	} 

	public void setUserAppDownloadKey(String string) {
		this.appDownloadKey=string;

	} 

	//getters
	public String getUserAppName() {
		return this.appName;
	}

	public String getUserAppIcon() {
		return this.appIcon;
	}

	public String getUserAppStatus() {
		return this.appStatus;
	}

	public String getUserAppVersion() {
		return this.appVersion;
	}

	public String getUserAppVersionCode() {
		return this.appVersionCode;
	}
	public String getUserAppPackage() {
		return this.appPackage;
	}

	public String getUserAppID() {
		return this.appID;

	} 
	public String getUserAppDownloadID() {
		return this.appDownloadID;

	} 

	public String getUserAppDownloadKey() {
		return this.appDownloadKey;

	} 

}
