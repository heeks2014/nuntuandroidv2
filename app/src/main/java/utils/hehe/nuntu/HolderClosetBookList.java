package utils.hehe.nuntu;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by snickwizard on 2/11/15.
 */
public class HolderClosetBookList extends BaseAdapter {

    private final Context mActivity;
    private ArrayList<ClosetLocalBook> mLocalBooks;
    private LayoutInflater l_Inflater;
    private static float ScaleFactor = 1.00f;

    public HolderClosetBookList(Context ctx, ArrayList<ClosetLocalBook> Books) {
        this.mActivity = ctx;
        mLocalBooks = Books;
        l_Inflater = LayoutInflater.from(ctx);
        DisplayMetrics metrics = mActivity.getResources().getDisplayMetrics();
        ScaleFactor = (metrics.heightPixels / metrics.density) / 640.00f;
        Log.e("SCALE FACTOR", "by sceen height " + ScaleFactor + " pix " + metrics.heightPixels + " density " + metrics.density);
    }

    public int getCount() {
        return mLocalBooks.size();
    }


    public Object getItem(int position) {
        return mLocalBooks.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {

        final ViewHolder holder;

        if (convertView == null) {
            convertView = l_Inflater.inflate(R.layout.closet_book_list_layout, null);
            holder = new ViewHolder();

            convertView.setTag(holder);

        } else {
            convertView.refreshDrawableState();
            holder = (ViewHolder) convertView.getTag();

        }
        if (ScaleFactor < 1.00f) {
            holder.parent = (CardView) convertView.findViewById(R.id.card_view);
            holder.parent.setScaleX(ScaleFactor);
            holder.parent.setScaleY(ScaleFactor);
        }
        holder.bookName = (TextView) convertView.findViewById(R.id.book_name);
        holder.bookAuthor = (TextView) convertView.findViewById(R.id.book_publisher);
        holder.cover = (ImageView) convertView.findViewById(R.id.book_icon);

        holder.bookName.setText(mLocalBooks.get(position).getBookTitle());
        holder.bookAuthor.setText(mLocalBooks.get(position).getBookAuthor());

        holder.bookName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if ("application/pdf".equalsIgnoreCase(mLocalBooks.get(position).getBookType())) {
                    Intent viewer = new Intent(mActivity, ClosetPDFViewer.class);
                    viewer.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    viewer.putExtra("path", mLocalBooks.get(position).getBookLocation());
                    viewer.putExtra("header", mLocalBooks.get(position).getBookTitle() + "\t -  " + mLocalBooks.get(position).getBookAuthor());

                    viewer.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                    mActivity.startActivity(viewer);
                } else if ("application/epub+zip".equalsIgnoreCase(mLocalBooks.get(position).getBookType())) {
                    Toast.makeText(mActivity, "epub support is coming soon...", Toast.LENGTH_LONG).show();
                }
            }
        });
        holder.bookAuthor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if ("application/pdf".equalsIgnoreCase(mLocalBooks.get(position).getBookType())) {
                    Intent viewer = new Intent(mActivity, ClosetPDFViewer.class);
                    viewer.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    viewer.putExtra("path", mLocalBooks.get(position).getBookLocation());
                    viewer.putExtra("header", mLocalBooks.get(position).getBookTitle() + "\t -  " + mLocalBooks.get(position).getBookAuthor());

                    viewer.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                    mActivity.startActivity(viewer);
                } else if ("application/epub+zip".equalsIgnoreCase(mLocalBooks.get(position).getBookType())) {
                    Toast.makeText(mActivity, "epub support is coming soon...", Toast.LENGTH_LONG).show();
                }
            }
        });

        if (mLocalBooks.get(position).getBookCover() != null) {
            holder.cover.setImageBitmap(mLocalBooks.get(position).getBookCover());
        } else if (TelephonyInfo.hasConnection(mActivity) && mLocalBooks.get(position).getIconURL().length() >= 10) {
            Picasso.with(mActivity)
                    .load(mLocalBooks.get(position).getIconURL())
                    .resize(256, 256)
                    .centerCrop()
                    .error(R.drawable.book_logo)
                    .placeholder(R.drawable.book_logo)
                    .into(holder.cover);
        }
        holder.cover.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if ("application/pdf".equalsIgnoreCase(mLocalBooks.get(position).getBookType())) {
                    Intent viewer = new Intent(mActivity, ClosetPDFViewer.class);
                    viewer.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    viewer.putExtra("path", mLocalBooks.get(position).getBookLocation());
                    viewer.putExtra("header", mLocalBooks.get(position).getBookTitle() + "\t -  " + mLocalBooks.get(position).getBookAuthor());

                    viewer.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                    mActivity.startActivity(viewer);
                } else if ("application/epub+zip".equalsIgnoreCase(mLocalBooks.get(position).getBookType())) {
                    Toast.makeText(mActivity, "epub support is coming soon...", Toast.LENGTH_LONG).show();
                }
            }
        });


        return convertView;
    }


    static class ViewHolder {
        TextView bookName, bookAuthor;
        ImageView cover;
        CardView parent;

    }
}