package utils.hehe.nuntu;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.util.Log;


public class NuntuPollService extends Service {

	private static final String TAG = "NuntuService";
	public String APKPackageName;
	public String AppName="Application";
	public String InstallAppPackageName;
	final String SECUREHOST="https://nuntu.hehelabs.com";
	final String HOST="http://nuntu.hehelabs.com";
	String SocialLoginRoute="/social/login/";

	private static Context backcontext=null;

	ArrayList<String> userCredentials,indexdata,
	NuntuUserAppPackageNames,requestedApps,
	NuntuUserAppID,UpdateApps=null;

	boolean isUserLoggedIn=false;

	String  status,appVersion,
	userID="0",username,
	userpass,lastLoginType,
	email,query="failed";

	Timer timer,regUserApp;

	int delayPeriod = 120000; // delay for 2 sec.
	NuntuFile filesys;
	Nuntu Parent;
	private String IMEI;
	NotificationManager AppUpdateBuilder;
	NotificationCompat.Builder AppUpdateNotificationManager;
	SharedPreferences pollInterval;
	String pollvalue;

	@Override
	public void onCreate() {
		super.onCreate();
		if(backcontext==null) {
			backcontext=getApplication().getApplicationContext();
		}
		if(filesys==null){ 
			filesys=new NuntuFile();
		}

		if(AppUpdateNotificationManager==null){ 
			AppUpdateNotificationManager = new NotificationCompat.Builder(backcontext);
		}

		if(AppUpdateBuilder==null){ 
			AppUpdateBuilder=(NotificationManager) getSystemService(NOTIFICATION_SERVICE);
		}

		appVersion=NuntuConstants.APP_VERSION.toString();

		if(Parent==null) {
			Parent=new Nuntu();
		}
		if(requestedApps==null) {
			requestedApps=new ArrayList<String>();
		}

		if(NuntuUserAppPackageNames==null) {
			NuntuUserAppPackageNames=new ArrayList<String>();
		}
		if(NuntuUserAppID==null) {
			NuntuUserAppID=new ArrayList<String>();
		}

		if(timer==null) {
			timer = new Timer();	
		}

		if(regUserApp==null) {
			regUserApp = new Timer();	
		}

		IMEI=new Device().getPhoneIMEI(backcontext);

		Log.e(TAG, "Nuntu Service creating");
	}


	@Override
	public void onDestroy() {
		Log.e("NPS","stopping NPS service");
		if(userCredentials!=null) {
			userCredentials.clear();
		}
		requestedApps.clear();
		if(indexdata!=null)
			indexdata.clear();
		backcontext=null;
		if(timer!=null) {
			timer.cancel();
			timer=null;
		}

		if(regUserApp!=null) {
			regUserApp.cancel();
			regUserApp=null;
		}

		System.gc();
		super.onDestroy();	
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {

		try {
			if(intent!=null) {
				Log.e(TAG, "Nuntu service started and ready ");
				String action  =  intent.getStringExtra("action");							
				if("talkservice".equalsIgnoreCase(action)) {
					SharedPreferences prefs = getSharedPreferences("nuntuprefs", Context.MODE_PRIVATE);					 	
					try {
						String VaultData= prefs.getString("nuntuvault"," "); 

						if(VaultData.length()>=15) {
							String UserCred=new NuntuCrypto().decrypt(VaultData);						
							JSONObject obj = new JSONObject(UserCred.toString());
							if("standard".equals(obj.getString("type"))) {
								lastLoginType=obj.getString("type");
								username=obj.getString("username");
								userpass=obj.getString("password");
								Log.e("STANDARD ",username);

							}else {
								lastLoginType=obj.getString("type");
								email=obj.getString("email");
								Log.e("SOCIAL ",email);
							}
						}

					} catch (Exception e) {		
						e.printStackTrace();
					}

					login();

					Log.e(TAG, "Nuntu talk service started and ready ");	
					if(timer==null) {
						timer=new Timer();
					}
					timer.schedule(new TimerTask() {
						@Override
						public void run() {								
							pollInterval =PreferenceManager.getDefaultSharedPreferences(getBaseContext());
							pollvalue=pollInterval.getString("list_preference","60000");
							Log.e("POLL INTERVAL","......"+pollvalue);
							if(null!=pollvalue) {
								delayPeriod=Integer.valueOf(pollvalue);
							}
							if(isUserLoggedIn) {
								if(canUserPoll(userID)) {
									requestAppsReadyForDownload(userID);
								}else {
									Log.e("SHIIT","no poll");
								}

							}else if("-1".equalsIgnoreCase(userID)){	
								login();
								Log.e("NUNTU SERVICE","Nuntu polled service started and ready to talk but login failed");
							}
							Log.e("POLL INTERVAL","going to sleep and waking in "+delayPeriod);
						}

					}, delayPeriod);

					if(regUserApp==null) {
						regUserApp=new Timer();
					}
					regUserApp.schedule(new TimerTask() {
						@Override
						public void run() {														
							registerInstalledUserApps();
						}

					},300000);

				}else if("autodestruct".equalsIgnoreCase(action)) {
					onDestroy();
				}else if("clean".equalsIgnoreCase(action)) {
					String packagelocation=intent.getStringExtra("package");
					removeAppApk(packagelocation);
					if("install".equalsIgnoreCase(intent.getStringExtra("type"))) {
						updateUserApp(packagelocation,userID,"1");
					}else if("uninstall".equalsIgnoreCase(intent.getStringExtra("type"))) {
						updateUserApp(packagelocation,userID,"3");
					}
				}
			}else {
				Log.e("NUNTU UNKOWN INTENT","Nuntu  is in intent that is know expected  "); 
			}
		}catch(Exception exc) {
			exc.printStackTrace();
			Log.e("NUNTU START EXC","Nuntu failed while starting service "+exc.getMessage()+"\n"+exc.getStackTrace().toString());  
		}    
		return START_STICKY;
	}

	@Override
	public void onStart(Intent intent, int startid) {
		try {
			if(intent!=null) {
				Log.e(TAG, "Nuntu service started and ready ");
				String action  =  intent.getStringExtra("action");							
				if("talkservice".equalsIgnoreCase(action)) {
					SharedPreferences prefs = getSharedPreferences("nuntuprefs", Context.MODE_PRIVATE);					 	
					try {
						String VaultData= prefs.getString("nuntuvault"," "); 

						if(VaultData.length()>=15) {
							String UserCred=new NuntuCrypto().decrypt(VaultData);						
							JSONObject obj = new JSONObject(UserCred.toString());
							if("standard".equals(obj.getString("type"))) {
								lastLoginType=obj.getString("type");
								username=obj.getString("username");
								userpass=obj.getString("password");
								Log.e("STANDARD ",username);

							}else {
								lastLoginType=obj.getString("type");
								email=obj.getString("email");
								Log.e("SOCIAL ",email);
							}
						}

					} catch (Exception e) {		
						e.printStackTrace();
					}

					login();

					Log.e(TAG, "Nuntu talk service started and ready ");	
					if(timer==null) {
						timer=new Timer();
					}

					pollInterval =PreferenceManager.getDefaultSharedPreferences(getBaseContext());
					pollvalue=pollInterval.getString("list_preference","60000");
					Log.e("POLL INTERVAL","......"+pollvalue);
					if(null!=pollvalue) {
						delayPeriod=Integer.valueOf(pollvalue);
					}

					timer.schedule(new TimerTask() {
						@Override
						public void run() {								
							pollInterval =PreferenceManager.getDefaultSharedPreferences(getBaseContext());
							pollvalue=pollInterval.getString("list_preference","60000");
							Log.e("POLL INTERVAL","......"+pollvalue);
							if(null!=pollvalue) {
								delayPeriod=Integer.valueOf(pollvalue);
							}
							if(isUserLoggedIn) {
								if(canUserPoll(userID)) {
									requestAppsReadyForDownload(userID);
								}else {
									Log.e("SHIIT","no poll");
								}
							}else if("-1".equalsIgnoreCase(userID)){	
								login();
								Log.e("NUNTU SERVICE","Nuntu polled service started and ready to talk but login failed");
							}

							Log.e("POLL INTERVAL","going to sleep and waking in "+delayPeriod+" millis");
						}

					}, delayPeriod);

					if(regUserApp==null) {
						regUserApp=new Timer();
					}
					regUserApp.schedule(new TimerTask() {
						@Override
						public void run() {														
							registerInstalledUserApps();
						}

					},300000);

				}else if("autodestruct".equalsIgnoreCase(action)) {
					onDestroy();
				}else if("clean".equalsIgnoreCase(action)) {
					String packagelocation=intent.getStringExtra("package");
					removeAppApk(packagelocation);
					if("install".equalsIgnoreCase(intent.getStringExtra("type"))) {
						updateUserApp(packagelocation,userID,"1");
					}else if("uninstall".equalsIgnoreCase(intent.getStringExtra("type"))) {
						updateUserApp(packagelocation,userID,"3");
					}
				}
			}else {
				Log.e("NUNTU UNKOWN INTENT","Nuntu  is in intent that is know expected  "); 
			}
		}catch(Exception exc) {
			exc.printStackTrace();
			Log.e("NUNTU START EXC","Nuntu failed while starting service "+exc.getMessage()+"\n"+exc.getStackTrace().toString());  
		}
	}

	private ArrayList<String> getInstalledAppPackages() {
		try{

			ArrayList<String> packageNames=new ArrayList<String>();	
			List<PackageInfo> packages = backcontext.getPackageManager().getInstalledPackages(0);
			for(PackageInfo pack : packages)
			{				
				packageNames.add(pack.packageName.trim().toLowerCase(Locale.ENGLISH).trim());	
			}
			System.gc();
			return packageNames;		
		}catch(Exception exc) {
			exc.printStackTrace();
			return null;
		}
	}

	private int getInstallPackageVersionInfo(String appPackageName) {       

		List<PackageInfo> packs = backcontext.getPackageManager().getInstalledPackages(0);
		int version=-1;
		for(int i=0;i<packs.size();i++) 
		{
			PackageInfo p = packs.get(i);
			if(p.packageName.equalsIgnoreCase(appPackageName.trim())){
				version= p.versionCode;	
				break;							
			}
		}
		return version;
	}

	private boolean canUserPoll(String userID) {

		try{
			if(IMEI==null&&IMEI.length()<10) {
				return false;
			}
			URL url = new URL(HOST+"/poll/user/"+userID+"/"+IMEI);
			Log.e("NUNTU SERVICE","Requesting a route.... "+url.getPath());
			HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
			if(urlConnection==null) {
				return false;
			}else {
				urlConnection.setRequestProperty("User-Agent","nuntumobile");
				BufferedReader reader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
				String response="";
				String line = "";
				while((line = reader.readLine()) != null){
					response += line + "\n";
				}               
				Log.e("POLL",response);
				System.gc();
				if(response!=null&&"success".equals(response.trim().toLowerCase())) {
					return true;
				}else {
					return false;
				}
			}
		}
		catch (Exception e){
			System.gc();
			e.printStackTrace();
			return false;
		}

	}

	private ArrayList<Map<String, ArrayList<String>>> getHTTPdata(String route) {
		try{

			URL url = new URL(HOST+route);
			Log.e("NUNTU SERVICE","Requesting a route.... "+url.getPath());
			HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
			if(urlConnection==null) {
				return null;
			}
			urlConnection.addRequestProperty("appversioncode",appVersion);
			urlConnection.addRequestProperty("deviceimei",IMEI);

			ArrayList<Map<String, ArrayList<String>>> data;
			if(urlConnection.getResponseCode()==HttpURLConnection.HTTP_OK) {
				BufferedReader reader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
				String response = "";
				String line = "";
				while((line = reader.readLine()) != null){
					response += line + "\n";
				}               
				if(!urlConnection.getContentType().toLowerCase(Locale.ENGLISH).equals("application/json")) {
					Log.e("Bad Content","Received bad content type while expecting json");
					return null;
				}
				Log.e("READY APPS ",response.toString());
				if(response.length()>=10) {
					JSONObject obj = new JSONObject(response.toString());						
					status=obj.getString("status");
					if("success".equalsIgnoreCase(status)) {
						JSONArray requestedApp=new JSONArray(obj.getString("details"));						
						data=new ArrayList<Map<String,ArrayList<String>>>();
						ArrayList<String> installed=getInstalledAppPackages();	

						if(NuntuUserAppPackageNames!=null) {
							NuntuUserAppPackageNames.clear();
						}

						if(NuntuUserAppID!=null) {
							NuntuUserAppID.clear();
						}

						UpdateApps=new ArrayList<String>();

						for(int i=0 ;i<requestedApp.length();i++){
							Map<String,ArrayList<String>>thisapp = null;
							if(requestedApp.getJSONObject(i)!=null) {
								JSONObject app=requestedApp.getJSONObject(i);								
								if(app!=null) {
									if(!app.getString("downloadKey").equalsIgnoreCase("ABORT")) {
										String packageName=app.getString("packagename");									   
										if(packageName!=null) {
											packageName=packageName.replace("'","").trim().toLowerCase(Locale.ENGLISH);
											NuntuUserAppPackageNames.add(packageName);
											NuntuUserAppID.add(app.getString("appid"));
											if(!installed.contains(packageName)){																							
												ArrayList<String> appdata=new ArrayList<String>();
												thisapp=new HashMap<String,ArrayList<String>>();
												appdata.add(app.getString("appid"));
												appdata.add(app.getString("name"));
												appdata.add(app.getString("packagename").replace("'",""));							
												appdata.add(app.getString("downloadID"));
												appdata.add(app.getString("downloadKey"));																							
												thisapp.put("app",appdata);

											}else {												
												System.gc();
												int InstalledVersion=getInstallPackageVersionInfo(packageName);
												if(InstalledVersion<Integer.valueOf(app.getString("versioncode"))) {
													UpdateApps.add(app.getString("appid"));													
												}
											}
										}
									}
								}

							}else {
								Log.e("NUNTU CONNECTION ","HTTP body was empty" );	
							}	
							if(thisapp!=null) {
								data.add(thisapp);
							}
						}
						System.gc();
						if(UpdateApps.size()>=1) {
							Log.e("UPDATE APP","app "+UpdateApps.get(0)+" need updates");
							notifyUpdates();
						}else {
							Log.e("UPDATE APP"," no app needs update");	
						}
						return data;
					}else {						
						return null;
					}
				}else {

					Log.e("NUNTU CONNECTION ","Nuntu has a bad connection with response code "+urlConnection.getResponseCode());
					return null;
				}
			}else {

				return null;
			}
		}
		catch (Exception e){
			System.gc();
			e.printStackTrace();
			Log.e("EXC","An exception occured while getting data "+e.getMessage()+" at "+e.getStackTrace().toString());
			return null;
		}
	}

	private void notifyUpdates() {		
		try {			
			AppUpdateNotificationManager.setContentTitle("NUNTU UPDATES");
			if(UpdateApps.size()==1) {
				AppUpdateNotificationManager.setContentText("one app needs your approval to update");
			}else {
				AppUpdateNotificationManager.setContentText("("+UpdateApps.size()+")"+" apps need your approval to update");

			}
			AppUpdateNotificationManager.setSmallIcon(R.drawable.download);	
			AppUpdateNotificationManager.setAutoCancel(true);	
			Intent intent = new Intent(backcontext,UserAppActivity.class);
			intent.setAction("android.intent.action.MAIN");
			PendingIntent pi = PendingIntent.getActivity(getApplicationContext(),0,intent,PendingIntent.FLAG_UPDATE_CURRENT);				
			AppUpdateNotificationManager.setContentIntent(pi);
			AppUpdateBuilder.notify(2014, AppUpdateNotificationManager.build());
		}catch(Exception exc) {
			exc.printStackTrace();
			Log.e("PENDUP","Exception occurred while updating downloads AppUpdate count"+exc.getMessage());
		}


	}

	private void requestAppsReadyForDownload(String user) {
		ArrayList<Map<String, ArrayList<String>>> userApps;
		userApps=getHTTPdata("/user/"+user+"/ready/app/android");
		if(userApps!=null) {
			for(int i=0;i<userApps.size();i++) {
				ArrayList<String> app=userApps.get(i).get("app");
				//avoid torrent downloads
				if(app!=null&&!requestedApps.contains(app.get(2))){
					String DownloadLink="/user/"+userID+"/claim/download/"+app.get(3)+"/"+app.get(4);
					Intent downloadApp =new Intent(backcontext, NuntuTransactDaemon.class);
					if(downloadApp!=null) {	
						downloadApp.putExtra("action","download");
						downloadApp.putExtra("type","app");
						downloadApp.putExtra("appID",app.get(0));				
						downloadApp.putExtra("appName",app.get(1));
						downloadApp.putExtra("packagename",app.get(2));
						downloadApp.putExtra("link",DownloadLink);					
						backcontext.startService(downloadApp);	
						requestedApps.add(app.get(2));
						System.out.println("download link "+HOST+DownloadLink);
					}				

				}else {
					System.out.println("App is null");
				}
			}
		}else {
			System.out.println("No user apps");
		}
		System.gc();
	}


	public String postData(String route,String data) {
		try {		
			HttpClient httpclient = NuntuUtils.getSecureHttpClient();
			// specify the URL you want to post to
			HttpPost httppost = new HttpPost(SECUREHOST+route);
			httppost.setEntity(new StringEntity(data,"UTF8"));     
			httppost.addHeader("Content-type", "application/json");
			httppost.addHeader("appversioncode", appVersion);
			httppost.addHeader("Accept", "application/json");
			httppost.addHeader("User-Agent", "nuntumobile");	
			try {
				// send the variable and value, in other words post, to the URL
				HttpResponse response = httpclient.execute(httppost);
				if(response!=null&&response.getStatusLine().getStatusCode()== HttpStatus.SC_OK) {
					HttpEntity entity = response.getEntity();
					String Response=EntityUtils.toString(entity); 					
					try {
						Log.e("POST Rec DATA",Response);
						if(Response!=null&&Response.length()>=5) {
							JSONObject ResponseJson = (JSONObject) new JSONTokener(Response).nextValue();
							query=ResponseJson.getString("status");
							if("success".equalsIgnoreCase(query)) {
								userID=ResponseJson.getJSONObject("details").getString("id");
							}else {
								userID="-1";
							}
							return null;

						}else {
							return null;
						}
					}catch(Exception exc) {
						exc.printStackTrace();
						Log.e("Exception in postData","this detail "+exc.getMessage());
						return null;
					}
				}else {
					return null;
				}
			}catch(Exception exc) {
				exc.printStackTrace();
				Log.e("Exception in postData","this detail "+exc.getMessage());
				return null;
			}
		}catch(Exception Exc) {
			Exc.printStackTrace();
			Log.e("POST DATA ",Exc.getLocalizedMessage());
			return null;
		}
	}



	private class LoginAsynch extends AsyncTask<String, Integer, Double>{

		@Override
		protected Double doInBackground(String... params) {
			postData(params[0],params[1]);			
			return null;
		}

		@Override
		protected void onPostExecute(Double result) 
		{	
			super.onPostExecute(result);
			if("success".equalsIgnoreCase(query)){
				isUserLoggedIn=true;
			}
		}

	}


	private void registerInstalledUserApps() {
		try {
			Log.e("REGINSTALLED","in registerInstalledUserApps()....");
			if(!"0".equalsIgnoreCase(userID)) {				
				if(NuntuUserAppPackageNames.size()>=1) {
					JSONArray apps = new JSONArray();
					for(int i=0;i<NuntuUserAppPackageNames.size();i++) {
						//Log.e("REGINSTALLED","looping packs");
						int appVersioCode=getInstallPackageVersionInfo(NuntuUserAppPackageNames.get(i));					
						try {
							if(NuntuUserAppID.get(i)!=null&&NuntuUserAppID.get(i).length()>=1) {
								JSONObject app = new JSONObject();
								app.put("appID",NuntuUserAppID.get(i));
								app.put("version",appVersioCode);
								app.put("status",1);
								apps.put(app);
							}
						}catch(JSONException jsonExc) {
							jsonExc.printStackTrace();
							Log.e("REGINSTALLED","JSONException in registerInstalledUserApps( )"+jsonExc.getMessage());
						}
					}					
					String data=apps.toString();
					Log.e("REGINSTALLED","sent data "+data);
					postData("/data/userapp/android/"+userID+"/"+IMEI,data);
				}else {
					Log.e("REGINSTALLED","no packs found");
				}
			}else {
				Log.e("REGINSTALLED","bad userID");
			}
		}catch(Exception e) {
			e.printStackTrace();
			Log.e("WHOOO","an exception was caught in back service "+e.getMessage());
		}
		System.gc();
	}

	private void login() {
		if("standard".equalsIgnoreCase(lastLoginType)) {
			JSONObject object = new JSONObject();	
			try {
				object.put("uname", username);
				object.put("password", userpass);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			new LoginAsynch().execute("/user/login",object.toString());

		}else if(email!=null){
			JSONObject object = new JSONObject();	
			try {
				object.put("email", email);
				object.put("password","");
				object.put("firstname","");
				object.put("lastname","");
				object.put("photo","");
				object.put("web","");
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			new LoginAsynch().execute(SocialLoginRoute+lastLoginType,object.toString());			
		}else {
			Log.e("SHITTT","no email");

		}
	}

	private void  removeAppApk(String packageName){		
		String SDCardRoot,AppFileLocation="/sdcard/nuntu/downloads/apps";
		try {

			if(filesys.hasSDStorage(true)) {
				SDCardRoot = Environment.getExternalStorageDirectory().toString();
			}else {
				SDCardRoot = Environment.getDataDirectory().toString();
			}

			File NuntuDownloads=new File(SDCardRoot+"/nuntu/downloads/apps");
			if(!NuntuDownloads.exists()) {
				if(!NuntuDownloads.mkdirs()) {
					Log.e("failedmkdir", "Failed to mkdir for nuntu downloads" );	
				}
			}else {
				Log.e("APK CLEAN", "apk not found" );
			}

			AppFileLocation=NuntuDownloads.getAbsolutePath()+"/."+packageName;
			File APK=new File(AppFileLocation);
			if(APK.exists()&&APK.delete()) {							
				Log.e("SHITT YEAH", " cleaning up apk was ok");
			}else {
				Log.e("CLEAN",packageName+" not found in local storage or not deleted");
			}
		}catch(Exception e) {
			e.printStackTrace();
		}

	}

	public void updateUserApp(final String packageName,final String userID,final String status) {
		Log.e("UPDATE ","user app update requested");
		AsyncTask<String,String,String> getTask = new AsyncTask<String,String,String>(){
			int httpcode=0;
			@Override
			protected String doInBackground(String... params) {
				HttpURLConnection urlConnection=null;
				try{
					URL url = new URL(params[0]);
					urlConnection = (HttpURLConnection)url.openConnection();
					httpcode=urlConnection.getResponseCode();
				}

				catch (Exception e){
					if(urlConnection!=null) {
						urlConnection.disconnect();	
					}
				}

				return null;

			}

			@Override
			protected void onPostExecute(String result) {
				Log.e("UPDATE ","user app update status "+httpcode);
				if(httpcode!=200) {
					updateUserApp(packageName,userID,status);	
				}
			}

		};
		getTask.execute(HOST+"/data/android/user/"+userID+"/app/"+packageName+"/"+status);
	} 

	@Override
	public IBinder onBind(Intent arg0) {
		// TODO Auto-generated method stub
		return null;
	}
}
