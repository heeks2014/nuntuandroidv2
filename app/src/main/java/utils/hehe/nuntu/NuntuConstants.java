package utils.hehe.nuntu;

/**
 * Created by snickwizard on 2/6/15.
 */
public enum NuntuConstants {


    HOST("http://nuntu.hehelabs.com"),
    SECURE_HOST("https://nuntu.hehelabs.com"),
    CLIENT("android"),
    AGENT("nuntuapp"),
    GHOST_AGENT("nuntumobile"),
    PRIVATE_KEY(" "),
    ABOUT_APP("Nuntu is a digital content store " +
            "designed for you to access great and exclusive" +
            " African content wherever you are. Nuntu features" +
            " apps by developers from across the continent," +
            " audio content including music, audio books, " +
            "and poetry, as well as magazines and books.\n\n\nThis Application" +
            " is a Nuntu mobile client for Android platform." +
            "Besides helping you keeping track of your local downloads " +
            "from Nuntu servers," +
            "it is also crucial in retrieving apps you have chosen from Nuntu." +
            "This app supports Android devices with Android operating systems later than 2.3"),
    NUNTU_PRIVACY("HeHe Labs’s privacy policies explain how we treat " +
            "your personal information and protect your privacy when you use our Services.\n" +
            "By using our Services, you agree that HeHe Labs can use such data in accordance " +
            "with our privacy policies.\n\nHeHe Labs provides information to help copyright holders " +
            "manage their intellectual property online.\n We respond to notices of alleged copyright" +
            " infringement and terminate accounts of repeat infringers in accordance with the Rwandan law.\n" +
            "To view terms and conditions that guide privacy policy, follow the link below."),
    NUNTU_CREDITS("This app uses the following software systems API, in part or in whole:\n\n" +
            "1.\"Actionbarsherlock\" provided by Jake Wharton under Apache License v.2.0\n\n" +
            "2.\"Picasso\" provided by Square,Inc under Apache License v.2.0\n\n" +
            "3.\"Open Graph\" provided by Facebook,Inc under Creative Commons Attribution-ShareAlike License v.3.0\n\n" +
            "4.\"Google+ API\" provided by Google,Inc under Creative Commons Attribution License v.3.0\n\n"),

    APP_VERSION("201503");

    private final String name;


    NuntuConstants(String s) {
        name = s;
    }


    public String toString() {
        return name;
    }
}