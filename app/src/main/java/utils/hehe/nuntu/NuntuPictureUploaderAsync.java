package utils.hehe.nuntu;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by gakuba on 4/20/2015.
 */
public class NuntuPictureUploaderAsync extends AsyncTask<String, String, String> {

    String ImageFilePath, message = "We couldn't could connect to nuntu";
    private Activity UploadingActivty;

    public NuntuPictureUploaderAsync(Activity ctx) {
        UploadingActivty = ctx;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        Toast.makeText(UploadingActivty, " Do your thing !\n    sending ... ", Toast.LENGTH_LONG).show();
        ImageFilePath = "";
    }

    @Override
    protected String doInBackground(String... params) {

        HttpURLConnection conn;
        DataOutputStream dos;
        String lineEnd = "\r\n";
        String twoHyphens = "--";
        String boundary = "*****";
        int bytesRead, bytesAvailable, bufferSize;
        byte[] buffer;
        int maxBufferSize = 1 * 1024 * 1024;
        File sourceFile = new File(params[0]);

        if (!sourceFile.isFile()) {
            message = "Couldn't retrieve image from device";
            return null;
        } else {
            try {

                // open a URL connection to the Servlet
                FileInputStream fileInputStream = new FileInputStream(sourceFile);
                URL url = new URL(params[1]);

                // Open a HTTP  connection to  the URL
                conn = (HttpURLConnection) url.openConnection();
                conn.setDoInput(true); // Allow Inputs
                conn.setDoOutput(true); // Allow Outputs
                conn.setUseCaches(false); // Don't use a Cached Copy
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Connection", "Keep-Alive");
                conn.setRequestProperty("User-Agent", "nuntumobile_snickwizard");
                conn.setRequestProperty("ENCTYPE", "multipart/form-data");
                conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
                conn.setRequestProperty("uploaded_file", params[0]);

                dos = new DataOutputStream(conn.getOutputStream());

                dos.writeBytes(twoHyphens + boundary + lineEnd);
                dos.writeBytes("Content-Disposition: form-data; name=\"upl\";filename=" + params[0] + "" + lineEnd);
                dos.writeBytes(lineEnd);

                // create a buffer of  maximum size
                bytesAvailable = fileInputStream.available();

                bufferSize = Math.min(bytesAvailable, maxBufferSize);
                buffer = new byte[bufferSize];

                // read file and write it into form...
                bytesRead = fileInputStream.read(buffer, 0, bufferSize);

                while (bytesRead > 0) {

                    dos.write(buffer, 0, bufferSize);
                    bytesAvailable = fileInputStream.available();
                    bufferSize = Math.min(bytesAvailable, maxBufferSize);
                    bytesRead = fileInputStream.read(buffer, 0, bufferSize);

                }

                // send multipart form data necesssary after file data...
                dos.writeBytes(lineEnd);
                dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

                // Responses from the server (code and message)

                String serverResponseMessage = conn.getResponseMessage();

                Log.e("uploadFile", "HTTP Response is : " + serverResponseMessage + ": " + serverResponseMessage);

                //close the streams //
                fileInputStream.close();
                dos.flush();
                dos.close();
                return serverResponseMessage;

            } catch (MalformedURLException ex) {

                Log.e("Upload file to server", "error: " + ex.getMessage(), ex);
            } catch (Exception e) {

                e.printStackTrace();
                Log.e("Upload File", "Exception : " + e.getMessage());
            }

            return null;
        }

    }

    @Override
    protected void onPostExecute(String res) {
        super.onPostExecute(res);
        if (res != null && res.contains("OK")) {
            final NuntuLoginAsync LoginTask = new NuntuLoginAsync(UploadingActivty, "reload_vault", false);
            LoginTask.execute(NuntuConstants.HOST.toString() + "/user/login", User.getNuntuLocalLogin(UploadingActivty), "unfocused", null);
        }

    }
}
