package utils.hehe.nuntu;

import android.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;


public class NuntuCrypto {
    // bytes used for the encryption key
    private static byte[] keyBytes = new byte[]{0x1f, 0x21, 0x0d, 0x36, 0x5b, 0x0c, 0x12, 0x3c, 0x3e, 0x7d,
            0xa, 0x7e, 0x1e, 0x2d, 0x3b, 0x0e, 0x4b, 0x11, 0x3c, 0x7d, 0x42, 0x3f, 0x5d, 0x3f};

    // encrypt the clearText, base64 encode the cipher text and return it.
    public String encrypt(String clearText) throws Exception {
        SecretKeySpec keySpec = new SecretKeySpec(keyBytes, "AES");
        // init cipher
        Cipher cipher = Cipher.getInstance("AES");
        cipher.init(Cipher.ENCRYPT_MODE, keySpec);
        byte[] cipherText = cipher.doFinal(clearText.getBytes("UTF-8"));
        return new String(Base64.encode(cipherText, Base64.DEFAULT));
    }

    // decrypt our resource string back into it's source format.
    public String decrypt(String cipherText) throws Exception {
        SecretKeySpec keySpec = new SecretKeySpec(keyBytes, "AES");
        // init cipher
        Cipher cipher = Cipher.getInstance("AES");
        cipher.init(Cipher.DECRYPT_MODE, keySpec);
        byte[] clearText = cipher.doFinal(Base64.decode(cipherText.getBytes("UTF-8"), Base64.DEFAULT));
        return new String(clearText);
    }

}