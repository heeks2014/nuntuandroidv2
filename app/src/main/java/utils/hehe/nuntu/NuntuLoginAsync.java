package utils.hehe.nuntu;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Button;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

/**
 * Created by gakuba on 4/21/2015.
 */
public class NuntuLoginAsync extends AsyncTask<String, Integer, Double> {

    String action, query;
    JSONObject detailsData;
    String _LoginMode = "";
    String _Pass = null;
    public static boolean hasUserLoggedOn = false;
    Activity parentActivity;
    ProgressDialog progressDialog = null;
    String AfterLoginAction = "load_profile";
    boolean updateUI = false;

    public NuntuLoginAsync(Activity caller, String action, boolean onUI) {
        parentActivity = caller;
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(parentActivity);
        }
        AfterLoginAction = action;
        updateUI = onUI;
    }

    @Override
    protected void onPreExecute() {
        hasUserLoggedOn = false;
        if (updateUI) {
            progressDialog.setMessage("\t\t\t\t wait... ");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }
    }

    @Override
    protected Double doInBackground(String... params) {
        try {

            _LoginMode = params[2];
            _Pass = params[3];
            if ("focused".equalsIgnoreCase(_LoginMode) && (_Pass == null || _Pass.length() < 8)) {
                return 0.23;
            } else if ("reload_vault".equalsIgnoreCase(AfterLoginAction)) {
                JSONObject Local = new JSONObject(params[1]);
                JSONObject object = new JSONObject();
                _Pass = Local.getString("password");
                object.put("uname", Local.getString("email"));
                object.put("password", _Pass);
                params[1] = object.toString();
            }
            HttpClient httpclient = NuntuUtils.getSecureHttpClient();
            HttpPost httppost = new HttpPost(params[0]);

            httppost.setEntity(new StringEntity(params[1], "UTF8"));
            httppost.addHeader("Content-type", "application/json");
            httppost.addHeader("Accept", "application/json");
            httppost.addHeader("User-Agent", "nuntumobile");
            httppost.addHeader("appversioncode", NuntuConstants.APP_VERSION.toString());

            // send the variable and value, in other words post, to the URL
            HttpResponse response = httpclient.execute(httppost);
            if (response != null) {
                HttpEntity entity = response.getEntity();
                String Response = EntityUtils.toString(entity);
                Log.e("RCV DATA", Response);
                try {
                    JSONObject ResponseJson = (JSONObject) new JSONTokener(Response).nextValue();
                    query = ResponseJson.getString("status");
                    if ("success".equalsIgnoreCase(query)) {
                        detailsData = ResponseJson.getJSONObject("details");
                    } else {
                        action = "logout";
                        Log.e("servermsg", ResponseJson.getString("details"));

                    }
                } catch (Exception exc) {
                    Log.e("Exception in postData", "this detail " + exc.getMessage());
                }


            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(final Double result) {
        super.onPostExecute(result);
        if (progressDialog != null) {
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
        }
        parentActivity.runOnUiThread(new Runnable() {
            @SuppressWarnings("deprecation")
            public void run() {
                if ("success".equalsIgnoreCase(query)) {

                    if (detailsData != null) {
                        try {

                            String username = detailsData.getString("username");
                            String id = detailsData.getString("id");
                            String email = detailsData.getString("email");
                            String fname = detailsData.getString("firstname");
                            String lname = detailsData.getString("lastname");
                            String role = detailsData.getString("role");
                            String rank = detailsData.getString("rank");
                            String image = detailsData.getString("redirect");
                            try {
                                JSONObject VaultCredentials = new JSONObject();
                                VaultCredentials.put("username", username);
                                if (_Pass != null && _Pass.length() >= 8) {
                                    VaultCredentials.put("password", _Pass);
                                    VaultCredentials.put("userid", id);
                                    VaultCredentials.put("email", email);
                                    VaultCredentials.put("firstname", fname);
                                    VaultCredentials.put("lastname", lname);
                                    VaultCredentials.put("role", role);
                                    VaultCredentials.put("rank", rank);
                                    VaultCredentials.put("image", image);
                                    VaultCredentials.put("type", "standard");
                                    User.updateNuntuLocalLogin(VaultCredentials.toString(), parentActivity.getApplicationContext());
                                    Log.e("GOOOD", "next login will be auto from standard");
                                } else {
                                    Log.e("BAD", "next login will be requested ");
                                }


                            } catch (Exception e) {
                                Log.e("GODDAMN", e.getMessage());
                            }

                            if ("load_profile".equalsIgnoreCase(AfterLoginAction)) {
                                Intent RegisterTalkPoll = new Intent(parentActivity.getApplicationContext(), NuntuPollService.class);
                                RegisterTalkPoll.putExtra("action", "talkservice");
                                parentActivity.getApplicationContext().startService(RegisterTalkPoll);
                                Log.e("SERVICE", "nuntu poll service requested");
                                FragmentNuntuHome.loadUserCustomItemAsync(id, "init");
                            } else if ("reload_vault".equalsIgnoreCase(AfterLoginAction)) {
                                if ("utils.hehe.nuntu.Nuntu".equalsIgnoreCase(parentActivity.getComponentName().getClassName())) {
                                    new Nuntu().initUserHeader("refresh");
                                } else if ("utils.hehe.nuntu.ClosetPDFViewer".equalsIgnoreCase(parentActivity.getComponentName().getClassName())) {
                                    new ClosetPDFViewer().initUserHeader("refresh");
                                } else if ("utils.hehe.nuntu.AlbumActivity".equalsIgnoreCase(parentActivity.getComponentName().getClassName())) {
                                    new AlbumActivity().initUserHeader("refresh");
                                }
                                Log.e("USER HEADER", "refreshing user header after upload image service requested by " + parentActivity.getComponentName().getClassName());
                            }
                            hasUserLoggedOn = true;
                        } catch (JSONException e) {
                            Log.e("BAD NUNTU DATA", "An exception was thrown while parsing user data " + e.getMessage());
                        }

                    } else if (0.23 == result) {
                        if (updateUI && "focused".equalsIgnoreCase(_LoginMode)) {
                            AlertDialog.Builder alertbox = new AlertDialog.Builder(parentActivity);
                            alertbox.setTitle("sign in failed");
                            alertbox.setMessage("Please try again ...");
                            alertbox.setNegativeButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {

                                }

                            });
                            AlertDialog alert = alertbox.create();
                            alert.show();
                            Button negative_button = alert.getButton(DialogInterface.BUTTON_NEGATIVE);
                            if (negative_button != null) {
                                negative_button.setBackgroundDrawable(parentActivity.getApplicationContext().getResources().getDrawable(R.drawable.app_button_background));
                                negative_button.setTextColor(parentActivity.getApplicationContext().getResources().getColor(android.R.color.white));
                            }
                        }
                        Log.e("BAD NUNTU PASS", "the local pass was null");
                    }
                } else {
                    try {
                        if ("logout".equalsIgnoreCase(action)) {
                            User.updateNuntuLocalLogin("", parentActivity.getApplicationContext());
                        }
                        if (updateUI && "focused".equalsIgnoreCase(_LoginMode)) {
                            AlertDialog.Builder alertbox = new AlertDialog.Builder(parentActivity);
                            alertbox.setTitle("sign in failed");
                            alertbox.setMessage("Please try again ...");
                            alertbox.setNegativeButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                }

                            });
                            AlertDialog alert = alertbox.create();
                            alert.show();
                            Button negative_button = alert.getButton(DialogInterface.BUTTON_NEGATIVE);
                            if (negative_button != null) {
                                negative_button.setBackgroundDrawable(parentActivity.getApplicationContext().getResources().getDrawable(R.drawable.app_button_background));
                                negative_button.setTextColor(parentActivity.getApplicationContext().getResources().getColor(android.R.color.white));
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

}
