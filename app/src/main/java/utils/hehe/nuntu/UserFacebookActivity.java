package utils.hehe.nuntu;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.Signature;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.FacebookRequestError;
import com.facebook.HttpMethod;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.model.GraphObject;
import com.facebook.model.GraphUser;
import com.facebook.widget.LoginButton;
import com.facebook.widget.LoginButton.UserInfoChangedCallback;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.List;

public class UserFacebookActivity extends FragmentActivity {

    private static final List<String> PERMISSIONS = Arrays.asList("email", "public_userID", "user_friends");
    final String TAG = "FACEBOOK";
    private ScrollView Parent;
    private LoginButton loginBtn;
    private Button Cancel;
    private Button Continue;
    private TextView userName;
    private UiLifecycleHelper uiHelper;
    private String firstname = " ", lastname = " ", email = " ", userID = " ", mode = "new", CurrentEmail = " ";
    private boolean hasRequested = false, hasRequestedPerms = false;
    private Context FacebookLoginCtx;
    private Session.StatusCallback statusCallback = new Session.StatusCallback() {
        @Override
        public void call(Session session, SessionState state, Exception exception) {
            onSessionStateChange(session, state, exception);
        }

    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= 14) {
            getWindow().setDimAmount(0.5f);
        }
        FacebookLoginCtx = this;
        setContentView(R.layout.activity_facebook);
        Parent = (ScrollView) findViewById(R.id.parent);
        userName = (TextView) findViewById(R.id.user_name);
        Continue = (Button) findViewById(R.id.ContinueLogin);
        loginBtn = (LoginButton) findViewById(R.id.fb_login_button);
        Cancel = (Button) findViewById(R.id.CancelLogin);
        Parent.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e(TAG,"parent clicked");
                onBackPressed();
            }
        });
        Continue.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (email != null && email.length() >= 12) {
                    Intent data = new Intent();
                    data.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    data.putExtra("status", "proceed");
                    data.putExtra("firstname", firstname);
                    data.putExtra("lastname", lastname);
                    data.putExtra("email", email);
                    data.putExtra("ID", userID);
                    setResult(RESULT_OK, data);
                    finish();
                } else {
                    Toast.makeText(UserFacebookActivity.this, "Please wait for facebook login", Toast.LENGTH_LONG).show();
                }
            }
        });
        uiHelper = new UiLifecycleHelper(this, statusCallback);
        uiHelper.onCreate(savedInstanceState);
        mode = getIntent().getStringExtra("mode");
        CurrentEmail = getIntent().getStringExtra("email");
        getUserData(mode, CurrentEmail);


        loginBtn.setUserInfoChangedCallback(new UserInfoChangedCallback() {
            @Override
            public void onUserInfoFetched(GraphUser user) {
                if (user != null) {
                    userName.setText(user.getName());
                } else {
                    userName.setText("Not logged in");
                }
            }
        });


        Cancel.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View view) {
                CancelUserLogin();
            }
        });


        try {
            PackageInfo info = getPackageManager().getPackageInfo("utils.hehe.nuntu", PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.e("Using KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (NameNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

    }

    private void onSessionStateChange(Session session, SessionState state, Exception exception) {
        if (session != null && state.isOpened()) {
            Log.d(TAG, "Facebook session opened");
            List<String> ActualPerms = session.getPermissions();
            if (ActualPerms != null) {
                boolean fetch = false;
                for (int p = 0; p < PERMISSIONS.size(); p++) {
                    if (!ActualPerms.contains(PERMISSIONS.get(p).trim())) {
                        fetch = true;
                        break;
                    }
                }
                if (!hasRequestedPerms && fetch) {
                    requestPermissions();
                }
            } else {

                requestPermissions();
            }
            getUserData("new", " ");
        } else {
            Log.e("SESSION", "status " + session.getState() + " token " + session.getAccessToken() + " app id " + session.getApplicationId() + " session perms ");
            userName.setText("nuntu couldn't access your facebook");
            userName.setTextColor(Color.argb(125, 255, 255, 0));
            Continue.setEnabled(false);
        }
    }


    public void CancelUserLogin() {
        if (Session.getActiveSession() != null) {
            Session.getActiveSession().close();
        }
        Intent data = new Intent();
        data.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        data.putExtra("status", "canceled");
        setResult(RESULT_OK, data);
        finish();
    }

    public void getUserData(final String mode, final String LastEmail) {
        if (Build.VERSION.SDK_INT >= 11) {
            Continue.setAlpha(1f);
        }
        Continue.setEnabled(true);
        userName.setTextColor(Color.argb(255, 255, 255, 255));
        hasRequested = true;
        new Request(
                Session.getActiveSession(),
                "/me",
                null,
                HttpMethod.GET,
                new Request.Callback() {
                    public void onCompleted(Response response) {
                        FacebookRequestError error = response.getError();
                        if (error != null) {
                            Log.d(TAG, "Getting user info failed: " + error.getErrorMessage());

                        } else {
                            Log.d(TAG, "Getting user info was successful: ");
                            GraphObject graphObject = response.getGraphObject();
                            if (graphObject != null) {
                                if (graphObject.getProperty("id") != null) {
                                    firstname = graphObject.getProperty("first_name").toString();
                                    lastname = graphObject.getProperty("last_name").toString();
                                    userID = graphObject.getProperty("id").toString();
                                    final String gender = graphObject.getProperty("gender").toString();
                                    try {
                                        String Femail = graphObject.getProperty("email").toString();
                                        if (Femail != null && Femail.length() >= 3) {
                                            email = Femail;
                                            if ("return".equals(mode) && LastEmail.equals(email)) {
                                                Intent data = new Intent();
                                                data.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                                data.putExtra("status", "proceed");
                                                data.putExtra("firstname", firstname);
                                                data.putExtra("lastname", lastname);
                                                data.putExtra("email", email);
                                                data.putExtra("ID", userID);
                                                setResult(RESULT_OK, data);
                                                finish();
                                            }
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        if (email != null || email.length() <= 3) {
                                            runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    if ("male".equalsIgnoreCase(gender)) {
                                                        Toast.makeText(FacebookLoginCtx, "Mr " + firstname + " we didn't get your email from Facebook", Toast.LENGTH_LONG).show();
                                                    } else {
                                                        Toast.makeText(FacebookLoginCtx, "Ms " + firstname + "  we didn't get your email from Facebook", Toast.LENGTH_LONG).show();

                                                    }
                                                }
                                            });
                                        }
                                    }
                                } else {
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(FacebookLoginCtx, " We didn't get any of your data from Facebook", Toast.LENGTH_LONG).show();
                                        }
                                    });
                                }
                            }

                        }
                    }
                }).executeAsync();
    }


    public void requestPermissions() {
        Log.e("SESSION", "requesting perms ");
        Session s = Session.getActiveSession();
        if (s != null) {
            Log.e("SESSION", "perms request ok ");
            hasRequestedPerms = true;
            s.requestNewReadPermissions(new Session.NewPermissionsRequest(
                    this, PERMISSIONS));
        } else {
            Log.e("PERM", "Session is null ");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        uiHelper.onResume();

    }

    @Override
    public void onPause() {
        super.onPause();
        uiHelper.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        uiHelper.onDestroy();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        uiHelper.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onSaveInstanceState(Bundle savedState) {
        super.onSaveInstanceState(savedState);
        uiHelper.onSaveInstanceState(savedState);
    }

    @Override
    public void onBackPressed() {
        try {
            if (!NuntuUtils.isValidEmail(email)) {
                CancelUserLogin();
            } else if (lastname.length() >= 2 || firstname.length() >= 2) {
                Intent data = new Intent();
                data.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                data.putExtra("status", "proceed");
                data.putExtra("firstname", firstname);
                data.putExtra("lastname", lastname);
                data.putExtra("email", email);
                data.putExtra("ID", userID);
                setResult(RESULT_OK, data);
                finish();
            } else {
                Toast.makeText(UserFacebookActivity.this, "Please wait for facebook login", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
            CancelUserLogin();
        }
    }

}
