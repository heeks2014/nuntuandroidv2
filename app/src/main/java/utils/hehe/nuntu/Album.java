package utils.hehe.nuntu;


import java.util.ArrayList;

public class Album {

    private String AlbumName = " ";
    private String AlbumID = " ";
    private String AlbumPublisher = " ";
    private String Status = " ";
    private String AlbumPrice = " ";
    private String Banner = " ";
    private ArrayList<String> CurrentLocalSongs;


    public void setAlbumID(String string) {
        // TODO Auto-generated method stub
        this.AlbumID = string;
    }

    public void setAlbumPrice(String string) {
        // TODO Auto-generated method stub
        this.AlbumPrice = string;
    }

    public void setAlbumStatus(String string) {
        // TODO Auto-generated method stub
        this.Status = string;
    }

    public void setAlbumLocalTracks(ArrayList<String> strings) {
        // TODO Auto-generated method stub
        this.CurrentLocalSongs = strings;
    }

    public void setAlbumName(String string) {
        // TODO Auto-generated method stub
        this.AlbumName = string;
    }

    public void setAlbumBanner(String Banner) {

        this.Banner = Banner;

    }

    public void setAlbumPublisher(String pub) {

        this.AlbumPublisher = pub;

    }


    public String getAlbumName() {
        // TODO Auto-generated method stub
        return this.AlbumName;
    }


    public String getAlbumPrice() {
        // TODO Auto-generated method stub
        return this.AlbumPrice;
    }

    public String getAlbumID() {
        // TODO Auto-generated method stub
        return this.AlbumID;
    }

    public String getAlbumPublisher() {
        // TODO Auto-generated method stub
        return this.AlbumPublisher;
    }


    public String getAlbumStatus() {

        return this.Status;

    }

    public String getAlbumBanner() {

        return this.Banner;

    }

    public ArrayList<String> getAlbumLocalTracks() {
        // TODO Auto-generated method stub
        return CurrentLocalSongs;
    }

}
