package utils.hehe.nuntu;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class CustomDrawerAdapter extends RecyclerView.Adapter<CustomDrawerAdapter.MyViewHolder> {

    ArrayList<DrawerItem> data;
    private LayoutInflater inflater;
    private Context currentCTX;
    private String CallingActivity;
    private NavigationDrawerFragment CallerInstance;

    public CustomDrawerAdapter(Context context, NavigationDrawerFragment _Instance, ArrayList<DrawerItem> data, String TopActivity) {

        this.inflater = LayoutInflater.from(context);
        this.data = data;
        CallingActivity = TopActivity;
        this.currentCTX = context;
        this.CallerInstance = _Instance;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        try {
            View view = inflater.inflate(R.layout.custom_row, parent, false);
            return new MyViewHolder(view);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        try {
            final DrawerItem current = data.get(position);

            if ("header".equalsIgnoreCase(current.getItemType().trim())) {
                Log.e("HEADER", current.getItemName());
                if (holder.ItemHeaderName.getVisibility() != View.VISIBLE) {
                    holder.ItemHeaderName.setVisibility(View.VISIBLE);
                }

                if (holder.headerUnderLine.getVisibility() != View.VISIBLE) {
                    holder.headerUnderLine.setVisibility(View.VISIBLE);
                }

                holder.ItemHeaderName.setText(current.getItemName());
                holder.title.setVisibility(View.INVISIBLE);
                holder.icon.setVisibility(View.INVISIBLE);
            } else {

                if (!"BARGAIN".equalsIgnoreCase(current.getItemName())) {
                    holder.headerUnderLine.setVisibility(View.INVISIBLE);
                } else {
                    if (holder.headerUnderLine.getVisibility() != View.VISIBLE) {
                        holder.headerUnderLine.setVisibility(View.VISIBLE);
                    }
                }
                if (!"local".equalsIgnoreCase(current.getItemType())) {
                    holder.icon.setVisibility(View.VISIBLE);
                    if (current.getItemRemoteIcon() != null) {

                        Picasso.with(currentCTX)
                                .load(current.getItemRemoteIcon())
                                .error(R.drawable.app_logo)
                                .transform(new CircleTransform(40, 4))
                                .resize(64, 64)
                                .centerCrop()
                                .placeholder(R.drawable.app_logo)
                                .into(holder.icon);
                    }

                    holder.icon.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            CallerInstance.closeDrawer();
                            reportToParent(current);

                        }
                    });

                } else {
                    Log.e("LOCAL", current.getItemName());
                    holder.icon.setVisibility(View.INVISIBLE);
                    holder.ItemHeaderName.setVisibility(View.INVISIBLE);
                }

                if (holder.title.getVisibility() != View.VISIBLE) {
                    holder.title.setVisibility(View.VISIBLE);
                }
                holder.title.setText(current.getItemName());
                holder.title.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        CallerInstance.closeDrawer();
                        reportToParent(current);
                    }
                });
            }
            if ("CRAZE".equalsIgnoreCase(current.getItemName().trim())) {
                holder.headerUnderLine.setVisibility(View.VISIBLE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } catch (OutOfMemoryError ex) {
            ex.printStackTrace();
        }
    }

    private void reportToParent(DrawerItem current) {
        Log.e("CLICK ", "item " + current.getItemName() + " for  " + CallingActivity);
        if ("nuntu".equalsIgnoreCase(CallingActivity)) {
            if (!"local".equalsIgnoreCase(current.getItemType())) {
                if ("apps".equalsIgnoreCase(current.getItemName())) {
                    new Nuntu().respondToSelectedDrawerItem("online", "app");
                } else if ("books".equalsIgnoreCase(current.getItemName())) {
                    new Nuntu().respondToSelectedDrawerItem("online", "book");
                } else if ("music".equalsIgnoreCase(current.getItemName())) {
                    new Nuntu().respondToSelectedDrawerItem("online", "music");
                }

            } else {
                Log.e("CLICK ", "item " + current.getItemName() + "  " + current.getItemType());
                if ("Settings".equalsIgnoreCase(current.getItemName())) {
                    Intent i = new Intent(currentCTX, NuntuSettings.class);
                    i.putExtra("userID", "0");
                    currentCTX.startActivity(i);
                } else if ("About".equalsIgnoreCase(current.getItemName())) {
                    new Nuntu().respondToSelectedDrawerItem("local", "about");
                } else if ("Logout".equalsIgnoreCase(current.getItemName())) {
                    new Nuntu().respondToSelectedDrawerItem("local", "logout");
                } else if ("Login".equalsIgnoreCase(current.getItemName())) {
                    new Nuntu().respondToSelectedDrawerItem("local", "login");
                } else if ("Help".equalsIgnoreCase(current.getItemName())) {
                    new Nuntu().respondToSelectedDrawerItem("local", "help");
                } else {
                    new Nuntu().respondToSelectedDrawerItem("local", current.getItemName());
                }
            }
        } else if ("viewer".equalsIgnoreCase(CallingActivity)) {
            if (!"local".equalsIgnoreCase(current.getItemType())) {
                if ("apps".equalsIgnoreCase(current.getItemName())) {
                    new ClosetPDFViewer().respondToSelectedDrawerItem("online", "app");
                } else if ("books".equalsIgnoreCase(current.getItemName())) {
                    new ClosetPDFViewer().respondToSelectedDrawerItem("online", "book");
                } else if ("music".equalsIgnoreCase(current.getItemName())) {
                    new ClosetPDFViewer().respondToSelectedDrawerItem("online", "music");
                }

            } else {
                Log.e("CLICK ", "item " + current.getItemName() + "  " + current.getItemType());
                if ("Settings".equalsIgnoreCase(current.getItemName())) {
                    Intent i = new Intent(currentCTX, NuntuSettings.class);
                    i.putExtra("userID", "0");
                    currentCTX.startActivity(i);
                } else if ("About".equalsIgnoreCase(current.getItemName())) {
                    new ClosetPDFViewer().respondToSelectedDrawerItem("local", "about");
                } else if ("Logout".equalsIgnoreCase(current.getItemName())) {
                    new ClosetPDFViewer().respondToSelectedDrawerItem("local", "logout");
                } else if ("Login".equalsIgnoreCase(current.getItemName())) {
                    new ClosetPDFViewer().respondToSelectedDrawerItem("local", "login");
                } else if ("Help".equalsIgnoreCase(current.getItemName())) {
                    new ClosetPDFViewer().respondToSelectedDrawerItem("local", "help");
                } else {
                    new ClosetPDFViewer().respondToSelectedDrawerItem("local", current.getItemName());
                }
            }
        }
    }

    public void refreshDrawer(final ArrayList<DrawerItem> newlist, String str) {

        if (newlist != null && newlist.size() >= 2) {
            CallingActivity = str;
            data = newlist;
            if (data.size() >= 1) {
                Log.e("REFRESHING", " content items " + newlist.get(1).getItemName());
            }
            this.notifyDataSetChanged();
        } else {
            Log.e("REFRESHING", " list content was empty ");
        }

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        TextView title, ItemHeaderName;
        ImageView icon;
        RelativeLayout ItemHeader;
        View headerUnderLine;

        public MyViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.listText);
            icon = (ImageView) itemView.findViewById(R.id.listIcon);
            ItemHeader = (RelativeLayout) itemView.findViewById(R.id.item_header_layout);
            ItemHeaderName = (TextView) itemView.findViewById(R.id.header_itemName);
            headerUnderLine = itemView.findViewById(R.id.underliner);
        }
    }
}
