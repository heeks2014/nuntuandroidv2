package utils.hehe.nuntu;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import org.json.JSONObject;

public class User {

    private static String userID;
    private static String fname;
    private static String lname;
    private static String username;
    private static String role;
    private static String rank;
    private static String password;
    private static String isDeviceRegisted = "0";

    public static Boolean login(String userID, String Pass) {

        return true;
    }

    public static Boolean logout(Context l_outCtx) {
        try {
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(l_outCtx);
            SharedPreferences.Editor editor = preferences.edit();
            editor.clear();
            editor.apply();
            editor.commit();
            userID = null;
            fname = null;
            lname = null;
            username = null;
            role = null;
            rank = null;
            return true;
        } catch (Exception exc) {
            return false;
        }
    }

    public void setUserPass(String pass) {

        this.password = pass;
    }

    public String getUserFirstName() {

        return fname;
    }

    public void setUserFirstName(String setter) {

        this.fname = setter;
    }

    public String getUserLastName() {

        return lname;
    }

    public void setUserLastName(String setter) {

        this.lname = setter;
    }

    public String getUserID() {

        return userID;
    }

    public void setUserID(String string) {

        this.userID = string;
    }

    public String getUserName() {

        return username;
    }

    public void setUserName(String setter) {

        this.username = setter;
    }

    public String getUserRole() {

        return role;
    }

    public void setUserRole(String roleSet) {

        this.role = roleSet;
    }

    public String getUserRank() {

        return rank;
    }

    public void setUserRank(String setter) {

        this.rank = setter;
    }

    public String getUserPassWord() {

        return password;
    }

    public String getUserDevice() {

        return isDeviceRegisted;
    }

    public void setUserDevice(String device) {

        this.isDeviceRegisted = device;
    }

    public static String getNuntuLocalLogin(Context ctx) {
        String LoginCredentials = null;
        try {
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(ctx);
            String vault = preferences.getString("nuntuvault", "");
            Log.e("USER VAULT", "enc'ed " + vault);

            if (vault != null && vault.length() >= 10) {
                NuntuCrypto DEC = new NuntuCrypto();
                LoginCredentials = DEC.decrypt(vault);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        Log.e("USER VAULT", "dec'ed " + LoginCredentials);
        return LoginCredentials;
    }

    public static boolean updateNuntuLocalLogin(String _new, Context ctx) {
        NuntuCrypto ENC = new NuntuCrypto();
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(ctx);
        SharedPreferences.Editor editor = preferences.edit();
        try {
            editor.putString("nuntuvault", ENC.encrypt(_new));
            editor.apply();
            return true;
        } catch (Exception e) {
            e.printStackTrace();

        }
        return false;
    }


    public static String getUserImageURl(Context CallerCTX) {
        try {
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(CallerCTX);
            final String vault = preferences.getString("nuntuvault", "");
            if (vault != null && vault.length() >= 10) {
                NuntuCrypto DEC = new NuntuCrypto();
                final String LoginCredentials = DEC.decrypt(vault);
                if (LoginCredentials != null && LoginCredentials.length() >= 10) {
                    JSONObject VaultDecoded = new JSONObject(LoginCredentials);
                    return NuntuConstants.HOST.toString() + "/" + VaultDecoded.getString("image").replace("../", "");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return NuntuConstants.HOST.toString() + "/data/defaults/user/avatar.png";
    }
}
