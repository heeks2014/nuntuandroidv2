package utils.hehe.nuntu;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.IOException;
import java.util.ArrayList;

public class SignUpActivity extends FragmentActivity {


    private static Context signupContext;
    final String TAG = "SIGNUP";
    AlertDialog.Builder alertbox;
    private Button signupButton;
    private ProgressBar pb;
    private EditText Email;
    private ScrollView Parent;
    private ImageView CloseSignup;
    private String mEmail;
    private String mPasscode;
    private String mConfPasscode;
    private boolean emailOK, passOK;
    private Context signUpContext;
    private AsyncTask<String, Integer, Double> SignUpAsyncTask;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        signUpContext = this;
        alertbox = new AlertDialog.Builder(signUpContext);
        alertbox.setCancelable(false);
        final EditText PassCode = (EditText) findViewById(R.id.etPass);
        final EditText ConfPassCode = (EditText) findViewById(R.id.etConfPass);

        signupButton = (Button) findViewById(R.id.btnSignUp);
        CloseSignup = (ImageView) findViewById(R.id.close);
        pb = (ProgressBar) findViewById(R.id.SignUpProgressBar);
        pb.setVisibility(View.INVISIBLE);
        pb.setIndeterminate(true);

        Email = (EditText) findViewById(R.id.etEmail);
        Parent = (ScrollView) findViewById(R.id.signupParent);
        Parent.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e(TAG, "parent clicked");
                onBackPressed();
            }
        });


        signupContext = getApplicationContext();

        final TextView termsLink = (TextView) findViewById(R.id.terms);

        if (termsLink != null) {
            termsLink.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View arg0) {
                    termsLink.setMovementMethod(LinkMovementMethod.getInstance());
                }
            });

        }

        final CharSequence suggestEmail = NuntuUtils.getUserData(this, "email");
        if (suggestEmail != null && suggestEmail.length() >= 5) {
            Email.setText(suggestEmail);
        }

        CloseSignup.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        signupButton.setOnClickListener(new OnClickListener() {
            @Override
            @SuppressWarnings("deprecation")
            public void onClick(View v) {
                if (!TelephonyInfo.hasConnection(signupContext)) {
                    alertbox.setIcon(R.drawable.nonet);
                    alertbox.setTitle("No Connection");
                    alertbox.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }

                    });
                    alertbox.setMessage("No Connection to internet");
                    final AlertDialog alert = alertbox.create();
                    alert.show();


                    final Button positive_button = alert
                            .getButton(DialogInterface.BUTTON_POSITIVE);


                    if (positive_button != null) {
                        positive_button.setBackgroundDrawable(signupContext
                                .getResources().getDrawable(
                                        R.drawable.app_button_background));

                        positive_button
                                .setTextColor(signupContext.getResources()
                                        .getColor(android.R.color.white));
                    }


                } else {
                    mEmail = Email.getText().toString().trim();
                    mPasscode = PassCode.getText().toString().trim();
                    mConfPasscode = ConfPassCode.getText().toString().trim();
                    if (mEmail != null && mEmail.contains("@")) {
                        String[] EmailArrayed = mEmail.split("@");
                        if (EmailArrayed != null && EmailArrayed[0] != null && EmailArrayed[1] != null && EmailArrayed[1].contains(".")) {
                            emailOK = true;
                        } else {

                            Email.setError("Bad email");
                            emailOK = false;
                        }
                    } else {
                        Email.setError("No email");
                        emailOK = false;
                    }

                    if (mConfPasscode == null && mPasscode == null) {
                        Email.setError("Please make sure you do not send empty password");
                        ConfPassCode.setError("Please make sure you do not send empty password");
                        ConfPassCode.setSelection(0, mConfPasscode.length());
                        PassCode.setSelection(0, mPasscode.length());
                        passOK = false;
                    } else {
                        if (!mPasscode.equals(mConfPasscode)) {
                            ConfPassCode.setError("your password keys do not match.");
                            ConfPassCode.setSelection(0, mConfPasscode.length());
                            PassCode.setSelection(0, mPasscode.length());
                            passOK = false;
                        } else if (mPasscode.length() < 8 || mPasscode.length() > 32) {
                            ConfPassCode.setError("Nuntu accepts password keys between 8 and 32 characters only.");
                            ConfPassCode.setSelection(0, mConfPasscode.length());
                            PassCode.setSelection(0, mPasscode.length());
                            passOK = false;
                        } else {
                            passOK = true;
                        }
                    }

                    if (emailOK && passOK) {
                        JSONObject formData = new JSONObject();
                        try {
                            formData.put("email", mEmail);
                            formData.put("password", mPasscode);
                            SignUpAsyncTask = new MyAsyncTask();
                            SignUpAsyncTask.execute(NuntuConstants.SECURE_HOST.toString() + "/user", formData.toString());
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(signUpContext, "something bad happened ..." + e.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    }
                }
            }
        });

    }


    private void toggleDefaultKeyboard(String mode) {
        InputMethodManager imm;
        imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if ("hide".equals(mode)) {
            imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
        } else {
            imm.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT, 0);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        System.gc();
    }

    private class MyAsyncTask extends AsyncTask<String, Integer, Double> {

        String msg, query;
        JSONObject Response;

        @Override
        protected void onPreExecute() {

            pb.setVisibility(View.VISIBLE);
            CloseSignup.setVisibility(View.INVISIBLE);
            toggleDefaultKeyboard("hide");

        }

        @Override
        protected Double doInBackground(String... params) {
            try {
                HttpPost httppost = new HttpPost(params[0]);
                HttpClient httpclient = NuntuUtils.getSecureHttpClient();
                httppost.setEntity(new StringEntity(params[1], "UTF8"));
                httppost.addHeader("Content-type", "application/json");
                httppost.addHeader("Accept", "application/json");
                httppost.addHeader("User-Agent", "nuntumobile");
                // send the variable and value, in other words post, to the URL
                HttpResponse response = httpclient.execute(httppost);
                HttpEntity entity = response.getEntity();
                String Response = EntityUtils.toString(entity);
                Log.e("SIGN UP DATA", Response);
                JSONObject obj = new JSONObject(Response.toString());
                query = obj.getString("status");
                if ("success".equalsIgnoreCase(query)) {
                    try {
                        Response = obj.getString("details");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    msg = obj.getString("details");
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }


        @Override
        protected void onPostExecute(Double result) {
            pb.setVisibility(View.INVISIBLE);
            CloseSignup.setVisibility(View.VISIBLE);

            if ("success".equalsIgnoreCase(query)) {
                if (Response != null) {
                    try {
                        final String username = Response.getString("username");
                        final String id = Response.getString("id");
                        Toast.makeText(signupContext, " welcome " + username + " ! \n Enjoy what NUNTU brings to digital era.", Toast.LENGTH_LONG).show();
                    } catch (final JSONException e) {
                        Log.d("BAD NUNTU DATA", "An exception was thrown while parsing user data " + e.getMessage());
                    }

                } else {
                    Log.d("BAD NUNTU DATA", "the server response was null");
                }
            } else {
                toggleDefaultKeyboard("show");
                runOnUiThread(new Runnable() {
                    @Override
                    @SuppressWarnings("deprecation")
                    public void run() {
                        alertbox.setIcon(android.R.drawable.ic_dialog_alert);
                        alertbox.setTitle("     signup failed   ");
                        alertbox.setMessage(msg);
                        alertbox.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                JSONObject formData = new JSONObject();
                                try {
                                    formData.put("email", mEmail);
                                    formData.put("password", mPasscode);
                                    SignUpAsyncTask = new MyAsyncTask();
                                    SignUpAsyncTask.execute(NuntuConstants.SECURE_HOST.toString() + "/user", formData.toString());
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    Toast.makeText(signUpContext, "something bad happened ..." + e.getMessage(), Toast.LENGTH_LONG).show();
                                }
                            }

                        });

                        alertbox.setNegativeButton("EXIT", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                onBackPressed();
                            }

                        });
                        final AlertDialog alert = alertbox.create();
                        alert.show();
                        // retrieving the button view in order to handle it.
                        final Button negative_button = alert.getButton(DialogInterface.BUTTON_NEGATIVE);
                        final Button positive_button = alert.getButton(DialogInterface.BUTTON_POSITIVE);

                        if (positive_button != null) {
                            positive_button.setBackgroundDrawable(signupContext.getResources().getDrawable(R.drawable.app_button_background));
                            positive_button.setTextColor(signupContext.getResources().getColor(android.R.color.white));
                        }

                        if (negative_button != null) {
                            negative_button.setBackgroundDrawable(signupContext.getResources().getDrawable(R.drawable.app_button_background));
                            negative_button.setTextColor(signupContext.getResources().getColor(android.R.color.white));
                        }
                    }
                });
            }
        }
    }

}