package utils.hehe.nuntu;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;
import org.lucasr.twowayview.TwoWayView;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class AlbumActivity extends ActionBarActivity {

    private static Context albumDetailContext;
    private final static String _WAIT = "...";
    TextView AlbumDesc,
            AlbumDescTag,
            AlbumPrice,
            AlbumName,
            AlbumHeader,
            AlbumPublisher,
            AlbumDwonloadCount,
            AlbumTicks,
            AlbumSize,
            AlbumPageCount,
            AlbumCategory,
            MoreCommentsTag;

    private ImageView albumIconImage,
            OnCommentImage,
            ActionLogo,
            AlbumDescCollapseLogo;

    public static ProgressBar UserHeaderProgress, SimilarLoaderProgress;
    public static ImageView UserImage, UploadPicLogo;
    public static TextView HeaderEmail, UserName, UserRank, UserBal;

    private ProgressDialog _outDialog;
    private NavigationDrawerFragment drawerFragment;
    private Toolbar toolbar;
    private DrawerLayout parent;
    private TwoWayView SimilarAlbums;
    private ListView AlbumCommentList;

    private String ImageFilePath = "", UserID = "0", AlbumID = "0", FileLocation = "";
    private boolean isImageFileReady = false;
    private ArrayList<SimilarAsset> AlbumSimilarList;
    private HolderSimilarAsset SimilarAlbumsAdapter;
    private CommentRemoteAsync AlbumsCommentLoader;

    @SuppressWarnings("deprecation")
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        albumDetailContext = this;
        setContentView(R.layout.detail_album_layout);

        toolbar = (Toolbar) findViewById(R.id.app_bar);
        parent = (DrawerLayout) findViewById(R.id.album_detail_drawer_layout);
        SimilarLoaderProgress = (ProgressBar) findViewById(R.id.similar_loader);

        UserImage = (ImageView) findViewById(R.id.user_image);
        UploadPicLogo = (ImageView) findViewById(R.id.upload_new);
        OnCommentImage = (ImageView) findViewById(R.id.current_user_image);

        Picasso.with(this)
                .load(User.getUserImageURl(this))
                .resize(256, 256)
                .transform(new CircleTransform(128, 2))
                .centerCrop()
                .error(R.drawable.profile)
                .placeholder(R.drawable.profile)
                .into(OnCommentImage);

        UploadPicLogo.setVisibility(View.GONE);
        HeaderEmail = (TextView) findViewById(R.id.user_email);
        UserName = (TextView) findViewById(R.id.user_name);
        UserRank = (TextView) findViewById(R.id.user_rank);
        UserBal = (TextView) findViewById(R.id.user_balance);
        UserHeaderProgress = (ProgressBar) findViewById(R.id.user_header_progress);

        AlbumHeader = (TextView) findViewById(R.id.album_header);
        albumIconImage = (ImageView) findViewById(R.id.albumIcon);
        AlbumPrice = (TextView) findViewById(R.id.albumPrice);
        AlbumName = (TextView) findViewById(R.id.albumdetailname);
        AlbumPublisher = (TextView) findViewById(R.id.albumPublisher);
        AlbumCategory = (TextView) findViewById(R.id.albumCategory);
        AlbumSize = (TextView) findViewById(R.id.albumsize);
        AlbumPageCount = (TextView) findViewById(R.id.albumPageCount);
        AlbumTicks = (TextView) findViewById(R.id.ticksCount);
        AlbumDwonloadCount = (TextView) findViewById(R.id.downCount);
        ActionLogo = (ImageView) findViewById(R.id.action_logo);
        AlbumDescTag = (TextView) findViewById(R.id.albumDetailTag);
        AlbumDescCollapseLogo = (ImageView) findViewById(R.id.collapseIcon);
        AlbumDesc = (TextView) findViewById(R.id.albumDetail);
        SimilarAlbums = (TwoWayView) findViewById(R.id.similar_album_list);
        AlbumCommentList = (ListView) findViewById(R.id.album_comments);
        MoreCommentsTag = (TextView) findViewById(R.id.moreComments);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        drawerFragment = (NavigationDrawerFragment) getFragmentManager().findFragmentById(R.id.fragment_navigation_drawer_album_detail);
        if (Build.VERSION.SDK_INT >= 14) {
            getWindow().setDimAmount(0.5f);
        }

        FileLocation = getIntent().getStringExtra("path");
        AlbumID = getIntent().getStringExtra("albumID");
        final SwipeRefreshLayout DrawerRefresher = (SwipeRefreshLayout) findViewById(R.id.refresh_drawer);


        drawerFragment.setUp(R.id.fragment_navigation_drawer_album_detail, parent, toolbar, UserHeaderProgress, DrawerRefresher, "viewer");
        if (DrawerRefresher != null) {
            if (Build.VERSION.SDK_INT >= 14) {
                DrawerRefresher.setColorScheme(android.R.color.holo_blue_bright,
                        android.R.color.holo_green_light,
                        android.R.color.holo_orange_light,
                        android.R.color.holo_red_light);
            }

            DrawerRefresher.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    Log.e("REFRESH", "swipe refreshing in reader");
                    drawerFragment.getDrawerItemsAsync("/api/mobile/drawer/Android");
                }
            });
        }
        final RotateAnimation Expand = new RotateAnimation(0, 180, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        Expand.setInterpolator(new LinearInterpolator());
        Expand.setDuration(300);
        Expand.setFillEnabled(true);
        Expand.setFillAfter(true);
        Expand.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                AlphaAnimation show = new AlphaAnimation(0, 1);
                show.setInterpolator(new LinearInterpolator());
                show.setDuration(250);
                show.setFillEnabled(true);
                show.setFillAfter(true);
                AlbumDesc.startAnimation(show);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                AlbumDesc.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        final RotateAnimation Collapse = new RotateAnimation(180, 0, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        Collapse.setInterpolator(new LinearInterpolator());
        Collapse.setDuration(300);
        Collapse.setFillEnabled(true);
        Collapse.setFillAfter(true);
        Collapse.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                AlphaAnimation hide = new AlphaAnimation(1, 0);
                hide.setInterpolator(new LinearInterpolator());
                hide.setDuration(250);
                hide.setFillEnabled(true);
                hide.setFillAfter(true);
                AlbumDesc.startAnimation(hide);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                AlbumDesc.clearAnimation();
                AlbumDesc.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        AlbumDescTag.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                AlbumDescCollapseLogo.clearAnimation();
                AlbumDesc.clearAnimation();
                if (AlbumDesc.getVisibility() != View.VISIBLE) {
                    AlbumDescCollapseLogo.startAnimation(Expand);
                } else {
                    AlbumDescCollapseLogo.startAnimation(Collapse);
                }
            }
        });

        AlbumDescCollapseLogo.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                AlbumDescCollapseLogo.clearAnimation();
                AlbumDesc.clearAnimation();
                if (AlbumDesc.getVisibility() != View.VISIBLE) {
                    AlbumDescCollapseLogo.startAnimation(Expand);
                } else {
                    AlbumDescCollapseLogo.startAnimation(Collapse);
                }
            }
        });

        UserImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (UploadPicLogo.getVisibility() != View.VISIBLE) {
                    UploadPicLogo.setVisibility(View.VISIBLE);
                } else {
                    UploadPicLogo.setVisibility(View.GONE);
                }
            }
        });
        UploadPicLogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isImageFileReady) {
                    Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                    photoPickerIntent.setType("image/*");
                    startActivityForResult(photoPickerIntent, 1);
                } else {
                    UploadPicLogo.setBackgroundColor(Color.argb(0, 0, 0, 0));
                    UploadPicLogo.setImageResource(android.R.drawable.ic_menu_edit);
                    UploadPicLogo.setVisibility(View.GONE);
                    if (Integer.valueOf(UserID) >= 1 && ImageFilePath != null && ImageFilePath.length() >= 6) {
                        new NuntuPictureUploaderAsync(AlbumActivity.this).execute(ImageFilePath, NuntuConstants.HOST.toString() + "/user/" + UserID + "/picture");
                        isImageFileReady = true;
                    } else {
                        Toast.makeText(albumDetailContext, "Image upload failed", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });

        _outDialog = new ProgressDialog(this);
        initUserHeader("init");
        getAlbumDataAsync();
        getSimilarAlbumsAsync("/api/list/similar/android/100");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuItem BackStack = menu.add(0, 0, Menu.NONE, R.string.back_menu_item);
        if (Build.VERSION.SDK_INT < 14) {
            BackStack.setShowAsAction(MenuItem.SHOW_AS_ACTION_WITH_TEXT);
        } else {
            BackStack.setShowAsActionFlags(MenuItem.SHOW_AS_ACTION_WITH_TEXT);
        }

        MenuItem ShareMenuItem = menu.add(0, 1, Menu.NONE, R.string.share_text);
        if (Build.VERSION.SDK_INT < 14) {
            ShareMenuItem.setShowAsAction(MenuItem.SHOW_AS_ACTION_WITH_TEXT);
        } else {
            ShareMenuItem.setShowAsActionFlags(MenuItem.SHOW_AS_ACTION_WITH_TEXT);
        }
        ShareMenuItem.setIcon(android.R.drawable.ic_menu_share);

        MenuItem TickMenuItem = menu.add(0, 2, Menu.NONE, R.string.tick_text);
        if (Build.VERSION.SDK_INT < 14) {
            TickMenuItem.setShowAsAction(MenuItem.SHOW_AS_ACTION_WITH_TEXT);
        } else {
            TickMenuItem.setShowAsActionFlags(MenuItem.SHOW_AS_ACTION_WITH_TEXT);
        }
        TickMenuItem.setIcon(android.R.drawable.ic_menu_set_as);

        BackStack.setIcon(android.R.drawable.ic_menu_close_clear_cancel);

        MenuItem HoldMenuItem = menu.add(0, 3, Menu.NONE, R.string.put_on_hold);
        if (Build.VERSION.SDK_INT < 14) {
            HoldMenuItem.setShowAsAction(MenuItem.SHOW_AS_ACTION_WITH_TEXT);
        } else {
            HoldMenuItem.setShowAsActionFlags(MenuItem.SHOW_AS_ACTION_WITH_TEXT);
        }
        HoldMenuItem.setIcon(android.R.drawable.ic_menu_add);

        return true;
    }


    public void initUserHeader(String mode) {
        try {

            String userLocalCredentials = User.getNuntuLocalLogin(albumDetailContext);
            if (userLocalCredentials != null) {
                try {
                    JSONObject UserData = new JSONObject(userLocalCredentials);
                    if (NuntuUtils.isValidEmail(UserData.getString("email"))) {
                        HeaderEmail.setText(UserData.getString("email"));
                        UserName.setText(UserData.getString("firstname") + " " + UserData.getString("lastname"));
                        UserRank.setText(UserData.getString("rank"));
                        UserBal.setText("---");
                        UserID = UserData.getString("userid");

                        try {
                            String ImageUrl = UserData.getString("image");
                            if (ImageUrl != null) {
                                if (ImageUrl.contains("../../../../")) {
                                    ImageUrl = NuntuConstants.HOST.toString() + ImageUrl.replace("../../../../", "/");
                                }
                                Picasso.with(albumDetailContext)
                                        .load(ImageUrl)
                                        .resize(128, 128)
                                        .centerCrop()
                                        .transform(new CircleTransform(84, 0))
                                        .placeholder(R.drawable.profile)
                                        .error(R.drawable.profile)
                                        .into(UserImage);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    } else {
                        HeaderEmail.setText(" ");
                        UserName.setText("Please log in");
                        UserRank.setText("---");
                        UserBal.setText("---");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (TelephonyInfo.hasConnection(albumDetailContext)) {
                    //load user balance and user image
                }
            } else {
                HeaderEmail.setText(" ");
                UserName.setText("Please log in");
                UserRank.setText("---");
                UserBal.setText("---");

            }

            if (!"refresh".equalsIgnoreCase(mode)) {
                if (UserHeaderProgress != null) {
                    UserHeaderProgress.setVisibility(View.VISIBLE);
                }
            } else {
                drawerFragment.getDrawerItemsAsync("/api/mobile/drawer/Android");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getAlbumDataAsync() {
        AsyncTask<String, String, String> getTask = new AsyncTask<String, String, String>() {
            String query = "none",
                    _albumDesc = "-",
                    _albumCategory = "-",
                    _albumPageCount = "-",
                    _albumSize = "-",
                    _albumTicks = "-",
                    _albumDwonloadCount = "-",
                    _albumPublisher = "-",
                    _albumName = "-",
                    _albumPrice = "-",
                    icon_url = NuntuConstants.HOST.toString() + "/data/defaults/music.png";

            @Override
            protected void onPreExecute() {
                AlbumDesc.setText(_WAIT);
                AlbumCategory.setText(_WAIT);
                AlbumPageCount.setText(_WAIT);
                AlbumSize.setText(_WAIT);
                AlbumTicks.setText(_WAIT);
                AlbumDwonloadCount.setText(_WAIT);
                AlbumPublisher.setText(_WAIT);
                AlbumHeader.setText(_WAIT);
                AlbumName.setText(_WAIT);
                AlbumPrice.setText(_WAIT);
            }

            @Override
            protected String doInBackground(String... params) {
                String response = "";
                try {
                    URL url = new URL(NuntuConstants.HOST.toString() + params[0]);
                    HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                    urlConnection.setConnectTimeout(30000);
                    BufferedReader reader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                    String line = "";

                    while ((line = reader.readLine()) != null) {
                        response += line + "\n";
                    }
                    if (!urlConnection.getContentType().equalsIgnoreCase("application/json")) {
                        Log.e("Bad Content", "Received bad content type while expecting json");
                        return null;
                    }
                    Log.e("NUNTU SERVER DATA", response);
                    JSONObject obj = new JSONObject(response);
                    query = obj.getString("status");
                    if ("success".equalsIgnoreCase(query)) {
                        JSONObject AlbumData = new JSONObject(obj.getString("data"));
                        _albumDesc = AlbumData.getString("music_story");
                        _albumCategory = AlbumData.getString("music_category");
                        _albumPageCount = AlbumData.getString("music_likes");
                        _albumSize = AlbumData.getString("music_size");
                        _albumDwonloadCount = AlbumData.getString("music_download_count");
                        _albumTicks = AlbumData.getString("music_likes");
                        _albumPublisher = AlbumData.getString("music_publisher");
                        _albumName = AlbumData.getString("music_name");
                        if (AlbumData.getString("music_price").startsWith("0.00")) {
                            _albumPrice = "FREE";
                        } else {
                            _albumPrice = AlbumData.getString("music_price") + " " + AlbumData.getString("music_currency");
                        }
                        icon_url = AlbumData.getString("music_icon");
                        if (icon_url.contains("../../../../")) {
                            icon_url = NuntuConstants.HOST.toString() + icon_url.replace("../../../../", "/");
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e("NUNTU EXC", "An exception occurred while getting data" + e.getMessage());
                }

                return query;
            }

            protected void onPostExecute(String result) {
                super.onPostExecute(result);
                try {
                    AlbumDesc.setText(_albumDesc);
                    AlbumCategory.setText(_albumCategory);
                    AlbumPageCount.setText(_albumPageCount);
                    AlbumTicks.setText(NuntuUtils.formatToUrbanShort(_albumTicks) + " ticks");
                    AlbumSize.setText(_albumSize);
                    AlbumDwonloadCount.setText(NuntuUtils.formatToUrbanShort(_albumDwonloadCount) + " downloads");
                    AlbumPublisher.setText(_albumPublisher);
                    AlbumHeader.setText(_albumName);
                    AlbumName.setText(_albumName);
                    AlbumPrice.setText(_albumPrice);
                    Picasso.with(AlbumActivity.this)
                            .load(icon_url)
                            .resize(256, 256)
                            .transform(new CircleTransform(5, 2))
                            .centerCrop()
                            .error(R.drawable.music_logo)
                            .placeholder(R.drawable.music_logo)
                            .into(albumIconImage);
                } catch (Exception exc) {
                    exc.printStackTrace();

                }
            }
        };
        getTask.execute("/data/music/info/" + AlbumID);
    }

    private void getSimilarAlbumsAsync(String route) {

        AsyncTask<String, String, String> getTask = new AsyncTask<String, String, String>() {
            String query = "failed";

            @Override
            protected void onPreExecute() {
                SimilarLoaderProgress.setVisibility(View.VISIBLE);
            }

            @Override
            protected String doInBackground(String... params) {
                String response = "";
                try {
                    URL url = new URL(NuntuConstants.HOST.toString() + params[0]);
                    HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                    urlConnection.setConnectTimeout(30000);
                    BufferedReader reader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                    String line = "";

                    while ((line = reader.readLine()) != null) {
                        response += line + "\n";
                    }
                    if (!urlConnection.getContentType().equalsIgnoreCase("application/json")) {
                        Log.e("Bad Content", "Received bad content type while expecting json");
                        return null;
                    }
                    Log.e("SERVER DATA", response);
                    JSONObject obj = new JSONObject(response);
                    query = obj.getString("status");
                    if ("success".equalsIgnoreCase(query)) {

                        if (AlbumSimilarList != null) {
                            AlbumSimilarList.clear();
                        } else {
                            AlbumSimilarList = new ArrayList<>();
                        }

                        JSONArray musics = new JSONArray(obj.getString("data"));
                        for (int i = 0; i < 20; i++) {
                            JSONObject music = musics.getJSONObject(i);
                            SimilarAsset current = new SimilarAsset();
                            AlbumSimilarList.add(current);
                        }

                    }
                    //return "ok";
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e("EXC", "An exception occurred while getting data" + e.getMessage());
                }
                if (AlbumSimilarList != null) {
                    AlbumSimilarList.clear();
                } else {
                    AlbumSimilarList = new ArrayList<>();
                }

                for (int i = 0; i < 20; i++) {
                    SimilarAsset current = new SimilarAsset();
                    current.setAssetID("" + i + 1);
                    current.setType("music");
                    current.setAssetPublisher("Mike Kayihura Vol " + i + 1);
                    current.setAssetName("Mike Kayihura");
                    if (i % 2 == 0) {
                        current.setAssetImageURL("http://nuntu.hehelabs.com/data/promos/albums/419/Siriprize_rmx_covers/BITE-.png");
                    } else {
                        current.setAssetImageURL("http://nuntu.hehelabs.com/data/promos/albums/499/Empty_House_covers/Banner.jpg");
                    }
                    AlbumSimilarList.add(current);
                }

                return response;


            }

            protected void onPostExecute(String result) {
                super.onPostExecute(result);
                SimilarLoaderProgress.setVisibility(View.GONE);
                try {


                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (AlbumSimilarList != null && AlbumSimilarList.size() >= 1) {
                                SimilarAlbumsAdapter = new HolderSimilarAsset(AlbumActivity.this, AlbumSimilarList);
                                SimilarAlbums.setAdapter(SimilarAlbumsAdapter);
                                SimilarAlbumsAdapter.notifyDataSetChanged();
                            }
                        }
                    });
                    AlbumsCommentLoader = new CommentRemoteAsync(albumDetailContext, AlbumCommentList);
                    AlbumsCommentLoader.getCommentsAsync(NuntuConstants.HOST.toString() + "/data/mobile_music/" + AlbumID + "/comments", false);
                } catch (Exception exc) {
                    exc.printStackTrace();
                }
            }
        };
        getTask.execute(route);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            Log.d("NUNTU RESULT", "req " + requestCode + " resp " + resultCode + " data " + data.toString());
            switch (requestCode) {
                case 1:
                    try {
                         Uri selectedImage = data.getData();
                        if (selectedImage != null && selectedImage.getScheme().compareTo("content") == 0) {
                            Cursor cursor = getContentResolver().query(selectedImage, null, null, null, null);
                            if (cursor.moveToFirst()) {
                                int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                                selectedImage = Uri.parse(cursor.getString(column_index));
                                ImageFilePath = selectedImage.getPath();
                            }
                            cursor.close();
                            if (ImageFilePath == null || !new File(ImageFilePath).exists()) {
                                Toast.makeText(albumDetailContext, "Couldn't retrieve your image", Toast.LENGTH_LONG).show();
                            } else {
                                isImageFileReady = true;
                                UploadPicLogo.setImageResource(android.R.drawable.ic_menu_upload);
                                UploadPicLogo.setBackgroundColor(Color.argb(255, 0, 0, 0));
                                Picasso.with(albumDetailContext)
                                        .load(selectedImage)
                                        .transform(new CircleTransform(64, 0))
                                        .resize(128, 128)
                                        .centerCrop()
                                        .error(R.drawable.profile)
                                        .into(UserImage);
                                UserImage.invalidate();
                            }
                        } else {
                            Toast.makeText(albumDetailContext, "Couldn't retrieve your image", Toast.LENGTH_LONG).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResume() {
        try {
            super.onResume();
            initUserHeader("refresh");

        } catch (Exception exc) {
            exc.printStackTrace();
            Log.e("BACK", "exc " + exc.getMessage());
        }
    }
}