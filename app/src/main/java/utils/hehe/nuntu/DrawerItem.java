package utils.hehe.nuntu;

public class DrawerItem {

    String ItemName, Type, ID;
    int imgResID;
    String ItemRemoteIcon;

    public DrawerItem(String itemName, String type, String id, int res) {
        this.ItemName = itemName;
        this.Type = type;
        this.imgResID = res;
        this.ID = id;
    }

    public DrawerItem(String itemName, String type, String id, String iconUrl) {
        this.ItemName = itemName;
        this.Type = type;
        this.ItemRemoteIcon = iconUrl;
        this.ID = id;
    }

    public DrawerItem(String itemHeaderName, String type) {
        this.ItemName = itemHeaderName;
        this.Type = type;
    }


    public String getItemName() {
        return ItemName;
    }

    public void setItemName(String itemName) {
        ItemName = itemName;
    }

    public String getItemType() {
        return Type;
    }

    public void setItemMode(String ItemMode) {
        this.Type = ItemMode;
    }

    public int getImgResID() {
        return imgResID;
    }

    public void setImgResID(int imgResID) {
        this.imgResID = imgResID;
    }

    public int getItemID() {
        return imgResID;
    }

    public String getItemRemoteIcon() {
        return this.ItemRemoteIcon;
    }

    @Override
    public String toString() {
        return " ";//ToStringBuilder.reflectionToString(this);
    }
}
