package utils.hehe.nuntu;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

public class NuntuTextView extends TextView {

    public NuntuTextView(Context context) {
        super(context);
    }

    public NuntuTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        UiUtil.setCustomFont(this, context, attrs,
                R.styleable.utils_hehe_nuntu_NuntuTextView,
                R.styleable.utils_hehe_nuntu_NuntuTextView_font);
    }

    public NuntuTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        UiUtil.setCustomFont(this, context, attrs,
                R.styleable.utils_hehe_nuntu_NuntuTextView,
                R.styleable.utils_hehe_nuntu_NuntuTextView_font);
    }
}
