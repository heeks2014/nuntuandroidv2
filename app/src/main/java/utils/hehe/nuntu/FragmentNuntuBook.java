package utils.hehe.nuntu;

import android.content.Context;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by snickwizard on 2/10/15.
 */
public class FragmentNuntuBook extends Fragment {

    private static final String DEBUG = "APPS FRAG";
    private static String PROMO_IMAGE_URL = "http://nuntu.hehelabs.com/data/defaults/user/books.jpg";
    private static SwipeRefreshLayout bookSwipeLayout;
    private static  GridView bookGridView;

    ImageView Banner;
    Context BookFragContext;
    public static ArrayList<Book> BookList;
    HolderBookBaseAdapter bookAdapter;

    public static FragmentNuntuBook getInstance(int position) {
        FragmentNuntuBook frag = new FragmentNuntuBook();
        Bundle args = new Bundle();
        args.putInt("position", position);
        args.putString("type", "book");
        frag.setArguments(args);
        Log.e(DEBUG, "in constructor with id " + position);
        return frag;

    }


    // Store instance variables based on arguments passed
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        BookFragContext = getActivity().getApplicationContext();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        try {
            final View layout = inflater.inflate(R.layout.fragment_book_list_layout, container, false);
            bookSwipeLayout = (SwipeRefreshLayout) layout.findViewById(R.id.book_content_container);
            bookGridView = (GridView) layout.findViewById(R.id.grid_view);
            Banner = (ImageView) layout.findViewById(R.id.promo_banner);

            Picasso.with(BookFragContext)
                    .load(PROMO_IMAGE_URL)
                    .error(R.drawable.placeholder_book)
                    .placeholder(R.drawable.placeholder_book)
                    .into(Banner);

            int width = Device.getDeviceScreenSize("width", BookFragContext,"dpi");
            if (width < 240) {
                bookGridView.setNumColumns(1);
            } else if (width > 240 && width < 600) {
                bookGridView.setNumColumns(2);
            } else {
                bookGridView.setNumColumns(3);
            }
            if (Build.VERSION.SDK_INT >= 14) {
                bookSwipeLayout.setColorScheme(android.R.color.holo_blue_bright,
                        android.R.color.holo_green_light,
                        android.R.color.holo_orange_light,
                        android.R.color.holo_red_light);
            } else {
                bookSwipeLayout.setColorScheme(Color.argb(255, 255, 165, 0),
                        Color.argb(180, 255, 165, 0),
                        Color.argb(125, 255, 165, 0),
                        Color.argb(72, 255, 165, 0));
            }


            bookSwipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    getListBooksAsync("/api/list/book/android/100");
                }
            });

            getListBooksAsync("/api/list/book/android/100");

            return layout;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } catch (OutOfMemoryError e) {
            e.printStackTrace();
            return null;
        }
    }

    public void getListBooksAsync(String route) {

        AsyncTask<String, String, String> getTask = new AsyncTask<String, String, String>() {
            String query = "failed";

            @Override
            protected void onPreExecute() {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        bookSwipeLayout.setProgressViewOffset(false, 0, (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 24, getResources().getDisplayMetrics()));
                        bookSwipeLayout.setRefreshing(true);
                    }
                });
            }

            @Override
            protected String doInBackground(String... params) {
                String response = "";
                try {
                    URL url = new URL(NuntuConstants.HOST.toString() + params[0]);
                    HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                    urlConnection.setConnectTimeout(30000);
                    BufferedReader reader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                    String line = "";

                    while ((line = reader.readLine()) != null) {
                        response += line + "\n";
                    }
                    if (!urlConnection.getContentType().equalsIgnoreCase("application/json")) {
                        Log.e("Bad Content", "Received bad content type while expecting json");
                        return null;
                    }
                    Log.e("SERVER DATA", response);
                    JSONObject obj = new JSONObject(response);
                    query = obj.getString("status");
                    if ("success".equalsIgnoreCase(query)) {

                        if (BookList != null) {
                            BookList.clear();
                        } else {
                            BookList = new ArrayList<>();
                        }

                        JSONArray books = new JSONArray(obj.getString("data"));
                        for (int i = 0; i < books.length(); i++) {
                            JSONObject book = books.getJSONObject(i);
                            Book current = new Book();

                            if (book.getString("book_banner") != null) {
                                String AppImage = book.getString("book_banner").replace("../", "");
                                current.setbookBanner(NuntuConstants.HOST.toString() + "/" + AppImage);
                            }
                            current.setbookID(book.getString("book_id"));
                            current.setbookName(book.getString("book_name"));
                            if (null == book.getString("book_price") || book.getString("book_price").startsWith("0.00")) {
                                current.setbookPrice("FREE");
                                current.setbookStatus("0");
                            } else {
                                current.setbookStatus("2");
                                current.setbookPrice(book.getString("book_price") + " " + book.getString("book_currency"));
                            }

                            String BookLocation = searchBookLocation(book.getString("book_name"));
                            if (BookLocation != null) {
                                current.setbookStatus("1");
                                current.setbookLocation(BookLocation);
                            }

                            current.setbookPublisher(book.getString("book_publisher"));
                            BookList.add(current);
                        }

                    }
                    return "ok";
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e("EXC", "An exception occurred while getting data" + e.getMessage());
                }
                return response;


            }

            protected void onPostExecute(String result) {
                super.onPostExecute(result);
                try {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            bookSwipeLayout.setRefreshing(false);
                            if (BookList != null && BookList.size() >= 1) {
                                bookAdapter = new HolderBookBaseAdapter(getActivity(), BookList, "0", bookGridView.getNumColumns());
                                bookGridView.setAdapter(bookAdapter);
                                bookAdapter.notifyDataSetChanged();
                            }
                        }
                    });


                } catch (Exception exc) {
                    exc.printStackTrace();
                    Log.e("LAYOUTERROR", "An exception thrown at post ex " + exc.getMessage());
                }
            }
        };
        getTask.execute(route);
    }


    private String searchBookLocation(String _Bname) {
        String BookURI = null;
        try {
            BookURI = Environment.getExternalStorageState() + _Bname.replace(" ", "_");
            if (new File(BookURI).exists()) {
                return BookURI;
            } else {
                return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return BookURI;
    }

    public static void hideOpenMenus() {
        if (bookGridView != null) {
            int c = bookGridView.getChildCount();
            for (int i = 0; i < c; i++) {
                View view = bookGridView.getChildAt(i);
                if (view != null) {
                    CardView IcMenu = (CardView) view.findViewById(R.id.item_controls);
                    if (IcMenu != null) {
                        IcMenu.setVisibility(View.GONE);
                    }
                }
            }
        }
    }
}