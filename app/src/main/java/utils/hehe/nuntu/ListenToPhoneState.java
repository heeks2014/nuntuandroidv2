package utils.hehe.nuntu;

import android.media.MediaPlayer;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;

/**
 * Created by snick on 2/25/15.
 */
public class ListenToPhoneState extends PhoneStateListener {

    MediaPlayer mp;
    RelativeLayout layout;
    private int pausedPosition = 0;

    ListenToPhoneState(MediaPlayer mp, RelativeLayout layout) {
        this.mp = mp;
        this.layout = layout;
        Log.e("CALL LIST", "registered");
    }

    @Override
    public void onCallStateChanged(int state, String incomingNumber) {

        switch (state) {
            case TelephonyManager.CALL_STATE_IDLE:
                resumeInAndroid();
                return;
            default:
                Log.e("CALL LIST", "paused on " + state + " call from " + incomingNumber);
                pauseInAndroid();
                return;
        }
    }

    private void resumeInAndroid() {
        if (mp != null) {
            mp.seekTo(pausedPosition);
            mp.start();
        }
        if (layout != null) {
            layout.setVisibility(View.VISIBLE);
        }
        Log.e("CALL LIST", "resumed on idle");
    }

    private void pauseInAndroid() {
        if (mp != null && mp.isPlaying()) {
            pausedPosition = mp.getCurrentPosition();
            mp.pause();
        }
        if (layout != null) {
            layout.setVisibility(View.GONE);
        }
    }


}