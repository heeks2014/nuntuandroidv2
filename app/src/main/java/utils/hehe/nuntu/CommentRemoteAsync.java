package utils.hehe.nuntu;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by gakuba on 4/21/2015.
 */
public class CommentRemoteAsync {
    private Context CallerContext;
    public static ArrayList<Comment> CurrentLoadedComments;
    private ListView CommentList;
    CommentListBaseAdapter commentAdapter;

    public CommentRemoteAsync(Context ctx, ListView List) {
        CallerContext = ctx;
        CurrentLoadedComments = new ArrayList<>();
        CommentList = List;
    }

    public void getCommentsAsync(String path, final boolean isOnUI) {
        AsyncTask<String, String, String> getTask = new AsyncTask<String, String, String>() {
            String query, details;

            @Override
            protected String doInBackground(String... params) {
                String response = "";
                try {
                    URL url = new URL(params[0]);
                    HttpURLConnection urlConnection = (HttpURLConnection)
                            url.openConnection();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                    String line = "";
                    while ((line = reader.readLine()) != null) {
                        response += line + "\n";
                    }
                    if (!urlConnection.getContentType().toLowerCase(Locale.ENGLISH).equals("booklication/json")) {
                        Log.e("Bad Content", "Received bad content type while expecting sjon");
                    }
                    try {
                        JSONObject obj = new JSONObject(response);
                        query = obj.getString("status");
                        if ("success".equalsIgnoreCase(query)) {
                            JSONArray jsonArry = new JSONArray(obj.getString("comments"));
                            for (int i = 0; i < jsonArry.length(); i++) {
                                JSONObject comment = jsonArry.getJSONObject(i);
                                Comment current = new Comment();
                                current.setCommentAuthor(comment.getString("name"));
                                current.setCommentDate(comment.getString("date"));
                                current.setCommentTitle(comment.getString("title"));
                                current.setCommentBody(comment.getString("comment"));

                                String ProfilePic = comment.getString("userPic");
                                if (ProfilePic != null) {
                                    if (ProfilePic.contains("../../../..")) {
                                        ProfilePic = ProfilePic.replace("../../../..", "");
                                        current.setCommentorAvatarUrl(NuntuConstants.HOST.toString() + ProfilePic);
                                    } else {
                                        current.setCommentorAvatarUrl(ProfilePic);
                                    }
                                } else {
                                    current.setCommentorAvatarUrl(NuntuConstants.HOST.toString() + "/data/defaults/avatar.png");
                                }
                                CurrentLoadedComments.add(current);
                            }
                        } else if ("failed".equalsIgnoreCase(query)) {
                            details = obj.getString("comments");
                        }

                    } catch (JSONException exc) {
                        exc.printStackTrace();
                        Log.d("EXCEPTION ", "somathing bad to JSOn hbooken " + exc.getMessage());
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e("EXC", "An eception occured while getting data" + e.getMessage());

                }
                return response;

            }

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                try {
                    if (isOnUI) {
                        Toast.makeText(CallerContext, "loading comments...", Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @SuppressWarnings("deprecation")
            @Override
            protected void onPostExecute(String result) {

                if ("success".equalsIgnoreCase(query)) {
                    commentAdapter = new CommentListBaseAdapter(CallerContext, CurrentLoadedComments);
                    CommentList.setAdapter(commentAdapter);
                    commentAdapter.notifyDataSetChanged();
                } else if ("failed".equalsIgnoreCase(query)) {
                    if (isOnUI) {
                        Toast.makeText(CallerContext, "couldn't get comments...\n" + details, Toast.LENGTH_SHORT).show();
                    }
                } else {
                    if (isOnUI) {
                        Toast.makeText(CallerContext, "couldn't get comments !\nconnection error...", Toast.LENGTH_LONG).show();
                    }
                }
            }

        };
        getTask.execute(path);
    }
}
