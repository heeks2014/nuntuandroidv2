package utils.hehe.nuntu;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.LayoutAnimationController;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.filefilter.TrueFileFilter;
import org.json.JSONArray;
import org.json.JSONObject;
import org.lucasr.twowayview.TwoWayView;
import org.pdfparse.model.PDFDocInfo;
import org.pdfparse.model.PDFDocument;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import nl.siegmann.epublib.domain.Author;
import nl.siegmann.epublib.domain.Book;
import nl.siegmann.epublib.epub.EpubReader;

/**
 * Created by snickwizard on 2/10/15.
 */
public class FragmentNuntuCloset extends Fragment {

    private static final String DEBUG = "CLOSET FRAG";
    public static NestedListView trackList;
    public static SwipeRefreshLayout SwipableParent;
    private static Context closetContext;
    private static View layout;
    private static File NuntuMusic, NuntuBook;
    private static long BiggestBookSize;
    private static ArrayList<HashMap<String, String>> songsList = new ArrayList<>();
    private final String[] PilotApps = {"rw.lineapps.lineinfo",
            "com.hehe.safeboda",
            "com.pswdevelopers.turirimbe",
            "com.phonegap.seka",
            "com.phonegap.sakwe",
            "com.phonegap.fora",
            "com.phonegap.ihema",
            "com.phonegap.besmart",
            "com.hehe.snv",
            "com.hehe.soma",
            "com.nyaruka.quickaccess"};
    private final String[] Devs = {"LineInfo", "HeHe labs",
            "Pswdevelopers", "Code Clubs 2014", "Code Clubs 2014",
            "Code Clubs 2014", "Code Clubs 2014", "Code Clubs 2014",
            "HeHe labs", "HeHe labs", " Nyaruka"};
    SharedPreferences prefs;
    public static ArrayList<ClosetLocalAlbum> MusicList;
    public static ArrayList<ClosetLocalApp> AppList;
    public static ArrayList<ClosetLocalBook> BookList;
    ScrollView ScollParent;
    private TwoWayView albumList, appList, bookList;
    private ProgressBar LoadAlbums, LoadBooks;
    private TextView LoadingTrackFile, loadingBook, NoAppTag, NoBookTag, NoMusicTag, NuntuCopyright;
    private ImageView NoAppLogo, NoBookLogo, NoMusicLogo;
    private int albumprogress = 0;
    private boolean isMusicTaskDone = false, isAppTaskDone = false, isBookTaskDone = false;
    AsyncTask<String, String, String> getLocalAlbumAsync, getLocalAppAsync, getLocalBookAsync;

    public static FragmentNuntuCloset getInstance(int position) {
        FragmentNuntuCloset frag = new FragmentNuntuCloset();
        Bundle args = new Bundle();
        args.putInt("position", position);
        args.putString("type", "closet");
        frag.setArguments(args);
        Log.e(DEBUG, "in constructor with id " + position);
        return frag;
    }


    private static void animateListView(ListView lv) {
        AnimationSet set = new AnimationSet(true);
        Animation animation = new AlphaAnimation(0.0f, 1.0f);
        animation.setDuration(300);
        set.addAnimation(animation);
        LayoutAnimationController controller = new LayoutAnimationController(set, 0.5f);
        lv.setLayoutAnimation(controller);
    }

    public static void updateTrackListView(HolderClosetTrackList listadapter, ArrayList<ClosetLocalTrack> tracks) {
        int k = 0;
        songsList.clear();
        while (tracks != null && k < tracks.size()) {
            HashMap<String, String> song = new HashMap<String, String>();
            song.put("songTitle", tracks.get(k).getTrackTitle());
            song.put("songPath", tracks.get(k).getTrackLocation());
            // Adding each song to SongList
            songsList.add(song);
            k++;
        }

        listadapter.notifyDataSetChanged();


        if (listadapter.getCount() > 0 && trackList != null) {
            trackList.setVisibility(View.VISIBLE);
        }

        animateListView(trackList);
        trackList.setAdapter(listadapter);
        listadapter.notifyDataSetChanged();
        Log.e("REFRESH OK", "finished refresh");
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        setRetainInstance(true);
    }


    // Store instance variables based on arguments passed
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        isMusicTaskDone = true;
        isAppTaskDone = true;
        isBookTaskDone = true;
        closetContext = getActivity().getApplicationContext();
        prefs = closetContext.getSharedPreferences("closet", Context.MODE_PRIVATE);

        NuntuMusic = new File(Environment.getExternalStorageDirectory().toString() + "/Music/NuntuMusic");
        if (!NuntuMusic.isDirectory() || NuntuMusic.listFiles().length < 1) {
            NuntuMusic = new File(Environment.getDataDirectory().toString() + "/Music/NuntuMusic");
        }
        NuntuBook = new File(Environment.getExternalStorageDirectory().toString() + "/Documents/NuntuBooks");
        if (!NuntuBook.isDirectory()) {
            NuntuBook = new File(Environment.getDataDirectory().toString() + "/Documents/NuntuBooks");
        }
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        try {
            layout = inflater.inflate(R.layout.fragment_closet_layout, container, false);
            ScollParent = (ScrollView) layout.findViewById(R.id.parent_scroller);
            SwipableParent = (SwipeRefreshLayout) layout.findViewById(R.id.closet_content_container);
            NoAppLogo = (ImageView) layout.findViewById(R.id.no_app_logo);
            NoBookLogo = (ImageView) layout.findViewById(R.id.no_book_logo);
            NoMusicLogo = (ImageView) layout.findViewById(R.id.no_music_logo);

            NoAppTag = (TextView) layout.findViewById(R.id.no_app_tag);
            NoBookTag = (TextView) layout.findViewById(R.id.no_book_tag);
            NoMusicTag = (TextView) layout.findViewById(R.id.no_music_tag);

            NuntuCopyright = (TextView) layout.findViewById(R.id.copyright);
            NoAppLogo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Nuntu.changeTab("app");
                }
            });
            NoAppTag.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Nuntu.changeTab("app");
                }
            });

            NoBookLogo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Nuntu.changeTab("book");
                }
            });

            NoBookTag.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Nuntu.changeTab("book");
                }
            });

            NoMusicLogo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Nuntu.changeTab("music");
                }
            });
            NoMusicTag.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Nuntu.changeTab("music");
                }
            });

            SwipableParent.setMinimumHeight(Device.getDeviceScreenSize("height", closetContext, "normal"));
            if (Build.VERSION.SDK_INT >= 14) {
                SwipableParent.setColorScheme(android.R.color.holo_blue_bright,
                        android.R.color.holo_green_light,
                        android.R.color.holo_orange_light,
                        android.R.color.holo_red_light);
            }
            SwipableParent.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    if (BiggestBookSize <= NuntuUtils.getMallocHeap()[1]) {
                        executeClosetTasks(true);
                    } else {
                        Toast.makeText(closetContext, "Can't refresh. Device is low on memory...available " + NuntuUtils.getMallocHeap()[1] / 1048576 + " MB", Toast.LENGTH_LONG).show();
                    }
                }

            });
            LoadAlbums = (ProgressBar) layout.findViewById(R.id.albumProgressBar);
            LoadBooks = (ProgressBar) layout.findViewById(R.id.bookProgress);
            LoadingTrackFile = (TextView) layout.findViewById(R.id.loading_track_file);
            loadingBook = (TextView) layout.findViewById(R.id.bookProgressValue);
            albumList = (TwoWayView) layout.findViewById(R.id.closet_album_list);
            appList = (TwoWayView) layout.findViewById(R.id.closet_app_list);
            bookList = (TwoWayView) layout.findViewById(R.id.closet_book_list);
            trackList = (NestedListView) layout.findViewById(R.id.closet_album_track_list);

            return layout;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        trackList.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

                Log.e("SCROLL", "state of scrolling changed to " + scrollState);
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                int topRowVerticalPosition = (trackList == null || trackList.getChildCount() == 0) ? 0 : trackList.getChildAt(0).getTop();
                SwipableParent.setEnabled(firstVisibleItem == 0 && topRowVerticalPosition >= 0);
            }
        });

        if (savedInstanceState != null) {
            boolean hasAnyAssetSaved = false;

            if (MusicList != null && MusicList.size() >= 1) {
                try {
                    albumList.setVisibility(View.VISIBLE);
                    HolderClosetAlbumList holder = new HolderClosetAlbumList(closetContext, MusicList);
                    albumList.setAdapter(holder);
                    holder.notifyDataSetChanged();
                    hasAnyAssetSaved = true;
                } catch (IllegalStateException e) {
                    e.printStackTrace();
                }

            } else {
                albumList.setVisibility(View.GONE);
                NoMusicTag.setVisibility(View.VISIBLE);
                NoMusicLogo.setVisibility(View.VISIBLE);
            }

            if (AppList != null && AppList.size() >= 1) {
                try {
                    HolderClosetAppList holder = new HolderClosetAppList(closetContext, AppList);
                    appList.setAdapter(holder);
                    holder.notifyDataSetChanged();
                    hasAnyAssetSaved = true;
                } catch (IllegalStateException e) {
                    e.printStackTrace();
                }

            } else {
                NoAppTag.setVisibility(View.VISIBLE);
                NoAppLogo.setVisibility(View.VISIBLE);
            }

            if (BookList != null && BookList.size() >= 1) {
                try {
                    HolderClosetBookList holder = new HolderClosetBookList(closetContext, BookList);
                    bookList.setAdapter(holder);
                    holder.notifyDataSetChanged();
                    hasAnyAssetSaved = true;
                } catch (IllegalStateException e) {
                    e.printStackTrace();
                }

            } else {

                NoBookTag.setVisibility(View.VISIBLE);
                NoBookLogo.setVisibility(View.VISIBLE);

            }

            if (!hasAnyAssetSaved) {
                executeClosetTasks(false);
                updateTrackListView(new HolderClosetTrackList(closetContext, new ArrayList<ClosetLocalTrack>()), null);

            }

        } else {
            executeClosetTasks(false);
            updateTrackListView(new HolderClosetTrackList(closetContext, new ArrayList<ClosetLocalTrack>()), null);
        }
    }

    private void executeClosetTasks(final boolean isRefreshing) {
        if (!isMusicTaskDone && getLocalAlbumAsync != null && getLocalAlbumAsync.getStatus() != AsyncTask.Status.FINISHED) {
            getLocalAlbumAsync.cancel(true);
        }

        if (!isAppTaskDone && getLocalAppAsync != null && getLocalAppAsync.getStatus() != AsyncTask.Status.FINISHED) {
            getLocalAppAsync.cancel(true);
        }

        if (!isBookTaskDone && getLocalBookAsync != null && getLocalBookAsync.getStatus() != AsyncTask.Status.FINISHED) {
            getLocalBookAsync.cancel(true);
        }

        getLocalAlbumAsync = new AsyncTask<String, String, String>() {
            int ProgressWidth = 0;
            String TotalLocalTracks = "0";

            @Override
            protected void onPreExecute() {
                albumprogress = 0;
                albumList.setVisibility(View.GONE);
                NoMusicLogo.setVisibility(View.GONE);
                NoMusicTag.setVisibility(View.GONE);

                if (!SwipableParent.isRefreshing()) {
                    SwipableParent.setProgressViewOffset(false, 0, (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 24, getResources().getDisplayMetrics()));
                    SwipableParent.setRefreshing(true);

                }

                if (LoadAlbums != null) {
                    LoadAlbums.setVisibility(View.VISIBLE);
                    LoadAlbums.setProgress(0);
                    ProgressWidth = LoadAlbums.getWidth();
                    if (ProgressWidth < 100) {
                        ProgressWidth = Device.getDeviceScreenSize("width", closetContext, "normal");
                    }
                }


                trackList.setVisibility(View.GONE);
                albumList.setVisibility(View.GONE);

                LoadingTrackFile.setText(" ");
                LoadingTrackFile.setVisibility(View.VISIBLE);

            }

            @Override
            protected void onCancelled() {
                super.onCancelled();
                onPostExecute("");
            }

            @Override
            protected void onProgressUpdate(String... update) {
                LoadAlbums.setProgress(Integer.valueOf(update[0]));
                LoadingTrackFile.setText(update[0] + "/" + TotalLocalTracks);
                float Tx = (Integer.valueOf(update[0]) * ProgressWidth / Integer.valueOf(TotalLocalTracks));
                LoadingTrackFile.setTranslationX(Tx);
            }

            @Override
            protected String doInBackground(String... params) {

                if (!isRefreshing && MusicList != null && MusicList.size() >= 1) {
                    return "ok";
                }

                if (MusicList == null) {
                    MusicList = new ArrayList<>();
                } else {
                    MusicList.clear();
                }
                try {
                    //get all mp3 in NuntuMusic dir
                    if (NuntuMusic.exists()) {
                        List<String> Albums = new ArrayList<>();
                        HashMap<String, String> Author = new HashMap<>();
                        HashMap<String, Drawable> Cover = new HashMap<>();
                        HashMap<String, ArrayList<String>> albumTracks = new HashMap<>();
                        List<File> files = (List<File>) FileUtils.listFiles(NuntuMusic, TrueFileFilter.INSTANCE, TrueFileFilter.INSTANCE);

                        LoadAlbums.setMax(files.size());
                        TotalLocalTracks = "" + files.size();

                        for (final File file : files) {
                            albumprogress++;
                            String mime = NuntuUtils.getMIMEType(file.getAbsolutePath());
                            publishProgress(String.valueOf(albumprogress));
                            if ("audio/mpeg".equalsIgnoreCase(mime)) {
                                Drawable cover = null;
                                String album = NuntuUtils.getAlbumTitle(closetContext, file);
                                String author = NuntuUtils.getAlbumAuthor(closetContext, file);

                                Bitmap foo = NuntuUtils.getAlbumCover(closetContext, file);
                                if (foo != null) {
                                    cover = new BitmapDrawable(closetContext.getResources(), foo);
                                    //Log.e("cover: ", "height " + cover.getIntrinsicHeight());
                                }
                                if (album == null) {
                                    album = "Unkown";
                                }


                                if (author == null) {
                                    author = "Unkown";
                                }

                                Albums.add(album);
                                Author.put(album, author);
                                Cover.put(album, cover);
                                ArrayList<String> prev = albumTracks.get(album);
                                if (prev != null) {
                                    prev.add(file.getAbsolutePath());
                                } else {
                                    prev = new ArrayList<>();
                                    prev.add(file.getAbsolutePath());
                                    albumTracks.put(album, prev);
                                }
                            } else {
                                if (mime != null) {
                                    // Log.e("MIME TYPE", mime);
                                }
                            }
                        }

                        Set<String> UniqueAlbums = new HashSet<>(Albums);

                        for (Iterator<String> it = UniqueAlbums.iterator(); it.hasNext(); ) {
                            ClosetLocalAlbum current = new ClosetLocalAlbum();
                            String uniqueAlbum = it.next();
                            current.setAlbumTitle(uniqueAlbum);
                            current.setAlbumAuthor(Author.get(uniqueAlbum));
                            current.setCoverImage(Cover.get(uniqueAlbum));
                            current.setAlbumTracks(albumTracks.get(uniqueAlbum));
                            MusicList.add(current);
                        }


                    } else {
                        if (!NuntuMusic.mkdirs()) {
                            Log.e("Nuntu Music", "Problem creating NuntuMusic folder");
                        } else {
                            Log.e("Nuntu Music", "created NuntuMusic folder");
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    //Log.e("EXC", "An exception occurred while getting data" + e.getMessage());
                }
                return null;

            }

            protected void onPostExecute(String result) {
                super.onPostExecute(result);
                isMusicTaskDone = true;

                if (SwipableParent.isRefreshing()) {
                    SwipableParent.setRefreshing(false);
                }
                try {
                    LoadAlbums.setProgress(0);
                    LoadAlbums.setVisibility(View.GONE);
                    LoadingTrackFile.setVisibility(View.GONE);


                    try {
                        if (MusicList != null && MusicList.size() >= 1) {
                            albumList.setVisibility(View.VISIBLE);
                            HolderClosetAlbumList holder = new HolderClosetAlbumList(closetContext, MusicList);
                            albumList.setAdapter(holder);
                            holder.notifyDataSetChanged();
                        } else {
                            if (albumList != null) {
                                albumList.setVisibility(View.GONE);
                            }
                            NoMusicTag.setVisibility(View.VISIBLE);
                            NoMusicLogo.setVisibility(View.VISIBLE);
                        }
                    } catch (IllegalStateException e) {
                        e.printStackTrace();
                    }


                } catch (Exception exc) {
                    exc.printStackTrace();
                    Log.e(DEBUG, "An exception thrown at post ex " + exc.getMessage());
                }
            }
        };
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            getLocalAlbumAsync.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "");
        } else {
            getLocalAlbumAsync.execute("");
        }
        getLocalAppAsync = new AsyncTask<String, String, String>() {
            @Override
            protected void onPreExecute() {
                NoAppLogo.setVisibility(View.GONE);
                NoAppTag.setVisibility(View.GONE);
                appList.setVisibility(View.GONE);
                if (!SwipableParent.isRefreshing()) {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            SwipableParent.setProgressViewOffset(false, 0, (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 24, getResources().getDisplayMetrics()));
                            SwipableParent.setRefreshing(true);

                        }
                    });
                }
            }

            @Override
            protected void onCancelled() {
                super.onCancelled();
                onPostExecute("");
            }

            @Override
            protected String doInBackground(String... params) {
                try {
                    if (!isRefreshing && AppList != null && AppList.size() >= 1) {
                        return "ok";
                    }
                    AppList = new ArrayList<>();
                    PackageManager pm = closetContext.getPackageManager();
                    String appName;
                    if (prefs != null) {
                        String LocalApps = prefs.getString("apps", "none");
                        if ("none".equalsIgnoreCase(LocalApps)) {
                            try {
                                JSONArray apps = new JSONArray();
                                for (int i = 0; i < PilotApps.length; i++) {
                                    JSONObject app = new JSONObject();
                                    app.put("pub", Devs[i]);
                                    app.put("name", PilotApps[i]);
                                    apps.put(i, app);
                                    try {
                                        appName = pm.getApplicationInfo(PilotApps[i].trim(), 0).loadLabel(pm).toString();
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        appName = null;
                                    }
                                    if (appName != null && appName.length() >= 1) {
                                        ClosetLocalApp current = new ClosetLocalApp();
                                        current.setAppAuthor(Devs[i]);
                                        current.setAppTitle(appName);
                                        current.setAppPackageName(pm.getApplicationInfo(PilotApps[i].trim(), 0).packageName);
                                        current.setIcon(pm.getApplicationIcon(PilotApps[i].trim()));
                                        AppList.add(current);
                                    }
                                }

                                SharedPreferences.Editor editor = prefs.edit();
                                editor.putString("apps", apps.toString());
                                editor.apply();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        } else {
                            try {
                                JSONArray apps = new JSONArray(LocalApps);
                                if (apps.length() >= PilotApps.length) {
                                    for (int j = 0; j < apps.length(); j++) {
                                        try {
                                            JSONObject app = apps.getJSONObject(j);
                                            try {
                                                appName = pm.getApplicationInfo(app.getString("name"), 0).loadLabel(pm).toString();
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                                appName = null;
                                            }
                                            if (appName != null && appName.length() >= 1) {
                                                ClosetLocalApp current = new ClosetLocalApp();
                                                current.setAppAuthor(app.getString("pub"));
                                                current.setAppTitle(appName);
                                                current.setAppPackageName(pm.getApplicationInfo(app.getString("name"), 0).packageName);
                                                current.setIcon(pm.getApplicationIcon(app.getString("name")));
                                                AppList.add(current);
                                            }
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }
                                } else {
                                    try {
                                        apps = new JSONArray();
                                        for (int i = 0; i < PilotApps.length; i++) {
                                            JSONObject app = new JSONObject();
                                            app.put("pub", Devs[i]);
                                            app.put("name", PilotApps[i]);
                                            apps.put(i, app);
                                            try {
                                                appName = pm.getApplicationInfo(PilotApps[i].trim(), 0).loadLabel(pm).toString();
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                                appName = null;
                                            }
                                            if (appName != null && appName.length() >= 1) {
                                                ClosetLocalApp current = new ClosetLocalApp();
                                                current.setAppAuthor(Devs[i]);
                                                current.setAppTitle(appName);
                                                current.setAppPackageName(pm.getApplicationInfo(PilotApps[i].trim(), 0).packageName);
                                                current.setIcon(pm.getApplicationIcon(PilotApps[i].trim()));
                                                AppList.add(current);
                                            }
                                        }

                                        SharedPreferences.Editor editor = prefs.edit();
                                        editor.putString("apps", apps.toString());
                                        editor.apply();

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }


                    } else {
                        Log.e("PREFS", " mofo is null ");
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    //Log.e("EXC", "An exception occurred while getting data" + e.getMessage());
                }
                return null;

            }

            protected void onPostExecute(String result) {
                super.onPostExecute(result);
                isAppTaskDone = true;
                if (appList != null) {
                    appList.setVisibility(View.VISIBLE);
                }
                try {

                    if (AppList != null && AppList.size() >= 1) {
                        appList.setVisibility(View.VISIBLE);
                        try {
                            HolderClosetAppList holder = new HolderClosetAppList(closetContext, AppList);
                            appList.setAdapter(holder);
                            holder.notifyDataSetChanged();
                        } catch (IllegalStateException e) {
                            e.printStackTrace();
                        }

                    } else {
                        NoAppTag.setVisibility(View.VISIBLE);
                        NoAppLogo.setVisibility(View.VISIBLE);
                    }


                } catch (Exception exc) {
                    exc.printStackTrace();
                    Log.e(DEBUG, "An exception thrown at post ex " + exc.getMessage());
                }
            }
        };

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            getLocalAppAsync.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "");
        } else {
            getLocalAppAsync.execute("");
        }

        getLocalBookAsync = new AsyncTask<String, String, String>() {
            private int SkippedBooks = 0,
                    bookprogress = 0;

            @Override
            protected void onPreExecute() {
                SkippedBooks = 0;
                bookprogress = 0;
                NoBookTag.setVisibility(View.GONE);
                NoBookLogo.setVisibility(View.GONE);
                bookList.setVisibility(View.GONE);

                if (!SwipableParent.isRefreshing()) {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            SwipableParent.setProgressViewOffset(false, 0, (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 24, getResources().getDisplayMetrics()));
                            SwipableParent.setRefreshing(true);

                        }
                    });
                }

                if (LoadBooks != null) {
                    LoadBooks.setVisibility(View.VISIBLE);
                    loadingBook.setText(" ");

                }

            }


            @Override
            protected void onProgressUpdate(String... update) {

                loadingBook.setText(update[0]);
            }

            @Override
            protected String doInBackground(String... params) {
                if (!isRefreshing && BookList != null && BookList.size() >= 1) {
                    return "ok";
                }

                String bookTitle = "untitled", bookAuthor = "unknown";
                try {

                    if (BookList != null) {
                        BookList.clear();
                    } else {
                        BookList = new ArrayList<>();
                    }
                    if (NuntuBook.exists()) {
                        String[] extensions = new String[]{"pdf", "epub", "PDF", "EPUB"};
                        List<File> Allfiles = (List<File>) FileUtils.listFiles(NuntuBook, extensions, true);

                        ClosetLocalBook current;
                        final long filesCount = Allfiles.size();
                        for (final File file : Allfiles) {
                            bookprogress++;
                            publishProgress(bookprogress + " of " + filesCount);
                            final String mime = NuntuUtils.getMIMEType(file.getAbsolutePath());
                            if ("application/pdf".equalsIgnoreCase(mime) || "application/epub+zip".equalsIgnoreCase(mime)) {

                                if (BiggestBookSize < file.length()) {
                                    BiggestBookSize = file.length();
                                }
                                current = new ClosetLocalBook();
                                current.setBookLocation(file.getAbsolutePath());
                                current.setBookType(mime);
                                current.setIconURL("http://nuntu.hehelabs.com/data/promos/books/120/Breaking_Boxes_covers/Cover_nuntu.jpg");
                                if ("application/pdf".equalsIgnoreCase(mime)) {

                                    PDFDocument pdf = new PDFDocument(file.getAbsolutePath(), getActivity());

                                    PDFDocInfo info = pdf.getDocumentInfo();

                                    if (info != null) {

                                        bookTitle = info.getTitle();
                                        if (bookTitle == null || bookTitle.length() <= 1) {
                                            bookTitle = info.getSubject();
                                        }

                                        bookAuthor = info.getAuthor();

                                        if (bookAuthor == null || bookAuthor.length() <= 1) {
                                            bookAuthor = info.getCreator();
                                        }

                                        if (bookAuthor == null || bookAuthor.length() <= 1) {
                                            bookAuthor = info.getProducer();
                                        }

                                        pdf.close();

                                    }
                                } else if ("application/epub+zip".equalsIgnoreCase(mime)) {
                                    try {
                                        EpubReader epubReader = new EpubReader();
                                        Book book = epubReader.readEpub(new FileInputStream(file.getAbsolutePath()));
                                        if (book != null) {
                                            List<Author> MetaAuthor = book.getMetadata().getAuthors();
                                            for (Author author : MetaAuthor) {
                                                if (author != null && author.getFirstname().length() >= 2) {
                                                    bookAuthor = author.getFirstname() + " " + author.getLastname();
                                                    break;
                                                }
                                            }
                                            // Log the book's title
                                            bookTitle = book.getTitle();
                                            Log.e("epublib", "title: " + bookTitle);

                                            Bitmap coverImage = BitmapFactory.decodeStream(book.getCoverImage().getInputStream());
                                            current.setbookCover(coverImage);

                                        } else {
                                            Log.e("epublib", "book was null");
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        Log.e("epublib", e.getMessage());

                                    }
                                }

                                if (bookAuthor == null || bookAuthor.length() <= 1) {
                                    bookAuthor = "Unknown";
                                }

                                if (bookTitle == null || bookTitle.length() <= 1) {
                                    bookTitle = FilenameUtils.getName(file.getAbsolutePath());
                                    bookTitle = bookTitle.substring(0, bookTitle.lastIndexOf('.'));
                                }

                                bookAuthor = bookAuthor.replaceAll("[^\\x0A\\x0D\\x20-\\x7E]", "");
                                bookTitle = bookTitle.replaceAll("[^\\x0A\\x0D\\x20-\\x7E]", "");

                                bookTitle = bookTitle.replaceAll("[^a-zA-Z0-9]+", " ");

                                current.setBookAuthor(NuntuUtils.capitalizeFirstLetter(bookAuthor));
                                current.setBookTitle(NuntuUtils.capitalizeFirstLetter(bookTitle));
                                BookList.add(current);

                            } else {
                                SkippedBooks++;
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e("EXC", "An exception occurred while loading books...." + e.getMessage());
                }


                return null;

            }

            @Override
            protected void onCancelled() {
                super.onCancelled();
                onPostExecute("");
            }

            protected void onPostExecute(String result) {
                super.onPostExecute(result);
                isBookTaskDone = true;
                LoadBooks.setVisibility(View.GONE);
                loadingBook.setVisibility(View.GONE);
                try {
                    if (SkippedBooks >= 1) {

                        Toast.makeText(closetContext, SkippedBooks + " book files skipped", Toast.LENGTH_LONG).show();
                    }

                    if (BookList != null && BookList.size() >= 1) {
                        bookList.setVisibility(View.VISIBLE);
                        Collections.reverse(BookList);
                        try {
                            HolderClosetBookList holder;
                            if (BookList.size() <= 1000) {
                                holder = new HolderClosetBookList(closetContext, BookList);
                            } else {

                                holder = new HolderClosetBookList(closetContext, new ArrayList<>(BookList.subList(0, 49)));
                            }
                            bookList.setAdapter(holder);
                            holder.notifyDataSetChanged();
                        } catch (IllegalStateException e) {
                            e.printStackTrace();
                        }

                    } else {
                        NoBookTag.setVisibility(View.VISIBLE);
                        NoBookLogo.setVisibility(View.VISIBLE);
                    }


                } catch (Exception exc) {
                    exc.printStackTrace();
                    Log.e(DEBUG, "An exception thrown at post ex " + exc.getMessage());
                }
            }
        };

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            getLocalBookAsync.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "");
        } else {
            getLocalBookAsync.execute("");
        }
        System.gc();

    }


    @Override
    public void onResume() {
        Log.e("DEBUG", "onResume of ClosetFragment");
        super.onResume();
        getRetainInstance();
    }

    @Override
    public void onPause() {
        Log.e("DEBUG", "OnPause of ClosetFragment");
        super.onPause();
        setRetainInstance(true);
    }
}