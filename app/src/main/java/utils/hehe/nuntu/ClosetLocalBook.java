package utils.hehe.nuntu;

import android.graphics.Bitmap;

/**
 * Created by snickwizard on 2/11/15.
 */
public class ClosetLocalBook {
    private String remoteURL;
    private String name;
    private String author;
    private String location;
    private String type;
    private Bitmap Cover = null;

    public String getIconURL() {
        return remoteURL;
    }

    public void setIconURL(String id) {
        remoteURL = id;
    }

    public String getBookTitle() {
        return name;
    }

    public void setBookTitle(String name) {
        this.name = name;
    }

    public String getBookAuthor() {
        return author;
    }

    public void setBookAuthor(String author) {
        this.author = author;
    }

    public String getBookLocation() {
        return location;
    }

    public void setBookLocation(String loc) {
        this.location = loc;
    }

    public String getBookType() {
        return type;
    }

    public void setBookType(String type) {
        this.type = type;
    }
    public void setbookCover(Bitmap map) {
        // TODO Auto-generated method stub
        Cover = map;
    }

    public Bitmap getBookCover() {
        // TODO Auto-generated method stub
        return Cover;
    }
}
