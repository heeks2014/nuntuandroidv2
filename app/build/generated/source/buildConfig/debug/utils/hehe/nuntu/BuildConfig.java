/**
 * Automatically generated file. DO NOT MODIFY
 */
package utils.hehe.nuntu;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "utils.hehe.nuntu";
  public static final String BUILD_TYPE = "debug";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 201504;
  public static final String VERSION_NAME = "2.1.0";
}
